package bit.crunzh.utilities.file.internal

import bit.crunzh.utilities.file.FileDescription
import bit.crunzh.utilities.file.FileId
import bit.crunzh.utilities.file.FileStorageService
import java.io.File
import java.io.InputStream
import java.time.LocalDateTime

class FileStorageServiceImpl(val rootFolder: File) : FileStorageService {
    init {
        //TODO validate rootFolder
        //TODO cache FileDescriptions based on last access as weak reference?
    }

    override fun getFileDescription(fileId: FileId): FileDescription {
        TODO("Not yet implemented")
    }

    override fun getFileDescriptions(): List<FileDescription> {
        TODO("Not yet implemented")
    }

    override fun createFile(expirationTime: LocalDateTime, fileContent: ByteArray): FileDescription {
        TODO("Not yet implemented")
    }

    override fun createFile(expirationTime: LocalDateTime, fileContent: InputStream): FileDescription {
        TODO("Not yet implemented")
    }

    override fun createFile(expirationTime: LocalDateTime, fileSize: Int, fileId: FileId): FileDescription {
        TODO("Not yet implemented")
    }

    override fun deleteFile(fileId: FileId): Boolean {
        TODO("Not yet implemented")
    }
}