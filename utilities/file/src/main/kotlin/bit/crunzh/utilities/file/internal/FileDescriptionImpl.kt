package bit.crunzh.utilities.file.internal

import bit.crunzh.utilities.file.FileDescription
import bit.crunzh.utilities.file.FileId
import bit.crunzh.utilities.file.internal.io.BufferedFileIo
import bit.crunzh.utilities.file.io.FileContentRange
import bit.crunzh.utilities.file.io.FileIo
import bit.crunzh.utilities.file.io.ReadBuffer
import bit.crunzh.utilities.file.range.FileRange
import bit.crunzh.utilities.file.range.FileRangeSet
import java.io.File
import java.io.InputStream
import java.time.LocalDateTime
import kotlinx.coroutines.sync.Mutex

class FileDescriptionImpl(override val fileId: FileId, private val fileIo: FileIo, private val readBuffer: ReadBuffer) : FileDescription {
    private val mutex = Mutex()
    private var isComplete = false

    init {
        //TODO read file state
        //TODO set fileId
        //TODO set fileSize
        //TODO set expiryTime
    }


    override val fileSize: Int
        get() = TODO("Not yet implemented")
    override var expiryTime: LocalDateTime
        get() = TODO("Not yet implemented")
        set(value) {}

    override fun isComplete(): Boolean {
        TODO("Not yet implemented")
    }

    override fun getContent(): InputStream {
        TODO("Not yet implemented")
    }

    override fun getRange(range: FileRange): ByteArray {
        if (!fileIo.getWrittenBytes().isSupersetOf(range)) {
            throw IllegalStateException("Range '$range' is not completed in file. Available file ranges is '${fileIo.getWrittenBytes().fileRanges}'")
        }
        return readBuffer.read(range)
    }

    override fun setRange(startIdx: Int, content: ByteArray): Boolean {
        val byteRange = FileRangeSet(fileSize, FileRange(startIdx, startIdx + content.size))
        val rangesToSet = byteRange.difference(fileIo.getWrittenBytes())
        if (rangesToSet.isEmpty()) {
            return false
        }
        val chunksToWrite: List<FileContentRange> = createWriteRequests(startIdx, content, rangesToSet)
        fileIo.write(chunksToWrite)
        return true
    }

    private fun createWriteRequests(startIdx: Int, content: ByteArray, rangesToSet: FileRangeSet): List<FileContentRange> {
        TODO("Not yet implemented")
    }

    override fun getCompleteSections(): FileRangeSet {
        TODO("Not yet implemented")
    }

    override fun getEmptySections(): FileRangeSet {
        TODO("Not yet implemented")
    }
}