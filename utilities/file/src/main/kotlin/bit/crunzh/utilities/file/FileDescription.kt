package bit.crunzh.utilities.file

import bit.crunzh.utilities.file.range.FileRange
import bit.crunzh.utilities.file.range.FileRangeSet
import java.io.InputStream
import java.time.LocalDateTime

interface FileDescription {
    /**
     * The id of the file - which is a hash of its content. Multiple files may reference the same file id.
     */
    val fileId: FileId

    /**
     * The byte size of the file.
     */
    val fileSize: Int

    /**
     * Time of when this file will automatically removed from the local file storage
     */
    var expiryTime: LocalDateTime

    /**
     * If all the content of the file is available, and the file is thus complete
     */
    fun isComplete(): Boolean

    /**
     * @return an input stream, allowing to read all the bytes of the file.
     * @throws IllegalStateException if not [isComplete]
     */
    fun getContent(): InputStream

    /**
     * @return the bytes representing the given range of the file
     * @throws IllegalStateException if parts or all of the given range is not available.
     */
    fun getRange(startIdx: Int, endIdx: Int): ByteArray = getRange(FileRange(startIdx, endIdx))

    /**
     * @return the bytes representing the given range of the file
     * @throws IllegalStateException if parts or all of the given range is not available.
     */
    fun getRange(range: FileRange): ByteArray

    /**
     * @return true if new content was written to the file, else false
     * @throws IllegalArgumentException if startIdx is less than 0, or startIdx + content.size is larger than file size.
     */
    fun setRange(startIdx: Int, content: ByteArray): Boolean

    /**
     * @return the completed sections of this file
     * This is the inverse of [getEmptySections]
     */
    fun getCompleteSections(): FileRangeSet

    /**
     * @return the missing sections of this file.
     * This is the inverse of [getCompleteSections]
     */
    fun getEmptySections(): FileRangeSet
}