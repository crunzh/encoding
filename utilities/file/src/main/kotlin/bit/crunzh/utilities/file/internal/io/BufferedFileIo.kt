package bit.crunzh.utilities.file.internal.io

import bit.crunzh.utilities.file.internal.FileMetaData
import bit.crunzh.utilities.file.io.FileContentRange
import bit.crunzh.utilities.file.io.FileIo
import bit.crunzh.utilities.file.range.FileRange
import bit.crunzh.utilities.file.range.FileRangeSet
import kotlinx.coroutines.sync.Mutex
import java.util.*
import kotlin.collections.ArrayList

class BufferedFileIo(val file: FileIo) : FileIo {
    val mutex = Mutex()
    val writeBuffer = ArrayList<FileContentRange>()
    //private var readSections: FileRangeSet
    //private var writtenSections: FileRangeSet

    //constructor() {
        //TODO read meta data from disk
        //FileMetaData fileMetaData = readMetaData()
    //}

    private fun flushToDisk(dataTo: FileContentRange) {

    }

    override fun getWrittenBytes(): FileRangeSet {

        TODO("Not yet implemented")
    }

    override fun write(dataToWrite: FileContentRange) {
        //TODO mutex
        writeBuffer.add(dataToWrite)
        TODO("Not yet implemented")
    }

    override fun read(range: FileRange): FileContentRange {
        TODO("Not yet implemented")
    }

    /*
    if(success) {
            readBuffer.removeFromBuffer(range)
            //TODO write meta data file
        } else {
            //TODO schedule
            // 1) a retry of write
            // 2) after 3 retries give up and log/notify that there is a storage problem and that we will try to keep file in memory while possible.
        }
     */
}