package bit.crunzh.utilities.file.internal

import bit.crunzh.utilities.file.FileId
import bit.crunzh.utilities.file.range.FileRangeSet
import java.time.LocalDateTime

data class FileMetaData(val fileId: FileId, val writtenRanges: FileRangeSet, val expiryTime: LocalDateTime)