package bit.crunzh.utilities.file.io

import bit.crunzh.utilities.file.range.FileRange

data class FileContentRange(val startIdx: Int, val content: ByteArray) {
    val fileRange = FileRange(startIdx, startIdx + content.size)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FileContentRange

        if (startIdx != other.startIdx) return false
        if (!content.contentEquals(other.content)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = startIdx
        result = 31 * result + content.contentHashCode()
        return result
    }

}