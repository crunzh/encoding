package bit.crunzh.utilities.file.io

import bit.crunzh.utilities.file.range.FileRange

interface ReadBuffer {
    /**
     * Read the bytes of the range requested. The range given will always be valid.
     * @return the bytes matching the file range, read from the file
     * @throws IllegalArgumentException if the given range is not available in the file.
     */
    fun read(range: FileRange): ByteArray

    fun addToBuffer(fileContentRange: FileContentRange)

    fun addToBuffer(fileContentRanges: List<FileContentRange>) {
        fileContentRanges.forEach { addToBuffer(it) }
    }

    fun removeFromBuffer(fileContentRange: FileContentRange)

    fun removeFromBuffer(fileContentRange: List<FileContentRange>) {
        fileContentRange.forEach { removeFromBuffer(it) }
    }
}