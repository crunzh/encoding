package bit.crunzh.utilities.file.range

import kotlin.math.max
import kotlin.math.min

class FileRangeSet(
        /**
         * The file size defines the "universe" in set theory.
         * Ie. the complement of this set of file ranges, is all the bytes not represented by file ranges.
         */
        val fileSizeBytes: Int,
        fileRangesOfSet: List<FileRange> = emptyList()) {
    val fileRanges: List<FileRange>

    constructor(fileSizeBytes: Int, fileRange: FileRange) : this(fileSizeBytes, listOf(fileRange))

    init {
        val sortedFileRanges = FileRangeUtil.sortFileRangesOnStartIndex(fileRangesOfSet)
        fileRanges = union(sortedFileRanges)
    }

    /**
     * @return the inverse or compliment of the [fileRanges] in this set
     */
    fun complement(): FileRangeSet {
        if (fileRanges.isEmpty()) {
            return createFullSet()
        }

        val complimentRanges = ArrayList<FileRange>()
        var previousIndex = 0
        fileRanges.forEach { fileRange ->
            val range = FileRange(previousIndex, fileRange.startIdx)
            if (!range.isEmpty()) {
                complimentRanges.add(range)
            }
            previousIndex = fileRange.endIdx
        }
        if (fileRanges.last().endIdx != fileSizeBytes) {
            complimentRanges.add(FileRange(previousIndex, fileSizeBytes))
        }
        return FileRangeSet(fileSizeBytes, complimentRanges)
    }

    /**
     * @return a [FileRangeSet] which is the union of the [fileRanges] in this set and the [fileRangeSet] [FileRange]s.
     * @throws IllegalArgumentException if the [fileSizeBytes] of [fileRangeSet] does not equal the [fileSizeBytes] of this set.
     */
    fun union(fileRangeSet: FileRangeSet): FileRangeSet {
        if (fileRangeSet.fileSizeBytes != fileSizeBytes) throw IllegalArgumentException("The file ranges to union must have the same file size.")
        val sortedFileRanges = FileRangeUtil.sortFileRangesOnStartIndex(fileRangeSet.fileRanges, fileRanges)
        return FileRangeSet(fileSizeBytes, union(sortedFileRanges))
    }

    private fun union(sortedFileRanges: List<FileRange>): List<FileRange> {
        var previousFileRange: FileRange? = null
        val notOverlappingFileRanges = ArrayList<FileRange>()
        for (fileRange in sortedFileRanges) {
            if (fileRange.isEmpty()) {
                continue
            }
            if (fileRange.endIdx > fileSizeBytes) {
                throw IllegalArgumentException("FileRange.endIdx '${fileRange.endIdx}' may not be larger than fileSize '$fileSizeBytes'")
            }
            if (previousFileRange == null) {
                previousFileRange = fileRange
                continue
            }
            if (previousFileRange.endIdx >= fileRange.startIdx) {
                previousFileRange = FileRange(previousFileRange.startIdx, max(fileRange.endIdx, previousFileRange.endIdx))
            } else {
                notOverlappingFileRanges.add(previousFileRange)
                previousFileRange = fileRange
            }
        }
        if (previousFileRange != null) {
            notOverlappingFileRanges.add(previousFileRange)
        }
        return notOverlappingFileRanges
    }

    /**
     * @return a [FileRangeSet] which is the union of the [fileRanges] in this set and the [fileRange].
     */
    fun union(fileRange: FileRange): FileRangeSet = union(FileRangeSet(fileSizeBytes, listOf(fileRange)))

    /**
     * @return a [FileRangeSet] which is the intersection of the [fileRanges] in this set and the [fileRangeSet] [FileRange]s.
     * @throws IllegalArgumentException if the [fileSizeBytes] of [fileRangeSet] does not equal the [fileSizeBytes] of this set.
     */
    fun intersection(fileRangeSet: FileRangeSet): FileRangeSet {
        if (fileRangeSet.fileSizeBytes != fileSizeBytes) throw IllegalArgumentException("The file ranges to union must have the same file size.")
        val intersectionRanges = ArrayList<FileRange>()
        for (local in fileRanges) {
            for (other in fileRangeSet.fileRanges) {
                if (local.endIdx <= other.startIdx) {
                    break
                } else if (local.startIdx >= other.endIdx) {
                    continue
                } else {
                    intersectionRanges.add(FileRange(max(local.startIdx, other.startIdx), min(local.endIdx, other.endIdx)))
                }
            }
        }
        return FileRangeSet(fileSizeBytes, intersectionRanges)
    }

    /**
     * @return a [FileRangeSet] which is the intersection of the [fileRanges] in this set and the [fileRange].
     */
    fun intersection(fileRange: FileRange): FileRangeSet = intersection(FileRangeSet(fileSizeBytes, listOf(fileRange)))

    /**
     * @return a [FileRangeSet] with the bytes distinct to this set, not represented in [fileRangeSet].
     * @throws IllegalArgumentException if the [fileSizeBytes] of [fileRangeSet] does not equal the [fileSizeBytes] of this set.
     */
    fun difference(fileRangeSet: FileRangeSet): FileRangeSet {
        if (fileRangeSet.fileSizeBytes != fileSizeBytes) throw IllegalArgumentException("The file ranges to union must have the same file size.")
        val differenceRanges = ArrayList<FileRange>()
        for (local in fileRanges) {
            var remainingRanges = listOf(local)
            for (other in fileRangeSet.fileRanges) {
                if (local.endIdx <= other.startIdx) {
                    break
                } else if (local.startIdx >= other.endIdx) {
                    continue
                } else {
                    val newRemainingRanges = ArrayList<FileRange>()
                    for (remaining in remainingRanges) {
                        if (remaining.startIdx < other.startIdx) {
                            if (remaining.endIdx > other.startIdx) {
                                newRemainingRanges.add(FileRange(remaining.startIdx, other.startIdx))
                            } else {
                                newRemainingRanges.add(remaining)
                            }
                        }
                        if (remaining.endIdx > other.endIdx) {
                            if (remaining.startIdx < other.endIdx) {
                                newRemainingRanges.add(FileRange(other.endIdx, local.endIdx))
                            } else {
                                newRemainingRanges.add(remaining)
                            }
                        }
                    }
                    remainingRanges = newRemainingRanges
                }
            }
            differenceRanges.addAll(remainingRanges)
        }
        return FileRangeSet(fileSizeBytes, differenceRanges)
    }

    /**
     * @return a [FileRangeSet] with the bytes distinct to this set, not represented in [fileRange].
     */
    fun difference(fileRange: FileRange): FileRangeSet = difference(FileRangeSet(fileSizeBytes, listOf(fileRange)))

    /**
     * @return a [FileRangeSet] with the bytes distinct to this set and distinct in [fileRangeSet], ie. the union of the two sets, subtracted the intersection.
     * @throws IllegalArgumentException if the [fileSizeBytes] of [fileRangeSet] does not equal the [fileSizeBytes] of this set.
     */
    fun symmetricDifference(fileRangeSet: FileRangeSet): FileRangeSet {
        if (fileRangeSet.fileSizeBytes != fileSizeBytes) throw IllegalArgumentException("The file ranges to union must have the same file size.")
        val localDifference = difference(fileRangeSet)
        val otherDifference = fileRangeSet.difference(this)
        val fileRanges = ArrayList<FileRange>()
        fileRanges.addAll(localDifference.fileRanges)
        fileRanges.addAll(otherDifference.fileRanges)
        return FileRangeSet(fileSizeBytes, fileRanges)
    }

    fun symmetricDifference(fileRange: FileRange): FileRangeSet = symmetricDifference(FileRangeSet(fileSizeBytes, listOf(fileRange)))

    /**
     * @return whether the bytes of this set is distinct from the bytes in [fileRangeSet], ie. the intersection is Ø.
     * @throws IllegalArgumentException if the [fileSizeBytes] of [fileRangeSet] does not equal the [fileSizeBytes] of this set.
     */
    fun isDisjoint(fileRangeSet: FileRangeSet): Boolean {
        if (fileRangeSet.fileSizeBytes != fileSizeBytes) throw IllegalArgumentException("The file ranges to union must have the same file size.")
        for (local in fileRanges) {
            for (other in fileRangeSet.fileRanges) {
                if (local.endIdx <= other.startIdx) {
                    break
                }
                if (other.endIdx <= local.startIdx) {
                    continue
                }
                if (local.startIdx < other.startIdx && local.endIdx > other.startIdx) {
                    return false
                }
                if (local.startIdx > other.startIdx && local.startIdx < other.endIdx) {
                    return false
                }
            }
        }
        return true
    }

    fun isDisjoint(fileRange: FileRange): Boolean = isDisjoint(FileRangeSet(fileSizeBytes, listOf(fileRange)))

    /**
     * @return whether the bytes of this set is distinct from the bytes in [fileRangeSet], ie. the intersection is Ø.
     * @throws IllegalArgumentException if the [fileSizeBytes] of [fileRangeSet] does not equal the [fileSizeBytes] of this set.
     */
    fun overlaps(fileRangeSet: FileRangeSet): Boolean = !isDisjoint(fileRangeSet)

    fun overlaps(fileRange: FileRange): Boolean = overlaps(FileRangeSet(fileSizeBytes, listOf(fileRange)))

    fun isSubsetOf(fileRangeSet: FileRangeSet): Boolean {
        if (fileRangeSet.fileSizeBytes != fileSizeBytes) throw IllegalArgumentException("The file ranges to union must have the same file size.")
        if (fileRangeSet.isEmpty() && !this.isEmpty()) {
            return false
        }
        for (local in fileRanges) {
            for (other in fileRangeSet.fileRanges) {
                if (local.startIdx >= other.endIdx) {
                    continue
                }
                if (local.endIdx <= other.startIdx) {
                    break
                }
                if (local.startIdx < other.startIdx && local.endIdx > other.startIdx) {
                    return false
                }
                if (local.startIdx > other.startIdx && local.endIdx > other.endIdx) {
                    return false
                }
            }
        }
        return true
    }

    fun isSubsetOf(fileRange: FileRange): Boolean = isSubsetOf(FileRangeSet(fileSizeBytes, listOf(fileRange)))

    fun isSupersetOf(fileRangeSet: FileRangeSet): Boolean {
        return fileRangeSet.isSubsetOf(this)
    }

    fun isSupersetOf(fileRange: FileRange): Boolean = isSupersetOf(FileRangeSet(fileSizeBytes, listOf(fileRange)))

    fun createEmptySet(): FileRangeSet = FileRangeSet(fileSizeBytes)

    fun createFullSet(): FileRangeSet = FileRangeSet(fileSizeBytes, listOf(FileRange(0, fileSizeBytes)))

    fun isFull(): Boolean {
        return fileRanges.size == 1 && fileRanges[0].startIdx == 0 && fileRanges[0].endIdx == fileSizeBytes
    }

    fun isEmpty(): Boolean {
        return fileRanges.isEmpty()
    }

    companion object {
        fun emptySet(fileSizeBytes: Int) = FileRangeSet(fileSizeBytes)
        fun fullSet(fileSizeBytes: Int) = FileRangeSet(fileSizeBytes, listOf(FileRange(0, fileSizeBytes)))
    }
}