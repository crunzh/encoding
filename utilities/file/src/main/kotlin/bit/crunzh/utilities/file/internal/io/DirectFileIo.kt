package bit.crunzh.utilities.file.internal.io

import bit.crunzh.utilities.file.io.FileContentRange
import bit.crunzh.utilities.file.io.ReadBuffer
import bit.crunzh.utilities.file.range.FileRange
import bit.crunzh.utilities.file.range.FileRangeSet
import java.io.File
import java.util.*

class DirectFileIo(val file: File) : ReadBuffer  {
    private val buffer = LinkedList<FileContentRange>()
    private var bufferRangeSet = FileRangeSet(file.length().toInt())

    override fun read(range: FileRange): ByteArray {
        TODO("Not yet implemented")
    }

    override fun addToBuffer(fileContentRange: FileContentRange) {
        val diff = bufferRangeSet.difference(fileContentRange.fileRange)
        bufferRangeSet = bufferRangeSet.union(fileContentRange.fileRange)
        if(diff.isEmpty()) {
            return
        }
        for(bufRange in buffer) {

        }
        //buffer.forEachIndexed()
        TODO("Not yet implemented")
    }

    override fun removeFromBuffer(fileContentRange: FileContentRange) {
        TODO("Not yet implemented")
    }
}