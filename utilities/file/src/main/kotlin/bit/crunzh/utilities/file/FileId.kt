package bit.crunzh.utilities.file

data class FileId (val hashValue: ByteArray) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FileId

        if (!hashValue.contentEquals(other.hashValue)) return false

        return true
    }

    override fun hashCode(): Int {
        return hashValue.contentHashCode()
    }
}
