package bit.crunzh.utilities.file

import java.io.InputStream
import java.time.LocalDateTime

interface FileStorageService {
    /**
     * @return a file description for the file with the given id, if it is available in the file storage. Null if not available.
     */
    fun getFileDescription(fileId: FileId): FileDescription?

    /**
     * @return a list of all files in the storage. This function is heavy on the system as it will walk through the file system.
     * TODO make it return files as pages.
     */
    fun getFileDescriptions(): List<FileDescription>

    /**
     * Create a file with the given content and expiration time. If a file with the same file content already exists, nothing happens.
     * @return created or existing file
     */
    fun createFile(expirationTime: LocalDateTime, fileContent: ByteArray): FileDescription

    /**
     * Create a file with the given content and expiration time. If a file with the same file content already exists, nothing happens.
     * @return created or existing file
     */
    fun createFile(expirationTime: LocalDateTime, fileContent: InputStream): FileDescription

    /**
     * Create an empty file with the given id, size and expiration time. If a file with the same file content already exists, nothing happens.
     * @return created or existing file
     */
    fun createFile(expirationTime: LocalDateTime, fileSize: Int, fileId: FileId): FileDescription

    /**
     * Removes the file from the file storage.
     * @return true if file existed before deletion, else false.
     */
    fun deleteFile(fileId: FileId): Boolean
}