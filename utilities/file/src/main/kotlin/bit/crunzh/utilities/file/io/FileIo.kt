package bit.crunzh.utilities.file.io

import bit.crunzh.utilities.file.range.FileRange
import bit.crunzh.utilities.file.range.FileRangeSet
import java.util.ArrayList

interface FileIo {
    /**
     * Description of which part of the file is available to read.
     * Available bytes does not necessarily have to be flushed to disk.
     */
    fun getWrittenBytes(): FileRangeSet
    fun write(dataToWrite: FileContentRange)
    fun write(dataToWrite: List<FileContentRange>) {
        dataToWrite.forEach {
            write(it)
        }
    }

    /**
     * Read the bytes of the range requested. The range given will always be valid.
     * @return the bytes matching the file range, read from the file
     * @throws IllegalArgumentException if the given range is not available in the file.
     */
    fun read(range: FileRange): FileContentRange

    fun read(ranges: List<FileRange>): List<FileContentRange> {
        val readContent = ArrayList<FileContentRange>()
        ranges.forEach {
            readContent.add(read(it))
        }
        return readContent
    }
}