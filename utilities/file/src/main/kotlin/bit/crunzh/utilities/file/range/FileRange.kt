package bit.crunzh.utilities.file.range

data class FileRange(
        /**
         * Start index of range, inclusive.
         */
        val startIdx: Int,
        /**
         * End index of range, exclusive.
         */
        val endIdx: Int) {
    val length = endIdx - startIdx

    init {
        if (startIdx < 0) throw IllegalArgumentException("startIdx must be positive.")
        if (startIdx > endIdx) throw IllegalArgumentException("startIdx '$startIdx' must less than or equal to endIdx '$endIdx'.")
    }

    fun isEmpty() = startIdx == endIdx
}