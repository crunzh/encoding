package bit.crunzh.utilities.file.io

import bit.crunzh.utilities.file.FileId

interface FileIoFactory {
    fun create(fileId: FileId): FileIo
    fun delete(fileId: FileId): Boolean
    fun getFiles(): List<FileId>
}