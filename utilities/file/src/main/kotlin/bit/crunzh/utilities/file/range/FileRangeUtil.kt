package bit.crunzh.utilities.file.range

object FileRangeUtil {
    fun sortFileRangesOnStartIndex(vararg fileRangeLists: List<FileRange>): List<FileRange> {
        val unorderedFileRanges = ArrayList<FileRange>()
        fileRangeLists.forEach { fileRanges ->
            unorderedFileRanges.addAll(fileRanges)
        }
        return unorderedFileRanges.sortedBy { fileRange -> fileRange.startIdx }
    }
}