package bit.crunzh.utilities.file

import bit.crunzh.utilities.file.range.FileRange
import bit.crunzh.utilities.file.range.FileRangeSet
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import kotlin.test.assertFalse

class FileRangeSetTest {
    @Test
    fun getFileRangesDisjoint() {
        //Arrange
        val expected = listOf(FileRange(100, 200), FileRange(300, 400), FileRange(900, 1000))
        val rangeSet = FileRangeSet(1000, expected)

        //Act
        val actual = rangeSet.fileRanges

        //Assert
        assertEquals(expected, actual)
    }

    @Test
    fun getFileRangesShoulderToShoulder() {
        //Arrange
        val ranges = listOf(FileRange(100, 200), FileRange(200, 400), FileRange(500, 600), FileRange(600, 700))
        val rangeSet = FileRangeSet(1000, ranges)

        //Act
        val actual = rangeSet.fileRanges

        //Assert
        assertEquals(listOf(FileRange(100, 400), FileRange(500, 700)), actual)
    }

    @Test
    fun getFileRangesOverlap() {
        //Arrange
        val ranges = listOf(FileRange(100, 200), FileRange(100, 400), FileRange(150, 180), FileRange(300, 500))
        val rangeSet = FileRangeSet(1000, ranges)

        //Act
        val actual = rangeSet.fileRanges

        //Assert
        assertEquals(listOf(FileRange(100, 500)), actual)
    }

    @Test
    fun getFileRangesNotSortedOrder() {
        //Arrange
        val ranges = listOf(FileRange(400, 500), FileRange(300, 400), FileRange(600, 700), FileRange(500, 600))
        val rangeSet = FileRangeSet(1000, ranges)

        //Act
        val actual = rangeSet.fileRanges

        //Assert
        assertEquals(listOf(FileRange(300, 700)), actual)
    }

    @Test
    fun getFileRangesEmpty() {
        //Arrange
        val ranges = listOf(FileRange(300, 400), FileRange(500, 500))
        val rangeSet = FileRangeSet(1000, ranges)

        //Act
        val actual = rangeSet.fileRanges

        //Assert
        assertEquals(listOf(FileRange(300, 400)), actual)
    }

    @Test
    fun complement() {
        //Arrange
        val ranges = listOf(FileRange(0, 100), FileRange(400, 500), FileRange(600, 700))
        val rangeSet = FileRangeSet(1000, ranges)

        //Act
        val actual = rangeSet.complement()

        //Assert
        assertEquals(listOf(FileRange(100, 400), FileRange(500, 600), FileRange(700, 1000)), actual.fileRanges)
    }

    @Test
    fun union() {
        //Arrange
        val ranges1 = listOf(FileRange(0, 100), FileRange(400, 500), FileRange(600, 700))
        val rangeSet1 = FileRangeSet(1000, ranges1)
        val ranges2 = listOf(FileRange(50, 150), FileRange(500, 600), FileRange(850, 900))
        val rangeSet2 = FileRangeSet(1000, ranges2)

        //Act
        val actual = rangeSet1.union(rangeSet2)

        //Assert
        assertEquals(listOf(FileRange(0, 150), FileRange(400, 700), FileRange(850, 900)), actual.fileRanges)
    }

    @Test
    fun unionEmpty() {
        //Arrange
        val ranges1 = listOf(FileRange(0, 100), FileRange(400, 500), FileRange(600, 700))
        val rangeSet1 = FileRangeSet(1000, ranges1)
        val rangeSet2 = FileRangeSet(1000, emptyList())

        //Act
        val actual = rangeSet1.union(rangeSet2)

        //Assert
        assertEquals(ranges1, actual.fileRanges)
    }

    @Test
    fun unionFull() {
        //Arrange
        val ranges1 = listOf(FileRange(0, 100), FileRange(400, 500), FileRange(600, 700))
        val rangeSet1 = FileRangeSet(1000, ranges1)
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(0, 1000)))

        //Act
        val actual = rangeSet1.union(rangeSet2)

        //Assert
        assertTrue(actual.isFull())
    }

    @Test
    fun intersection() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(0, 200), FileRange(300, 500), FileRange(600, 700)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(100, 400), FileRange(550, 650)))

        //Act
        val actual = rangeSet1.intersection(rangeSet2)

        //Assert
        assertEquals(listOf(FileRange(100, 200), FileRange(300, 400), FileRange(600, 650)), actual.fileRanges)
    }

    @Test
    fun intersectionFullSet() {
        //Arrange
        val ranges1 = listOf(FileRange(0, 200), FileRange(300, 500), FileRange(600, 700))
        val rangeSet1 = FileRangeSet(1000, ranges1)
        val rangeSet2 = FileRangeSet.fullSet(1000)

        //Act
        val actual = rangeSet1.intersection(rangeSet2)

        //Assert
        assertEquals(ranges1, actual.fileRanges)
    }

    @Test
    fun intersectionEmptySet() {
        //Arrange
        val ranges1 = listOf(FileRange(0, 200), FileRange(300, 500), FileRange(600, 700))
        val rangeSet1 = FileRangeSet(1000, ranges1)
        val rangeSet2 = FileRangeSet.emptySet(1000)

        //Act
        val actual = rangeSet1.intersection(rangeSet2)

        //Assert
        assertTrue(actual.isEmpty())
    }

    @Test
    fun intersectionSubSet() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(0, 600)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(100, 500)))

        //Act
        val actual = rangeSet1.intersection(rangeSet2)

        //Assert
        assertEquals(listOf(FileRange(100, 500)), actual.fileRanges)
    }

    @Test
    fun intersectionSuperSet() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(100, 600)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(0, 700)))

        //Act
        val actual = rangeSet1.intersection(rangeSet2)

        //Assert
        assertEquals(listOf(FileRange(100, 600)), actual.fileRanges)
    }

    @Test
    fun difference() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(100, 200), FileRange(300, 600)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(0, 150), FileRange(400, 500)))

        //Act
        val actual = rangeSet1.difference(rangeSet2)

        //Assert
        assertEquals(listOf(FileRange(150, 200), FileRange(300, 400), FileRange(500, 600)), actual.fileRanges)
    }

    @Test
    fun differenceFullSet() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(100, 200), FileRange(300, 600)))
        val rangeSet2 = FileRangeSet.fullSet(1000)

        //Act
        val actual = rangeSet1.difference(rangeSet2)

        //Assert
        assertTrue(actual.isEmpty())
    }

    @Test
    fun differenceEmptySet() {
        //Arrange
        val ranges1 = listOf(FileRange(100, 200), FileRange(300, 600))
        val rangeSet1 = FileRangeSet(1000, ranges1)
        val rangeSet2 = FileRangeSet.emptySet(1000)

        //Act
        val actual = rangeSet1.difference(rangeSet2)

        //Assert
        assertEquals(ranges1, actual.fileRanges)
    }

    @Test
    fun differenceSubSet() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 600)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(400, 500)))

        //Act
        val actual = rangeSet1.difference(rangeSet2)

        //Assert
        assertEquals(listOf(FileRange(300, 400), FileRange(500, 600)), actual.fileRanges)
    }

    @Test
    fun differenceSuperSet() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 600)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(200, 700)))

        //Act
        val actual = rangeSet1.difference(rangeSet2)

        //Assert
        assertTrue(actual.isEmpty())
    }

    @Test
    fun differenceSame() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 600)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(300, 600)))

        //Act
        val actual = rangeSet1.difference(rangeSet2)

        //Assert
        assertTrue(actual.isEmpty())
    }

    @Test
    fun symmetricDifference() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 600), FileRange(700, 900)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(400, 700), FileRange(900, 1000)))

        //Act
        val actual = rangeSet1.symmetricDifference(rangeSet2)

        //Assert
        assertEquals(listOf(FileRange(300, 400), FileRange(600, 1000)), actual.fileRanges)
    }

    @Test
    fun symmetricDifferenceFullSet() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 600), FileRange(700, 900)))
        val rangeSet2 = FileRangeSet.fullSet(1000)

        //Act
        val actual = rangeSet1.symmetricDifference(rangeSet2)

        //Assert
        assertEquals(listOf(FileRange(0, 300), FileRange(600, 700), FileRange(900, 1000)), actual.fileRanges)
    }

    @Test
    fun symmetricDifferenceEmptySet() {
        //Arrange
        val ranges1 = listOf(FileRange(300, 600), FileRange(700, 900))
        val rangeSet1 = FileRangeSet(1000, ranges1)
        val rangeSet2 = FileRangeSet.emptySet(1000)

        //Act
        val actual = rangeSet1.symmetricDifference(rangeSet2)

        //Assert
        assertEquals(ranges1, actual.fileRanges)
    }

    @Test
    fun symmetricDifferenceSubset() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 600)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(400, 500)))

        //Act
        val actual = rangeSet1.symmetricDifference(rangeSet2)

        //Assert
        assertEquals(listOf(FileRange(300, 400), FileRange(500, 600)), actual.fileRanges)
    }

    @Test
    fun symmetricDifferenceSuperSet() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(400, 500)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(300, 600)))

        //Act
        val actual = rangeSet1.symmetricDifference(rangeSet2)

        //Assert
        assertEquals(listOf(FileRange(300, 400), FileRange(500, 600)), actual.fileRanges)
    }

    @Test
    fun symmetricDifferenceSame() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 600)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(300, 600)))

        //Act
        val actual = rangeSet1.symmetricDifference(rangeSet2)

        //Assert
        assertTrue(actual.isEmpty())
    }

    @Test
    fun isDisjointShoulderToShoulder() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(400, 500)))

        //Act
        val actual = rangeSet1.isDisjoint(rangeSet2)

        //Assert
        assertTrue(actual)
    }

    @Test
    fun isDisjointOverlap() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 401)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(400, 500)))

        //Act
        val actual = rangeSet1.isDisjoint(rangeSet2)

        //Assert
        assertFalse(actual)
    }

    @Test
    fun isDisjointFullSet() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet.fullSet(1000)

        //Act
        val actual = rangeSet1.isDisjoint(rangeSet2)

        //Assert
        assertFalse(actual)
    }

    @Test
    fun isDisjointEmptySet() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet.emptySet(1000)

        //Act
        val actual = rangeSet1.isDisjoint(rangeSet2)

        //Assert
        assertTrue(actual)
    }

    @Test
    fun overlapsShoulderToShoulder() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(400, 500)))

        //Act
        val actual = rangeSet1.overlaps(rangeSet2)

        //Assert
        assertFalse(actual)
    }

    @Test
    fun overlapsOverlap() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 401)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(400, 500)))

        //Act
        val actual = rangeSet1.overlaps(rangeSet2)

        //Assert
        assertTrue(actual)
    }

    @Test
    fun overlapsFullSet() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet.fullSet(1000)

        //Act
        val actual = rangeSet1.overlaps(rangeSet2)

        //Assert
        assertTrue(actual)
    }

    @Test
    fun overlapsEmptySet() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet.emptySet(1000)

        //Act
        val actual = rangeSet1.overlaps(rangeSet2)

        //Assert
        assertFalse(actual)
    }

    @Test
    fun isSubsetOfSuperSet() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(200, 500)))

        //Act
        val actual = rangeSet1.isSubsetOf(rangeSet2)

        //Assert
        assertTrue(actual)
    }

    @Test
    fun isSubsetOfSuperSetManyRanges() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(0, 100), FileRange(300, 400), FileRange(700, 800), FileRange(900, 1000)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(0, 500), FileRange(700, 800), FileRange(899, 1000)))

        //Act
        val actual = rangeSet1.isSubsetOf(rangeSet2)

        //Assert
        assertTrue(actual)
    }

    @Test
    fun isSubsetOfPartialLow() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(200, 350)))

        //Act
        val actual = rangeSet1.isSubsetOf(rangeSet2)

        //Assert
        assertFalse(actual)
    }

    @Test
    fun isSubsetOfPartialHigh() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(350, 500)))

        //Act
        val actual = rangeSet1.isSubsetOf(rangeSet2)

        //Assert
        assertFalse(actual)
    }

    @Test
    fun isSubsetOfSubset() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(350, 380)))

        //Act
        val actual = rangeSet1.isSubsetOf(rangeSet2)

        //Assert
        assertFalse(actual)
    }

    @Test
    fun isSubsetOfSame() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(300, 400)))

        //Act
        val actual = rangeSet1.isSubsetOf(rangeSet2)

        //Assert
        assertTrue(actual)
    }

    @Test
    fun isSupersetOfSuperSet() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(200, 500)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(300, 400)))

        //Act
        val actual = rangeSet1.isSubsetOf(rangeSet2)

        //Assert
        assertFalse(actual)
    }

    @Test
    fun isSupersetOfSuperSetManyRanges() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(0, 500), FileRange(700, 800), FileRange(899, 1000)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(0, 100), FileRange(300, 400), FileRange(700, 800), FileRange(900, 1000)))

        //Act
        val actual = rangeSet1.isSubsetOf(rangeSet2)

        //Assert
        assertFalse(actual)
    }

    @Test
    fun isSupersetOfPartialLow() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(200, 300)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(100, 250)))

        //Act
        val actual = rangeSet1.isSupersetOf(rangeSet2)

        //Assert
        assertFalse(actual)
    }

    @Test
    fun isSupersetOfPartialHigh() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(350, 500)))

        //Act
        val actual = rangeSet1.isSupersetOf(rangeSet2)

        //Assert
        assertFalse(actual)
    }

    @Test
    fun isSupersetOfSubset() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(350, 380)))

        //Act
        val actual = rangeSet1.isSupersetOf(rangeSet2)

        //Assert
        assertTrue(actual)
    }

    @Test
    fun isSupersetOfSame() {
        //Arrange
        val rangeSet1 = FileRangeSet(1000, listOf(FileRange(300, 400)))
        val rangeSet2 = FileRangeSet(1000, listOf(FileRange(300, 400)))

        //Act
        val actual = rangeSet1.isSupersetOf(rangeSet2)

        //Assert
        assertTrue(actual)
    }

    @Test
    fun createEmptySet() {
        //Arrange
        val fileSize = 1000

        //Act
        val emptySet = FileRangeSet.emptySet(fileSize)

        //Assert
        assertTrue(emptySet.isEmpty())
        assertFalse(emptySet.isFull())
        assertTrue(emptySet.fileRanges.isEmpty())
        assertEquals(fileSize, emptySet.fileSizeBytes)
    }

    @Test
    fun createFullSet() {
        //Arrange
        val fileSize = 1000

        //Act
        val emptySet = FileRangeSet.fullSet(fileSize)

        //Assert
        assertFalse(emptySet.isEmpty())
        assertTrue(emptySet.isFull())
        assertEquals(1, emptySet.fileRanges.size)
        assertEquals(FileRange(0, fileSize), emptySet.fileRanges.first())
        assertEquals(fileSize, emptySet.fileSizeBytes)
    }
}