import org.gradle.jvm.tasks.Jar

plugins {
    kotlin("jvm")
    `java-gradle-plugin`
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(project(":utilities:encoding"))
    implementation("com.squareup:kotlinpoet:1.7.2")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

gradlePlugin {
    plugins {
        create("classDescriptorGeneratorPlugin") {
            id = "bit.crunzh.class-desc-gen"
            implementationClass = "bit.crunzh.utilities.codegen.encoding.ClassDescriptorGenerator"
        }
    }
}