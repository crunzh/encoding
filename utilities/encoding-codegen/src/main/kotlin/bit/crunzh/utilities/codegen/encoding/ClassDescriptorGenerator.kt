package bit.crunzh.utilities.codegen.encoding

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.TypeSpec
import org.gradle.api.Plugin
import org.gradle.api.Project
import java.net.URL
import java.net.URLClassLoader
import kotlin.reflect.KClass
import kotlin.reflect.KType


class ClassDescriptorGenerator : Plugin<Project> {
    private val packageRegEx = Regex("(\\w+\\.)+(\\w+)")

    override fun apply(project: Project) {

        project.task("assertBackwardsCompatible") { task ->
            val assertConfiguration = task.extensions.create("assert", AssertConfiguration::class.java)
        }

        project.task("generate") { task ->
            val generateConfiguration = task.extensions.create("assert", CodeGenConfiguration::class.java)
            val typeToGenerate = URLClassLoader.newInstance(arrayOf(URL(generateConfiguration.typeToGenerate))).loadClass(generateConfiguration.typeToGenerate)
            val kotlinType = typeToGenerate.kotlin

            FileSpec.builder("", "").build()
        }
    }

    fun build(type: KClass<*>, configuration: CodeGenConfiguration): FileSpec {
        val className = ClassName(getPackageName(type), createDecoratorClassName(type, configuration))
        val fileSpec = FileSpec.builder(className.packageName, className.simpleName + ".kt")

        val classBuilder = TypeSpec.classBuilder(className)

        return fileSpec.addType(classBuilder.build()).build()
    }

    private fun createDecoratorClassName(type: KClass<*>, configuration: CodeGenConfiguration): String {
        return configuration.prefix + type.simpleName + configuration.postFix
    }

    private fun getPackageName(type: KClass<*>): String {
        if (type.qualifiedName == null) {
            return ""
        }
        val matcher = packageRegEx.matchEntire(type.qualifiedName!!) ?: return ""
        return matcher.groups[2]?.value ?: return ""
    }
}

