plugins {
    `java-library`
    `maven-publish`
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.7") //TODO figure out to move kotlin dependency version out to root.
    implementation("io.github.microutils:kotlin-logging:2.0.4")
    // Use the Kotlin test library.

    // Use the Kotlin JUnit integration.
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("com.google.code.gson:gson:2.8.6")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.5.0")
}