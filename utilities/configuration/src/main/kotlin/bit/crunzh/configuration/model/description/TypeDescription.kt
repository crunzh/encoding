package bit.crunzh.configuration.model.description

import kotlin.reflect.KClass

interface TypeDescription<T : Any> {
    val type: KClass<T>
    fun validate(propertyName: String, obj: T): ValidationResult
}