package bit.crunzh.configuration.model.annotation.properties

/**
 * Property must also additionally be annotated with one of the collection types to be valid.
 */
@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ListProperty