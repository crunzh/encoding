package bit.crunzh.configuration.model.annotation.properties.collection.map

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class LongMapKey(
        val minValue: Long = Long.MIN_VALUE,
        val maxValue: Long = Long.MAX_VALUE
)