package bit.crunzh.configuration.model.annotation.properties.collection

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class FloatCollectionValue (
        val minValue: Float = Float.MIN_VALUE,
        val maxValue: Float = Float.MAX_VALUE
)