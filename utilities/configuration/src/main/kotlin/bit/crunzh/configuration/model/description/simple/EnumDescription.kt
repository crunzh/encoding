package bit.crunzh.configuration.model.description.simple

import bit.crunzh.configuration.model.description.SimpleTypeDescription
import bit.crunzh.configuration.model.description.ValidationResult
import kotlin.reflect.KClass

class EnumDescription<T : Enum<T>>(
    override val type: KClass<T>,
    val enumFunction: (String) -> T,
    val unknownValue: T? = null,
    val stringFunction: (T) -> String

) : SimpleTypeDescription<T> {

    override fun valueToString(value: T): String {
        return value.toString()
    }

    override fun stringToValue(string: String): T {
        return enumFunction(string)
    }

    override fun validate(propertyName: String, obj: T): ValidationResult {
        return ValidationResult(true, propertyName)
    }
}