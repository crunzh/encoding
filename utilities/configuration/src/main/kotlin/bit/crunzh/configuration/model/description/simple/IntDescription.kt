package bit.crunzh.configuration.model.description.simple

import bit.crunzh.configuration.model.description.LocaleDescriptions
import bit.crunzh.configuration.model.description.SimpleTypeDescription
import bit.crunzh.configuration.model.description.ValidationResult

class IntDescription(
    val minValue: Int = Int.MIN_VALUE,
    val maxValue: Int = Int.MAX_VALUE
) : SimpleTypeDescription<Int> {
    override val type = Int::class

    override fun valueToString(value: Int): String {
        return value.toString()
    }

    override fun stringToValue(string: String): Int {
        return string.toInt()
    }

    override fun validate(propertyName: String, obj: Int): ValidationResult {
        if (obj < minValue || obj > maxValue) {
            return ValidationResult(
                false,
                propertyName,
                localeErrorMap = LocaleDescriptions.createDescriptionMap(
                    LocaleDescriptions.ERROR_VALUE_RANGE_KEY,
                    type.simpleName!!,
                    minValue.toString(),
                    maxValue.toString(),
                    obj.toString()
                )
            )
        }
        return ValidationResult(true, propertyName)
    }
}