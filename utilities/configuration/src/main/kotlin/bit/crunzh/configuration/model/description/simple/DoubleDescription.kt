package bit.crunzh.configuration.model.description.simple

import bit.crunzh.configuration.model.description.LocaleDescriptions
import bit.crunzh.configuration.model.description.SimpleTypeDescription
import bit.crunzh.configuration.model.description.ValidationResult

class DoubleDescription(
    val minValue: Double = Double.MIN_VALUE,
    val maxValue: Double = Double.MAX_VALUE
) : SimpleTypeDescription<Double> {
    override val type = Double::class

    override fun valueToString(value: Double): String {
        return value.toString()
    }

    override fun stringToValue(string: String): Double {
        return string.toDouble()
    }

    override fun validate(propertyName: String, obj: Double): ValidationResult {
        if (obj < minValue || obj > maxValue) {
            return ValidationResult(
                false,
                propertyName,
                localeErrorMap = LocaleDescriptions.createDescriptionMap(
                    LocaleDescriptions.ERROR_VALUE_RANGE_KEY,
                    type.simpleName!!,
                    minValue.toString(),
                    maxValue.toString(),
                    obj.toString()
                )
            )
        }
        return ValidationResult(true, propertyName)
    }
}