package bit.crunzh.configuration.model.description

import java.util.*

data class PropertyDescription<PropertyType : Any, ParentType: Any>(
    val propertyName: String,
    val propertyGetFunction: (ParentType) -> PropertyType?,
    val isNullable: Boolean,
    /**
     * Default value is used to indicate to a user what a good value would be when configuring this property.
     * The default value is also used to assign property value if receiving a configuration in a prior version model where a new property was not defined.
     */
    val defaultValue: PropertyType? = null,
    val typeDescription: TypeDescription<PropertyType>,
    /**
     * The version of the configuration model. This can be used for explicit handling of configurations received in prior configuration model versions.
     * The version of the configuration model must be larger than or equal to the version of the properties, and it must be equal to the largest version of the properties.
     */
    val configurationModelVersion: Int = 1,
    val defaultLocale: Locale = Locale.ENGLISH,
    val nameMap: Map<Locale, String>,
    val descriptionMap: Map<Locale, String>
) {

    init {
        if (nameMap[defaultLocale] == null) {
            throw IllegalArgumentException("nameMap must at least provide a value in the default locale '$defaultLocale'.")
        }
        if (descriptionMap[defaultLocale] == null) {
            throw IllegalArgumentException("descriptionMap must at least provide a value in the default locale '$defaultLocale'.")
        }
    }

    fun getName(locale: Locale = Locale.ENGLISH): String {
        return nameMap[locale] ?: nameMap[defaultLocale]!!
    }

    fun getDescription(locale: Locale = Locale.ENGLISH): String {
        return descriptionMap[locale] ?: descriptionMap[defaultLocale]!!
    }

    fun validateProperty(parentObj: ParentType): ValidationResult {
        val obj = propertyGetFunction(parentObj)
            ?: return if (!isNullable) {
                ValidationResult(
                    isValid = false,
                    propertyName,
                    localeErrorMap = LocaleDescriptions.createDescriptionMap(LocaleDescriptions.ERROR_PROPERTY_NULL_KEY, propertyName)
                )
            } else {
                ValidationResult( true, propertyName)
            }
        return typeDescription.validate(propertyName, obj)
    }
}
