package bit.crunzh.configuration.model.description.simple

import bit.crunzh.configuration.model.description.LocaleDescriptions
import bit.crunzh.configuration.model.description.SimpleTypeDescription
import bit.crunzh.configuration.model.description.ValidationResult

class ShortDescription(
    val minValue: Short = Short.MIN_VALUE,
    val maxValue: Short = Short.MAX_VALUE
) : SimpleTypeDescription<Short> {
    override val type = Short::class

    override fun valueToString(value: Short): String {
        return value.toString()
    }

    override fun stringToValue(string: String): Short {
        return string.toShort()
    }

    override fun validate(propertyName: String, obj: Short): ValidationResult {
        if (obj < minValue || obj > maxValue) {
            return ValidationResult(
                false,
                propertyName,
                localeErrorMap = LocaleDescriptions.createDescriptionMap(
                    LocaleDescriptions.ERROR_VALUE_RANGE_KEY,
                    type.simpleName!!,
                    minValue.toString(),
                    maxValue.toString(),
                    obj.toString()
                )
            )
        }
        return ValidationResult(true, propertyName)
    }
}