package bit.crunzh.configuration.model.annotation.properties.collection

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ShortCollectionValue(
        val minValue: Short = Short.MIN_VALUE,
        val maxValue: Short = Short.MAX_VALUE
)