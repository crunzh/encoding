package bit.crunzh.configuration.model.annotation.properties

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class FloatProperty(
        val minValue: Float = Float.MIN_VALUE,
        val maxValue: Float = Float.MAX_VALUE
)