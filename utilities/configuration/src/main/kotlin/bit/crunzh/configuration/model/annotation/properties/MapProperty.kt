package bit.crunzh.configuration.model.annotation.properties

/**
 * Property must also additionally be annotated with two of the collection annotation types to be valid.
 * One must be a MapKey and the other a CollectionValue.
 */
@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class MapProperty (
        /**
         * Ignore map entries with keys of unknown types.
         * Alternative the parsing of the configuration will fail.
         */
        val ignoreUnknownKeys: Boolean = true,

        /**
         * Ignore map entries with values of unknown types.
         * Alternative the map entry value be assigned to null.
         */
        val ignoreUnknownValues: Boolean = true
)