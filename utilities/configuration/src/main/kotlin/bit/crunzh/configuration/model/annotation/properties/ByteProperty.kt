package bit.crunzh.configuration.model.annotation.properties

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ByteProperty(
        val minValue: Byte = Byte.MIN_VALUE,
        val maxValue: Byte = Byte.MAX_VALUE
)