package bit.crunzh.configuration.model.annotation.properties.collection.map


@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class IntMapKey(
        val minValue: Int = Int.MIN_VALUE,
        val maxValue: Int = Int.MAX_VALUE
)