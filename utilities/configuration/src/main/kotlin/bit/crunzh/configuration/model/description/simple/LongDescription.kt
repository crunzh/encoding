package bit.crunzh.configuration.model.description.simple

import bit.crunzh.configuration.model.description.LocaleDescriptions
import bit.crunzh.configuration.model.description.SimpleTypeDescription
import bit.crunzh.configuration.model.description.ValidationResult

class LongDescription(
    val minValue: Long = Long.MIN_VALUE,
    val maxValue: Long = Long.MAX_VALUE
) : SimpleTypeDescription<Long> {
    override val type = Long::class

    override fun valueToString(value: Long): String {
        return value.toString()
    }

    override fun stringToValue(string: String): Long {
        return string.toLong()
    }

    override fun validate(propertyName: String, obj: Long): ValidationResult {
        if (obj < minValue || obj > maxValue) {
            return ValidationResult(
                false,
                propertyName,
                localeErrorMap = LocaleDescriptions.createDescriptionMap(
                    LocaleDescriptions.ERROR_VALUE_RANGE_KEY,
                    type.simpleName!!,
                    minValue.toString(),
                    maxValue.toString(),
                    obj.toString()
                )
            )
        }
        return ValidationResult(true, propertyName)
    }
}