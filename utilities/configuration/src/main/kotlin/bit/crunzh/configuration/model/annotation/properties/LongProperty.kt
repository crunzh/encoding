package bit.crunzh.configuration.model.annotation.properties

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class LongProperty(
        val minValue: Long = Long.MIN_VALUE,
        val maxValue: Long = Long.MAX_VALUE
)