package bit.crunzh.configuration.model.value

data class ConfigurationString(val value: String) : ConfigurationValue