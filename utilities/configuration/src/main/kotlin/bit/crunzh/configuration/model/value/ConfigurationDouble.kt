package bit.crunzh.configuration.model.value

data class ConfigurationDouble(val value: Double) : ConfigurationValue