package bit.crunzh.configuration.model.description.simple

import bit.crunzh.configuration.model.description.LocaleDescriptions
import bit.crunzh.configuration.model.description.SimpleTypeDescription
import bit.crunzh.configuration.model.description.ValidationResult

class FloatDescription(
    val minValue: Float = Float.MIN_VALUE,
    val maxValue: Float = Float.MAX_VALUE
) : SimpleTypeDescription<Float> {
    override val type = Float::class

    override fun valueToString(value: Float): String {
        return value.toString()
    }

    override fun stringToValue(string: String): Float {
        return string.toFloat()
    }

    override fun validate(propertyName: String, obj: Float): ValidationResult {
        if (obj < minValue || obj > maxValue) {
            return ValidationResult(
                false,
                propertyName,
                localeErrorMap = LocaleDescriptions.createDescriptionMap(
                    LocaleDescriptions.ERROR_VALUE_RANGE_KEY,
                    type.simpleName!!,
                    minValue.toString(),
                    maxValue.toString(),
                    obj.toString()
                )
            )
        }
        return ValidationResult(true, propertyName)
    }
}