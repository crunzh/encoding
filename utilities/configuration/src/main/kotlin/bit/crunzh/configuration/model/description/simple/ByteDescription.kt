package bit.crunzh.configuration.model.description.simple

import bit.crunzh.configuration.model.description.LocaleDescriptions
import bit.crunzh.configuration.model.description.SimpleTypeDescription
import bit.crunzh.configuration.model.description.ValidationResult

class ByteDescription   (
    val minValue: Byte = Byte.MIN_VALUE,
    val maxValue: Byte = Byte.MAX_VALUE
) : SimpleTypeDescription<Byte> {
    override val type = Byte::class

    override fun valueToString(value: Byte): String {
        return value.toString()
    }

    override fun stringToValue(string: String): Byte {
        return string.toByte()
    }

    override fun validate(propertyName: String, obj: Byte): ValidationResult {
        if (obj < minValue || obj > maxValue) {
            return ValidationResult(
                false,
                propertyName,
                localeErrorMap = LocaleDescriptions.createDescriptionMap(
                    LocaleDescriptions.ERROR_VALUE_RANGE_KEY,
                    type.simpleName!!,
                    minValue.toString(),
                    maxValue.toString(),
                    obj.toString()
                )
            )
        }
        return ValidationResult(true, propertyName)
    }
}