package bit.crunzh.configuration.model.annotation.properties.collection

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class BooleanCollectionValue