package bit.crunzh.configuration.model.annotation.properties.collection.map

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ShortMapKey(
        val minValue: Short = Short.MIN_VALUE,
        val maxValue: Short = Short.MAX_VALUE
)