package bit.crunzh.configuration.model

import bit.crunzh.configuration.model.description.ClassTypeDescription
import bit.crunzh.configuration.model.description.ValidationResult
import bit.crunzh.configuration.model.value.ConfigurationObject

class ConfigurationValueBuilder<T : Any>(val typeDescription: ClassTypeDescription<T>) {


    fun addSimpleValue(propertyName: String, value: String): ValidationResult {
        TODO()
    }

    fun addComplexValue(propertyName: String, value: Any?): ValidationResult {
        TODO()
    }

    fun validate(): ValidationResult {
        TODO()
    }

    fun build(): T {
        //val configurationObj = ConfigurationObject(typeDescription.type.qualifiedName!!, )
        //typeDescription.create()
        TODO()
    }
}