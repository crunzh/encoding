package bit.crunzh.configuration.model.annotation.properties.collection

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class StringCollectionValue (
        /**
         * if regEx is the empty string, no regular expression will be instantiated to evaluate any value string.
         */
        val regEx: String = ""
)