package bit.crunzh.configuration.model.annotation.properties.collection

import kotlin.reflect.KClass

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class EnumCollectionValue (
        /**
         * This value is used to handle situation where a future version of an enum has been expanded with new values. In case the configuration value for the enum is a local unknown value, the enum translation will default to the enum value with this ordinal value.
         */
        val unknownValueOrdinal: Int = -1,
        val enumType: KClass<*>
)