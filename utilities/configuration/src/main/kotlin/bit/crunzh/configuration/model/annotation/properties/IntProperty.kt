package bit.crunzh.configuration.model.annotation.properties


@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class IntProperty(
        val minValue: Int = Int.MIN_VALUE,
        val maxValue: Int = Int.MAX_VALUE
)