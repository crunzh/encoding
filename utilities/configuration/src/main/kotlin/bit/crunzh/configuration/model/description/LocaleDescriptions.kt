package bit.crunzh.configuration.model.description

import mu.KotlinLogging
import java.util.*
import kotlin.collections.HashMap

object LocaleDescriptions {
    private val logger = KotlinLogging.logger {}
    const val ERROR_PROPERTY_NULL_KEY = "validation.error.property.null"
    const val ERROR_VALUE_RANGE_KEY = "validation.error.value.range"
    const val SUCCESS_KEY = "validation.success"
    private val localeDescriptionMap = HashMap<String, Map<Locale, String>>()

    init {
        //TODO load locales
    }

    fun createDescriptionMap(descriptionKey: String, vararg strFormatArgs: String): Map<Locale, String> {
        val localeMap = localeDescriptionMap[descriptionKey]
        val returnMap = HashMap<Locale, String>()
        if (localeMap == null) {
            logger.warn { "No locale description map defined for '$descriptionKey'." }
            return emptyMap()
        }
        localeMap.forEach { (locale, strTemplate) -> returnMap[locale] = strTemplate.format(strFormatArgs) }
        return returnMap
    }

    fun getDescription(descriptionKey: String, locale: Locale = Locale.ENGLISH): String {
        val localeMap = localeDescriptionMap[descriptionKey]
        if (localeMap == null) {
            logger.warn { "No description defined for '$descriptionKey' in any Locales." }
            return descriptionKey
        }
        var localeString = localeMap[locale]
        if (localeString == null) {
            localeString = localeMap[Locale.ENGLISH]
            if (localeString == null) {
                logger.warn { "No description defined for '$descriptionKey' in Locale '$locale' or default Locale '${Locale.ENGLISH}'." }
                localeString = descriptionKey
            } else {
                logger.warn { "No description defined for '$descriptionKey' in Locale '$locale', defaulting to Locale '${Locale.ENGLISH}'." }
            }
        }
        return localeString
    }
}