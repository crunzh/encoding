package bit.crunzh.configuration.model.annotation.properties.collection

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class DoubleCollectionValue (
        val minValue: Double = Double.MIN_VALUE,
        val maxValue: Double = Double.MAX_VALUE
)