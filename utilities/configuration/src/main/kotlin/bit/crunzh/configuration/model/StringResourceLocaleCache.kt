package bit.crunzh.configuration.model

import java.util.*
import kotlin.collections.HashMap

class StringResourceLocaleCache(val stringLocaleLookup: (String, Locale) -> String) {
    //TODO refactor to allow multiple stringLocalLookups, this will allow us to also inject the generic configuration locals this ways and to cache them.
    private val stringCache = HashMap<String, MutableMap<Locale, String>>()

    fun getString(key: String, locale: Locale): String {
        val localeMap = stringCache.getOrPut(key) { HashMap() }
        return localeMap.getOrPut(locale) { stringLocaleLookup(key, locale) }
    }
}
