package bit.crunzh.configuration.model.annotation.properties.collection

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ByteCollectionValue(
        val minValue: Byte = Byte.MIN_VALUE,
        val maxValue: Byte = Byte.MAX_VALUE
)