package bit.crunzh.configuration.model.description.simple

import bit.crunzh.configuration.model.description.SimpleTypeDescription
import bit.crunzh.configuration.model.description.ValidationResult

class BooleanDescription : SimpleTypeDescription<Boolean> {
    override val type = Boolean::class

    override fun valueToString(value: Boolean): String {
        return value.toString()
    }

    override fun stringToValue(string: String): Boolean {
        return string.toBoolean()
    }

    override fun validate(propertyName: String, obj: Boolean): ValidationResult {
        return ValidationResult(true, propertyName)
    }
}