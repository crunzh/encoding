package bit.crunzh.configuration.model.value

data class ConfigurationArray(val elements: List<ConfigurationValue>) : ConfigurationValue
