package bit.crunzh.configuration.model.value

data class ConfigurationBoolean(val value: Boolean) : ConfigurationValue