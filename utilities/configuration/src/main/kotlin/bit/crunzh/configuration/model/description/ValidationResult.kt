package bit.crunzh.configuration.model.description

import java.util.*

data class ValidationResult(
    //TODO move handling of locale to the locale StringResourceLocaleCache
        val isValid: Boolean,
        val validatedItem: String,
        val defaultLocale: Locale = Locale.ENGLISH,
        val localeErrorMap: Map<Locale, String> = emptyMap(),
        val nestedValidations: List<ValidationResult> = emptyList()
) {
    val isInvalid = !isValid

    init {
        if (!isValid && localeErrorMap[defaultLocale] == null) {
            throw IllegalArgumentException("If validation is not valid, localErrorMap, must at least provide a validation error description in the default locale '$defaultLocale'.")
        }
    }

    constructor(
        isValid: Boolean,
        propertyName: String,
        defaultLocale: Locale = Locale.ENGLISH,
        localeErrorMap: Map<Locale, String> = emptyMap(),
        nestedValidation: ValidationResult
    ) : this(isValid, propertyName, defaultLocale, localeErrorMap, listOf(nestedValidation))

    /**
     * Get the description for this validation result - ignoring any nested validations.
     */
    fun getDescription(locale: Locale = Locale.ENGLISH): String {
        if (isValid) {
            return LocaleDescriptions.getDescription(LocaleDescriptions.SUCCESS_KEY, locale)
        }
        return localeErrorMap[locale] ?: localeErrorMap[defaultLocale]!!
    }

    /**
     * Get all error descriptions for this and nested validations.
     */
    fun getErrorDescriptions(locale: Locale = Locale.ENGLISH): List<String> {
        val errorDescriptions = ArrayList<String>()
        if (isInvalid) {
            errorDescriptions.add(getDescription(locale))
        }
        for (nested in nestedValidations) {
            if (nested.isInvalid) {
                errorDescriptions.addAll(nested.getErrorDescriptions(locale))
            }
        }
        return errorDescriptions
    }

    override fun toString(): String {
        if (isValid) {
            return LocaleDescriptions.getDescription(LocaleDescriptions.SUCCESS_KEY, Locale.ENGLISH)
        }
        var aggregatedString = "Invalid: "
        getErrorDescriptions().forEach { errString ->
            run {
                if (aggregatedString.isEmpty()) {
                    aggregatedString += errString
                } else {
                    aggregatedString += ", $errString"
                }
            }
        }
        return aggregatedString
    }
}
