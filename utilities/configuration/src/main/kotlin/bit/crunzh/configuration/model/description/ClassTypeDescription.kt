package bit.crunzh.configuration.model.description

import bit.crunzh.configuration.model.value.ConfigurationObject
import kotlin.reflect.KClass

class ClassTypeDescription<ClassType : Any>(
    override val type: KClass<ClassType>,
    /**
     * The version of the configuration model. This can be used for explicit handling of configurations received in prior configuration model versions.
     * The version of the configuration model must be larger than or equal to the version of the properties, and it must be equal to the largest version of the properties.
     */
    val configurationModelVersion: Int = 1,
    private val createFunction: (ConfigurationObject) -> ClassType,
    private val validationFunction: (String, ClassType) -> ValidationResult = { propName, _ -> ValidationResult(true, propName) },
    private val propertyDescriptions: Map<String, PropertyDescription<*, ClassType>>
) : TypeDescription<ClassType> {

    fun create(configurationObject: ConfigurationObject): ClassType {
        val obj = createFunction(configurationObject)
        val validationResult = validate("any", obj)
        if (validationResult.isInvalid) throw IllegalArgumentException("Could not create configuration object, as it was not valid. Reason: '${validationResult.getDescription()}'")
        return obj
    }

    override fun validate(propertyName: String, obj: ClassType): ValidationResult {
        val validationResults = ArrayList<ValidationResult>()
        var allPropertiesValid = true
        propertyDescriptions.values.forEach { propDesc ->
            val validation = propDesc.validateProperty(obj)
            validationResults.add(validation)
            if (validation.isInvalid) {
                allPropertiesValid = false
            }
        }
        val classValidation = validationFunction(propertyName, obj)
        return ValidationResult(
            allPropertiesValid && classValidation.isValid,
            propertyName,
            classValidation.defaultLocale,
            classValidation.localeErrorMap,
            validationResults
        )
    }

    fun propertyDescriptions(): List<PropertyDescription<*, ClassType>> {
        return ArrayList(propertyDescriptions.values)
    }

    fun <PropertyType : Any> getPropertyDescription(propertyName: String): PropertyDescription<PropertyType, ClassType> {
        return propertyDescriptions[propertyName] as PropertyDescription<PropertyType, ClassType>
    }
}