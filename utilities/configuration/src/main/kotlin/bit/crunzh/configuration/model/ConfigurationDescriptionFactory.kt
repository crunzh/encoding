package bit.crunzh.configuration.model

import bit.crunzh.configuration.model.annotation.Configuration
import bit.crunzh.configuration.model.description.ClassTypeDescription
import bit.crunzh.configuration.model.description.PropertyDescription
import bit.crunzh.configuration.model.description.ValidationResult
import bit.crunzh.configuration.model.value.ConfigurationObject
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf

object ConfigurationDescriptionFactory {
    fun <T : Any> createDescription(
        /**
         * The type to create a configuration description for
         */
        type: KClass<T>,
        /**
         * A string resource lookup function, allowing localization.
         * It is recommended to implement this using a PropertyResourceBundle.
         */
        stringLocaleLookup: (String, Locale) -> String = { key, _ -> key },
        configurationValidator: (String, T) -> ValidationResult = { _, _ -> ValidationResult(true, type.simpleName!!) }
    ): ClassTypeDescription<T> {
        val stringResourceCache = StringResourceLocaleCache(stringLocaleLookup)
        if (type.qualifiedName == null) throw IllegalArgumentException("Type must have a qualified name.")
        val configurationModelVersion = getConfigurationModelVersion(type)
        val propertyMap: Map<String, PropertyDescription<*, T>> = getPropertyMap(type)
        val createFunction: (ConfigurationObject) -> T = getCreateFunction(type, propertyMap)
        return ClassTypeDescription(type, configurationModelVersion, createFunction, configurationValidator, propertyMap)
    }

    private fun <T: Any> getPropertyMap(type: KClass<T>): Map<String, PropertyDescription<*, T>> {
        TODO("Not yet implemented")
    }

    private fun <T: Any> getCreateFunction(type: KClass<T>, propertyMap: Map<String, PropertyDescription<*, T>>): (ConfigurationObject) -> T {
        TODO("Not yet implemented")
    }

    private fun getConfigurationModelVersion(type: KClass<*>): Int {
        for (annotation in type.annotations) {
            if (annotation is Configuration) {
                return annotation.configurationModelVersion
            }
        }
        return 1
    }
}