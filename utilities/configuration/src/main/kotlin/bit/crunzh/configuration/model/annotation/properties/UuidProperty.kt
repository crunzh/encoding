package bit.crunzh.configuration.model.annotation.properties

import java.util.*

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class UuidProperty