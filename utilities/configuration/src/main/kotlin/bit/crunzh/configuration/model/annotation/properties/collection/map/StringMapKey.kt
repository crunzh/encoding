package bit.crunzh.configuration.model.annotation.properties.collection.map

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class StringMapKey (
        /**
         * if regEx is the empty string, no regular expression will be instantiated to evaluate any value string.
         */
        val regEx: String = ""
)