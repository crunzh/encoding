package bit.crunzh.configuration.model.annotation.properties.collection.map

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class FloatMapKey (
        val minValue: Float = Float.MIN_VALUE,
        val maxValue: Float = Float.MAX_VALUE
)