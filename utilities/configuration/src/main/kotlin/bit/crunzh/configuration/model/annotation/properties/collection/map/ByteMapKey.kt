package bit.crunzh.configuration.model.annotation.properties.collection.map

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ByteMapKey(
        val minValue: Byte = Byte.MIN_VALUE,
        val maxValue: Byte = Byte.MAX_VALUE
)