package bit.crunzh.configuration.model.annotation.properties

import kotlin.reflect.KClass

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ClassProperty(
    /**
     * Any additional types extending the type of the property, which may be assigned to the property.
     * If the subTypes is to be expanded with new types in the future, the property must be nullable, for handling forwards compatibility, otherwise it will not be possible to load a configuration with a future type.
     */
    val subTypes: Array<KClass<*>> = []
)