package bit.crunzh.configuration.model.annotation

annotation class Configuration(
        val configurationModelVersion: Int = 1
)
