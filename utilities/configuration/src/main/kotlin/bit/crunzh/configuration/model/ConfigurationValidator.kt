package bit.crunzh.configuration.model

import bit.crunzh.configuration.model.description.ValidationResult

interface ConfigurationValidator<T> {
        fun validate(obj: T): ValidationResult = ValidationResult(true, obj!!::class.simpleName!!)
}

