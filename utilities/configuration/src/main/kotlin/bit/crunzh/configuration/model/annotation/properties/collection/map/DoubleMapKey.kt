package bit.crunzh.configuration.model.annotation.properties.collection.map

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class DoubleMapKey (
        val minValue: Double = Double.MIN_VALUE,
        val maxValue: Double = Double.MAX_VALUE
)