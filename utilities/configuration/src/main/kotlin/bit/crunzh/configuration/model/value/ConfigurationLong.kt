package bit.crunzh.configuration.model.value

data class ConfigurationLong(val value: Long) : ConfigurationValue