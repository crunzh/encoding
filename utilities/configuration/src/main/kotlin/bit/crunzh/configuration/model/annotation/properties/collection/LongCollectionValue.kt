package bit.crunzh.configuration.model.annotation.properties.collection

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class LongCollectionValue(
        val minValue: Long = Long.MIN_VALUE,
        val maxValue: Long = Long.MAX_VALUE
)