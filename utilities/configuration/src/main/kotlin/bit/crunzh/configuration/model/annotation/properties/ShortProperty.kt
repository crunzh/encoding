package bit.crunzh.configuration.model.annotation.properties

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ShortProperty(
        val minValue: Short = Short.MIN_VALUE,
        val maxValue: Short = Short.MAX_VALUE
)