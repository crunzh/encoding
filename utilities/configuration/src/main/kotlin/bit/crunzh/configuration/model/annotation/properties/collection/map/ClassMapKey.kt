package bit.crunzh.configuration.model.annotation.properties.collection.map

import kotlin.reflect.KClass

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ClassMapKey (
        val topType: KClass<*>,
        val subTypes: Array<KClass<*>> = [],
)

