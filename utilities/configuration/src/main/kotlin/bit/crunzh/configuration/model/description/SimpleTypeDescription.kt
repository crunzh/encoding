package bit.crunzh.configuration.model.description

interface SimpleTypeDescription<T : Any> : TypeDescription<T> {
    fun valueToString(value: T): String
    fun stringToValue(string: String): T
}