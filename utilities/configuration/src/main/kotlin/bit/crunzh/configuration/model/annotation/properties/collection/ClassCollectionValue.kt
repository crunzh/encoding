package bit.crunzh.configuration.model.annotation.properties.collection

import kotlin.reflect.KClass

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ClassCollectionValue (
        val topType: KClass<*>,
        val subTypes: Array<KClass<*>> = []
)

