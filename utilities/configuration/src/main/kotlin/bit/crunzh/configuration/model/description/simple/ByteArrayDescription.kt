package bit.crunzh.configuration.model.description.simple

import bit.crunzh.configuration.model.description.SimpleTypeDescription
import bit.crunzh.configuration.model.description.ValidationResult
import java.util.*

class ByteArrayDescription : SimpleTypeDescription<ByteArray> {
    override val type = ByteArray::class

    override fun valueToString(value: ByteArray): String {
        return Base64.getEncoder().encodeToString(value)
    }

    override fun stringToValue(string: String): ByteArray {
        return Base64.getDecoder().decode(string)
    }

    override fun validate(propertyName: String, obj: ByteArray): ValidationResult {
        return ValidationResult(true, propertyName)
    }
}