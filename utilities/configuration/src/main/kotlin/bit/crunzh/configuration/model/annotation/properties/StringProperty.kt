package bit.crunzh.configuration.model.annotation.properties

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class StringProperty (
        /**
         * if regEx is the empty string, no regular expression will be instantiated to evaluate any value string.
         */
        val regEx: String = ""
)