package bit.crunzh.configuration.model.value

data class ConfigurationObject(
    val type: String,
    val typeVersion: String = "1",
    val properties: Map<String, ConfigurationValue?>
) : ConfigurationValue
