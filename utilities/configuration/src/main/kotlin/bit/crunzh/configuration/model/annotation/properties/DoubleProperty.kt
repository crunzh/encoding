package bit.crunzh.configuration.model.annotation.properties

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class DoubleProperty(
        val minValue: Double = Double.MIN_VALUE,
        val maxValue: Double = Double.MAX_VALUE
)