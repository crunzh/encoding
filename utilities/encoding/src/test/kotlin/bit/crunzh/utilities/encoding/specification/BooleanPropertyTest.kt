package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.collection.BooleanCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.IntCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.BooleanMapKey
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.IntMapKey
import bit.crunzh.utilities.encoding.specification.annotation.property.BooleanProperty
import bit.crunzh.utilities.encoding.specification.annotation.property.ListProperty
import bit.crunzh.utilities.encoding.specification.annotation.property.MapProperty
import org.junit.Test

class BooleanPropertyTest {
    @ClassEncoding
    data class RootClass(
            @BooleanProperty(seqNo = 1)
            override val prop: Boolean,
            @ListProperty(seqNo = 2)
            @BooleanCollectionValue
            override val listProp: List<Boolean>,
            @MapProperty(seqNo = 3)
            @BooleanMapKey
            @IntCollectionValue(maxValue = 10)
            override val mapPropKey: Map<Boolean, Int>,
            @MapProperty(seqNo = 4)
            @IntMapKey(maxValue = 10)
            @BooleanCollectionValue
            override val mapPropValue: Map<Int, Boolean>) : TestInterface<Boolean>

    @Test
    fun test() {
        //Arrange
        val prop = true
        val listProp = listOf(false, true)
        val keyMap = mapOf(Pair(true, 1), Pair(false, 2))
        val valueMap = mapOf(Pair(1, false), Pair(2, true))
        val expected = RootClass(prop, listProp, keyMap, valueMap)

        //Act
        testEncoding(expected, RootClass::class, true)
    }
}