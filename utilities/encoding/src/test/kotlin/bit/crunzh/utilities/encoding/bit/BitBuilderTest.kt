package bit.crunzh.utilities.encoding.bit

import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.nio.ByteBuffer
import java.nio.ByteOrder
import kotlin.test.assertTrue


internal class BitBuilderTest {
    var bitBuilder: BitBuilder = BitBuilder()

    @Before
    fun setUp() {
        bitBuilder = BitBuilder()
    }

    @Test
    fun testIntSigning() {
        val longBigEndian = ByteBuffer.allocate(8)
        longBigEndian.order(ByteOrder.BIG_ENDIAN).putLong(262143L)
        longBigEndian.rewind()
        val longLittleEndian = ByteBuffer.allocate(8)
        longLittleEndian.order(ByteOrder.LITTLE_ENDIAN).putLong(262143)
        longLittleEndian.rewind()
        val bigEndianRange = BitRange(longBigEndian.array()).printBinaryString()
        val littleEndianRange = BitRange(longLittleEndian.array()).printBinaryString()
        System.out.println(bigEndianRange + " " + littleEndianRange)
    }

    @Test
    fun appendBitRange() {
        //Arrange
        val value1 = 45.7
        val value2 = -12345
        val value3 = byteArrayOf(1, 2, 3, 4)
        val bitRange1 = BitBuilder(value1).toBitRange("First 1")
        val bitRange2 = BitBuilder(value2, 15, signed = true).toBitRange("Second 2")
        val bitRange3 = BitBuilder(value3).toBitRange("Third 3")

        //Act
        bitBuilder.append(bitRange1)
        bitBuilder.append(bitRange2)
        bitBuilder.append(bitRange3)

        //Assert
        val it = bitBuilder.iterator()
        assertEquals(111, it.size())
        assertEquals(111, it.remaining())
        assertEquals(value1, it.next(BitBuilder.doubleSizeBits).toDouble(), 0.00001)
        val value2Range = it.next(15)
        assertEquals(value2, value2Range.toInt(signed = true))
        assertArrayEquals(value3, it.next(4 * Byte.SIZE_BITS).toByteArray())
        assertEquals(0, it.remaining())
    }

    @Test
    fun testAppendBitBuilder() {
        //Arrange
        val bb1 = BitBuilder(true).append(43.2).append(1).append(-1, signed = true)

        //Act
        val bb2 = BitBuilder(54.2.toFloat()).append(bb1)

        //Assert
        val iterator = bb2.iterator()
        assertEquals(54.2.toFloat(), iterator.next(BitBuilder.floatSizeBits).toFloat())
        assertEquals(true, iterator.next(1).toBoolean())
        assertEquals(43.2, iterator.next(BitBuilder.doubleSizeBits).toDouble(), 0.00001)
        assertEquals(1, iterator.next(Int.SIZE_BITS).toInt())
        assertEquals(-1, iterator.next(Int.SIZE_BITS).toInt(signed = true))
        assertEquals(0, iterator.remaining())
    }

    @Test
    fun testAppendByteSigned() {
        //Arrange
        val expected: Byte = -128

        //Act
        val actual = BitBuilder(expected, signed = true).toBitRange().toByte(signed = true)

        //Assert
        assertEquals(expected, actual)
    }

    @Test
    fun testAppendByteUnsigned() {
        //Arrange
        val expected: Byte = 127

        //Act
        val actual = BitBuilder(expected, signed = false, fitIntoBits = 7).toBitRange().toByte(signed = false)

        //Assert
        assertEquals(expected, actual)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testAppendByteZeroSize() {
        //Arrange
        val expected: Byte = 127

        //Act
        BitBuilder(expected, fitIntoBits = 0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testAppendByteSignedTooLarge() {
        //Arrange
        val expected: Byte = 65

        //Act
        BitBuilder(expected, fitIntoBits = 7, signed = true)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testAppendByteUnsignedTooLarge() {
        //Arrange
        val expected: Byte = 65

        //Act
        BitBuilder(expected, fitIntoBits = 6, signed = false)
    }

    @Test
    fun testAppendShortBigEndian() {
        //Arrange
        val expected: Short = 10

        //Act
        val actualByteArr = BitBuilder(expected, byteOrder = ByteOrder.BIG_ENDIAN).toByteArray()
        val actual = ByteArrayBitIterator(actualByteArr).next(Short.SIZE_BITS).toShort(byteOrder = ByteOrder.BIG_ENDIAN)

        //Assert
        assertEquals(expected, actual)
        assertEquals(Short.SIZE_BYTES, actualByteArr.size)
        assertEquals(expected, ByteBuffer.wrap(actualByteArr).order(ByteOrder.BIG_ENDIAN).short)
    }

    @Test
    fun testAppendShortSmallEndian() {
        //Arrange
        val expected: Short = 10

        //Act
        val actualByteArr = BitBuilder(expected, byteOrder = ByteOrder.LITTLE_ENDIAN).toByteArray()
        val actual = ByteArrayBitIterator(actualByteArr).next(Short.SIZE_BITS).toShort(byteOrder = ByteOrder.LITTLE_ENDIAN)

        //Assert
        assertEquals(expected, actual)
        assertEquals(Short.SIZE_BYTES, actualByteArr.size)
        assertEquals(expected, ByteBuffer.wrap(actualByteArr).order(ByteOrder.LITTLE_ENDIAN).short)
    }

    @Test
    fun testAppendShortSigned() {
        //Arrange
        val expected: Short = Short.MIN_VALUE

        //Act
        val actual = BitBuilder(expected, signed = true).toBitRange().toShort(signed = true)

        //Assert
        assertEquals(expected, actual)
    }

    @Test
    fun testAppendShortUnsigned() {
        //Arrange
        val expected: Short = Short.MAX_VALUE

        //Act
        val actual = BitBuilder(expected, signed = false, fitIntoBits = Short.SIZE_BITS - 1).toBitRange().toShort(signed = false)

        //Assert
        assertEquals(expected, actual)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testAppendShortZeroSize() {
        //Arrange
        val expected: Short = 1

        //Act
        BitBuilder(expected, fitIntoBits = 0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testAppendShortSignedTooLarge() {
        //Arrange
        val expected: Short = 128

        //Act
        BitBuilder(expected, fitIntoBits = 8, signed = true)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testAppendShortUnsignedTooLarge() {
        //Arrange
        val expected: Short = 256

        //Act
        BitBuilder(expected, fitIntoBits = 8)
    }

    @Test
    fun testAppendIntBigEndian() {
        //Arrange
        val expected = 10

        //Act
        val actualByteArr = BitBuilder(expected, byteOrder = ByteOrder.BIG_ENDIAN).toByteArray()
        val actual = ByteArrayBitIterator(actualByteArr).next(Int.SIZE_BITS).toInt(byteOrder = ByteOrder.BIG_ENDIAN)

        //Assert
        assertEquals(expected, actual)
        assertEquals(Int.SIZE_BYTES, actualByteArr.size)
        assertEquals(expected, ByteBuffer.wrap(actualByteArr).order(ByteOrder.BIG_ENDIAN).int)
    }

    @Test
    fun testAppendIntSmallEndian() {
        //Arrange
        val expected = 10

        //Act
        val actualByteArr = BitBuilder(expected, byteOrder = ByteOrder.LITTLE_ENDIAN).toByteArray()
        val actual = ByteArrayBitIterator(actualByteArr).next(Int.SIZE_BITS).toInt(byteOrder = ByteOrder.LITTLE_ENDIAN)

        //Assert
        assertEquals(expected, actual)
        assertEquals(Int.SIZE_BYTES, actualByteArr.size)
        assertEquals(expected, ByteBuffer.wrap(actualByteArr).order(ByteOrder.LITTLE_ENDIAN).int)
    }

    @Test
    fun testAppendIntSigned() {
        //Act
        val actualMin = BitBuilder(Int.MIN_VALUE, signed = true).toBitRange().toInt(signed = true)
        val actualMax = BitBuilder(Int.MAX_VALUE, signed = true).toBitRange().toInt(signed = true)

        //Assert
        assertEquals(Int.MIN_VALUE, actualMin)
        assertEquals(Int.MAX_VALUE, actualMax)
    }

    @Test
    fun testAppendIntUnsigned() {
        //Arrange
        val expected = Int.MAX_VALUE

        //Act
        val actual = BitBuilder(expected, signed = false, fitIntoBits = Int.SIZE_BITS - 1).toBitRange().toInt(signed = false)

        //Assert
        assertEquals(expected, actual)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testAppendIntZeroSize() {
        //Arrange
        val expected = 1

        //Act
        BitBuilder(expected, fitIntoBits = 0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testAppendIntSignedTooLarge() {
        //Arrange
        val expected = (Int.MIN_VALUE / 2) - 1

        //Act
        BitBuilder(expected, fitIntoBits = Int.SIZE_BITS - 1, signed = true)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testAppendIntUnsignedTooLarge() {
        //Arrange
        val expected = (Int.MAX_VALUE / 2) + 1

        //Act
        BitBuilder(expected, fitIntoBits = Int.SIZE_BITS - 2, signed = false)
    }

    @Test
    fun testAppendLongBigEndian() {
        //Arrange
        val expected: Long = 10

        //Act
        val actualByteArr = BitBuilder(expected, byteOrder = ByteOrder.BIG_ENDIAN).toByteArray()
        val actual = ByteArrayBitIterator(actualByteArr).next(Long.SIZE_BITS).toLong(byteOrder = ByteOrder.BIG_ENDIAN)

        //Assert
        assertEquals(expected, actual)
        assertEquals(Long.SIZE_BYTES, actualByteArr.size)
        assertEquals(expected, ByteBuffer.wrap(actualByteArr).order(ByteOrder.BIG_ENDIAN).long)
    }

    @Test
    fun testAppendLongSmallEndian() {
        //Arrange
        val expected: Long = 10

        //Act
        val actualByteArr = BitBuilder(expected, byteOrder = ByteOrder.LITTLE_ENDIAN).toByteArray()
        val actual = ByteArrayBitIterator(actualByteArr).next(Long.SIZE_BITS).toLong(byteOrder = ByteOrder.LITTLE_ENDIAN)

        //Assert
        assertEquals(expected, actual)
        assertEquals(Long.SIZE_BYTES, actualByteArr.size)
        assertEquals(expected, ByteBuffer.wrap(actualByteArr).order(ByteOrder.LITTLE_ENDIAN).long)
    }

    @Test
    fun testAppendLongSigned() {
        //Act
        val actualMin = BitBuilder(Long.MIN_VALUE, signed = true).toBitRange().toLong(signed = true)
        val actualMax = BitBuilder(Long.MAX_VALUE, signed = true).toBitRange().toLong(signed = true)

        //Assert
        assertEquals(Long.MIN_VALUE, actualMin)
        assertEquals(Long.MAX_VALUE, actualMax)
    }

    @Test
    fun testAppendLongUnsigned() {
        //Arrange
        val expected = Long.MAX_VALUE

        //Act
        val actual = BitBuilder(expected, signed = false, fitIntoBits = Long.SIZE_BITS - 1).toBitRange().toLong(signed = false)

        //Assert
        assertEquals(expected, actual)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testAppendLongZeroSize() {
        //Arrange
        val expected: Long = 1

        //Act
        BitBuilder(expected, fitIntoBits = 0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testAppendLongSignedTooLarge() {
        //Arrange
        val expected = (Long.MIN_VALUE / 2) - 1

        //Act
        BitBuilder(expected, fitIntoBits = Long.SIZE_BITS - 1, signed = true)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testAppendLongUnsignedTooLarge() {
        //Arrange
        val expected = (Long.MAX_VALUE / 2) + 1

        //Act
        BitBuilder(expected, fitIntoBits = Long.SIZE_BITS - 2, signed = false)
    }

    @Test
    fun testAppendFloat() {
        //Act
        val actualMin = BitBuilder(Float.MIN_VALUE).toBitRange().toFloat()
        val actualMax = BitBuilder(Float.MAX_VALUE).toBitRange().toFloat()

        //Assert
        assertEquals(Float.MIN_VALUE, actualMin, 0.0.toFloat())
        assertEquals(Float.MAX_VALUE, actualMax, 0.0.toFloat())
    }

    @Test
    fun testAppendDouble() {
        //Act
        val actualMin = BitBuilder(Double.MIN_VALUE).toBitRange().toDouble()
        val actualMax = BitBuilder(Double.MAX_VALUE).toBitRange().toDouble()

        //Assert
        assertEquals(Double.MIN_VALUE, actualMin, 0.0)
        assertEquals(Double.MAX_VALUE, actualMax, 0.0)
    }

    @Test
    fun testAppendEmptyString() {
        //Arrange
        val value = ""

        //Act
        val encodedValue = BitBuilder(value).iterator()
        val decodedValue = BitRange.readString(encodedValue)

        //Assert
        assertEquals(value, decodedValue)
    }

    @Test
    fun testAppendString() {
        //Arrange
        val value = "Hello World!"

        //Act
        val encodedValue = BitBuilder(value).iterator()
        val decodedValue = BitRange.readString(encodedValue)

        //Assert
        assertEquals(value, decodedValue)
    }

    @Test
    fun testAppendLongString() {
        //Arrange
        var value = "Hello World! "
        for (i in 0..10) {
            value += value //Double up size 11 times = 13 characters x (2^11) = 26.624 characters
        }

        //Act
        val encodedValue = BitBuilder(value).iterator()
        val decodedValue = BitRange.readString(encodedValue)

        //Assert
        assertEquals(value, decodedValue)
    }

    @Test
    fun testAppendStringUtf16() {
        //Arrange
        val value = "Hello World!"

        //Act
        val encodedValue = BitBuilder(value, charsetToUseForEncode = Charsets.UTF_16).iterator()
        val decodedValue = BitRange.readString(encodedValue, charsetToUseForDecode = Charsets.UTF_16)

        //Assert
        assertEquals(value, decodedValue)
    }

    @Test
    fun testAppendStringSpecializedVarIntEncoder() {
        //Arrange
        val optimizeVarIntEncoder = VarIntEncoder(1, 4)
        val expectedShort = "A"
        val expectedLong = "0123456789ABCDE"

        //Act
        val actualShort = BitBuilder(expectedShort, varIntEncoder = optimizeVarIntEncoder).toBitRange()
        val actualLong = BitBuilder(expectedLong, varIntEncoder = optimizeVarIntEncoder).toBitRange()
        val referenceShort = BitBuilder(expectedShort).toBitRange()
        val referenceLong = BitBuilder(expectedLong).toBitRange()

        //Assert
        assertEquals(Byte.SIZE_BITS + 1 + 1, actualShort.size())
        assertEquals((expectedLong.length * Byte.SIZE_BITS) + 1 + 4, actualLong.size())
        assertTrue(actualShort.size() < referenceShort.size())
        assertTrue(actualLong.size() < referenceLong.size())
    }

    @Test
    fun testAppendEnum() {
        //Act
        val iterator = BitBuilder().append(TestEnum.TWO, TestEnum::class).iterator()
        val output = BitRange.readEnum(iterator, TestEnum::class)

        //Assert
        assertEquals(TestEnum.TWO, output)
    }

    enum class TestEnum(val intField: Int, val stringField: String) {
        ONE(3, "One"),
        TWO(1, "Two"),
        THREE(2, "Three")
    }

    @Test
    fun testAppendBoolean() {
        //Act
        val actual = BitBuilder(true).toBitRange().toBoolean()

        //Assert
        assertEquals(true, actual)
    }

    @Test
    fun testAppendByteArray() {
        //Arrange
        val expected = byteArrayOf(1, 2, 3, 4, 5, 6, 7)

        //Act
        val actual = BitBuilder(expected).toBitRange().toByteArray()

        //Assert
        assertArrayEquals(expected, actual)
    }

    @Test
    fun testAppendByteBuffer() {
        //Arrange
        val expected = ByteBuffer.allocate(89)
        expected.putDouble(43.5)
        expected.putInt(123)
        expected.rewind()

        //Act
        val actual = BitBuilder(expected).toBitRange().toByteBuffer()

        //Assert
        assertArrayEquals(expected.array(), actual.array())
    }

    @Test
    fun testAppendByteBufferPointerNotZero() {
        //Arrange
        val expected = ByteBuffer.allocate(89)
        expected.putDouble(43.5)
        expected.putInt(123)
        val previousPosition = expected.position()

        //Act
        val actual = BitBuilder(expected).toBitRange().toByteBuffer()

        //Assert
        expected.position(previousPosition)
        val expectedBytes = ByteArray(expected.remaining())
        expected.get(expectedBytes)
        assertArrayEquals(expectedBytes, actual.array())
    }

    @Test
    fun getAppendedRanges() {
        //Arrange
        val bb = BitBuilder(true, description = "First")
                .append(123, description = "Second")
                .append("Test", description = "Third")

        //Act
        val value = bb.getBitRanges()

        //Assert
        assertEquals(5, value.size)
        assertEquals("First", value[0].description)
        assertEquals("Second", value[1].description)
        assertEquals("Third - string length - VarInt - StepSize", value[2].description)
        assertEquals("Third - string length - VarInt - Value", value[3].description)
        assertEquals("Third - string payload", value[4].description)
    }

    @Test
    fun testToString() {
        //Arrange
        val bb = BitBuilder(true, "First").append(false, "Second")

        //Act
        val value = bb.toString()

        //Assert
        assertEquals("Size: '2' bits, '2' BitRanges, {First, 1 bits, '1'}, {Second, 1 bits, '0'}", value)
    }

    @Test
    fun bitsRequired() {
        //Act
        val bitsLongMin = BitBuilder.getBitsRequired(Long.MIN_VALUE, true)
        val bitsLongMax = BitBuilder.getBitsRequired(Long.MAX_VALUE, true)
        val bitsLongMaxUnsigned = BitBuilder.getBitsRequired(Long.MAX_VALUE, false)
        val bits4BitInt = BitBuilder.getBitsRequired(15, false)

        //Assert
        assertEquals(Long.SIZE_BITS, bitsLongMin)
        assertEquals(Long.SIZE_BITS, bitsLongMax)
        assertEquals(Long.SIZE_BITS - 1, bitsLongMaxUnsigned)
        assertEquals(4, bits4BitInt)
    }

    @Test
    fun moveSignBitAndCompress() {
        //Arrange
        val byteBufferBe = ByteBuffer.allocate(4)
        byteBufferBe.order(ByteOrder.BIG_ENDIAN).putInt(262143)
        byteBufferBe.rewind()
        val byteBufferLe = ByteBuffer.allocate(4)
        byteBufferLe.order(ByteOrder.LITTLE_ENDIAN).putInt(262143)
        byteBufferLe.rewind()
        val int18BitBe = byteBufferBe.array()
        val int18BitLe = byteBufferLe.array()

        //Act
        val resultSignedBe = BitBuilder.moveSignBitAndCompress(true, int18BitBe, 20, ByteOrder.BIG_ENDIAN)
        val resultSignedLe = BitBuilder.moveSignBitAndCompress(true, int18BitLe, 20, ByteOrder.LITTLE_ENDIAN)
        val resultUnsignedBe = BitBuilder.moveSignBitAndCompress(false, int18BitBe, 19, ByteOrder.BIG_ENDIAN)
        val resultUnsignedLe = BitBuilder.moveSignBitAndCompress(false, int18BitLe, 19, ByteOrder.LITTLE_ENDIAN)

        //Assert
        assertEquals("11001111 11111111 11110000", BitRange(resultSignedBe).printBinaryString())
        assertEquals("11111111 11111111 11000000", BitRange(resultSignedLe).printBinaryString())
        assertEquals("11011111 11111111 11100000", BitRange(resultUnsignedBe).printBinaryString())
        assertEquals("11111111 11111111 11000000", BitRange(resultUnsignedLe).printBinaryString())
    }

    @Test
    fun moveSignBitAndCompressNegative() {
        //Arrange
        val byteBufferBe = ByteBuffer.allocate(4)
        byteBufferBe.order(ByteOrder.BIG_ENDIAN).putInt(-262144)
        byteBufferBe.rewind()
        val byteBufferLe = ByteBuffer.allocate(4)
        byteBufferLe.order(ByteOrder.LITTLE_ENDIAN).putInt(-262144)
        byteBufferLe.rewind()
        val int18BitBe = byteBufferBe.array()
        val int18BitLe = byteBufferLe.array()

        //Act
        val resultSignedBe = BitBuilder.moveSignBitAndCompress(true, int18BitBe, 19, ByteOrder.BIG_ENDIAN)
        val resultSignedLe = BitBuilder.moveSignBitAndCompress(true, int18BitLe, 19, ByteOrder.LITTLE_ENDIAN)

        //Assert
        assertEquals("00100000 00000000 00000000", BitRange(resultSignedBe).printBinaryString())
        assertEquals("00000000 00000000 00100000", BitRange(resultSignedLe).printBinaryString())
    }

    @Test
    fun toByteArray() {
        //Arrange
        val builder = BitBuilder()
        builder.append(3, 2)
        builder.append(255, 8)

        //Act
        val encodedBa = builder.toByteArray()

        //Assert
        assertEquals("11111111 11000000", BitRange(encodedBa).printBinaryString())
    }
}