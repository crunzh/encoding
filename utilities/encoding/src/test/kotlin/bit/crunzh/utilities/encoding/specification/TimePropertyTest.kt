package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.collection.TimeEpochMsCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.IntCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.IntMapKey
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.LongMapKey
import bit.crunzh.utilities.encoding.specification.annotation.property.*
import org.junit.Test
import java.time.LocalDateTime
import java.time.ZoneOffset

class TimePropertyTest {
    @ClassEncoding
    data class RootClass(
            @TimeProperty(seqNo = 1)
            override val prop: Long,
            @ListProperty(seqNo = 2)
            @TimeEpochMsCollectionValue
            override val listProp: List<Long>,
            @MapProperty(seqNo = 3)
            @LongMapKey(maxValue = 100)
            @IntCollectionValue(maxValue = 10)
            override val mapPropKey: Map<Long, Int>,
            @MapProperty(seqNo = 4)
            @IntMapKey(maxValue = 10)
            @TimeEpochMsCollectionValue
            override val mapPropValue: Map<Int, Long>) : TestInterface<Long>

    @Test
    fun test() {
        //Arrange
        val prop = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)*1000
        val listProp = listOf(prop+1000, prop+2000)
        val keyMap = emptyMap<Long, Int>()
        val valueMap = mapOf(Pair(1, prop+5000), Pair(2, prop+6000))
        val expected = RootClass(prop, listProp, keyMap, valueMap)

        //Act
        testEncoding(expected = expected, type = RootClass::class, printEncoding = true)
    }
}