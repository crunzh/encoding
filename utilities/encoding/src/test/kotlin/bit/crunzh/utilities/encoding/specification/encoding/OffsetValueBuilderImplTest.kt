package bit.crunzh.utilities.encoding.specification.encoding

import bit.crunzh.utilities.encoding.position.LossyPositionCompression
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueBuilderImpl
import junit.framework.TestCase.assertEquals
import org.junit.Test

class OffsetValueBuilderImplTest {

    @Test
    fun offsetBuilder() {
        //Arrange
        val builder = OffsetValueBuilderImpl()
        val precisionInMeters = 1.0
        val latitude = 89.0
        val longitude = 179.0
        val position1 = LossyPositionCompression.removePrecision(LossyPositionCompression.UncompressedPosition(latitude+ 0.00001, longitude+ 0.00001, precisionInMeters))
        val position2 = LossyPositionCompression.removePrecision(LossyPositionCompression.UncompressedPosition(latitude+ 0.00002, longitude+ 0.00002, precisionInMeters))
        val position3 = LossyPositionCompression.removePrecision(LossyPositionCompression.UncompressedPosition(latitude+ 0.00003, longitude+ 0.00003, precisionInMeters))
        val position4 = LossyPositionCompression.removePrecision(LossyPositionCompression.UncompressedPosition(latitude+ 0.00004, longitude+ 0.00004, precisionInMeters))
        val position5 = LossyPositionCompression.removePrecision(LossyPositionCompression.UncompressedPosition(latitude+ 0.00005, longitude+ 0.00005, precisionInMeters))

        //Act
        builder.addLatitude(precisionInMeters, position1.latitude)
        builder.addLongitude(precisionInMeters, position1.longitude)
        builder.addLatitude(precisionInMeters, position2.latitude)
        builder.addLongitude(precisionInMeters, position2.longitude)
        builder.addLatitude(precisionInMeters, position3.latitude)
        builder.addLongitude(precisionInMeters, position3.longitude)
        builder.addLatitude(precisionInMeters, position4.latitude)
        builder.addLongitude(precisionInMeters, position4.longitude)
        builder.addLatitude(precisionInMeters, position5.latitude)
        builder.addLongitude(precisionInMeters, position5.longitude)
        val values = builder.build()

        //Assert
        val deltaLatitude1 = values.getDeltaLatitude(position1.latitude, precisionInMeters)
        val deltaLongitude1 = values.getDeltaLongitude(position1.longitude, precisionInMeters)
        val deltaLatitude2 = values.getDeltaLatitude(position2.latitude, precisionInMeters)
        val deltaLongitude2 = values.getDeltaLongitude(position2.longitude, precisionInMeters)
        val deltaLatitude3 = values.getDeltaLatitude(position3.latitude, precisionInMeters)
        val deltaLongitude3 = values.getDeltaLongitude(position3.longitude, precisionInMeters)
        val deltaLatitude4 = values.getDeltaLatitude(position4.latitude, precisionInMeters)
        val deltaLongitude4 = values.getDeltaLongitude(position4.longitude, precisionInMeters)
        val deltaLatitude5 = values.getDeltaLatitude(position5.latitude, precisionInMeters)
        val deltaLongitude5 = values.getDeltaLongitude(position5.longitude, precisionInMeters)
        assertEquals(position1.latitude, values.getLatitude(deltaLatitude1, precisionInMeters))
        assertEquals(position1.longitude, values.getLongitude(deltaLongitude1, precisionInMeters))
        assertEquals(position2.latitude, values.getLatitude(deltaLatitude2, precisionInMeters))
        assertEquals(position2.longitude, values.getLongitude(deltaLongitude2, precisionInMeters))
        assertEquals(position3.latitude, values.getLatitude(deltaLatitude3, precisionInMeters))
        assertEquals(position3.longitude, values.getLongitude(deltaLongitude3, precisionInMeters))
        assertEquals(position4.latitude, values.getLatitude(deltaLatitude4, precisionInMeters))
        assertEquals(position4.longitude, values.getLongitude(deltaLongitude4, precisionInMeters))
        assertEquals(position5.latitude, values.getLatitude(deltaLatitude5, precisionInMeters))
        assertEquals(position5.longitude, values.getLongitude(deltaLongitude5, precisionInMeters))
    }
}