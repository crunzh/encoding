package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.specification.EnumPropertyTest.TestEnum
import bit.crunzh.utilities.encoding.specification.ReflectionEncoderTest.*
import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.collection.*
import bit.crunzh.utilities.encoding.specification.annotation.property.SetProperty
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SetPropertyTest {
    @ClassEncoding
    data class SetsOfStuff(
            @SetProperty(seqNo = 1)
            @BooleanCollectionValue
            val setOfBoolean: Set<Boolean> = setOf(true, false),
            @SetProperty(seqNo = 2, setSizeVarInt = [2, 4, 6, 10])
            @ByteArrayCollectionValue
            val setOfByteArray: Set<ByteArray> = setOf(byteArrayOf(1, 2, 3), byteArrayOf(4, 5, 6)),
            @SetProperty(seqNo = 3)
            @ClassCollectionValue(topType = TestDataClass2Interface::class, subTypes = [TestDataClass2::class, TestDataClass5::class])
            val setOfClasses: Set<TestDataClass2Interface> = setOf(TestDataClass2(1, 2), TestDataClass5(1, 2, TestDataClass1(2, 1))),
            @SetProperty(seqNo = 4)
            @EnumCollectionValue(enumType = TestEnum::class)
            val setOfEnums: Set<TestEnum> = setOf(TestEnum.EIGHT, TestEnum.FOUR),
            @SetProperty(seqNo = 5)
            @LongCollectionValue
            val setOfLong: Set<Long> = setOf(1, 2, 3),
            @SetProperty(seqNo = 6)
            @VarIntCollectionValue
            val setOfVarInt: Set<Int> = setOf(4, 5, 6),
            @SetProperty(seqNo = 7)
            @StringCollectionValue
            val setOfStrings: Set<String> = setOf("Hello", "World"),
            @SetProperty(seqNo = 8)
            @DoubleCollectionValue(maxValue = 9000.0)
            val setOfDouble: Set<Double> = setOf(1.0, 2.0, 3.0),
            @SetProperty(seqNo = 9)
            @FloatCollectionValue(maxValue = 9000.0f)
            val setOfFloat: Set<Float> = setOf(4.0f, 5.0f, 6.0f)
    )

    @Test
    fun testSet() {
        //Arrange
        val encoder = EncoderFactory.createEncoder(SetsOfStuff::class)
        val expected = SetsOfStuff()

        //Act
        val encoded = encoder.encode(expected)
        val actual = encoder.decode(encoded.iterator(), expected::class)

        //Assert
        assertEquals(expected.setOfBoolean, actual.setOfBoolean)
        assertSetOfByteArrayEquals(expected.setOfByteArray, actual.setOfByteArray)
        assertEquals(expected.setOfClasses, actual.setOfClasses)
        assertEquals(expected.setOfDouble, actual.setOfDouble)
        assertEquals(expected.setOfEnums, actual.setOfEnums)
        assertEquals(expected.setOfFloat, actual.setOfFloat)
        assertEquals(expected.setOfLong, actual.setOfLong)
        assertEquals(expected.setOfStrings, actual.setOfStrings)
        assertEquals(expected.setOfVarInt, actual.setOfVarInt)
    }

    private fun assertSetOfByteArrayEquals(expected: Set<ByteArray>, actual: Set<ByteArray>) {
        assertEquals(expected.size, actual.size)
        for(expectedBa in expected) {
            var found = false
            for(actualBa in actual) {
                if(expectedBa.size != actualBa.size) {
                    continue
                }
                for(idx in expectedBa.indices) {
                    if(expectedBa[idx] != actualBa[idx]) {
                        break
                    }
                    found = true
                }
            }
            assertTrue(found)
        }
    }
}