package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.collection.ClassCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.IntCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.ClassMapKey
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.IntMapKey
import bit.crunzh.utilities.encoding.specification.annotation.property.*
import org.junit.Test
import kotlin.test.assertEquals

class ClassPropertyTest {
    @ClassEncoding
    data class DataClass(@IntProperty(seqNo = 1, maxValue = 10) val prop: Int = 1)

    @ClassEncoding
    data class RootClass(
            @ClassProperty(seqNo = 1)
            override val prop: DataClass,
            @ListProperty(seqNo = 2)
            @ClassCollectionValue(topType = DataClass::class)
            override val listProp: List<DataClass>,
            @MapProperty(seqNo = 3)
            @ClassMapKey(topType = DataClass::class)
            @IntCollectionValue(maxValue = 10)
            override val mapPropKey: Map<DataClass, Int>,
            @MapProperty(seqNo = 4)
            @IntMapKey(maxValue = 10)
            @ClassCollectionValue(topType = DataClass::class)
            override val mapPropValue: Map<Int, DataClass>) : TestInterface<DataClass>

    @Test
    fun testClass() {
        //Arrange
        val prop = DataClass(1)
        val listProp = listOf(DataClass(2), DataClass(3))
        val keyMap = mapOf(Pair(DataClass(4), 1), Pair(DataClass(5), 2))
        val valueMap = mapOf(Pair(1, DataClass(6)), Pair(2, DataClass(7)))
        val expected = RootClass(prop, listProp, keyMap, valueMap)

        //Act
        testEncoding(expected, RootClass::class, true)
    }

    @ClassEncoding
    data class ClassWithNonConstructorFields(
            @IntProperty(seqNo = 1)
            val prop1: Int = 1,
            @IntProperty(seqNo = 2)
            var prop2: Int //Assignable property without default value
    ) {
        @IgnoredProperty
        val prop3: Int = prop1 + 2 //Non assignable property.

        @IntProperty(seqNo = 3)
        var prop4: Int = prop1 + 3 //Assignable property with default value
    }

    @Test
    fun testVarAndNonAssignableProperties() {
        //Arrange
        val minimal = ClassWithNonConstructorFields(prop2 = 2)
        val max = ClassWithNonConstructorFields(prop1 = 11, prop2 = 22)
        max.prop4 = 44
        val encoder = EncoderFactory.createEncoder(ClassWithNonConstructorFields::class)

        //Act
        val encodedMinimal = encoder.encode(minimal)
        val actualMinimal = encoder.decode(encodedMinimal.iterator(), ClassWithNonConstructorFields::class)

        val encodedMax = encoder.encode(max)
        val actualMax = encoder.decode(encodedMax.iterator(), ClassWithNonConstructorFields::class)

        //Assert
        assertEquals(minimal, actualMinimal)
        assertEquals(max, actualMax)
    }

    fun assertEquals(expected: ClassWithNonConstructorFields, actual: ClassWithNonConstructorFields) {
        assertEquals(expected.prop1, actual.prop1)
        assertEquals(expected.prop2, actual.prop2)
        assertEquals(expected.prop3, actual.prop3)
        assertEquals(expected.prop4, actual.prop4)
    }

    @Test
    fun testExtensionProperties() {
        //Arrange
        val encoder = EncoderFactory.createEncoder(ExtensionProp::class)
        val obj = ExtensionProp(2)

        //Act
        val encoded = encoder.encode(obj)
        val decodedObj = encoder.decode(encoded.iterator(), ExtensionProp::class)

        //Assert
        assertEquals(obj.prop1, decodedObj.prop1)
        assertEquals(obj.extension, decodedObj.extension)

    }

    @ClassEncoding
    data class ExtensionProp(
            @IntProperty(seqNo = 1)
            val prop1: Int = 1
    )

    var ExtensionProp.extension: String
        get() = "Hello World"
        set(value) = throw IllegalStateException("Do not try to set extension properties.")
}