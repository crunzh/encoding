package bit.crunzh.utilities.encoding.position

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class LossyPositionCompressionTest {

    @Test
    fun compressPosition() {
        //Arrange
        val expectedPosition = LossyPositionCompression.UncompressedPosition(56.152022, 10.177306, 1.0)

        //Act
        val compressedPosition = LossyPositionCompression.compressPosition(expectedPosition)
        val actualPosition = LossyPositionCompression.decompressPosition(compressedPosition)
        val reCompressedPosition = LossyPositionCompression.decompressPosition(LossyPositionCompression.compressPosition(actualPosition))

        //Assert
        assertEquals(expectedPosition.latitude, actualPosition.latitude, 0.00002)
        assertEquals(expectedPosition.longitude, actualPosition.longitude, 0.00005)
        assertEquals(expectedPosition.precisionInMeters, actualPosition.precisionInMeters, 0.0)
        assertEquals(actualPosition.latitude, reCompressedPosition.latitude, 0.0)
        assertEquals(actualPosition.longitude, reCompressedPosition.longitude, 0.0)
        assertEquals(actualPosition.precisionInMeters, reCompressedPosition.precisionInMeters, 0.0)
    }

    @Test
    fun compressLongitude() {
        //Arrange
        val encoder = LossyPositionCompression()
        val precisionInMeters = 100.0

        //Act
        val compressedMaxMax = LossyPositionCompression.compressLongitude(90.0, 180.0, precisionInMeters)
        val compressedZeroZero = LossyPositionCompression.compressLongitude(0.0, 0.0, precisionInMeters)
        val compressedMinMin = LossyPositionCompression.compressLongitude(-90.0, -180.0, precisionInMeters)

        //Assert
        assertTrue(compressedMaxMax >= 0)
        assertTrue(compressedZeroZero >= 0)
        assertTrue(compressedMinMin >= 0)
    }

    @Test
    fun compressLatitude() {
        //Arrange
        val precisionInMeters = 100.0
        val precisionOnLatitude = 0.0002
        val max = 90.0
        val zero = 0.0
        val min = -90.0

        //Act
        val compressMax = LossyPositionCompression.compressLatitude(max, precisionInMeters)
        val deCompressedMax = LossyPositionCompression.decompressLatitude(compressMax, precisionInMeters)
        val compressZero = LossyPositionCompression.compressLatitude(zero, precisionInMeters)
        val deCompressedZero = LossyPositionCompression.decompressLatitude(compressZero, precisionInMeters)
        val compressMin = LossyPositionCompression.compressLatitude(min, precisionInMeters)
        val deCompressedMin = LossyPositionCompression.decompressLatitude(compressMin, precisionInMeters)

        //Assert
        assertTrue(compressMax >= 0)
        assertEquals(max, deCompressedMax, precisionOnLatitude)
        assertTrue(deCompressedMax >= -180.0 && deCompressedMax <= 180.0)

        assertTrue(compressZero >= 0)
        assertEquals(zero, deCompressedZero, precisionOnLatitude)
        assertTrue(deCompressedZero >= -180.0 && deCompressedZero <= 180.0)

        assertTrue(compressMin >= 0)
        assertEquals(min, deCompressedMin, precisionOnLatitude)
        assertTrue(deCompressedMin >= -180.0 && deCompressedMin <= 180.0)
    }
}