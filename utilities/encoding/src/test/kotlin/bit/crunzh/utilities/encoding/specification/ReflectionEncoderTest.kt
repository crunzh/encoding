package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.collection.ClassCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.property.*
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class ReflectionEncoderTest {
    class TestClass
    data class TestDataClass(val intProp: Int = 1) {}
    sealed class TestSealedClass
    abstract class TestAbstractClass
    open class TestOpenClass
    interface TestInterface

    interface TestDataClass1Interface {
        val intProp: Int?;
        val longProp: Long
    }

    interface TestDataClass2Interface {
        val intProp: Int
        val longProp: Long?
    }

    @ClassEncoding
    data class TestDataClass1(
            @VarIntProperty(seqNo = 1, varIntSpecification = [4, 8, 16, 32])
            override val intProp: Int? = null,
            @VarLongProperty(seqNo = 2, varIntSpecification = [8, 16, 32, 64])
            override var longProp: Long = 2) : TestDataClass1Interface

    @ClassEncoding
    data class TestDataClass2(
            @VarLongProperty(seqNo = 1, varIntSpecification = [4, 8, 16, 32])
            override val longProp: Long? = null,
            @VarIntProperty(seqNo = 2, varIntSpecification = [5, 10, 15, 20])
            override var intProp: Int = 2,
            @VarShortProperty(seqNo = 3)
            val shortProp: Short = 3) : TestDataClass2Interface

    @ClassEncoding
    data class TestDataClassComplexInterface(
            @VarIntProperty(seqNo = 1, varIntSpecification = [4, 8, 16, 32])
            val intProp: Int? = null,
            @ClassProperty(seqNo = 2, subTypes = [TestDataClass1::class])
            val otherClass: TestDataClass1Interface,
            @VarLongProperty(seqNo = 3, varIntSpecification = [8, 16, 32, 64])
            var longProp: Long = 2,
            @ClassCollectionValue(topType = TestDataClass2Interface::class, subTypes = [TestDataClass2::class, TestDataClass5::class])
            @ListProperty(seqNo = 4)
            val listOfTestData: List<TestDataClass2Interface>)

    @ClassEncoding
    data class TestDataClassListAndClassProperty(
            @VarIntProperty(seqNo = 1, varIntSpecification = [4, 8, 16, 32])
            val intProp: Int? = null,
            @ClassProperty(seqNo = 2)
            val otherClass: TestDataClass1,
            @VarLongProperty(seqNo = 3, varIntSpecification = [8, 16, 32, 64])
            var longProp: Long = 2,
            @ClassCollectionValue(topType = TestDataClass2::class)
            @ListProperty(seqNo = 4)
            val listOfTestData: List<TestDataClass2>)

    @ClassEncoding
    data class TestDataClass5(
            @VarLongProperty(seqNo = 1, varIntSpecification = [4, 8, 16, 32])
            override val longProp: Long? = null,
            @VarIntProperty(seqNo = 2, varIntSpecification = [5, 10, 15, 20])
            override var intProp: Int = 2,
            @ClassProperty(seqNo = 3, subTypes = [TestDataClass1::class])
            val otherClass: TestDataClass1Interface) : TestDataClass2Interface

    @ClassEncoding
    data class TestDataClassAnnotatedWrongStartPropertyIdx(
            @VarIntProperty(2, varIntSpecification = [4, 8, 16, 32])
            val intProp: Int? = null,
            @VarLongProperty(3, varIntSpecification = [8, 16, 32, 64])
            var longProp: Long = 2)

    @ClassEncoding
    data class TestDataClassAnnotatedGapIntPropertyIdx(
            @VarIntProperty(1, varIntSpecification = [4, 8, 16, 32])
            val intProp: Int? = null,
            @VarLongProperty(3, varIntSpecification = [8, 16, 32, 64])
            var longProp: Long = 2)

    @Before
    fun setUp() {
    }

    @Test(expected = IllegalArgumentException::class)
    fun initTestClass() {
        //Act
        EncoderFactory.createEncoder(TestClass::class)
    }

    @Test(expected = IllegalArgumentException::class)
    fun initDataClassNotAnnotated() {
        //Act
        EncoderFactory.createEncoder(TestDataClass::class)
    }

    @Test(expected = IllegalArgumentException::class)
    fun initSealedClass() {
        //Act
        EncoderFactory.createEncoder(TestSealedClass::class)
    }

    @Test(expected = IllegalArgumentException::class)
    fun initAbstractClass() {
        //Act
        EncoderFactory.createEncoder(TestAbstractClass::class)
    }

    @Test(expected = IllegalArgumentException::class)
    fun initOpenClass() {
        //Act
        EncoderFactory.createEncoder(TestOpenClass::class)
    }

    @Test(expected = IllegalArgumentException::class)
    fun initInterface() {
        //Act
        EncoderFactory.createEncoder(TestInterface::class)
    }

    @Test(expected = IllegalArgumentException::class)
    fun initWrongPropStartIdx() {
        //Act
        EncoderFactory.createEncoder(TestDataClassAnnotatedWrongStartPropertyIdx::class)
    }

    @Test(expected = IllegalArgumentException::class)
    fun initGapIngPropIdx() {
        //Act
        EncoderFactory.createEncoder(TestDataClassAnnotatedGapIntPropertyIdx::class)
    }

    @Test
    fun initDataClass() {
        //Arrange
        val expected = TestDataClass1(longProp = 3)

        //Act
        val encoder = EncoderFactory.createEncoder(TestDataClass1::class, printEncoding = true)
        val payload = encoder.encode(expected)
        val actual = encoder.decode(payload.iterator(), TestDataClass1::class)

        //Assert
        assertEquals(expected.intProp, actual.intProp)
        assertEquals(expected.longProp, actual.longProp)
    }

    @Test
    fun dataClassWithClassPropertyAndList() {
        //Arrange
        val expected = TestDataClassListAndClassProperty(intProp = 1, otherClass = TestDataClass1(2, 3), longProp = 4, listOfTestData = listOf(TestDataClass2(longProp = 5, intProp = 6), TestDataClass2(intProp = 7, longProp = 8), TestDataClass2(longProp = 9, intProp = 10)));

        //Act
        val encoder = EncoderFactory.createEncoder(TestDataClassListAndClassProperty::class, printEncoding = true)
        val payload = encoder.encode(expected)
        val actual = encoder.decode(payload.iterator(), TestDataClassListAndClassProperty::class)

        //Assert
        assertEquals(expected.intProp, actual.intProp)
        assertEquals(expected.longProp, actual.longProp)
    }

    @Test
    fun dataClassWithClassPropertyAndInterface() {
        //Arrange
        val expected = TestDataClassListAndClassProperty(intProp = 1, otherClass = TestDataClass1(2, 3), longProp = 4, listOfTestData = listOf(TestDataClass2(longProp = 5, intProp = 6), TestDataClass2(intProp = 7, longProp = 8), TestDataClass2(longProp = 9, intProp = 10)));

        //Act
        val encoder = EncoderFactory.createEncoder(TestDataClassListAndClassProperty::class, printEncoding = true)
        val payload = encoder.encode(expected)
        val actual = encoder.decode(payload.iterator(), TestDataClassListAndClassProperty::class)

        //Assert
        assertEquals(expected.intProp, actual.intProp)
        assertEquals(expected.longProp, actual.longProp)
    }

    @Test
    fun abstractClassAndListTypes() {
        //Arrange
        val expected = TestDataClassComplexInterface(
                intProp = 123,
                otherClass = TestDataClass1(42, 57),
                longProp = 456,
                listOfTestData = listOf(
                        TestDataClass5(longProp = null, intProp = 2, otherClass = TestDataClass1(intProp = 2, longProp = 3)),
                        TestDataClass5(longProp = 4, intProp = 2, otherClass = TestDataClass1(intProp = 4, longProp = 5)),
                        TestDataClass2(null, 3)
                )
        )
        val encoder = EncoderFactory.createEncoder(TestDataClassComplexInterface::class)

        //Act
        val payload = encoder.encode(expected)
        val actual = encoder.decode(payload.iterator(), TestDataClassComplexInterface::class)

        //Assert
        assertEquals(expected.intProp, actual.intProp)
        assertEquals(expected.longProp, actual.longProp)
        val expectedSubClass = expected.otherClass as TestDataClass1
        val actualSubClass = actual.otherClass as TestDataClass1
        assertEquals(expectedSubClass.intProp, actualSubClass.intProp)
        assertEquals(expectedSubClass.longProp, actualSubClass.longProp)
        assertEquals(expected.listOfTestData.size, actual.listOfTestData.size)

        expected.listOfTestData.forEach { expectedElement ->
            assertTrue(actual.listOfTestData.contains(expectedElement)) //We rely on Data Class equals generation here.
        }
    }

    @Test
    fun calculateSize() {
    }
}