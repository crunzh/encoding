package bit.crunzh.utilities.encoding.hash

import bit.crunzh.utilities.encoding.hash.HashAlgorithmFactory.HashType
import org.junit.Test

import org.junit.Assert.*
import org.junit.Ignore
import java.math.BigInteger
import java.nio.ByteBuffer
import java.nio.ByteOrder

private const val HEX_CHARS = "0123456789ABCDEF"

fun String.hexStringToByteArray(): ByteArray {
    val prunedString = this.toUpperCase().replace(" ", "")
    val result = ByteArray(prunedString.length / 2)

    for (i in prunedString.indices step 2) {
        val firstIndex = HEX_CHARS.indexOf(prunedString[i]);
        val secondIndex = HEX_CHARS.indexOf(prunedString[i + 1]);

        val octet = firstIndex.shl(4).or(secondIndex)
        result.set(i.shr(1), octet.toByte())
    }

    return result
}

@Suppress("LocalVariableName")
class HashAlgorithmFactoryTest {

    @Test
    fun diffAlgorithmsDiffResult() {
        //Arrange
        val input = "This is the value to hash".toByteArray()
        val outputs = ArrayList<ByteArray>()

        //Act
        outputs.add(HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1_32, input).getHash())
        outputs.add(HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1A_32, input).getHash())
        outputs.add(HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1_64, input).getHash())
        outputs.add(HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1A_64, input).getHash())
        outputs.add(HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1_128, input).getHash())
        outputs.add(HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1A_128, input).getHash())
        outputs.add(HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1_256, input).getHash())
        outputs.add(HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1A_256, input).getHash())
        outputs.add(HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1_512, input).getHash())
        outputs.add(HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1A_512, input).getHash())
        outputs.add(HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1_1024, input).getHash())
        outputs.add(HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1A_1024, input).getHash())

        //Assert
        val uniqueOutputs = HashSet<ByteArray>()
        outputs.forEach { output ->
            assertTrue(uniqueOutputs.add(output))
        }
    }

    @Test
    fun outputSize() {
        //Arrange
        val input = "This is the value to hash".toByteArray()

        //Act
        val size_1_32 = HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1_32, input).getHash().size
        val size_1a_32 = HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1A_32, input).getHash().size
        val size_1_64 = HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1_64, input).getHash().size
        val size_1a_64 = HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1A_64, input).getHash().size
        val size_1_128 = HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1_128, input).getHash().size
        val size_1a_128 = HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1A_128, input).getHash().size
        val size_1_256 = HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1_256, input).getHash().size
        val size_1a_256 = HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1A_256, input).getHash().size
        val size_1_512 = HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1_512, input).getHash().size
        val size_1a_512 = HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1A_512, input).getHash().size
        val size_1_1024 = HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1_1024, input).getHash().size
        val size_1a_1024 = HashAlgorithmFactory.createHashAlgorithm(HashType.FNV_1A_1024, input).getHash().size

        //Assert
        assertEquals(4, size_1_32)
        assertEquals(4, size_1a_32)
        assertEquals(8, size_1_64)
        assertEquals(8, size_1a_64)
        assertEquals(16, size_1_128)
        assertEquals(16, size_1a_128)
        assertEquals(32, size_1_256)
        assertEquals(32, size_1a_256)
        assertEquals(64, size_1_512)
        assertEquals(64, size_1a_512)
        assertEquals(128, size_1_1024)
        assertEquals(128, size_1a_1024)
    }

    @Test
    fun knownCollision_32_1() = assertZeroHash(HashType.FNV_1_32, "3d7A5K")

    @Test
    fun knownCollision_32_1_hex() = assertZeroHashHex(HashType.FNV_1_32, "01 47 6c 10 f3")

    @Test
    fun knownCollision_32_1A() = assertZeroHashHex(HashType.FNV_1A_32, "cc 24 31 c4")

    @Test
    fun knownCollision_64_1() = assertZeroHashHex(HashType.FNV_1_64, "92 06 77 4c e0 2f 89 2a d2")

    @Test
    fun knownCollision_64_1A() = assertZeroHashHex(HashType.FNV_1A_64, "d5 6b b9 53 42 87 08 36")

    @Test
    fun knownCollision_128_1() = assertZeroHashHex(HashType.FNV_1_128, "20 28 4e 43 40 55 6f 99 25 1b 89 f4 a8 18 ec 76 c0")

    @Test
    @Ignore
    fun knownCollision_128_1A() = assertZeroHashHex(HashType.FNV_1A_128, "3d7A5K")

    @Test
    @Ignore
    fun knownCollision_256_1() = assertZeroHashHex(HashType.FNV_1_256, "04 03 F4 7D 37 15 6D DB 3B 06 68 6F 07 0E 0E 3B 67 6E 43 53 0B 5F 5A 08 93 09 16 29 47 D9 4C 1B BD 7E 04 17 38")

    @Test
    @Ignore
    fun knownCollision_256_1A() = assertZeroHashHex(HashType.FNV_1A_256, "3d7A5K")

    @Test
    @Ignore
    fun knownCollision_512_1() = assertZeroHashHex(HashType.FNV_1_512, "3d7A5K")

    @Test
    @Ignore
    fun knownCollision_512_1A() = assertZeroHashHex(HashType.FNV_1A_512, "3d7A5K")

    @Test
    @Ignore
    fun knownCollision_1024_1() = assertZeroHashHex(HashType.FNV_1_1024, "3d7A5K")

    @Test
    @Ignore
    fun knownCollision_1024_1A() = assertZeroHashHex(HashType.FNV_1A_1024, "3d7A5K")

    fun assertZeroHash(type: HashType, strValue: String) = assertZeroHash(type, strValue.toByteArray(Charsets.US_ASCII))

    fun assertZeroHashHex(type: HashType, strValue: String) = assertZeroHash(type, strValue.hexStringToByteArray())

    fun assertZeroHash(type: HashType, input: ByteArray) {
        //Act
        val hashValueZero = HashAlgorithmFactory.createHashAlgorithm(type, input).getHash()

        //Assert
        hashValueZero.forEachIndexed { idx, byte ->
            assertEquals("Byte idx $idx did not have value 0", 0, byte.toInt())
        }
    }

    @Test
    fun collisionTest_1_32() {
        //Arrange
        val numberOfBytes = 2
        val uniqueHashes = HashMap<HashValue, HashParameters>()
        val collisions = ArrayList<Collision>()
        val type = HashType.FNV_1_32

        //Act
        findHashCollisions(numberOfBytes, type, uniqueHashes, collisions)

        //Assert
        assertCollisions(collisions, uniqueHashes, numberOfBytes, type)
    }

    @Test
    fun collisionTest_1a_32() {
        //Arrange
        val numberOfBytes = 2
        val uniqueHashes = HashMap<HashValue, HashParameters>()
        val collisions = ArrayList<Collision>()
        val type = HashType.FNV_1A_32

        //Act
        findHashCollisions(numberOfBytes, type, uniqueHashes, collisions)

        //Assert
        assertCollisions(collisions, uniqueHashes, numberOfBytes, type)
    }

    @Test
    fun collisionTest_1_64() {
        //Arrange
        val numberOfBytes = 2
        val uniqueHashes = HashMap<HashValue, HashParameters>()
        val collisions = ArrayList<Collision>()
        val type = HashType.FNV_1_64

        //Act
        findHashCollisions(numberOfBytes, type, uniqueHashes, collisions)

        //Assert
        assertCollisions(collisions, uniqueHashes, numberOfBytes, type)
    }

    @Test
    fun collisionTest_1A_64() {
        //Arrange
        val numberOfBytes = 2
        val uniqueHashes = HashMap<HashValue, HashParameters>()
        val collisions = ArrayList<Collision>()
        val type = HashType.FNV_1A_64

        //Act
        findHashCollisions(numberOfBytes, type, uniqueHashes, collisions)

        //Assert
        assertCollisions(collisions, uniqueHashes, numberOfBytes, type)
    }

    @Test
    fun collisionTest_1_128() {
        //Arrange
        val numberOfBytes = 2
        val uniqueHashes = HashMap<HashValue, HashParameters>()
        val collisions = ArrayList<Collision>()
        val type = HashType.FNV_1_128

        //Act
        findHashCollisions(numberOfBytes, type, uniqueHashes, collisions)

        //Assert
        assertCollisions(collisions, uniqueHashes, numberOfBytes, type)
    }

    @Test
    fun collisionTest_1A_128() {
        //Arrange
        val numberOfBytes = 2
        val uniqueHashes = HashMap<HashValue, HashParameters>()
        val collisions = ArrayList<Collision>()
        val type = HashType.FNV_1A_128

        //Act
        findHashCollisions(numberOfBytes, type, uniqueHashes, collisions)

        //Assert
        assertCollisions(collisions, uniqueHashes, numberOfBytes, type)
    }

    @Test
    fun collisionTest_1_256() {
        //Arrange
        val numberOfBytes = 2
        val uniqueHashes = HashMap<HashValue, HashParameters>()
        val collisions = ArrayList<Collision>()
        val type = HashType.FNV_1_256

        //Act
        findHashCollisions(numberOfBytes, type, uniqueHashes, collisions)

        //Assert
        assertCollisions(collisions, uniqueHashes, numberOfBytes, type)
    }

    @Test
    fun collisionTest_1A_256() {
        //Arrange
        val numberOfBytes = 2
        val uniqueHashes = HashMap<HashValue, HashParameters>()
        val collisions = ArrayList<Collision>()
        val type = HashType.FNV_1A_256

        //Act
        findHashCollisions(numberOfBytes, type, uniqueHashes, collisions)

        //Assert
        assertCollisions(collisions, uniqueHashes, numberOfBytes, type)
    }

    @Test
    fun collisionTest_1_512() {
        //Arrange
        val numberOfBytes = 2
        val uniqueHashes = HashMap<HashValue, HashParameters>()
        val collisions = ArrayList<Collision>()
        val type = HashType.FNV_1_512

        //Act
        findHashCollisions(numberOfBytes, type, uniqueHashes, collisions)

        //Assert
        assertCollisions(collisions, uniqueHashes, numberOfBytes, type)
    }

    @Test
    fun collisionTest_1A_512() {
        //Arrange
        val numberOfBytes = 2
        val uniqueHashes = HashMap<HashValue, HashParameters>()
        val collisions = ArrayList<Collision>()
        val type = HashType.FNV_1A_512

        //Act
        findHashCollisions(numberOfBytes, type, uniqueHashes, collisions)

        //Assert
        assertCollisions(collisions, uniqueHashes, numberOfBytes, type)
    }

    @Test
    fun collisionTest_1_1024() {
        //Arrange
        val numberOfBytes = 2
        val uniqueHashes = HashMap<HashValue, HashParameters>()
        val collisions = ArrayList<Collision>()
        val type = HashType.FNV_1_1024

        //Act
        findHashCollisions(numberOfBytes, type, uniqueHashes, collisions)

        //Assert
        assertCollisions(collisions, uniqueHashes, numberOfBytes, type)
    }

    @Test
    fun collisionTest_1A_1024() {
        //Arrange
        val numberOfBytes = 2
        val uniqueHashes = HashMap<HashValue, HashParameters>()
        val collisions = ArrayList<Collision>()
        val type = HashType.FNV_1A_1024

        //Act
        findHashCollisions(numberOfBytes, type, uniqueHashes, collisions)

        //Assert
        assertCollisions(collisions, uniqueHashes, numberOfBytes, type)
    }

    private fun assertCollisions(collisions: ArrayList<Collision>, uniqueHashes: HashMap<HashValue, HashParameters>, numberOfBytes: Int, type: HashType) {
        if (collisions.isNotEmpty()) {
            var collisionStr = "${collisions.size} collisions for type ${type.name}\r\n"
            collisions.forEach { collision ->
                collisionStr += "'${collision.existing}', collided with '${collision.colliding}'\r\n"
            }
            assertFalse(collisionStr, true)
        } else {
            println("Type ${type.name} has no collisions for all ${uniqueHashes.size} values in $numberOfBytes bytes.")
        }
    }

    private fun findHashCollisions(numberOfBytes: Int, type: HashType, uniqueHashes: HashMap<HashValue, HashParameters>, collisions: ArrayList<Collision>) {
        for (noBytes in 1..numberOfBytes) {
            val two = BigInteger.valueOf(2)
            val maxValue = two.pow(noBytes * Byte.SIZE_BITS).longValueExact()
            var valueToHash = 0L
            for (byteValue in 0 until maxValue) {
                val baToHash = createNewValue(valueToHash, noBytes)
                val hashValue = HashValue(HashAlgorithmFactory.createHashAlgorithm(type, baToHash).getHash())
                assertEquals("Expected output ba length ${type.outputSizeBytes}, but was ${hashValue.hashValue.size}", type.outputSizeBytes, hashValue.hashValue.size)
                val existing = uniqueHashes[hashValue]
                val hashParameters = HashParameters(valueToHash, baToHash, hashValue)
                if (existing != null) {
                    collisions.add(Collision(existing, hashParameters))
                } else {
                    uniqueHashes[hashValue] = hashParameters
                }

                valueToHash++
            }
        }
    }

    data class HashValue(val hashValue: ByteArray) {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as HashValue

            if (!hashValue.contentEquals(other.hashValue)) return false

            return true
        }

        override fun hashCode(): Int {
            return hashValue.contentHashCode()
        }
    }

    data class HashParameters(val numericValue: Long, val baValue: ByteArray, val hashValue: HashValue)
    data class Collision(val existing: HashParameters, val colliding: HashParameters)

    private fun createNewValue(valueToHash: Long, noBytes: Int): ByteArray {
        val bb = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN)
        bb.putLong(valueToHash)
        val ba = ByteArray(noBytes)
        bb.rewind()
        bb.get(ba)
        return ba
    }
}