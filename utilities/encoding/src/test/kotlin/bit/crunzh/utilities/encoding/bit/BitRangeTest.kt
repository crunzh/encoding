package bit.crunzh.utilities.encoding.bit

import org.junit.Assert.*
import org.junit.Test
import java.nio.ByteBuffer
import java.nio.ByteOrder

class BitRangeTest {

    @Test
    fun toBoolean() {
        //Arrange
        val oneEmptyByte = BitBuilder().append(0x00)
        val oneByteBit1 = BitBuilder().append(toAppend = 0x01, byteOrder = ByteOrder.LITTLE_ENDIAN)
        val oneByteBit2 = BitBuilder().append(0x02, byteOrder = ByteOrder.LITTLE_ENDIAN)
        val oneByteBit3 = BitBuilder().append(0x04, byteOrder = ByteOrder.LITTLE_ENDIAN)
        val oneByteBit4 = BitBuilder().append(0x08, byteOrder = ByteOrder.LITTLE_ENDIAN)
        val oneByteBit5 = BitBuilder().append(0x10, byteOrder = ByteOrder.LITTLE_ENDIAN)
        val oneByteBit6 = BitBuilder().append(0x20, byteOrder = ByteOrder.LITTLE_ENDIAN)
        val oneByteBit7 = BitBuilder().append(0x40, byteOrder = ByteOrder.LITTLE_ENDIAN)
        val oneByteBit8 = BitBuilder().append(toAppend = 0x80.toByte(), signed = true)
        val multiByte = BitBuilder().append(0x1000, byteOrder = ByteOrder.LITTLE_ENDIAN)
        val oneByteAllSet = BitBuilder().append(0xFF.toByte(), signed = true)

        //Assert
        assertNoBitIndexSet(oneEmptyByte)
        assertOnlyBitIndexSet(oneByteBit1, 0)
        assertOnlyBitIndexSet(oneByteBit2, 1)
        assertOnlyBitIndexSet(oneByteBit3, 2)
        assertOnlyBitIndexSet(oneByteBit4, 3)
        assertOnlyBitIndexSet(oneByteBit5, 4)
        assertOnlyBitIndexSet(oneByteBit6, 5)
        assertOnlyBitIndexSet(oneByteBit7, 6)
        assertOnlyBitIndexSet(oneByteBit8, 7)
        assertOnlyBitIndexSet(multiByte, 12)
        assertAllBitIndexSet(oneByteAllSet)
    }

    @Test
    fun testToString() {
        //Arrange
        val byteArray = byteArrayOf(0x01, 0x00, 0x10, 0xff.toByte())
        val bitRange = BitRange(byteArray)

        //Act
        val bitString = bitRange.printBinaryString()

        //Assert
        assertEquals("10000000 00000000 00001000 11111111", bitString)
    }

    @Test
    fun expandPositiveInteger() {
        //Arrange
        val byteBufferBe = ByteBuffer.allocate(4)
        byteBufferBe.order(ByteOrder.BIG_ENDIAN).putInt(262143)
        byteBufferBe.rewind()
        val byteBufferLe = ByteBuffer.allocate(4)
        byteBufferLe.order(ByteOrder.LITTLE_ENDIAN).putInt(262143)
        byteBufferLe.rewind()
        val int18BitBe = byteBufferBe.array()
        val int18BitLe = byteBufferLe.array()
        val signedBe = BitBuilder.moveSignBitAndCompress(true, int18BitBe, 19, ByteOrder.BIG_ENDIAN)
        val signedLe = BitBuilder.moveSignBitAndCompress(true, int18BitLe, 19, ByteOrder.LITTLE_ENDIAN)
        val unsignedBe = BitBuilder.moveSignBitAndCompress(false, int18BitBe, 19, ByteOrder.BIG_ENDIAN)
        val unsignedLe = BitBuilder.moveSignBitAndCompress(false, int18BitLe, 19, ByteOrder.LITTLE_ENDIAN)
        val brSignedBe = BitRange(signedBe, 0, 19)
        val brSignedLe = BitRange(signedLe, 0, 19)
        val brUnsignedBe = BitRange(unsignedBe, 0, 19)
        val brUnsignedLe = BitRange(unsignedLe, 0, 19)

        //Act
        val resultSignedBe = brSignedBe.expandToIntegerBytes(readAsSigned = true, allowRemainder = false, noOutputBytes = 4, byteOrderToRead = ByteOrder.BIG_ENDIAN)
        val resultSignedLe = brSignedLe.expandToIntegerBytes(readAsSigned = true, allowRemainder = false, noOutputBytes = 4, byteOrderToRead = ByteOrder.LITTLE_ENDIAN)
        val resultUnsignedBe = brUnsignedBe.expandToIntegerBytes(readAsSigned = false, allowRemainder = false, noOutputBytes = 4, byteOrderToRead = ByteOrder.BIG_ENDIAN)
        val resultUnsignedLe = brUnsignedLe.expandToIntegerBytes(readAsSigned = false, allowRemainder = false, noOutputBytes = 4, byteOrderToRead = ByteOrder.LITTLE_ENDIAN)

        //Assert
        assertEquals("00000000 11000000 11111111 11111111", BitRange(resultSignedBe).printBinaryString())
        assertEquals("11111111 11111111 11000000 00000000", BitRange(resultSignedLe).printBinaryString())
        assertEquals("00000000 11000000 11111111 11111111", BitRange(resultUnsignedBe).printBinaryString())
        assertEquals("11111111 11111111 11000000 00000000", BitRange(resultUnsignedLe).printBinaryString())
    }

    @Test
    fun expandNegativeInteger() {
        //Arrange
        val byteBufferBe = ByteBuffer.allocate(4)
        byteBufferBe.order(ByteOrder.BIG_ENDIAN).putInt(-262144)
        byteBufferBe.rewind()
        val byteBufferLe = ByteBuffer.allocate(4)
        byteBufferLe.order(ByteOrder.LITTLE_ENDIAN).putInt(-262144)
        byteBufferLe.rewind()
        val int18BitBe = byteBufferBe.array()
        val int18BitLe = byteBufferLe.array()
        val signedBe = BitBuilder.moveSignBitAndCompress(true, int18BitBe, 19, ByteOrder.BIG_ENDIAN)
        val signedLe = BitBuilder.moveSignBitAndCompress(true, int18BitLe, 19, ByteOrder.LITTLE_ENDIAN)
        val brSignedBe = BitRange(signedBe, 0, 19)
        val brSignedLe = BitRange(signedLe, 0, 19)

        //Act
        val resultSignedBe = brSignedBe.expandToIntegerBytes(readAsSigned = true, allowRemainder = false, noOutputBytes = 4, byteOrderToRead = ByteOrder.BIG_ENDIAN)
        val resultSignedLe = brSignedLe.expandToIntegerBytes(readAsSigned = true, allowRemainder = false, noOutputBytes = 4, byteOrderToRead = ByteOrder.LITTLE_ENDIAN)

        //Assert
        assertEquals("11111111 00111111 00000000 00000000", BitRange(resultSignedBe).printBinaryString())
        assertEquals("00000000 00000000 00111111 11111111", BitRange(resultSignedLe).printBinaryString())

    }

    private fun assertAllBitIndexSet(bitBuilder: BitBuilder) {
        bitBuilder.iterator().forEach { bit ->
            assertTrue(bit)
        }
    }

    private fun assertOnlyBitIndexSet(bitBuilder: BitBuilder, setBitIndex: Int) {
        var idx = 0
        bitBuilder.iterator().forEach { bit ->
            if (idx != setBitIndex) {
                assertFalse(bit)
            } else {
                assertTrue(bit)
            }
            idx++
        }
    }

    private fun assertNoBitIndexSet(oneEmptyByte: BitBuilder) {
        oneEmptyByte.iterator().forEach { bit ->
            assertFalse(bit)
        }
    }
}