package bit.crunzh.utilities.encoding.tlv

import org.junit.Test

import org.junit.Assert.*

class StringTlvEncoderTest {
    private val encoder = StringTlvEncoder()

    @Test
    fun encode() {
        //Arrange
        val expected = StringTlvTuple("TypeString 1", byteArrayOf(1, 2, 3, 4))

        //Act
        val encoded = encoder.encode(expected).iterator()
        val actual = encoder.decode(encoded)

        //Assert
        EnumTlvEncoderTest.assertTlvTupleEquals(expected, actual)
    }

    @Test
    fun calculateSize() {
        //Arrange
        val expected = StringTlvTuple("TypeString 1", byteArrayOf(1, 2, 3, 4))

        //Act
        val encodedSize = encoder.encode(expected).size()
        val calculatedSize = encoder.calculateSize(expected)

        //Assert
        assertEquals(encodedSize, calculatedSize)
    }
}