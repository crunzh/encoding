package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.collection.ByteArrayCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.IntMapKey
import bit.crunzh.utilities.encoding.specification.annotation.property.ByteStoreProperty
import bit.crunzh.utilities.encoding.specification.annotation.property.ListProperty
import bit.crunzh.utilities.encoding.specification.annotation.property.MapProperty
import org.junit.Assert.assertArrayEquals
import org.junit.Test
import kotlin.test.assertEquals

class ByteStorePropertyTest {
    @ClassEncoding
    data class RootClass(
            @ByteStoreProperty(seqNo = 1)
            val prop: ByteArray,
            @ListProperty(seqNo = 2)
            @ByteArrayCollectionValue
            val listProp: List<ByteArray>,
            @MapProperty(seqNo = 3)
            @IntMapKey(maxValue = 10)
            @ByteArrayCollectionValue
            val mapPropValue: Map<Int, ByteArray>) {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as RootClass

            if (!prop.contentEquals(other.prop)) return false
            if (listProp != other.listProp) return false
            if (mapPropValue != other.mapPropValue) return false

            return true
        }

        override fun hashCode(): Int {
            var result = prop.contentHashCode()
            result = 31 * result + listProp.hashCode()
            result = 31 * result + mapPropValue.hashCode()
            return result
        }
    }

    @Test
    fun test() {
        //Arrange
        val prop = byteArrayOf(1, 2, 3)
        val listProp = listOf(byteArrayOf(4, 5), byteArrayOf(6, 7))
        val valueMap = mapOf(Pair(1, byteArrayOf(14, 15)), Pair(2, byteArrayOf(16)))
        val expected = RootClass(prop, listProp, valueMap)
        val encoder = EncoderFactory.createEncoder(RootClass::class)

        //Act
        val encoded = encoder.encode(expected)
        val actual = encoder.decode(encoded.iterator(), expected::class)

        //Assert
        assertArrayEquals(expected.prop, actual.prop)
        assertListEquals(expected.listProp, actual.listProp)
        assertValueMapEquals(expected.mapPropValue, actual.mapPropValue)
    }

    private fun assertListEquals(expected: List<ByteArray>, actual: List<ByteArray>) {
        assertEquals(expected.size, actual.size)
        for (idx in expected.indices) {
            assertArrayEquals(expected[idx], actual[idx])
        }
    }

    private fun assertKeyMapEquals(expected: Map<ByteArray, Int>, actual: Map<ByteArray, Int>) {
        assertEquals(expected.size, actual.size)
        for (entry in expected.entries) {
            assertEquals(entry.value, actual[entry.key])
        }
    }

    private fun assertValueMapEquals(expected: Map<Int, ByteArray>, actual: Map<Int, ByteArray>) {
        assertEquals(expected.size, actual.size)
        for (entry in expected.entries) {
            assertArrayEquals(entry.value, actual[entry.key])
        }
    }
}