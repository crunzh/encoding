package bit.crunzh.utilities.encoding.bit

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class BitUtilTest {

    @Before
    fun setUp() {
    }

    @Test
    fun toByteArrayZeros() {
        //Arrange - 11111111 11111100 00000000 00011111 111
        val startIdx = 14
        val length = 13
        val bitRange = BitBuilder().append(0, length)
        val bitRangePadded = BitBuilder().append(16383, startIdx).append(bitRange).append(255, 8).toBitRange()

        //Act
        val output = BitUtil.toByteArray(bitRangePadded.bytes, startIdx, bitRange.size())
        val outputBitRange = BitRange(output, 0, bitRange.size())

        //Assert
        assertBitRangesEquals(bitRange.toBitRange(), outputBitRange)
    }

    @Test
    fun toByteArrayOnes() {
        //Arrange - 00000000 00000011 11111111 11100000 000
        val startIdx = 14
        val length = 13
        val bitRange = BitBuilder().append(8191, length) //All 1s
        val bitRangePadded = BitBuilder().append(0, startIdx).append(bitRange).append(0, 8).toBitRange()

        //Act
        val output = BitUtil.toByteArray(bitRangePadded.bytes, startIdx, bitRange.size())
        val outputBitRange = BitRange(output, 0, bitRange.size())

        //Assert
        assertBitRangesEquals(bitRange.toBitRange(), outputBitRange)
    }

    @Test
    fun getBit() {
        //Arrange - 11111111 11111100 00000000 00011111 111
        val startIdx = 14
        val length = 13
        val bitRange = BitBuilder().append(0, length)
        val bitRangePadded = BitBuilder().append(16383, startIdx).append(bitRange).append(255, 8).toBitRange()

        //Act
        val idx0 = BitUtil.getBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 0)
        val idx13 = BitUtil.getBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 13)
        val idx14 = BitUtil.getBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 14)
        val idx26 = BitUtil.getBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 26)
        val idx27 = BitUtil.getBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 27)
        val idx34 = BitUtil.getBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 34)

        //Assert
        assertTrue(idx0)
        assertTrue(idx13)
        assertFalse(idx14)
        assertFalse(idx26)
        assertTrue(idx27)
        assertTrue(idx34)
    }

    @Test
    fun setBit() {
        //Arrange - 11111111 11111100 00000000 00011111 111
        val startIdx = 14
        val length = 13
        val bitRange = BitBuilder().append(0, length)
        val bitRangePadded = BitBuilder().append(16383, startIdx).append(bitRange).append(255, 8).toBitRange()

        //Act - [1]1111111 11111[0][0]0 00000000 00[1][0]1111 11[1]
        BitUtil.setBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 0, true) //Keep as 1
        val idx0 = BitUtil.getBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 0)
        BitUtil.setBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 13, false) //Change to 0
        val idx13 = BitUtil.getBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 13)
        BitUtil.setBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 14, false) //Keep as 0
        val idx14 = BitUtil.getBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 14)
        BitUtil.setBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 26, true) //Change to 1
        val idx26 = BitUtil.getBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 26)
        BitUtil.setBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 27, false) //Change as 0
        val idx27 = BitUtil.getBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 27)
        BitUtil.setBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 34, true) //Keep as 1
        val idx34 = BitUtil.getBit(bitRangePadded.bytes, bitRangePadded.bitOffset, 34)

        //Assert
        assertTrue(idx0)
        assertFalse(idx13)
        assertFalse(idx14)
        assertTrue(idx26)
        assertFalse(idx27)
        assertTrue(idx34)
    }

    @Test
    fun setRangeByteAligned() {
        //Arrange
        val dstArray = byteArrayOf(0x01.toByte(), 0x02.toByte(), 0x03.toByte(), 0x04.toByte(), 0x05.toByte())
        val srcArray = byteArrayOf(0xf6.toByte(), 0xf7.toByte(), 0xf8.toByte(), 0xf9.toByte(), 0xfa.toByte(), 0xfb.toByte())
        val srcOffset = 2 * Byte.SIZE_BITS
        val dstOffset = 1 * Byte.SIZE_BITS
        val srcLength = 3 * Byte.SIZE_BITS

        //Act
        BitUtil.setBitRange(srcArray, srcOffset, srcLength, dstArray, dstOffset, false)

        //Assert
        assertEquals(0x01.toByte(), dstArray[0]) //Original dst[0]
        assertEquals(0xf8.toByte(), dstArray[1]) //Copied from src[2]
        assertEquals(0xf8.toByte(), srcArray[2])
        assertEquals(0xf9.toByte(), dstArray[2]) //Copied from src[3]
        assertEquals(0xf9.toByte(), srcArray[3])
        assertEquals(0xfa.toByte(), dstArray[3]) //Copied from src[4]
        assertEquals(0xfa.toByte(), srcArray[4])
        assertEquals(0x05.toByte(), dstArray[4]) //Original dst[4]
    }

    @Test
    fun setRangeByteAlignedEmptyDst() {
        //Arrange
        val dstArray = ByteArray(5)
        dstArray[0] = 1
        val srcArray = byteArrayOf(0xf6.toByte(), 0xf7.toByte(), 0xf8.toByte(), 0xf9.toByte(), 0xfa.toByte(), 0xfb.toByte())
        val srcOffset = 2 * Byte.SIZE_BITS
        val dstOffset = 1 * Byte.SIZE_BITS
        val srcLength = 3 * Byte.SIZE_BITS

        //Act
        BitUtil.setBitRange(srcArray, srcOffset, srcLength, dstArray, dstOffset, true)

        //Assert
        assertEquals(0x01.toByte(), dstArray[0]) //Original dst[0]
        assertEquals(0xf8.toByte(), dstArray[1]) //Copied from src[2]
        assertEquals(0xf8.toByte(), srcArray[2])
        assertEquals(0xf9.toByte(), dstArray[2]) //Copied from src[3]
        assertEquals(0xf9.toByte(), srcArray[3])
        assertEquals(0xfa.toByte(), dstArray[3]) //Copied from src[4]
        assertEquals(0xfa.toByte(), srcArray[4])
        assertEquals(0x00.toByte(), dstArray[4]) //Original dst[4]
    }

    @Test(expected = IllegalArgumentException::class)
    fun setRangeOutsideDst() {
        //Arrange
        val dstArray = ByteArray(5)
        val srcArray = ByteArray(6)
        val srcOffset = 0 * Byte.SIZE_BITS
        val dstOffset = 1 * Byte.SIZE_BITS
        val srcLength = (4 * Byte.SIZE_BITS) + 1

        //Act
        BitUtil.setBitRange(srcArray, srcOffset, srcLength, dstArray, dstOffset, false)
    }

    @Test
    fun setRangeExactlyInsideDst() {
        //Arrange
        val dstArray = ByteArray(5)
        val srcArray = ByteArray(6)
        val srcOffset = 0 * Byte.SIZE_BITS
        val dstOffset = 1 * Byte.SIZE_BITS
        val srcLength = (4 * Byte.SIZE_BITS)

        //Act
        BitUtil.setBitRange(srcArray, srcOffset, srcLength, dstArray, dstOffset, false)
    }

    @Test(expected = IllegalArgumentException::class)
    fun setRangeLargerThanSrc() {
        //Arrange
        val dstArray = ByteArray(5)
        val srcArray = ByteArray(4)
        val srcOffset = 0 * Byte.SIZE_BITS
        val dstOffset = 0 * Byte.SIZE_BITS
        val srcLength = (4 * Byte.SIZE_BITS) + 1

        //Act
        BitUtil.setBitRange(srcArray, srcOffset, srcLength, dstArray, dstOffset, false)
    }

    @Test
    fun setRangeExactlySrcSize() {
        //Arrange
        val dstArray = ByteArray(4)
        val srcArray = ByteArray(4)
        val srcOffset = 0 * Byte.SIZE_BITS
        val dstOffset = 0 * Byte.SIZE_BITS
        val srcLength = (4 * Byte.SIZE_BITS)

        //Act
        BitUtil.setBitRange(srcArray, srcOffset, srcLength, dstArray, dstOffset, false)
    }

    @Test
    fun setRangeBitAligned() {
        //Arrange
        //DST Array  [1111 1100, 0000 0000, 0001 1111, 1111 1111, 1111 1111]
        //SRC Array  [0000 0000, 0000 0011, 1111 1111, 1110 0000, 0000 0000]
        //DST Result [1111 11|11, 1111 1111, 111|1 1111, 1111 1111, 1111 1111]
        val dstArray = byteArrayOf(0x3f.toByte(), 0x00.toByte(), 0xf8.toByte(), 0xff.toByte(), 0xff.toByte())
        val dstArrayString = BitRange(dstArray, 0, dstArray.size * Byte.SIZE_BITS).printBinaryString()
        val srcArray = byteArrayOf(0x00.toByte(), 0xc0.toByte(), 0xff.toByte(), 0x07.toByte(), 0x00.toByte())
        val srcArrayString = BitRange(srcArray, 0, srcArray.size * Byte.SIZE_BITS).printBinaryString()
        val srcOffset = 14
        val dstOffset = 6
        val srcLength = 13

        //Act
        BitUtil.setBitRange(srcArray, srcOffset, srcLength, dstArray, dstOffset, false)

        //Assert
        assertEquals("11111100 00000000 00011111 11111111 11111111", dstArrayString)
        assertEquals("00000000 00000011 11111111 11100000 00000000", srcArrayString)

        assertEquals(0xff.toByte(), dstArray[0])
        assertEquals(0xff.toByte(), dstArray[1])
        assertEquals(0xff.toByte(), dstArray[2])
        assertEquals(0xff.toByte(), dstArray[3])
        assertEquals(0xff.toByte(), dstArray[4])

        assertEquals(0x00.toByte(), srcArray[0])
        assertEquals(0xc0.toByte(), srcArray[1])
        assertEquals(0xff.toByte(), srcArray[2])
        assertEquals(0x07.toByte(), srcArray[3])
        assertEquals(0x00.toByte(), srcArray[4])
    }

    @Test
    fun setRangeSrcHigher() {
        //Arrange
        //DST Array  [1100 0000, 0000 0000, 0001 1111, 1111 1111, 1111 1111]
        //SRC Array  [0000 0000, 0000 0011, 1111 1111, 1111 1110, 0000 0000]
        //DST Result [11|11 1111, 1111 1111, 111|1 1111, 1111 1111, 1111 1111]
        val dstArray = byteArrayOf(0x03.toByte(), 0x00.toByte(), 0xf8.toByte(), 0xff.toByte(), 0xff.toByte())
        val dstArrayString = BitRange(dstArray, 0, dstArray.size * Byte.SIZE_BITS).printBinaryString()
        val srcArray = byteArrayOf(0x00.toByte(), 0xc0.toByte(), 0xff.toByte(), 0x7f.toByte(), 0x00.toByte())
        val srcArrayString = BitRange(srcArray, 0, srcArray.size * Byte.SIZE_BITS).printBinaryString()
        val srcOffset = 14
        val dstOffset = 2
        val srcLength = 17

        //Act
        BitUtil.setBitRange(srcArray, srcOffset, srcLength, dstArray, dstOffset, false)

        //Assert
        assertEquals("11000000 00000000 00011111 11111111 11111111", dstArrayString)
        assertEquals("00000000 00000011 11111111 11111110 00000000", srcArrayString)

        assertEquals(0xff.toByte(), dstArray[0])
        assertEquals(0xff.toByte(), dstArray[1])
        assertEquals(0xff.toByte(), dstArray[2])
        assertEquals(0xff.toByte(), dstArray[3])
        assertEquals(0xff.toByte(), dstArray[4])

        assertEquals(0x00.toByte(), srcArray[0])
        assertEquals(0xc0.toByte(), srcArray[1])
        assertEquals(0xff.toByte(), srcArray[2])
        assertEquals(0x7f.toByte(), srcArray[3])
        assertEquals(0x00.toByte(), srcArray[4])
    }

    @Test
    fun setRangeDstHigher() {
        //Arrange
        //DST Array  [1111 1110, 0000 0000, 0001 1111, 1111 1111, 1111 1111]
        //SRC Array  [0000 0000, 0000 0011, 1111 1111, 1100 0000, 0000 0000]
        //DST Result [1111 111|1, 1111 1111, 111|1 1111, 1111 1111, 1111 1111]
        val dstArray = byteArrayOf(0x7f.toByte(), 0x00.toByte(), 0xf8.toByte(), 0xff.toByte(), 0xff.toByte())
        val dstArrayString = BitRange(dstArray, 0, dstArray.size * Byte.SIZE_BITS).printBinaryString()
        val srcArray = byteArrayOf(0x00.toByte(), 0xc0.toByte(), 0xff.toByte(), 0x03.toByte(), 0x00.toByte())
        val srcArrayString = BitRange(srcArray, 0, srcArray.size * Byte.SIZE_BITS).printBinaryString()
        val srcOffset = 14
        val dstOffset = 7
        val srcLength = 12

        //Act
        BitUtil.setBitRange(srcArray, srcOffset, srcLength, dstArray, dstOffset, false)

        //Assert
        assertEquals("11111110 00000000 00011111 11111111 11111111", dstArrayString)
        assertEquals("00000000 00000011 11111111 11000000 00000000", srcArrayString)

        assertEquals(0xff.toByte(), dstArray[0])
        assertEquals(0xff.toByte(), dstArray[1])
        assertEquals(0xff.toByte(), dstArray[2])
        assertEquals(0xff.toByte(), dstArray[3])
        assertEquals(0xff.toByte(), dstArray[4])

        assertEquals(0x00.toByte(), srcArray[0])
        assertEquals(0xc0.toByte(), srcArray[1])
        assertEquals(0xff.toByte(), srcArray[2])
        assertEquals(0x03.toByte(), srcArray[3])
        assertEquals(0x00.toByte(), srcArray[4])
    }

    @Test
    fun testRangeSrcByteAligned() {
        //Arrange
        //DST Array  [1000 0000, 0000 0000, 0001 1111, 1111 1111, 1111 1111]
        //SRC Array  [0000 0000, 1111 1111, 1111 1111, 1100 0000, 0000 0000]
        //DST Result [1111 111|1, 1111 1111, 111|1 1111, 1111 1111, 1111 1111]
        val dstArray = byteArrayOf(0x01.toByte(), 0x00.toByte(), 0xf8.toByte(), 0xff.toByte(), 0xff.toByte())
        val dstArrayString = BitRange(dstArray, 0, dstArray.size * Byte.SIZE_BITS).printBinaryString()
        val srcArray = byteArrayOf(0x00.toByte(), 0xff.toByte(), 0xff.toByte(), 0x03.toByte(), 0x00.toByte())
        val srcArrayString = BitRange(srcArray, 0, srcArray.size * Byte.SIZE_BITS).printBinaryString()
        val srcOffset = 8
        val dstOffset = 1
        val srcLength = 18

        //Act
        BitUtil.setBitRange(srcArray, srcOffset, srcLength, dstArray, dstOffset, false)

        //Assert
        assertEquals("10000000 00000000 00011111 11111111 11111111", dstArrayString)
        assertEquals("00000000 11111111 11111111 11000000 00000000", srcArrayString)
        assertEquals(0xff.toByte(), dstArray[0])
        assertEquals(0xff.toByte(), dstArray[1])
        assertEquals(0xff.toByte(), dstArray[2])
        assertEquals(0xff.toByte(), dstArray[3])
        assertEquals(0xff.toByte(), dstArray[4])

        assertEquals(0x00.toByte(), srcArray[0])
        assertEquals(0xff.toByte(), srcArray[1])
        assertEquals(0xff.toByte(), srcArray[2])
        assertEquals(0x03.toByte(), srcArray[3])
        assertEquals(0x00.toByte(), srcArray[4])
    }

    @Test
    fun testRangeDstByteAligned() {
        //Arrange
        //DST Array  [1111 1111, 0000 0000, 0001 1111, 1111 1111, 1111 1111]
        //SRC Array  [0000 0000, 0000 0001, 1111 1111, 1100 0000, 0000 0000]
        //DST Result [1111 111|1, 1111 1111, 111|1 1111, 1111 1111, 1111 1111]
        val dstArray = byteArrayOf(0xff.toByte(), 0x00.toByte(), 0xf8.toByte(), 0xff.toByte(), 0xff.toByte())
        val dstArrayString = BitRange(dstArray, 0, dstArray.size * Byte.SIZE_BITS).printBinaryString()
        val srcArray = byteArrayOf(0x00.toByte(), 0x80.toByte(), 0xff.toByte(), 0x03.toByte(), 0x00.toByte())
        val srcArrayString = BitRange(srcArray, 0, srcArray.size * Byte.SIZE_BITS).printBinaryString()
        val srcOffset = 15
        val dstOffset = 8
        val srcLength = 11

        //Act
        BitUtil.setBitRange(srcArray, srcOffset, srcLength, dstArray, dstOffset, false)

        //Assert
        assertEquals("11111111 00000000 00011111 11111111 11111111", dstArrayString)
        assertEquals("00000000 00000001 11111111 11000000 00000000", srcArrayString)
        assertEquals(0xff.toByte(), dstArray[0])
        assertEquals(0xff.toByte(), dstArray[1])
        assertEquals(0xff.toByte(), dstArray[2])
        assertEquals(0xff.toByte(), dstArray[3])
        assertEquals(0xff.toByte(), dstArray[4])

        assertEquals(0x00.toByte(), srcArray[0])
        assertEquals(0x80.toByte(), srcArray[1])
        assertEquals(0xff.toByte(), srcArray[2])
        assertEquals(0x03.toByte(), srcArray[3])
        assertEquals(0x00.toByte(), srcArray[4])
    }

    private fun assertBitRangesEquals(expected: BitRange, actual: BitRange) {
        assertEquals(expected.size(), actual.size())
        val expectedIt = expected.iterator()
        val actualIt = actual.iterator()
        while (expectedIt.hasNext()) {
            assertEquals("expected '$expected', actual '$actual'", expectedIt.next(), actualIt.next())
        }
        assertFalse(actualIt.hasNext())
    }
}

