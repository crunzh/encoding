package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.collection.IntCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.StringCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.IntMapKey
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.StringMapKey
import bit.crunzh.utilities.encoding.specification.annotation.property.ListProperty
import bit.crunzh.utilities.encoding.specification.annotation.property.MapProperty
import bit.crunzh.utilities.encoding.specification.annotation.property.StringProperty
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class StringPropertyTest {
    @ClassEncoding
    data class RootClass(
            @StringProperty(seqNo = 1)
            override val prop: String = "Test1",
            @ListProperty(seqNo = 2)
            @StringCollectionValue(templateValues = ["TEST2", "Test4"], templateIgnoreCase = true)
            override val listProp: List<String>,
            @MapProperty(seqNo = 3)
            @StringMapKey
            @IntCollectionValue
            override val mapPropKey: Map<String, Int>,
            @MapProperty(seqNo = 4)
            @IntMapKey
            @StringCollectionValue
            override val mapPropValue: Map<Int, String>) : TestInterface<String>

    @Test
    fun test() {
        //Arrange
        val prop = "Test1"
        val listProp = listOf("Test2", "Test3")
        val keyMap = mapOf(Pair("Test4", 1), Pair("Test5", 2))
        val valueMap = mapOf(Pair(1, "Test6"), Pair(2, "Test7"))
        val expected = RootClass(prop, listProp, keyMap, valueMap)
        val encoder = EncoderFactory.createEncoder(RootClass::class, printEncoding = true)

        //Act
        val encoded = encoder.encode(expected)
        val actual = encoder.decode(encoded.iterator(), expected::class)

        //Assert
        assertEquals(expected.prop, actual.prop)
        assertEquals(expected.listProp.size, actual.listProp.size)
        assertEquals(expected.listProp[0].toUpperCase(), actual.listProp[0].toUpperCase())
        assertEquals(expected.listProp[1].toUpperCase(), actual.listProp[1].toUpperCase())
        assertKeyMapEquals(expected.mapPropKey, actual.mapPropKey)
        assertValueMapEquals(expected.mapPropValue, actual.mapPropValue)
    }

    @Test
    fun charset() {
        //Arrange
        @ClassEncoding
        data class AsciiCharSet(
                @StringProperty(1, charset = "US-ASCII")
                val stringProp: String
        )
        @ClassEncoding
        data class Iso88591CharSet(
                @StringProperty(1, charset = "ISO-8859-1")
                val stringProp: String
        )
        val expectedString = "Expected String æøå"
        val asciiObj = AsciiCharSet(expectedString)
        val iso8859Obj = Iso88591CharSet(expectedString)
        val encoderAscii = EncoderFactory.createEncoder(AsciiCharSet::class)
        val encoderIso8859 = EncoderFactory.createEncoder(Iso88591CharSet::class)

        //Act
        val actualAsciiIso = encoderIso8859.decode(encoderAscii.encode(asciiObj).iterator(), Iso88591CharSet::class)
        val actualAsciiAscii = encoderAscii.decode(encoderAscii.encode(asciiObj).iterator(), AsciiCharSet::class)
        val actualIsoAscii = encoderAscii.decode(encoderIso8859.encode(iso8859Obj).iterator(), AsciiCharSet::class)
        val actualIsoIso = encoderIso8859.decode(encoderIso8859.encode(iso8859Obj).iterator(), Iso88591CharSet::class)

        //Assert
        assertNotEquals(expectedString, actualAsciiIso.stringProp)
        assertNotEquals(expectedString, actualAsciiAscii.stringProp) //Ascii cant carry the æøå characters.
        assertNotEquals(expectedString, actualIsoAscii.stringProp)
        assertEquals(expectedString, actualIsoIso.stringProp)
    }

    @Test(expected = IllegalArgumentException::class)
    fun regExValidationFail() {
        //Arrange
        @ClassEncoding
        data class RegExClass(
                @StringProperty(1, regEx = "\\d\\d.*")
                val stringProp: String
        )
        val encoder = EncoderFactory.createEncoder(RegExClass::class)
        val invalidObj = RegExClass("No Starting Digits")

        //Act
        encoder.encode(invalidObj)
    }

    @Test
    fun regExValidation() {
        //Arrange
        @ClassEncoding
        data class RegExClass(
                @StringProperty(1, regEx = "\\d\\d.*")
                val stringProp: String
        )
        val encoder = EncoderFactory.createEncoder(RegExClass::class)
        val validObj = RegExClass("02 starting digits")

        //Act
        val decodedValue = encoder.decode(encoder.encode(validObj).iterator(), RegExClass::class)

        //Assert
        assertEquals(validObj.stringProp, decodedValue.stringProp)
    }
}