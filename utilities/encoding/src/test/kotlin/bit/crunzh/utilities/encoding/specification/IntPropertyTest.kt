package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.collection.IntCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.IntMapKey
import bit.crunzh.utilities.encoding.specification.annotation.property.*
import org.junit.Assert
import org.junit.Test

class IntPropertyTest {
    @ClassEncoding
    data class TestDataClass1(
            @IntProperty(seqNo = 1, maxValue = 10)
            val prop1: Int? = null,
            @IntProperty(seqNo = 2, minValue = -10, maxValue = 10)
            val prop2: Int,
            @ByteProperty(seqNo = 3, minValue = -10, maxValue = 10)
            val prop3: Byte,
            @ShortProperty(seqNo = 4, minValue = Short.MIN_VALUE, maxValue = 0)
            val prop4: Short,
            @LongProperty(seqNo = 5, minValue = 7, maxValue = 10)
            val prop5: Long,
            @LongProperty(seqNo = 6, minValue = Long.MIN_VALUE, maxValue = Long.MAX_VALUE)
            val prop6: Long)

    @Test
    fun testAllTypesOfFields() {
        //Arrange
        val expected = TestDataClass1(prop1 = null, prop2 = -10, prop3 = 10, prop4 = Short.MIN_VALUE, prop5 = 8, prop6 = Long.MAX_VALUE)

        //Act
        val encoder = EncoderFactory.createEncoder(TestDataClass1::class, printEncoding = true)
        val payload = encoder.encode(expected)
        val actual = encoder.decode(payload.iterator(), TestDataClass1::class)

        //Assert
        Assert.assertEquals(expected.prop1, actual.prop1)
        Assert.assertEquals(expected.prop2, actual.prop2)
        Assert.assertEquals(expected.prop3, actual.prop3)
        Assert.assertEquals(expected.prop4, actual.prop4)
        Assert.assertEquals(expected.prop5, actual.prop5)
        Assert.assertEquals(expected.prop6, actual.prop6)
    }

    @ClassEncoding
    data class RootClass(
            @IntProperty(seqNo = 1, maxValue = 10)
            override val prop: Int = 1,
            @ListProperty(seqNo = 2)
            @IntCollectionValue(maxValue = 10)
            override val listProp: List<Int>,
            @MapProperty(seqNo = 3)
            @IntMapKey(maxValue = 100)
            @IntCollectionValue(maxValue = 10)
            override val mapPropKey: Map<Int, Int>,
            @MapProperty(seqNo = 4)
            @IntMapKey(maxValue = 10)
            @IntCollectionValue(maxValue = 100)
            override val mapPropValue: Map<Int, Int>) : TestInterface<Int>

    @Test
    fun test() {
        //Arrange
        val prop = 1
        val listProp = listOf(2, 3)
        val keyMap = mapOf(Pair(4, 1), Pair(5, 2))
        val valueMap = mapOf(Pair(1, 6), Pair(2, 7))
        val expected = RootClass(prop, listProp, keyMap, valueMap)

        //Act
        testEncoding(expected, RootClass::class, false)
    }
}