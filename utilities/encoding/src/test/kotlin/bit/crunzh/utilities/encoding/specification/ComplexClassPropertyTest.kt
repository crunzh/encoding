package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.collection.ClassCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.position.PositionClass
import bit.crunzh.utilities.encoding.specification.annotation.property.*
import com.google.gson.Gson
import bit.crunzh.utilities.encoding.specification.core.*
import bit.crunzh.utilities.encoding.specification.core.encoder.*
import bit.crunzh.utilities.encoding.varint.VarIntSpecification
import org.junit.Test
import kotlin.random.Random
import kotlin.reflect.KClass
import kotlin.system.measureNanoTime
import kotlin.system.measureTimeMillis

class ComplexClassPropertyTest {
    private val random = Random(1234)
    private val epochTimeOffset = 1591767777000
    private val latitudeOffset = 56.384855
    private val longitudeOffset = 10.244407

    enum class BandwidthClass(bandwidth: Long) {
        Gbit2(2_147_483_648),
        Gbit1(1_073_741_824),
        Mbit536(536_870_912),
        Mbit268(268_435_456),
        Mbit134(134_217_728),
        Mbit67(67_108_864),
        Mbit33(33_554_432),
        Mbit16(16_777_216),
        Mbit8(8_388_608),
        Mbit4(4_194_304),
        Mbit2(2_097_182),
        Mbit1(1_048_576),
        Kbit524(524_288),
        Kbit262(262_144),
        Kbit131(131_072),
        Kbit65(65_536),
        Kbit32(32_768),
        Kbit16(16_384),
        Kbit8(8_192),
        Kbit4(4_096),
        Kbit2(2_048),
        Kbit1(1_024),
        Bit512(512),
        Bit256(256),
        Bit128(128),
        Bit64(64),
        Bit32(32),
        Bit16(16),
        Bit8(8),
        Bit4(4),
        Zero(0),
        Unknown(-1)
    }

    enum class PackageLoss {
        PL_0,
        PL_1,
        PL_2,
        PL_5,
        PL_10,
        PL_20,
        PL_50,
        PL_90,
    }

    @ClassEncoding(isExtendable = false)
    data class RootClass(
        @IntProperty(seqNo = 1, maxValue = 262143) val senderPlatformId: Int,
        @ByteProperty(seqNo = 2, maxValue = 63) val txSeqNo: Byte,
        @EnumProperty(seqNo = 3, maxEnumValues = 64) val txBandwidth: BandwidthClass,
        @ListProperty(seqNo = 4)
        @ClassCollectionValue(topType = PlatformMessage::class, numberOfTypesToSupport = 1)
        val listProp: List<PlatformMessage>
    )

    @ClassEncoding(isExtendable = false)
    data class PlatformMessage(
        @IntProperty(seqNo = 1, maxValue = 262143) val orgPlatformId: Int,
        @TimeProperty(seqNo = 2) val timeOfTransmission: Long,
        @ClassProperty(seqNo = 3, numberOfTypesToSupport = 1) val positionInfo: PositionInfo?,
        @ClassProperty(seqNo = 4, numberOfTypesToSupport = 1) val routeInfo: RouteInfo?,
        @ClassProperty(seqNo = 5, numberOfTypesToSupport = 1) val linkStateInfo: LinkStateInfo?,
        @BooleanProperty(seqNo = 6) val deadRoute: Boolean
    )

    @ClassEncoding(isExtendable = false)
    @PositionClass
    data class PositionInfo(
        @LatitudeProperty(seqNo = 1) val latitude: Double,
        @LongitudeProperty(seqNo = 2) val longitude: Double
    )

    @ClassEncoding(isExtendable = false)
    data class RouteInfo(
        @EnumProperty(seqNo = 1, maxEnumValues = 64) val throughput: BandwidthClass,
        @EnumProperty(seqNo = 2, maxEnumValues = 64) val remainingCapacity: BandwidthClass,
        @IntProperty(seqNo = 3, maxValue = 63) val hopCount: Int,
        @BooleanProperty(seqNo = 4) val isOnDemand: Boolean
    )

    @ClassEncoding(isExtendable = false)
    data class LinkStateInfo(
        @EnumProperty(seqNo = 1) val rxPackageLoss: PackageLoss,
        @BooleanProperty(seqNo = 2) val isUnidirectional: Boolean
    )

    @ClassEncoding
    data class ListOfFft(
        @ListProperty(seqNo = 1)
        @ClassCollectionValue(topType = FftPosition::class, numberOfTypesToSupport = 1)
        val fftPositions: List<FftPosition>
    )

    @ClassEncoding(isExtendable = false)
    data class FftPosition(
        @IntProperty(seqNo = 1, maxValue = 262143) val senderPlatformId: Int,
        @TimeProperty(seqNo = 2) val timeOfPositionUpdate: Long,
        @ClassProperty(seqNo = 3, numberOfTypesToSupport = 1) val position: PositionInfo
    )

    @Test
    fun testFft() {
        //Arrange
        val oneFft = createListOfFft(1)
        val tenFft = createListOfFft(10)
        val fiftyFft = createListOfFft(50)
        val hundredFft = createListOfFft(100)
        val thousandFft = createListOfFft(1000)
        val encoder = EncoderFactory.createEncoder(ListOfFft::class, printEncoding = false, numberOfTypesToSupport = 1)

        //Act
        val oneEncoded = encoder.encode(oneFft)
        val tenEncoded = encoder.encode(tenFft)
        val fiftyEncoded = encoder.encode(fiftyFft)
        val hundredEncoded = encoder.encode(hundredFft)
        val thousandEncoded = encoder.encode(thousandFft)

        //Assert
        println("${oneEncoded.sizeBytes()} - ${oneEncoded.sizeBytes() / 1.0}")
        println("${tenEncoded.sizeBytes()} - ${tenEncoded.sizeBytes() / 10.0}")
        println("${fiftyEncoded.sizeBytes()} - ${fiftyEncoded.sizeBytes() / 50.0}")
        println("${hundredEncoded.sizeBytes()} - ${hundredEncoded.sizeBytes() / 100.0}")
        println("${thousandEncoded.sizeBytes()} - ${thousandEncoded.sizeBytes() / 1000.0}")
    }

    @Test
    fun compareEncodeTime() {
        //Arrange

        val reflectionEncoder = EncoderFactory.createEncoder(RootClass::class, printEncoding = false, numberOfTypesToSupport = 1)
        val compiledEncoder = createCompiledEncoder()
        val gson = Gson()

        //Act
        var reflectionEncoded: ByteArray
        var compileEncoded: ByteArray
        var sumReflection = 0L
        var sumCompile = 0L
        var sumJson = 0L
        val iterations = 100
        val minObj = RootClass(1, 0, BandwidthClass.Kbit8, createPlatformMessages(1, 2))
        for (i in 1..iterations) {
            val reflectionEncodeTime = measureNanoTime { reflectionEncoded = reflectionEncoder.encode(minObj).toByteArray() }
            sumReflection += reflectionEncodeTime
            val compiledEncodeTime = measureNanoTime { compileEncoded = compiledEncoder.encode(minObj).toByteArray() }
            sumCompile += compiledEncodeTime
            val jsonTime = measureNanoTime { gson.toJson(minObj) }
            sumJson += jsonTime
        }

        val avgReflection = sumReflection / (iterations * 1000000.0)
        val avgCompile = sumCompile / (iterations * 1000000.0)
        val avgJson = sumJson / (iterations * 1000000.0)
        val compilePctOfReflection = (avgCompile / avgReflection) * 100
        val compilePctOfJson = (avgCompile / avgJson) * 100

        //Assert
        println("Reflection: Avg. $avgReflection ms.")
        println("Compile: Avg. $avgCompile ms.")
        println("GSon: Avg. $avgJson ms.")

        println("Compile pct of Reflection: $compilePctOfReflection %.")
        println("Compile pct of GSon: $compilePctOfJson %.")
    }

    class RootClassDescriptor(
        platformEncoder: ValueEncoder<PlatformMessage>
    ) : ClassDescription<RootClass> {
        override val isExtendable = false
        override val supportOffsetValues = true
        override val type: KClass<RootClass> = RootClass::class
        override val propertyDescriptors: List<PropertyDescription<*, RootClass>>

        init {
            propertyDescriptors = ArrayList()
            propertyDescriptors.add(PropertyDescription("senderPlatformId", 1, false, Int::class, IntEncoder(maxValue = 262143)) { rootClass -> rootClass.senderPlatformId })
            propertyDescriptors.add(PropertyDescription("txSeqNo", 2, false, Byte::class, ByteEncoder(maxValue = 63)) { rootClass -> rootClass.txSeqNo })
            propertyDescriptors.add(PropertyDescription("listProp", 3, false, List::class, ListEncoder(platformEncoder, VarIntSpecification(4, 6, 10, 16))) { rootClass -> rootClass.listProp })
        }

        override fun create(constParams: PropertyValues): RootClass {
            return RootClass(
                constParams.getValue("senderPlatformId"),
                constParams.getValue("txSeqNo"),
                constParams.getValue(""),
                constParams.getValue("listProp")
            )
        }
    }

    class PlatformMessageClassDescriptor(
        classDescriptionCache: ClassDescriptionCache
    ) : ClassDescription<PlatformMessage> {
        override val isExtendable = false
        override val supportOffsetValues = true
        override val type: KClass<PlatformMessage> = PlatformMessage::class
        override val propertyDescriptors: List<PropertyDescription<*, PlatformMessage>>

        init {
            propertyDescriptors = ArrayList()
            propertyDescriptors.add(PropertyDescription("orgPlatformId", 1, false, Int::class, IntEncoder(maxValue = 262143)) { platformMsg -> platformMsg.orgPlatformId })
            propertyDescriptors.add(PropertyDescription("timeOfTransmission", 2, false, Long::class, TimeEpochMsEncoder()) { platformMsg -> platformMsg.timeOfTransmission })
            propertyDescriptors.add(PropertyDescription("positionInfo", 3, true, PositionInfo::class, ClassPropertyEncoder(TypeIndexCache(listOf(PositionInfo::class), 1), classDescriptionCache)) { platformMsg -> platformMsg.positionInfo })
            propertyDescriptors.add(PropertyDescription("routeInfo", 4, true, RouteInfo::class, ClassPropertyEncoder(TypeIndexCache(listOf(RouteInfo::class), 1), classDescriptionCache)) { platformMsg -> platformMsg.routeInfo })
            propertyDescriptors.add(PropertyDescription("linkStateInfo", 5, true, LinkStateInfo::class, ClassPropertyEncoder(TypeIndexCache(listOf(LinkStateInfo::class), 1), classDescriptionCache)) { platformMsg -> platformMsg.linkStateInfo })
        }

        override fun create(constParams: PropertyValues): PlatformMessage {
            return PlatformMessage(
                constParams.getValue("orgPlatformId"),
                constParams.getValue("timeOfTransmission"),
                constParams.getNullableValue("positionInfo"),
                constParams.getNullableValue("routeInfo"),
                constParams.getNullableValue("linkStateInfo"),
                false
            )
        }
    }

    class PositionInfoClassDescriptor : ClassDescription<PositionInfo> {
        override val isExtendable = false
        override val supportOffsetValues = true
        override val type: KClass<PositionInfo> = PositionInfo::class
        override val propertyDescriptors: List<PropertyDescription<*, PositionInfo>>

        init {
            propertyDescriptors = ArrayList()
            propertyDescriptors.add(PropertyDescription("latitude", 1, false, Double::class, LatitudeEncoder(1.0)) { obj -> obj.latitude })
            propertyDescriptors.add(PropertyDescription("longitude", 2, false, Double::class, LongitudeEncoder(1.0)) { obj -> obj.longitude })
        }

        override fun create(constParams: PropertyValues): PositionInfo {
            return PositionInfo(
                constParams.getValue("latitude"),
                constParams.getValue("longitude")
            )
        }
    }

    class RouteInfoClassDescriptor : ClassDescription<RouteInfo> {
        override val type: KClass<RouteInfo> = RouteInfo::class
        override val isExtendable = false
        override val supportOffsetValues = false
        override val propertyDescriptors: List<PropertyDescription<*, RouteInfo>>

        init {
            val ordinalBandwidthMap: MutableMap<Int, BandwidthClass> = HashMap()
            for (enumValue in BandwidthClass.values()) {
                ordinalBandwidthMap[enumValue.ordinal] = enumValue
            }
            propertyDescriptors = ArrayList()
            propertyDescriptors.add(PropertyDescription("throughPut", 1, false, BandwidthClass::class, EnumEncoder(BandwidthClass.values().size, getEnum = { ordinal, _ -> ordinalBandwidthMap[ordinal]!! }, getOrdinal = { enum -> enum.ordinal })) { obj -> obj.throughput })
            propertyDescriptors.add(PropertyDescription("remainingCapacity", 2, false, BandwidthClass::class, EnumEncoder(BandwidthClass.values().size, getEnum = { ordinal, _ -> ordinalBandwidthMap[ordinal]!! }, getOrdinal = { enum -> enum.ordinal })) { obj -> obj.throughput })
            propertyDescriptors.add(PropertyDescription("hopCount", 3, false, Int::class, IntEncoder(maxValue = 63)) { obj -> obj.hopCount })
            propertyDescriptors.add(PropertyDescription("isOnDemand", 4, false, Boolean::class, BooleanEncoder()) { obj -> obj.isOnDemand })
        }

        override fun create(constParams: PropertyValues): RouteInfo {
            return RouteInfo(
                throughput = constParams.getValue("throughput"),
                remainingCapacity = constParams.getValue("remainingCapacity"),
                hopCount = constParams.getValue("hopCount"),
                isOnDemand = constParams.getValue("isOnDemand")
            )
        }
    }

    class LinkStateInfoClassDescriptor(
    ) : ClassDescription<LinkStateInfo> {
        override val type: KClass<LinkStateInfo> = LinkStateInfo::class
        override val isExtendable = false
        override val supportOffsetValues = false
        override val propertyDescriptors: List<PropertyDescription<*, LinkStateInfo>>

        init {
            val ordinalEnumMap: MutableMap<Int, PackageLoss> = HashMap()
            for (enumValue in PackageLoss.values()) {
                ordinalEnumMap[enumValue.ordinal] = enumValue
            }
            propertyDescriptors = ArrayList()
            propertyDescriptors.add(PropertyDescription("rxPackageLoss", 1, false, PackageLoss::class, EnumEncoder(PackageLoss.values().size, getEnum = { ordinal, _ -> ordinalEnumMap[ordinal]!! }, getOrdinal = { enum -> enum.ordinal })) { obj -> obj.rxPackageLoss })
            propertyDescriptors.add(PropertyDescription("isUnidirectional", 2, false, Boolean::class, BooleanEncoder()) { obj -> obj.isUnidirectional })
        }

        override fun create(constParams: PropertyValues): LinkStateInfo {
            return LinkStateInfo(
                constParams.getValue("rxPackageLoss"),
                constParams.getValue("isUnidirectional")
            )
        }
    }

    private fun createCompiledEncoder(): Encoder {
        val classDescriptionCache = ClassDescriptionCache()
        classDescriptionCache.addClassDescriptor(LinkStateInfoClassDescriptor())
        classDescriptionCache.addClassDescriptor(RouteInfoClassDescriptor())
        classDescriptionCache.addClassDescriptor(PositionInfoClassDescriptor())
        classDescriptionCache.addClassDescriptor(PlatformMessageClassDescriptor(classDescriptionCache))
        val platformValueEncoder = ClassPropertyEncoder<PlatformMessage>(TypeIndexCache(listOf(PlatformMessage::class), 1), classDescriptionCache)
        val rootClassDescriptor = RootClassDescriptor(platformValueEncoder)
        return EncoderImpl(listOf(rootClassDescriptor), 1)
    }

    @Test
    fun testClass() {
        //Arrange
        val minObj = RootClass(1, 0, BandwidthClass.Kbit8, createPlatformMessages(0, 1))
        val avgObj = RootClass(1, 1, BandwidthClass.Kbit8, createPlatformMessages(6, 10))
        val maxObj = RootClass(1, 2, BandwidthClass.Kbit8, createPlatformMessages(100, 400))
        val encoder = EncoderFactory.createEncoder(RootClass::class, printEncoding = false, numberOfTypesToSupport = 1)

        //Act
        var minSize: Int = 0
        var avgSize: Int = 0
        var maxSize: Int = 0
        var jsonMinSize: Int = 0
        var jsonMin: String = ""
        var jsonAvgSize: Int = 0
        var jsonAvg: String = ""
        var jsonMaxSize: Int = 0
        var jsonMax: String = ""
        val gson = Gson()
        val minTime = measureTimeMillis { minSize = encoder.encode(minObj).sizeBytes() }
        val minSizePObj = minSize / 1.0
        println("Min Obj: Has only one Position + Route.")
        println("Avg Obj: Has 16 PlatformMessage, 10 of them is without LinkState object.")
        println("Max Obj: Has 500 PlatformMessage, 400 of them is without LinkState object.")
        println("Min Obj: Has only one Position + Route")
        println("Min size: '$minSize', p. obj = '$minSizePObj' bytes, in '$minTime' ms")
        val minTimeJson = measureTimeMillis {
            jsonMin = gson.toJson(minObj)
            jsonMinSize = jsonMin.toByteArray(Charsets.UTF_8).size
        }
        val minEfficiency = minSize.toDouble() / jsonMinSize.toDouble()
        println("JSON Min size: '$jsonMinSize', bytes, in '$minTimeJson' ms, efficiency: '$minEfficiency'")

        val avgTime = measureTimeMillis {
            val encoded = encoder.encode(avgObj)
            avgSize = encoded.sizeBytes()
            val decoded = encoder.decode(encoded.iterator())
            //println(decoded)
        }
        val avgSizePObj = avgSize / 16.0
        println("Avg size: '$avgSize', p. obj = '$avgSizePObj' bytes, in '$avgTime' ms")
        val avgTimeJson = measureTimeMillis {
            jsonAvg = gson.toJson(avgObj)
            jsonAvgSize = jsonAvg.toByteArray(Charsets.UTF_8).size
        }
        val avgEfficiency = avgSize.toDouble() / jsonAvgSize.toDouble()
        println("JSON Min size: '$jsonAvgSize', bytes, in '$avgTimeJson' ms, efficiency: '$avgEfficiency'")

        val maxTime = measureTimeMillis { maxSize = encoder.encode(maxObj).sizeBytes() }
        val maxSizePObj = maxSize / 500.0
        println("Max size: '$maxSize', p. obj = '$maxSizePObj' bytes, in '$maxTime' ms")
        val maxTimeJson = measureTimeMillis {
            jsonMax = gson.toJson(maxObj)
            jsonMaxSize = jsonMax.toByteArray(Charsets.UTF_8).size
        }
        val maxEfficiency = maxSize.toDouble() / jsonMaxSize.toDouble()
        val maxSizePObjJson = jsonMaxSize / 500.0
        println("JSON Max size: '$jsonMaxSize', p obj = '$maxSizePObjJson', bytes, in '$maxTimeJson' ms, efficiency: '$maxEfficiency'")

        /*println("JSON Min Obj:")
        println(jsonMin)
        println("JSON Avg Obj:")
        println(jsonAvg)
        println("JSON Max Obj:")
        println(jsonMax)*/
    }

    private fun createPlatformMessages(numberOfLinkState: Int, numberOfPosRoute: Int): List<PlatformMessage> {
        val idLinkStateOffset = 162143
        val idPosRouteOffset = 202143
        val createdList = ArrayList<PlatformMessage>()
        for (idx in 0 until numberOfLinkState) {
            val msg = PlatformMessage(
                orgPlatformId = idx + idLinkStateOffset,
                timeOfTransmission = getRandomTimeNow(),
                positionInfo = createRandomPosition(),
                routeInfo = createRandomRouteInfo(),
                linkStateInfo = createRandomLinkStateInfo(),
                deadRoute = false
            )
            createdList.add(msg)
        }
        for (idx in 0 until numberOfPosRoute) {
            val msg = PlatformMessage(
                orgPlatformId = idx + idPosRouteOffset,
                timeOfTransmission = getRandomTimeNow(),
                positionInfo = createRandomPosition(),
                routeInfo = createRandomRouteInfo(),
                linkStateInfo = null,
                deadRoute = false
            )
            createdList.add(msg)
        }
        return createdList
    }

    private fun createListOfFft(noOfPositions: Int): ListOfFft {
        val positionList = ArrayList<FftPosition>()
        for (idx in 0 until noOfPositions) {
            positionList.add(
                FftPosition(
                    senderPlatformId = idx,
                    timeOfPositionUpdate = getRandomTimeNow(),
                    position = createRandomPosition()
                )
            )
        }
        return ListOfFft(positionList)
    }

    private fun createRandomLinkStateInfo(): LinkStateInfo {
        return LinkStateInfo(PackageLoss.PL_20, false)
    }

    private fun createRandomRouteInfo(): RouteInfo {
        return RouteInfo(throughput = BandwidthClass.Kbit4, remainingCapacity = BandwidthClass.Bit128, hopCount = random.nextInt(32), isOnDemand = false)
    }

    private fun createRandomPosition(): PositionInfo {
        val positionVariance = 0.0001
        return PositionInfo(latitudeOffset + random.nextDouble(positionVariance), longitudeOffset + random.nextDouble(positionVariance))
    }

    private fun getRandomTimeNow(): Long {
        val timeVarianceMs = 30000
        return epochTimeOffset + random.nextInt(timeVarianceMs).toLong()
    }
}