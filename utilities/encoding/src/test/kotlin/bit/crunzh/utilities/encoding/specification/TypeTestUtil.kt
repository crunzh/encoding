package bit.crunzh.utilities.encoding.specification

import kotlin.reflect.KClass
import kotlin.test.assertEquals

fun <T : Any> testEncoding(expected: TestInterface<T>, type: KClass<*>, printEncoding: Boolean = false) {
    //Arrange
    val encoder = EncoderFactory.createEncoder(type, printEncoding = printEncoding)

    //Act
    val encoded = encoder.encode(expected)
    val actual = encoder.decode(encoded.iterator(), expected::class)

    //Assert
    assertEquals(expected.prop, actual.prop)
    assertListEquals(expected.listProp, actual.listProp)
    assertKeyMapEquals(expected.mapPropKey, actual.mapPropKey)
    assertValueMapEquals(expected.mapPropValue, actual.mapPropValue)
}

fun <T> assertListEquals(expected: List<T>, actual: List<T>) {
    assertEquals(expected.size, actual.size)
    for (idx in expected.indices) {
        assertEquals(expected[idx], actual[idx])
    }
}

fun <T> assertKeyMapEquals(expected: Map<T, Int>, actual: Map<T, Int>) {
    assertEquals(expected.size, actual.size)
    for (entry in expected.entries) {
        assertEquals(entry.value, actual[entry.key])
    }
}

fun <T> assertValueMapEquals(expected: Map<Int, T>, actual: Map<Int, T>) {
    assertEquals(expected.size, actual.size)
    for (entry in expected.entries) {
        assertEquals(entry.value, actual[entry.key])
    }
}

interface TestInterface<T : Any> {
    val prop: T
    val listProp: List<T>
    val mapPropKey: Map<T, Int>
    val mapPropValue: Map<Int, T>
}