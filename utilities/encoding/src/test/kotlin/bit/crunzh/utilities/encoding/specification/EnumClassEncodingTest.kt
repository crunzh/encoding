package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.EnumEncoding
import bit.crunzh.utilities.encoding.specification.annotation.ObjectEncoding
import bit.crunzh.utilities.encoding.specification.annotation.property.ClassProperty
import bit.crunzh.utilities.encoding.specification.annotation.property.IntProperty
import org.junit.Test
import kotlin.test.assertEquals


class EnumClassEncodingTest {
    @ClassEncoding
    data class TopType(
            @ClassProperty(seqNo = 1, subTypes = [DataClassSubType::class, EnumSubType::class, ObjectSubType::class])
            val subType: SubType
    )

    interface SubType

    @ClassEncoding
    data class DataClassSubType(
            @IntProperty(seqNo = 1) val subClassValue: Int
    ) : SubType

    @EnumEncoding()
    enum class EnumSubType : SubType {
        enumValue1,
        enumValue2,
        enumValue3
    }

    @ObjectEncoding
    object ObjectSubType : SubType {
        const val number = 1
        var name = "Hello World"
    }

    @Test
    fun testPolymorphicEnum() {
        //Arrange
        val encoder = EncoderFactory.createEncoder(TopType::class)
        val objWithDc = TopType(subType = DataClassSubType(1))
        val objWithEnum = TopType(subType = EnumSubType.enumValue2)
        val objWithObj = TopType(subType = ObjectSubType)

        //Act
        val encodedDc = encoder.encode(objWithDc)
        val encodedWithEnum = encoder.encode(objWithEnum)
        val encodedWithObj = encoder.encode(objWithObj)
        val decodedDc = encoder.decode(encodedDc.iterator())
        val decodedWithEnum = encoder.decode(encodedWithEnum.iterator())
        val decodedWithObj = encoder.decode(encodedWithObj.iterator())

        //Assert
        assertEquals(objWithDc, decodedDc)
        assertEquals(objWithEnum, decodedWithEnum)
        assertEquals(objWithObj, decodedWithObj)
    }
}