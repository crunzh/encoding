package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.collection.EnumCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.IntCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.EnumMapKey
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.IntMapKey
import bit.crunzh.utilities.encoding.specification.annotation.property.*
import org.junit.Test
import kotlin.test.assertEquals

class EnumPropertyTest {
    enum class TestEnum {
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE,
        SIX,
        SEVEN,
        EIGHT,
        NINE
    }


    @ClassEncoding
    data class RootClass(
            @EnumProperty(seqNo = 1)
            override val prop: TestEnum,
            @ListProperty(seqNo = 2)
            @EnumCollectionValue(14, TestEnum::class)
            override val listProp: List<TestEnum>,
            @MapProperty(seqNo = 3)
            @EnumMapKey(enumType = TestEnum::class)
            @IntCollectionValue(maxValue = 10)
            override val mapPropKey: Map<TestEnum, Int>,
            @MapProperty(seqNo = 4)
            @IntMapKey(maxValue = 10)
            @EnumCollectionValue(enumType = TestEnum::class)
            override val mapPropValue: Map<Int, TestEnum>) : TestInterface<TestEnum>


    @Test
    fun test() {
        //Arrange
        val prop = TestEnum.ONE
        val listProp = listOf(TestEnum.NINE, TestEnum.FOUR)
        val keyMap = mapOf(Pair(TestEnum.THREE, 1), Pair(TestEnum.FIVE, 2))
        val valueMap = mapOf(Pair(1, TestEnum.SIX), Pair(2, TestEnum.SEVEN))
        val expected = RootClass(prop, listProp, keyMap, valueMap)

        //Act
        testEncoding(expected, RootClass::class, false)
    }

    enum class OldEnum {
        Unknown,
        One,
        Two,
        Three
    }

    enum class NewEnum {
        Unknown,
        One,
        Two,
        Three,
        Four
    }
    @ClassEncoding
    data class EnumClassOld(@EnumProperty(seqNo = 1, maxEnumValues = 6, unknownValueOrdinal = 0) val enum:OldEnum)
    @ClassEncoding
    data class EnumClassNew(@EnumProperty(seqNo = 1, maxEnumValues = 6, unknownValueOrdinal = 0) val enum:NewEnum)

    @Test
    fun testReceivingUnknownEnumValue() {
        //Arrange
        //Arrange
        val encoderNew = EncoderFactory.createEncoder(EnumClassNew::class)
        val encoderOld = EncoderFactory.createEncoder(EnumClassOld::class)
        val newObj = EnumClassNew(NewEnum.Four) //Enum ordinal which is undefined in the old model, and thus the decoding defaults to Unknown.

        //Act
        val encoded = encoderNew.encode(newObj)
        val decodedObj = encoderOld.decode(encoded.iterator(), EnumClassOld::class)

        //Assert
        assertEquals(OldEnum.Unknown, decodedObj.enum)
    }
}