package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.position.LossyPositionCompression
import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.collection.ClassCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.position.PositionClass
import bit.crunzh.utilities.encoding.specification.annotation.property.LatitudeProperty
import bit.crunzh.utilities.encoding.specification.annotation.property.ListProperty
import bit.crunzh.utilities.encoding.specification.annotation.property.LongitudeProperty
import org.junit.Test

class LatLongPropertyTest {
    @PositionClass
    @ClassEncoding(isExtendable = false)
    data class Position(
            @LatitudeProperty(seqNo = 1)
            val latitude: Double,
            @LongitudeProperty(seqNo = 2)
            val longitude: Double
    )

    @ClassEncoding
    data class ListOfPositions(
            @ListProperty(seqNo = 1)
            @ClassCollectionValue(topType = Position::class, numberOfTypesToSupport = 1)
            val positions: List<Position>
    )

    @Test
    fun testEncodeListOfPositionsObj() {
        //Arrange
        val positions = createPositions(20)
        val obj = ListOfPositions(positions)
        val encoder = EncoderFactory.createEncoder(ListOfPositions::class, printEncoding = true)

        //Act
        val encoded = encoder.encode(obj)
        println("20 positions in list encoded in ${encoded.sizeBytes()} bytes.")
        val decoded = encoder.decode(encoded.iterator(), ListOfPositions::class)

        //Assert
        assertListEquals(positions, decoded.positions)
    }

    private fun createPositions(amount: Int): List<Position> {
        val positions = ArrayList<Position>()
        for(idx in 1..amount) {
            val position = LossyPositionCompression.removePrecision(89.0+(idx*0.00001), 179.0+(idx*0.00001), 1.0)
            positions.add(Position(position.latitude, position.longitude))
        }
        return positions
    }
}