package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.collection.DoubleCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.IntCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.DoubleMapKey
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.IntMapKey
import bit.crunzh.utilities.encoding.specification.annotation.property.DoubleProperty
import bit.crunzh.utilities.encoding.specification.annotation.property.ListProperty
import bit.crunzh.utilities.encoding.specification.annotation.property.MapProperty
import org.junit.Test

class DoublePropertyTest {
    @ClassEncoding
    data class RootClass(
            @DoubleProperty(seqNo = 1, maxValue = 10.1)
            override val prop: Double = 1.1,
            @ListProperty(seqNo = 2)
            @DoubleCollectionValue(maxValue = 10.0)
            override val listProp: List<Double>,
            @MapProperty(seqNo = 3)
            @DoubleMapKey(maxValue = 100.0)
            @IntCollectionValue(maxValue = 10)
            override val mapPropKey: Map<Double, Int>,
            @MapProperty(seqNo = 4)
            @IntMapKey(maxValue = 10)
            @DoubleCollectionValue(maxValue = 100.0)
            override val mapPropValue: Map<Int, Double>) : TestInterface<Double>

    @Test
    fun test() {
        //Arrange
        val prop = 1.0
        val listProp = listOf(2.0, 3.0)
        val keyMap = mapOf(Pair(4.0, 1), Pair(5.0, 2))
        val valueMap = mapOf(Pair(1, 6.0), Pair(2, 7.0))
        val expected = RootClass(prop, listProp, keyMap, valueMap)

        //Act
        testEncoding(expected, RootClass::class)
    }
}