package bit.crunzh.utilities.encoding.tlv

import org.junit.Test

import org.junit.Assert.*

class EnumTlvEncoderTest {
    val enum1Encoder = EnumTlvEncoder(TestEnum1::class, enumValuesToSupport = 8)
    val enum2Encoder = EnumTlvEncoder(TestEnum2::class, enumValuesToSupport = 8)

    enum class TestEnum1(val intValue: Int = 0, val stringValue: String = "Hello World") {
        ONE(1),
        TWO(stringValue = "Two"),
        THREE(3, "Three")
    }

    enum class TestEnum2(val intValue: Int = 0, val stringValue: String = "Hello World") {
        EN(1),
        TO(stringValue = "Two"),
        TRE(3, stringValue = "Three"),
        FIRE
    }

    @Test
    fun encode() {
        //Arrange
        val expected = EnumTlvTuple(TestEnum1.THREE, byteArrayOf(1, 2, 3, 4))

        //Act
        val encoded = enum1Encoder.encode(expected).iterator()
        val actual = enum1Encoder.decode(encoded)

        //Assert
        assertTlvTupleEquals(expected, actual)
    }

    @Test
    fun decodeOtherEnum() {
        //Arrange
        val expected = EnumTlvTuple(TestEnum1.THREE, byteArrayOf(1, 2, 3, 4))

        //Act
        val encoded = enum1Encoder.encode(expected).iterator()
        val actual = enum2Encoder.decode(encoded)

        //Assert
        assertEquals(TestEnum2.TRE, actual.type)
    }

    @Test
    fun calculateSize() {
        //Arrange
        val expected = EnumTlvTuple(TestEnum1.THREE, byteArrayOf(1, 2, 3, 4))

        //Act
        val encodedSize = enum1Encoder.encode(expected).size()
        val calculatedSize = enum1Encoder.calculateSize(expected)

        //Assert
        assertEquals(encodedSize, calculatedSize)
    }

    companion object {
        fun assertTlvTupleEquals(expected: TlvTuple<*>, actual: TlvTuple<*>) {
            assertEquals(expected.type, actual.type)
            assertEquals(expected.length, actual.length)
            assertArrayEquals(expected.value, actual.value)
        }
    }
}
