package bit.crunzh.utilities.encoding.byte

import org.junit.Test
import kotlin.test.assertEquals

class ByteIteratorTest {
    @Test
    fun testBitIteratorFirst() {
        //Arrange
        val data = byteArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 0)
        val byteIterator = ByteIterator(data)
        val bitIterator = byteIterator.bitIterator()

        //Act
        val bytesReadBefore = byteIterator.bytesRead()
        val range0 = bitIterator.next(0)
        val bytesRead0bits = byteIterator.bytesRead()
        val range1 = bitIterator.next(1)
        val bytesRead1bits = byteIterator.bytesRead()
        val range7 = bitIterator.next(7)
        val bytesRead8bits = byteIterator.bytesRead()
        val range8 = bitIterator.next(8)
        val bytesRead16bits = byteIterator.bytesRead()
        val range9 = bitIterator.next(9)
        val bytesRead25bits = byteIterator.bytesRead()

        //Assert
        assertEquals(0, bytesReadBefore)
        assertEquals(0, bytesRead0bits)
        assertEquals(1, bytesRead1bits)
        assertEquals(1, bytesRead8bits)
        assertEquals(2, bytesRead16bits)
        assertEquals(4, bytesRead25bits) //3 bytes and 1 bits means reading 3 bytes on the byte iterator.
        assertEquals("", range0.printBinaryString())
        assertEquals("1", range1.printBinaryString())
        assertEquals("0000000", range7.printBinaryString())
        assertEquals("01000000", range8.printBinaryString())
        assertEquals("11000000 0", range9.printBinaryString())
    }

    @Test
    fun testBitIteratorMiddle() {
        val data = byteArrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
        val byteIterator = ByteIterator(data)
        val bytesSkipped = 4
        byteIterator.skip(bytesSkipped)
        val bitIterator = byteIterator.bitIterator()

        //Act
        val bytesReadBefore = byteIterator.bytesRead()
        bitIterator.next(0)
        val bytesRead0bits = byteIterator.bytesRead()
        bitIterator.next(1)
        val bytesRead1bits = byteIterator.bytesRead()
        bitIterator.next(7)
        val bytesRead8bits = byteIterator.bytesRead()
        bitIterator.next(9)
        val bytesRead17bits = byteIterator.bytesRead()

        //Assert
        assertEquals(bytesSkipped + 0, bytesReadBefore)
        assertEquals(bytesSkipped + 0, bytesRead0bits)
        assertEquals(bytesSkipped + 1, bytesRead1bits)
        assertEquals(bytesSkipped + 1, bytesRead8bits)
        assertEquals(bytesSkipped + 3, bytesRead17bits) //2 bytes and 1 bits means reading 3 bytes on the byte iterator.
    }
}