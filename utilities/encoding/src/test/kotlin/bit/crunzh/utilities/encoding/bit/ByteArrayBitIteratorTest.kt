package bit.crunzh.utilities.encoding.bit

import org.junit.Test

import org.junit.Assert.*

class ByteArrayBitIteratorTest {

    @Test
    fun hasNextOne() {
        //Arrange
        val it = BitBuilder(123).iterator()
        it.next(Int.SIZE_BITS - 1)

        //Act
        val hasNextBefore = it.hasNext(1)
        it.next(1)
        val hasNextAfter = it.hasNext(1)

        //Assert
        assertTrue(hasNextBefore)
        assertFalse(hasNextAfter)
    }

    @Test
    fun hasNextMany() {
        //Arrange
        val it = BitBuilder(123).iterator()

        //Act
        val hasNextMatch = it.hasNext(Int.SIZE_BITS)
        val hasNextTooMany = it.hasNext(Int.SIZE_BITS + 1)
        it.next(Int.SIZE_BITS)
        val hasNextAfter = it.hasNext(1)

        //Assert
        assertTrue(hasNextMatch)
        assertFalse(hasNextTooMany)
        assertFalse(hasNextAfter)
    }

    @Test
    fun hasNextZero() {
        //Arrange
        val it = BitBuilder(123).iterator()
        it.next(Int.SIZE_BITS)

        //Act
        val hasNextMatch = it.hasNext(0) //0 remaining

        //Assert
        assertEquals(0, it.remaining())
        assertTrue(hasNextMatch)
    }

    @Test
    operator fun next() {
        //Arrange
        val expected = "Test"
        val range1 = BitBuilder(123, fitIntoBits = 9).toBitRange()
        val range2 = BitBuilder(expected).toBitRange()
        val range3 = BitBuilder(1.1).toBitRange()
        val byteArr = BitBuilder(range1).append(range2).append(range3).toByteArray()
        val it = ByteArrayBitIterator(byteArr, range1.size(), range2.size()) //Iterator should only

        //Arrange
        val actual = BitRange.readString(it)

        //Assert
        assertEquals(0, it.remaining())
        assertEquals(expected, actual)
    }

    @Test(expected = IllegalArgumentException::class)
    fun nextOutOfRange() {
        //Act
        BitBuilder(123).iterator().next(Int.SIZE_BITS + 1)
    }

    @Test
    fun byteSize() {
        //Arrange
        val expectedSizeBits = 9
        val it = BitBuilder(123, fitIntoBits = expectedSizeBits).iterator()

        //Act
        val actualSizeBits = it.size()
        val actualSizeBytes = it.byteSize()

        //Assert
        assertEquals(expectedSizeBits, actualSizeBits)
        assertEquals(2, actualSizeBytes)
    }
}