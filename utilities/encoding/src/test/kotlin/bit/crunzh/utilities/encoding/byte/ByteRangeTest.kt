package bit.crunzh.utilities.encoding.byte

import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class ByteRangeTest {
    private var byteArray = byteArrayOf(1, 2, 3, 4, 5, 0xff.toByte())
    private var range: ByteRange = ByteRange(byteArray)
    private var rangeOffsetStart: ByteRange = ByteRange(byteArray, offset = 2, size = 4)
    private var rangeOffsetEnd: ByteRange = ByteRange(byteArray, offset = 0, size = 4)
    private var rangeOffsetBothEnds: ByteRange = ByteRange(byteArray, offset = 1, size = 2)

    @Before
    fun setUp() {
        range = ByteRange(byteArray)
    }

    @Test
    fun printHex() {
        //Act
        val result1 = range.printHexString()
        val result2 = rangeOffsetStart.printHexString()
        val result3 = rangeOffsetEnd.printHexString()
        val result4 = rangeOffsetBothEnds.printHexString()

        //Assert
        assertEquals("01-02-03-04-05-FF", result1)
        assertEquals("03-04-05-FF", result2)
        assertEquals("01-02-03-04", result3)
        assertEquals("02-03", result4)
    }
}