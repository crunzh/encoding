package bit.crunzh.utilities.encoding.byte

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitRange
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ByteBuilderTest {
    var byteBuilder: ByteBuilder = ByteBuilder()

    @Before
    fun setUp() {
        byteBuilder = ByteBuilder()
    }

    @Test
    fun appendByteArray() {
        //Arrange
        val ba1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val ba2 = byteArrayOf(0x05, 0x06, 0x07, 0x08, 0x09)
        val ba3 = ByteArray(0)
        val ba4 = byteArrayOf(0x0a)
        val ba5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.append(ba1).append(ba2).append(ba3).append(ba4).append(ba5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(ba1, outputIterator.next(ba1.size).toByteArray())
        assertArrayEquals(ba2, outputIterator.next(ba2.size).toByteArray())
        assertArrayEquals(ba3, outputIterator.next(ba3.size).toByteArray())
        assertArrayEquals(ba4, outputIterator.next(ba4.size).toByteArray())
        assertArrayEquals(ba5, outputIterator.next(ba5.size).toByteArray())
    }

    @Test
    fun appendByteRange() {
        //Arrange
        val br1 = ByteRange(byteArrayOf(0x01, 0x02, 0x03, 0x04), 1, 2)
        val br2 = ByteRange(byteArrayOf(0x05, 0x06, 0x07, 0x08, 0x09), 1, 4)
        val br3 = ByteRange(ByteArray(0))
        val br4 = ByteRange(byteArrayOf(0x0a))
        val br5 = ByteRange(byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte()), 0, 2)

        //Act
        val output = byteBuilder.append(br1).append(br2).append(br3).append(br4).append(br5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(br1.toByteArray(), outputIterator.next(br1.size).toByteArray())
        assertArrayEquals(br2.toByteArray(), outputIterator.next(br2.size).toByteArray())
        assertArrayEquals(br3.toByteArray(), outputIterator.next(br3.size).toByteArray())
        assertArrayEquals(br4.toByteArray(), outputIterator.next(br4.size).toByteArray())
        assertArrayEquals(br5.toByteArray(), outputIterator.next(br5.size).toByteArray())
    }

    @Test
    fun appendBitBuilder() {
        //Arrange
        val value1 = ByteRange(byteArrayOf(0x01, 0x02, 0x03, 0x04))
        val value2 = BitBuilder(byteArrayOf(0x05, 0x06, 0x07, 0x08, 0x09)).append(1, 3)
        val value3 = BitBuilder(15, 4)
        val value4 = BitBuilder(ByteArray(0))
        val value5 = byteArrayOf(0x0a)
        val value6 = BitBuilder(byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte()))

        //Act
        val output = byteBuilder.append(value1).append(value2).append(value3).append(value4).append(value5).append(value6).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value1.toByteArray(), outputIterator.next(value1.size).toByteArray())
        assertArrayEquals(value2.toByteArray(), outputIterator.next(value2.sizeBytes()).toByteArray())
        assertEquals(15, outputIterator.bitIterator().next(value3.size()).toInt())
        assertArrayEquals(value4.toByteArray(), outputIterator.next(value4.sizeBytes()).toByteArray())
        assertArrayEquals(value5, outputIterator.next(value5.size).toByteArray())
        assertArrayEquals(value6.toByteArray(), outputIterator.next(value6.sizeBytes()).toByteArray())
    }

    @Test
    fun appendBitRange() {
        //Arrange
        val value1 = ByteRange(byteArrayOf(0x01, 0x02, 0x03, 0x04))
        val value2 = BitRange(byteArrayOf(0x05, 0x06, 0x07, 0x08, 0x09), 3, 9)
        val value3 = BitRange(ByteArray(0))
        val value4 = byteArrayOf(0x0a)
        val value5 = BitBuilder(byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte()))

        //Act
        val output = byteBuilder.append(value1).append(value2).append(value3).append(value4).append(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value1.toByteArray(), outputIterator.next(value1.size).toByteArray())
        assertArrayEquals(value2.toByteArray(), outputIterator.next(value2.sizeBytes()).toByteArray())
        assertArrayEquals(value3.toByteArray(), outputIterator.next(value3.sizeBytes()).toByteArray())
        assertArrayEquals(value4, outputIterator.next(value4.size).toByteArray())
        assertArrayEquals(value5.toByteArray(), outputIterator.next(value5.sizeBytes()).toByteArray())
    }

    @Test
    fun appendByte() {
        //Arrange
        val value1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val value2: Byte = 0x05
        val value3 = ByteArray(0)
        val value4: Byte = 0x0a
        val value5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.append(value1).append(value2).append(value3).append(value4).append(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value1, outputIterator.next(value1.size).toByteArray())
        assertEquals(value2, outputIterator.next())
        assertArrayEquals(value3, outputIterator.next(value3.size).toByteArray())
        assertEquals(value4, outputIterator.next())
        assertArrayEquals(value5, outputIterator.next(value5.size).toByteArray())
    }

    @Test
    fun appendShort() {
        //Arrange
        val value1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val value2: Short = Short.MAX_VALUE
        val value3: Short = Short.MIN_VALUE
        val value4: Short = 0
        val value5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.append(value1).append(value2).append(value3).append(value4).append(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value1, outputIterator.next(value1.size).toByteArray())
        assertEquals(value2, outputIterator.next(Short.SIZE_BYTES).toShort())
        assertEquals(value3, outputIterator.next(Short.SIZE_BYTES).toShort())
        assertEquals(value4, outputIterator.next(Short.SIZE_BYTES).toShort())
        assertArrayEquals(value5, outputIterator.next(value5.size).toByteArray())
    }

    @Test
    fun appendInt() {
        //Arrange
        val value1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val value2: Int = Int.MAX_VALUE
        val value3: Int = Int.MIN_VALUE
        val value4 = 0
        val value5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.append(value1).append(value2).append(value3).append(value4).append(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value1, outputIterator.next(value1.size).toByteArray())
        assertEquals(value2, outputIterator.next(Int.SIZE_BYTES).toInt())
        assertEquals(value3, outputIterator.next(Int.SIZE_BYTES).toInt())
        assertEquals(value4, outputIterator.next(Int.SIZE_BYTES).toInt())
        assertArrayEquals(value5, outputIterator.next(value5.size).toByteArray())
    }

    @Test
    fun appendLong() {
        //Arrange
        val value1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val value2: Long = Long.MAX_VALUE
        val value3: Long = Long.MIN_VALUE
        val value4: Long = 0
        val value5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.append(value1).append(value2).append(value3).append(value4).append(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value1, outputIterator.next(value1.size).toByteArray())
        assertEquals(value2, outputIterator.next(Long.SIZE_BYTES).toLong())
        assertEquals(value3, outputIterator.next(Long.SIZE_BYTES).toLong())
        assertEquals(value4, outputIterator.next(Long.SIZE_BYTES).toLong())
        assertArrayEquals(value5, outputIterator.next(value5.size).toByteArray())
    }

    @Test
    fun appendFloat() {
        //Arrange
        val value1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val value2: Float = Float.MAX_VALUE
        val value3: Float = Float.MIN_VALUE
        val value4 = 0.0f
        val value5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.append(value1).append(value2).append(value3).append(value4).append(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value1, outputIterator.next(value1.size).toByteArray())
        assertEquals(value2, outputIterator.next(Float.SIZE_BYTES).toFloat())
        assertEquals(value3, outputIterator.next(Float.SIZE_BYTES).toFloat())
        assertEquals(value4, outputIterator.next(Float.SIZE_BYTES).toFloat())
        assertArrayEquals(value5, outputIterator.next(value5.size).toByteArray())
    }

    @Test
    fun appendDouble() {
        //Arrange
        val value1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val value2: Double = Double.MAX_VALUE
        val value3: Double = Double.MIN_VALUE
        val value4 = 0.0
        val value5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.append(value1).append(value2).append(value3).append(value4).append(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value1, outputIterator.next(value1.size).toByteArray())
        assertEquals(value2, outputIterator.next(Double.SIZE_BYTES).toDouble(), 0.0)
        assertEquals(value3, outputIterator.next(Double.SIZE_BYTES).toDouble(), 0.0)
        assertEquals(value4, outputIterator.next(Double.SIZE_BYTES).toDouble(), 0.0)
        assertArrayEquals(value5, outputIterator.next(value5.size).toByteArray())
    }

    @Test
    fun prependByteArray() {
        //Arrange
        val ba1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val ba2 = byteArrayOf(0x05, 0x06, 0x07, 0x08, 0x09)
        val ba3 = ByteArray(0)
        val ba4 = byteArrayOf(0x0a)
        val ba5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.prepend(ba1).prepend(ba2).prepend(ba3).prepend(ba4).prepend(ba5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(ba5, outputIterator.next(ba5.size).toByteArray())
        assertArrayEquals(ba4, outputIterator.next(ba4.size).toByteArray())
        assertArrayEquals(ba3, outputIterator.next(ba3.size).toByteArray())
        assertArrayEquals(ba2, outputIterator.next(ba2.size).toByteArray())
        assertArrayEquals(ba1, outputIterator.next(ba1.size).toByteArray())
    }

    @Test
    fun prependByteRange() {
        //Arrange
        val br1 = ByteRange(byteArrayOf(0x01, 0x02, 0x03, 0x04), 1, 2)
        val br2 = ByteRange(byteArrayOf(0x05, 0x06, 0x07, 0x08, 0x09), 1, 4)
        val br3 = ByteRange(ByteArray(0))
        val br4 = ByteRange(byteArrayOf(0x0a))
        val br5 = ByteRange(byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte()), 0, 2)

        //Act
        val output = byteBuilder.prepend(br1).prepend(br2).prepend(br3).prepend(br4).prepend(br5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(br5.toByteArray(), outputIterator.next(br5.size).toByteArray())
        assertArrayEquals(br4.toByteArray(), outputIterator.next(br4.size).toByteArray())
        assertArrayEquals(br3.toByteArray(), outputIterator.next(br3.size).toByteArray())
        assertArrayEquals(br2.toByteArray(), outputIterator.next(br2.size).toByteArray())
        assertArrayEquals(br1.toByteArray(), outputIterator.next(br1.size).toByteArray())
    }

    @Test
    fun prependBitBuilder() {
        //Arrange
        val value1 = ByteRange(byteArrayOf(0x01, 0x02, 0x03, 0x04))
        val value2 = BitBuilder(byteArrayOf(0x05, 0x06, 0x07, 0x08, 0x09)).append(1, 3)
        val value3 = BitBuilder(ByteArray(0))
        val value4 = byteArrayOf(0x0a)
        val value5 = BitBuilder(byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte()))

        //Act
        val output = byteBuilder.prepend(value1).prepend(value2).prepend(value3).prepend(value4).prepend(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value5.toByteArray(), outputIterator.next(value5.sizeBytes()).toByteArray())
        assertArrayEquals(value4, outputIterator.next(value4.size).toByteArray())
        assertArrayEquals(value3.toByteArray(), outputIterator.next(value3.sizeBytes()).toByteArray())
        assertArrayEquals(value2.toByteArray(), outputIterator.next(value2.sizeBytes()).toByteArray())
        assertArrayEquals(value1.toByteArray(), outputIterator.next(value1.size).toByteArray())
    }

    @Test
    fun prependBitRange() {
        //Arrange
        val value1 = ByteRange(byteArrayOf(0x01, 0x02, 0x03, 0x04))
        val value2 = BitRange(byteArrayOf(0x05, 0x06, 0x07, 0x08, 0x09), 3, 9)
        val value3 = BitRange(ByteArray(0))
        val value4 = byteArrayOf(0x0a)
        val value5 = BitBuilder(byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte()))

        //Act
        val output = byteBuilder.prepend(value1).prepend(value2).prepend(value3).prepend(value4).prepend(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value5.toByteArray(), outputIterator.next(value5.sizeBytes()).toByteArray())
        assertArrayEquals(value4, outputIterator.next(value4.size).toByteArray())
        assertArrayEquals(value3.toByteArray(), outputIterator.next(value3.sizeBytes()).toByteArray())
        assertArrayEquals(value2.toByteArray(), outputIterator.next(value2.sizeBytes()).toByteArray())
        assertArrayEquals(value1.toByteArray(), outputIterator.next(value1.size).toByteArray())
    }

    @Test
    fun prependByte() {
        //Arrange
        val value1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val value2: Byte = 0x05
        val value3 = ByteArray(0)
        val value4: Byte = 0x0a
        val value5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.prepend(value1).prepend(value2).prepend(value3).prepend(value4).prepend(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value5, outputIterator.next(value5.size).toByteArray())
        assertEquals(value4, outputIterator.next())
        assertArrayEquals(value3, outputIterator.next(value3.size).toByteArray())
        assertEquals(value2, outputIterator.next())
        assertArrayEquals(value1, outputIterator.next(value1.size).toByteArray())
    }

    @Test
    fun prependShort() {
        //Arrange
        val value1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val value2: Short = Short.MAX_VALUE
        val value3: Short = Short.MIN_VALUE
        val value4: Short = 0
        val value5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.prepend(value1).prepend(value2).prepend(value3).prepend(value4).prepend(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value5, outputIterator.next(value5.size).toByteArray())
        assertEquals(value4, outputIterator.next(Short.SIZE_BYTES).toShort())
        assertEquals(value3, outputIterator.next(Short.SIZE_BYTES).toShort())
        assertEquals(value2, outputIterator.next(Short.SIZE_BYTES).toShort())
        assertArrayEquals(value1, outputIterator.next(value1.size).toByteArray())
    }

    @Test
    fun prependInt() {
        //Arrange
        val value1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val value2: Int = Int.MAX_VALUE
        val value3: Int = Int.MIN_VALUE
        val value4 = 0
        val value5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.prepend(value1).prepend(value2).prepend(value3).prepend(value4).prepend(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value5, outputIterator.next(value5.size).toByteArray())
        assertEquals(value4, outputIterator.next(Int.SIZE_BYTES).toInt())
        assertEquals(value3, outputIterator.next(Int.SIZE_BYTES).toInt())
        assertEquals(value2, outputIterator.next(Int.SIZE_BYTES).toInt())
        assertArrayEquals(value1, outputIterator.next(value1.size).toByteArray())
    }

    @Test
    fun prependLong() {
        //Arrange
        val value1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val value2: Long = Long.MAX_VALUE
        val value3: Long = Long.MIN_VALUE
        val value4: Long = 0
        val value5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.prepend(value1).prepend(value2).prepend(value3).prepend(value4).prepend(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value5, outputIterator.next(value5.size).toByteArray())
        assertEquals(value4, outputIterator.next(Long.SIZE_BYTES).toLong())
        assertEquals(value3, outputIterator.next(Long.SIZE_BYTES).toLong())
        assertEquals(value2, outputIterator.next(Long.SIZE_BYTES).toLong())
        assertArrayEquals(value1, outputIterator.next(value1.size).toByteArray())
    }

    @Test
    fun prependFloat() {
        //Arrange
        val value1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val value2: Float = Float.MAX_VALUE
        val value3: Float = Float.MIN_VALUE
        val value4 = 0.0f
        val value5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.prepend(value1).prepend(value2).prepend(value3).prepend(value4).prepend(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value5, outputIterator.next(value5.size).toByteArray())
        assertEquals(value4, outputIterator.next(Float.SIZE_BYTES).toFloat())
        assertEquals(value3, outputIterator.next(Float.SIZE_BYTES).toFloat())
        assertEquals(value2, outputIterator.next(Float.SIZE_BYTES).toFloat())
        assertArrayEquals(value1, outputIterator.next(value1.size).toByteArray())
    }

    @Test
    fun prependDouble() {
        //Arrange
        val value1 = byteArrayOf(0x01, 0x02, 0x03, 0x04)
        val value2: Double = Double.MAX_VALUE
        val value3: Double = Double.MIN_VALUE
        val value4 = 0.0
        val value5 = byteArrayOf(0xff.toByte(), 0x0b, 0xfe.toByte())

        //Act
        val output = byteBuilder.prepend(value1).prepend(value2).prepend(value3).prepend(value4).prepend(value5).toByteArray()

        //Assert
        val outputIterator = ByteIterator(output)
        assertArrayEquals(value5, outputIterator.next(value5.size).toByteArray())
        assertEquals(value4, outputIterator.next(Double.SIZE_BYTES).toDouble(), 0.0)
        assertEquals(value3, outputIterator.next(Double.SIZE_BYTES).toDouble(), 0.0)
        assertEquals(value2, outputIterator.next(Double.SIZE_BYTES).toDouble(), 0.0)
        assertArrayEquals(value1, outputIterator.next(value1.size).toByteArray())
    }
}