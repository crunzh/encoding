package bit.crunzh.utilities.encoding.bit

interface BitIterator: Iterator<Boolean> {
    fun hasNext(nextBits: Int = 1): Boolean
    fun next(bitsToRead: Int): BitRange
    fun size(): Int
    fun byteSize(): Int
    fun bitsRead(): Int
    fun remaining(): Int
    fun reset()

    /**
     * Create a new iterator where start index is this iterators current index.
     */
    fun iterator(): BitIterator

    override fun toString(): String
}