package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext

class EnumEncoder<EnumType : Enum<*>>(
        private val maxEnumOrdinal: Int,
        private val defaultEnumOrdinal: Int = -1,
        val getEnum: (ordinal: Int, default: Int) -> EnumType,
        val getOrdinal: (enum: EnumType) -> Int = fun(enum: EnumType): Int { return enum.ordinal }
) : ValueEncoder<EnumType> {
    private val bitsToRepresentEnumOrdinalValues = BitBuilder.getBitsRequired(maxEnumOrdinal)

    override fun encode(property: EnumType, encoderContext: EncoderContext): BitBuilder {
        return BitBuilder(getOrdinal(property), bitsToRepresentEnumOrdinalValues)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): EnumType {
        return getEnum(bits.next(bitsToRepresentEnumOrdinalValues).toInt(), defaultEnumOrdinal)
    }

    override fun assertValid(property: EnumType) {
        val ordinal = getOrdinal(property)
        if(ordinal > maxEnumOrdinal) throw IllegalArgumentException("Enum value '$property' had a larger ordinal '$ordinal' than maxEnumOrdinal '$maxEnumOrdinal'.")
    }
}