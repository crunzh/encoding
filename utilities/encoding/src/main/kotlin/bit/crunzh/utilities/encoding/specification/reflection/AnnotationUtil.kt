package bit.crunzh.utilities.encoding.specification.core

import bit.crunzh.utilities.encoding.specification.annotation.collection.*
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.*
import bit.crunzh.utilities.encoding.specification.annotation.property.*

fun isCollectionAnnotation(annotation: Annotation): Boolean {
    return isCollectionValueAnnotation(annotation) || isMapKeyAnnotation(annotation)
}

fun isOffsetCompressionAnnotation(annotation: Annotation): Boolean {
    return isLatLongProperty(annotation) || isTimeProperty(annotation)
}

fun isLatLongProperty(annotation: Annotation): Boolean {
    return when(annotation) {
        is LatitudeProperty -> true
        is LongitudeProperty -> true
        else -> false
    }
}

fun isTimeProperty(annotation: Annotation): Boolean {
    return when(annotation) {
        is TimeProperty -> true
        else -> false
    }
}

fun isPropertyAnnotation(annotation: Annotation): Boolean {
    return when (annotation) {
        is BooleanProperty -> true
        is ClassProperty -> true
        is VarShortProperty -> true
        is VarIntProperty -> true
        is VarLongProperty -> true
        is ByteProperty -> true
        is ShortProperty -> true
        is IntProperty -> true
        is LongProperty -> true
        is FloatProperty -> true
        is DoubleProperty -> true
        is ListProperty -> true
        is MapProperty -> true
        is StringProperty -> true
        is SetProperty -> true
        is EnumProperty -> true
        is ByteStoreProperty -> true
        is LatitudeProperty -> true
        is LongitudeProperty -> true
        is TimeProperty -> true
        is UuidProperty -> true
        is BitRangeProperty -> true
        is DurationProperty -> true
        is ByteStoreConstantSizeProperty -> true
        else -> false
    }
}

fun getSeqNo(annotation: Annotation): Int {
    return when (annotation) {
        is BooleanProperty -> annotation.seqNo
        is ClassProperty -> annotation.seqNo
        is EnumProperty -> annotation.seqNo
        is FloatProperty -> annotation.seqNo
        is DoubleProperty -> annotation.seqNo
        is ByteProperty -> annotation.seqNo
        is ShortProperty -> annotation.seqNo
        is IntProperty -> annotation.seqNo
        is LongProperty -> annotation.seqNo
        is ListProperty -> annotation.seqNo
        is MapProperty -> annotation.seqNo
        is SetProperty -> annotation.seqNo
        is StringProperty -> annotation.seqNo
        is VarShortProperty -> annotation.seqNo
        is VarIntProperty -> annotation.seqNo
        is VarLongProperty -> annotation.seqNo
        is ByteStoreProperty -> annotation.seqNo
        is LatitudeProperty -> annotation.seqNo
        is LongitudeProperty -> annotation.seqNo
        is TimeProperty -> annotation.seqNo
        is UuidProperty -> annotation.seqNo
        is BitRangeProperty -> annotation.seqNo
        is DurationProperty -> annotation.seqNo
        else -> throw IllegalStateException("Invalid property annotation type '$annotation'.")
    }
}

fun isCollectionValueAnnotation(annotation: Annotation): Boolean {
    return when (annotation) {
        is BooleanCollectionValue -> true
        is ClassCollectionValue -> true
        is EnumCollectionValue -> true
        is FloatCollectionValue -> true
        is DoubleCollectionValue -> true
        is ByteCollectionValue -> true
        is ShortCollectionValue -> true
        is IntCollectionValue -> true
        is LongCollectionValue -> true
        is StringCollectionValue -> true
        is VarShortCollectionValue -> true
        is VarIntCollectionValue -> true
        is VarLongCollectionValue -> true
        is ByteArrayCollectionValue -> true
        is TimeEpochMsCollectionValue -> true
        is UuidCollectionValue -> true
        is BitRangeCollectionValue -> true
        is TimeInstantCollectionValue-> true
        is TimeLocalDateCollectionValue -> true
        is TimeZonedDateCollectionValue -> true
        is DurationCollectionValue -> true
        else -> false
    }
}

fun isMapKeyAnnotation(annotation: Annotation): Boolean {
    return when (annotation) {
        is BooleanMapKey -> true
        is ClassMapKey -> true
        is EnumMapKey -> true
        is FloatMapKey -> true
        is DoubleMapKey -> true
        is ByteMapKey -> true
        is ShortMapKey -> true
        is IntMapKey -> true
        is LongMapKey -> true
        is StringMapKey -> true
        is VarShortMapKey -> true
        is VarIntMapKey -> true
        is VarLongMapKey -> true
        is UuidMapKey -> true
        is TimeInstantMapKey -> true
        is TimeEpochMsMapKey -> true
        is TimeLocalDateMapKey -> true
        is TimeZonedDateMapKey -> true
        is DurationMapKey -> true
        else -> false
    }
}
