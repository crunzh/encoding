package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import java.time.Duration
import kotlin.math.abs

class DurationEncoder(private val timeResolutionMs: Int = 1000, private val maxValueMs: Long = Long.MAX_VALUE, private val canBeNegative: Boolean = false) : ValueEncoder<Duration> {
    private val bitsForMs = BitBuilder.getBitsRequired(maxValueMs / timeResolutionMs, canBeNegative)

    override fun encode(property: Duration, encoderContext: EncoderContext): BitBuilder {
        return BitBuilder(property.toMillis() / timeResolutionMs, bitsForMs, signed = canBeNegative)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Duration {
        return Duration.ofMillis(bits.next(bitsForMs).toLong(signed = canBeNegative) * timeResolutionMs)
    }

    override fun assertValid(property: Duration) {
        val durationInMs = property.toMillis()
        if (durationInMs < 0 && !canBeNegative) throw IllegalArgumentException("Property value may not be negative, but was '$durationInMs'.")
        if (abs(durationInMs) > maxValueMs) throw IllegalArgumentException("Absolute property value may not be larger than '$maxValueMs' but was '$durationInMs'.")
    }
}