package bit.crunzh.utilities.encoding.specification.annotation.collection

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class BitRangeCollectionValue(
        val bitRangeLengthVarInt: IntArray = [4, 6, 10, 16]
)