package bit.crunzh.utilities.encoding.specification.annotation.collection.map

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class UuidMapKey