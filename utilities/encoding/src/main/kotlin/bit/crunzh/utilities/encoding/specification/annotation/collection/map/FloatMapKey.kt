package bit.crunzh.utilities.encoding.specification.annotation.collection.map

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class FloatMapKey (
        val minValue: Float = 0.0f,
        val maxValue: Float,
        val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
)