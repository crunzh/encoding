package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import java.nio.ByteOrder
import kotlin.math.max

class ShortEncoder(
        private val minValue: Short = Short.MIN_VALUE,
        private val maxValue: Short = Short.MAX_VALUE,
        private val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
) : ValueEncoder<Short> {
    private val signed: Boolean
    private val bitLength: Int
    private val offsetValue: Short

    init {
        if (maxValue < minValue) throw IllegalArgumentException("maxValue cannot be less than minValue")
        val diff = maxValue - minValue
        if (diff > Short.MAX_VALUE) {
            offsetValue = 0
            signed = minValue < 0
            bitLength = max(BitBuilder.getBitsRequired(minValue, signed), BitBuilder.getBitsRequired(maxValue, signed))
        } else {
            signed = false
            offsetValue = minValue
            bitLength = BitBuilder.getBitsRequired(diff)
        }
    }

    override fun encode(property: Short, encoderContext: EncoderContext): BitBuilder {
        val offSetValue = property - offsetValue
        return BitBuilder(offSetValue, bitLength, signed, byteOrder)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Short {
        return (bits.next(bitLength).toShort(signed = signed, byteOrder = byteOrder) + offsetValue).toShort()
    }

    override fun assertValid(property: Short) {
        if (property < minValue) throw IllegalArgumentException("Property value may not be less than '$minValue' but was '$property'.")
        if (property > maxValue) throw IllegalArgumentException("Property value may not be more than '$maxValue' but was '$property'")
    }
}