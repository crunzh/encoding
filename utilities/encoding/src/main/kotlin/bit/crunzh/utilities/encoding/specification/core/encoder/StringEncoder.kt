package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.bit.BitRange
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import bit.crunzh.utilities.encoding.varint.VarIntSpecification
import java.nio.charset.Charset

class StringEncoder(
        private val charset: Charset = Charsets.UTF_8,
        private val strLengthVarIntSpec: VarIntSpecification = VarIntSpecification(4, 6, 10, 16),
        private val templateValues: List<String> = ArrayList(),
        private val maxTemplates: Int = 0,
        private val templateIgnoreCase: Boolean = false,
        private val validationRegex: Regex? = null
) : ValueEncoder<String> {
    private val strLengthEncoder = VarIntEncoder(strLengthVarIntSpec)
    private val bitsRequiredForTpmIndex = BitBuilder.getBitsRequired(maxTemplates)
    private val templatesEnabled = maxTemplates > 0

    init {
        if (maxTemplates < 0) throw IllegalArgumentException("maxTemplateValues must be >= 0.")
        if (maxTemplates > 0 && templateValues.size > maxTemplates) throw IllegalArgumentException("More templates '$templateValues' than maxTemplates '$maxTemplates'")
        if (validationRegex != null) {
            for (template in templateValues) {
                if (!validationRegex.matches(template)) throw IllegalArgumentException("Template value '$template' did not match validation RegEx ${validationRegex.pattern}.")
            }
        }
    }

    override fun encode(property: String, encoderContext: EncoderContext): BitBuilder {
        val builder = BitBuilder()
        if (templatesEnabled) {
            val templateIdx = getTemplateIndex(property)
            builder.append(templateIdx, bitsRequiredForTpmIndex)
            if (templateIdx != 0) return builder //We template != 0, we have encoded string value as template.
        }
        builder.append(property, charset, strLengthEncoder)
        return builder
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): String {
        if (templatesEnabled) {
            val templateIdx = bits.next(bitsRequiredForTpmIndex).toInt()
            if (templateIdx > 0) {
                return getTemplateValue(templateIdx)
            }
        }
        return BitRange.readString(bits, charset, strLengthEncoder)
    }

    override fun assertValid(property: String) {
        if (validationRegex != null && !validationRegex.matches(property)) {
            throw IllegalArgumentException("Invalid string value '$property', must conform to regex: '${validationRegex.pattern}'")
        }
    }

    private fun getTemplateIndex(property: String): Int {
        templateValues.forEachIndexed { index, templateValue ->
            if (templateIgnoreCase && templateValue.toUpperCase() == property.toUpperCase()) return index + 1
            if (templateValue == property) return index + 1
        }
        return 0 //0 means not a template string
    }

    private fun getTemplateValue(templateIdx: Int): String {
        if (templateIdx > maxTemplates) {
            throw IllegalArgumentException("Received template index did not correspond to a template value.")
        }
        templateValues.forEachIndexed { index, templateValue ->
            if ((templateIdx - 1) == index) return templateValue
        }
        return "" //Unknown templates is represented as the empty string.
    }
}