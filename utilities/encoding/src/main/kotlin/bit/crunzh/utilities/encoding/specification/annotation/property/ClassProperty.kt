package bit.crunzh.utilities.encoding.specification.annotation.property

import kotlin.reflect.KClass

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
/**
 * Property describing a complex property, which may be multiple sub types.
 * The class of the declared property will be used as a type if it is not abstract.
 */
annotation class ClassProperty(
        val seqNo: Int,
        /**
         * The order of the types is significant, and re-ordering them will cause in-compatibility.
         * types may not be empty if the class of the property is abstract
         */
        val subTypes: Array<KClass<*>> = [],
        /**
         * This value must be at least 1, and should be a value of 2^bits, ie. 1, 2, 4, 8, 16, 32 etc.
         * */
        val numberOfTypesToSupport: Int = 16,
        val nullUnknownSubTypes: Boolean = true //if property is nullable, any decoded unknown type ids will cause null to be assigned to property.
)