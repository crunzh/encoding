package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.position.LossyPositionCompression
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueBuilder
import kotlin.math.ceil

const val OFFSET_LATITUDE_DESCRIPTION = "Offset Latitude"
const val LATITUDE_DESCRIPTION = "Latitude"

class LatitudeEncoder(
        private val precisionInMeters: Double
) : ValueEncoder<Double> {
    private val maxBitSize = BitBuilder.getBitsRequired(ceil(LossyPositionCompression.calculateMaxLatitudeSteps(precisionInMeters)).toInt())

    override fun collectOffsetValues(property: Double, offsetValueBuilder: OffsetValueBuilder) {
        offsetValueBuilder.addLatitude(precisionInMeters, property)
    }

    override fun encode(property: Double, encoderContext: EncoderContext): BitBuilder {
        return if (encoderContext.offsetValues.usePositionOffsetEncoding) {
            val deltaLatitude = encoderContext.offsetValues.getDeltaLatitude(property, precisionInMeters)
            BitBuilder(deltaLatitude, encoderContext.offsetValues.bitsRequiredForLatitudeDelta, description = OFFSET_LATITUDE_DESCRIPTION)
        } else {
            if (encoderContext.offsetValues.lastLatitude != null) throw IllegalStateException("Offset Latitude was already set.")
            val compressedLatitude = LossyPositionCompression.compressLatitude(property, precisionInMeters)
            encoderContext.offsetValues.lastLatitude = LossyPositionCompression.decompressLatitude(compressedLatitude, precisionInMeters)
            BitBuilder(compressedLatitude, fitIntoBits = maxBitSize, description = LATITUDE_DESCRIPTION)
        }
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Double {
        return if(encoderContext.offsetValues.usePositionOffsetEncoding) {
            val deltaLatitude = bits.next(encoderContext.offsetValues.bitsRequiredForLatitudeDelta).toInt()
            val latitude = encoderContext.offsetValues.getLatitude(deltaLatitude, precisionInMeters)
            latitude
        } else {
            if(encoderContext.offsetValues.lastLatitude != null) throw IllegalStateException("Last Latitude was already set on decode.")
            val latitudeSteps = bits.next(maxBitSize).toInt()
            val latitude = LossyPositionCompression.decompressLatitude(latitudeSteps, precisionInMeters)
            encoderContext.offsetValues.lastLatitude = latitude
            latitude
        }
    }

    override fun assertValid(property: Double) {
        if (property < -90) throw IllegalArgumentException("Property value may not be smaller than '-90.0' but was '$property'.")
        if (property > 90) throw IllegalArgumentException("Property value may not be larger than '90.0' but was '$property'")
    }
}