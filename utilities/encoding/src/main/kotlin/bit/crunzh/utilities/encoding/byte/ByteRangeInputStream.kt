package bit.crunzh.utilities.encoding.byte

import java.io.InputStream
import kotlin.math.min

class ByteRangeInputStream(private val bytes: ByteArray, private val offset: Int, size: Int) : InputStream() {
    val iterator = ByteIterator(bytes, offset, size)

    override fun read(): Int {
        if (!iterator.hasNext()) {
            return -1
        }
        iterator.skip()
        return bytes[iterator.pointer + 1].toInt()
    }

    override fun read(b: ByteArray): Int {
        if (!iterator.hasNext()) {
            return -1
        }
        val bytesToRead = min(iterator.remaining(), b.size)
        iterator.skip(bytesToRead)
        bytes.copyInto(b, 0, offset + iterator.bytesRead(), bytesToRead)
        return bytesToRead
    }

    override fun read(b: ByteArray, off: Int, len: Int): Int {
        if (b.size < off + len) {
            throw IllegalArgumentException("ByteArray to read into is smaller '${b.size}' than the range specified by offset '$off' and length '$len'.")
        }
        if (!iterator.hasNext()) {
            return -1
        }
        val bytesToRead = min(iterator.remaining(), len)
        iterator.skip(bytesToRead)
        bytes.copyInto(b, 0, offset + iterator.bytesRead(), bytesToRead)
        return bytesToRead
    }

    fun readAllBytes(): ByteArray {
        if (!iterator.hasNext()) {
            return ByteArray(0)
        }
        iterator.skip(iterator.remaining())
        return iterator.next(iterator.remaining()).toByteArray()
    }

    fun readNBytes(len: Int): ByteArray {
        if (!iterator.hasNext()) {
            return ByteArray(0)
        }
        val bytesToRead = min(iterator.remaining(), len)
        iterator.skip(bytesToRead)
        return iterator.next(bytesToRead).toByteArray()
    }

    override fun skip(n: Long): Long {
        if (n < 0) {
            return 0
        }
        val bytesToSkip = min(iterator.remaining(), n.toInt())
        iterator.skip(bytesToSkip)
        return bytesToSkip.toLong()
    }

    override fun available(): Int {
        return iterator.remaining()
    }
}