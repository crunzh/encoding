package bit.crunzh.utilities.encoding.specification.core

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.bit.BitRange
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueEncoder
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValues
import kotlin.reflect.KClass
import kotlin.reflect.full.cast

class EncoderImpl(val classDescriptionCache: ClassDescriptionCache, numberOfTypesToSupport: Int = classDescriptionCache.size(), override var print: Boolean = false) : Encoder {
    private val classEncodingDescription = "Class Encoding"
    private val typeIndexCache = TypeIndexCache(classDescriptionCache.getTypes(), numberOfTypesToSupport)

    constructor(classDescriptions: List<ClassDescription<*>>, numberOfTypesToSupport: Int = classDescriptions.size, print: Boolean = false) : this(ClassDescriptionCache(classDescriptions), numberOfTypesToSupport, print)


    override fun <T : Any> encode(objectToEncode: T): BitRange {
        @Suppress("UNCHECKED_CAST") val classDescription: ClassDescription<T> = classDescriptionCache.getClassDescription(objectToEncode::class) as ClassDescription<T>
        val builder = BitBuilder()

        val typeBuilder = ClassEncoder.encodeMultiTypeHeader(objectToEncode::class, typeIndexCache)

        val offsetValueBuilder = BitBuilder()
        val offsetValues: OffsetValues
        if (classDescription.supportOffsetValues) {
            offsetValues = OffsetValueEncoder.collect(objectToEncode, classDescription)
            offsetValueBuilder.append(OffsetValueEncoder.encode(offsetValues))
        } else {
            offsetValues = OffsetValues.empty
        }

        val encoderContext = EncoderContext(offsetValues, print, objectToEncode::class.simpleName ?: "Anonymous", null)
        val classBuilder = ClassEncoder.encode(objectToEncode, classDescription, encoderContext)

        builder.append(typeBuilder).append(offsetValueBuilder).append(classBuilder)
        if (print) printRanges(builder.getBitRanges())
        return builder.toBitRange(classEncodingDescription)
    }


    override fun <T : Any> decode(bitIterator: BitIterator, type: KClass<T>): T {
        return type.cast(decode(bitIterator))
    }

    override fun decode(bitIterator: BitIterator): Any {
        val objectType = ClassEncoder.decodeMultiTypeHeader(bitIterator, typeIndexCache)
        val classDescription = classDescriptionCache.getClassDescription(objectType)

        val offsetValues = if (classDescription.supportOffsetValues) {
            OffsetValueEncoder.decode(bitIterator)
        } else {
            OffsetValues.empty
        }

        val encoderContext = EncoderContext(offsetValues, print, objectType.simpleName ?: "Anonymous", null)
        return ClassEncoder.decode(bitIterator, classDescription, encoderContext)
    }

    private fun printRanges(appendedRanges: List<BitRange>) {
        appendedRanges.forEachIndexed { index, range ->
            println("$index: $range")
        }
    }
}