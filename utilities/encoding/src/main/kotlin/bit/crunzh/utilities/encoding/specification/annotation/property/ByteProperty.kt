package bit.crunzh.utilities.encoding.specification.annotation.property

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ByteProperty(
        val seqNo: Int,
        val minValue: Byte = 0,
        val maxValue: Byte = 127
)