package bit.crunzh.utilities.encoding.tlv

import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.bit.BitRange

interface TlvEncoder<T : TlvTuple<*>> {
    fun encode(tuple: T): BitRange

    fun decode(bitsToDecode: BitIterator): T

    fun calculateSize(tuple: T): Int
}