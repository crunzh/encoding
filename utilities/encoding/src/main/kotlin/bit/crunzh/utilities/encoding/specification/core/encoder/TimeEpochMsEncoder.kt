package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueBuilder
import bit.crunzh.utilities.encoding.time.LossyTimeCompression
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

const val OFFSET_EPOCH_TIME_DESCRIPTION = "Epoch Time Offset Compressed"
const val EPOCH_TIME_DESCRIPTION = "Epoch Time"

class TimeEpochMsEncoder(
        private val timeResolutionMs: Int = 1000,
        private val minTime: String = "2020-01-01T00:00:00+00:00",
        private val maxTime: String = "2050-01-01T00:00:00+00:00"
) : ValueEncoder<Long> {

    private val minTimeMsEpoch: Long = ZonedDateTime.parse(minTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME).toInstant().toEpochMilli()
    private val maxTimeMsEpoch: Long = ZonedDateTime.parse(maxTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME).toInstant().toEpochMilli()
    private val bitsRequiredForTime: Int

    init {
        val maxTimeFragments = LossyTimeCompression.calculateMaxTimeFragments(timeResolutionMs, minTimeMsEpoch, maxTimeMsEpoch)
        bitsRequiredForTime = BitBuilder.getBitsRequired(maxTimeFragments, false)
    }

    override fun collectOffsetValues(property: Long, offsetValueBuilder: OffsetValueBuilder) {
        offsetValueBuilder.addAbsoluteTime(property, timeResolutionMs, minTimeMsEpoch, maxTimeMsEpoch)
    }

    override fun encode(property: Long, encoderContext: EncoderContext): BitBuilder {
        return if (encoderContext.offsetValues.useTimeOffsetEncoding) {
            val offsetDeltaFragments = encoderContext.offsetValues.getOffsetDeltaTime(property, timeResolutionMs, minTimeMsEpoch)
            BitBuilder(offsetDeltaFragments, encoderContext.offsetValues.bitsRequiredForTimeDelta, description = OFFSET_EPOCH_TIME_DESCRIPTION)
        } else {
            val compressedTime = LossyTimeCompression.compressTime(property, timeResolutionMs, minTimeMsEpoch)
            BitBuilder(compressedTime.timeDeltaFragments, bitsRequiredForTime, description = EPOCH_TIME_DESCRIPTION)
        }
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Long {
        return if (encoderContext.offsetValues.useTimeOffsetEncoding) {
            val deltaFragments = bits.next(encoderContext.offsetValues.bitsRequiredForTimeDelta).toLong()
            encoderContext.offsetValues.getTimeEpochMs(deltaFragments, timeResolutionMs, minTimeMsEpoch)
        } else {
            val deltaFragments = bits.next(bitsRequiredForTime).toLong()
            LossyTimeCompression.decompressTime(deltaFragments, timeResolutionMs, minTimeMsEpoch).timeEpochMs
        }
    }

    override fun assertValid(property: Long) {
        if (property < minTimeMsEpoch) throw IllegalArgumentException("Property value may not be before '$minTime' but was '${ZonedDateTime.ofInstant(Instant.ofEpochMilli(property), ZoneId.of("UTC")).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)}'.")
        if (property > maxTimeMsEpoch) throw IllegalArgumentException("Property value may not be after '$maxTime' but was '${ZonedDateTime.ofInstant(Instant.ofEpochMilli(property), ZoneId.of("UTC")).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)}'")
    }
}