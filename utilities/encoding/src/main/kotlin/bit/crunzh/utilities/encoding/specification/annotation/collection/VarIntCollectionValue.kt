package bit.crunzh.utilities.encoding.specification.annotation.collection

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class VarIntCollectionValue (
        val signed: Boolean = false,
        val varIntSpecification: IntArray = [8, 16, 24, 31],
        val byteOrder: ByteOrder = ByteOrder.LITTLE_ENDIAN
)