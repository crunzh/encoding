package bit.crunzh.utilities.encoding.specification.core

import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValues


data class EncoderContext(val offsetValues: OffsetValues, val debugMode: Boolean, private val rootTypeName: String, val propertyPath: PropertyPath? = null) {
    data class PropertyPath(val parent: PropertyPath?, val seqNo: Int, val propertyName: String) {
        fun getPath(): String {
            return if (parent != null) {
                "${parent.getPath()}.$seqNo.$propertyName"
            } else {
                "$seqNo.$propertyName"
            }
        }
    }

    fun getPropertyPath(): String {
        return if (propertyPath != null) {
            "$rootTypeName.${propertyPath.getPath()}"
        } else {
            rootTypeName
        }
    }

    fun createContext(nextProperty: PropertyDescription<*, *>): EncoderContext {
        return EncoderContext(offsetValues, debugMode, rootTypeName, PropertyPath(propertyPath, nextProperty.seqNo, nextProperty.name))
    }
}