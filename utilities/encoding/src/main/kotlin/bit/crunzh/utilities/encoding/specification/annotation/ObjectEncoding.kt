package bit.crunzh.utilities.encoding.specification.annotation

@Target(AnnotationTarget.CLASS)
@MustBeDocumented
/**
 * This annotation can be used for when object types (object type, not an instance of a class) is part of a polymorphic type hierarchy, and thus needs to be part of the subtypes which can be encoded (An object implementing an interface).
 * The object is simple though and is only encoded as a representation of the type, ignoring any state and fields which the object may hold.
 */
annotation class ObjectEncoding