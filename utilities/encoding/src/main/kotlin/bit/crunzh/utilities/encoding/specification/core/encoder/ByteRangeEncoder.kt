package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.byte.ByteRange
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import bit.crunzh.utilities.encoding.varint.VarIntSpecification

class ByteRangeEncoder(lengthVarIntSpecification: VarIntSpecification = VarIntSpecification(4, 6, 10, 16)) : ValueEncoder<ByteRange> {
    private val lengthEncoder = VarIntEncoder(lengthVarIntSpecification)

    override fun encode(property: ByteRange, encoderContext: EncoderContext): BitBuilder {
        return lengthEncoder.encode(property.size).append(property.toBitRange())
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): ByteRange {
        val length = lengthEncoder.decode(bits).toInt() * Byte.SIZE_BITS
        return bits.next(length).toByteRange()
    }
}