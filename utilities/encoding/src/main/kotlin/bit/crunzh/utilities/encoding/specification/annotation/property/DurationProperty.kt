package bit.crunzh.utilities.encoding.specification.annotation.property

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
/**
 * This annotations should only be used on LocalDateTime properties.
 * If multiple absolute time is specified in any part of the root object, including lists etc. the time will be offset compressed.
 * The encoding will likely cause precision of the time property to loose precision, given the defined time resolution.
 */
annotation class DurationProperty(
        val seqNo: Int,
        /**
         * The granularity of time represented by this property. The lower the resolution (the higher the number), the less bits it will take to encode.
         */
        val timeResolutionMs: Int = 1000,
        /**
         * The longest duration in MS it must be possible to express with this property.
         */
        val maxValue: Long = Long.MAX_VALUE,
        /**
         * Whether the duration can be a negative time span.
         */
        val canBeNegative: Boolean = false
)