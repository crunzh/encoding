package bit.crunzh.utilities.encoding.specification.core

import bit.crunzh.utilities.encoding.bit.BitBuilder
import kotlin.reflect.KClass

class TypeIndexCache(types: List<KClass<*>>, maxSupportedTypesArg: Int = types.size) {
    private val idxType = HashMap<Int, KClass<*>>()
    private val typeIdx = HashMap<KClass<*>, Int>()
    private val maxSupportedTypes: Int
    val bitsForTypes: Int
    val isMultiType: Boolean

    init {
        types.forEachIndexed { idx, type ->
            idxType[idx] = type
            typeIdx[type] = idx
        }
        bitsForTypes = BitBuilder.getBitsRequired(maxSupportedTypesArg)
        maxSupportedTypes = BitBuilder.getMaxValue(bitsForTypes).toInt()
        isMultiType = maxSupportedTypes > 1
    }

    fun getIdx(type: KClass<*>): Int {
        return typeIdx[type] ?: throw UnknownTypeException("Type ${type.simpleName} was not indexed.")
    }

    fun getType(idx: Int): KClass<*> {
        return idxType[idx] ?: throw UnknownTypeException("Idx $idx was not found.")
    }

    fun getSingleton(): KClass<*> {
        if(idxType.size != 1) throw IllegalStateException("TypeIndexCache contained more than one value. Cannot get singleton value.")
        return idxType.values.first()
    }
}