package bit.crunzh.utilities.encoding.hash

import java.math.BigInteger

object HashAlgorithmFactory {
    private val two = BigInteger.valueOf(2)
    private val prime32 = BigInteger("16777619")
    private val prime64 = BigInteger("1099511628211")
    private val prime128 = BigInteger("309485009821345068724781371")
    private val prime256 = BigInteger("374144419156711147060143317175368453031918731002211")
    private val prime512 = BigInteger("35835915874844867368919076489095108449946327955754392558399825615420669938882575126094039892345713852759")
    private val prime1024 = BigInteger("5016456510113118655434598811035278955030765345404790744303017523831112055108147451509157692220295382716162651878526895249385292291816524375083746691371804094271873160484737966720260389217684476157468082573")
    private val init32 = BigInteger("2166136261")
    private val init64 = BigInteger("14695981039346656037")
    private val init128 = BigInteger("144066263297769815596495629667062367629")
    private val init256 = BigInteger("2166131000292579580525809070709686206257048370927960142411939452252845017414719255576261")
    private val init512 = BigInteger("9659303129496669498009435400716310466090418745672637896108374329434462657994582932197716438449813051892206539805784495328239340083876191928701583869517785")
    private val init1024 = BigInteger("14197795064947621068722070641403218320880622795441933960878474914617582723252296732303717722150864096521202355549365628174669108571814760471015076148029755969804077320157692458563003215304957150157403644460363550505412711285966361610267868082893823963790439336411086884584107735010676915")
    private val mod32 = two.pow(32)
    private val mod64 = two.pow(64)
    private val mod128 = two.pow(128)
    private val mod256 = two.pow(256)
    private val mod512 = two.pow(512)
    private val mod1024 = two.pow(1024)

    enum class HashType(val outputSizeBytes: Int) {
        FNV_1_32(4),
        FNV_1A_32(4),
        FNV_1_64(8),
        FNV_1A_64(8),
        FNV_1_128(16),
        FNV_1A_128(16),
        FNV_1_256(32),
        FNV_1A_256(32),
        FNV_1_512(64),
        FNV_1A_512(64),
        FNV_1_1024(128),
        FNV_1A_1024(128)
    }

    fun createHashAlgorithm(type: HashType, seed: ByteArray = ByteArray(0)): HashAlgorithm {
        val hashAlgorithm = when (type) {
            HashType.FNV_1_32 -> Fnv1(Fnv1.FnvType.Fnv1, init32, prime32, mod32, type.outputSizeBytes)
            HashType.FNV_1A_32 -> Fnv1(Fnv1.FnvType.Fnv1a, init32, prime32, mod32, type.outputSizeBytes)
            HashType.FNV_1_64 -> Fnv1(Fnv1.FnvType.Fnv1, init64, prime64, mod64, type.outputSizeBytes)
            HashType.FNV_1A_64 -> Fnv1(Fnv1.FnvType.Fnv1a, init64, prime64, mod64, type.outputSizeBytes)
            HashType.FNV_1_128 -> Fnv1(Fnv1.FnvType.Fnv1, init128, prime128, mod128, type.outputSizeBytes)
            HashType.FNV_1A_128 -> Fnv1(Fnv1.FnvType.Fnv1a, init128, prime128, mod128, type.outputSizeBytes)
            HashType.FNV_1_256 -> Fnv1(Fnv1.FnvType.Fnv1, init256, prime256, mod256, type.outputSizeBytes)
            HashType.FNV_1A_256 -> Fnv1(Fnv1.FnvType.Fnv1a, init256, prime256, mod256, type.outputSizeBytes)
            HashType.FNV_1_512 -> Fnv1(Fnv1.FnvType.Fnv1, init512, prime512, mod512, type.outputSizeBytes)
            HashType.FNV_1A_512 -> Fnv1(Fnv1.FnvType.Fnv1a, init512, prime512, mod512, type.outputSizeBytes)
            HashType.FNV_1_1024 -> Fnv1(Fnv1.FnvType.Fnv1, init1024, prime1024, mod1024, type.outputSizeBytes)
            HashType.FNV_1A_1024 -> Fnv1(Fnv1.FnvType.Fnv1a, init1024, prime1024, mod1024, type.outputSizeBytes)
        }
        hashAlgorithm.append(seed)
        return hashAlgorithm
    }
}