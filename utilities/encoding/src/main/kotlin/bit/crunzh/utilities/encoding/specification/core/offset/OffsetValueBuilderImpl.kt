package bit.crunzh.utilities.encoding.specification.core.offset

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.position.LossyPositionCompression
import bit.crunzh.utilities.encoding.time.LossyTimeCompression
import kotlin.math.ceil

class OffsetValueBuilderImpl : OffsetValueBuilder {
    private data class AddedPosition(val latitude: Int, val longitude: Int, val precisionInMeters: Double)
    private data class AddedTime(val compressedTime: Long, val timeResolutionMs: Int, val timeOffsetEpochMs: Long, val latestSupportedTime: Long)

    private val addedPositions = ArrayList<AddedPosition>()
    private val addedAbsoluteTime = ArrayList<AddedTime>()
    private var lastLatitude: Double? = null
    private var lastLatitudeSteps: Int? = null

    fun build(): OffsetValues {
        var latMin: Int = Int.MAX_VALUE
        var latMax = 0
        var longMin: Int = Int.MAX_VALUE
        var longMax = 0
        for (position in addedPositions) {
            if (position.longitude < longMin) longMin = position.longitude
            if (position.longitude > longMax) longMax = position.longitude
            if (position.latitude < latMin) latMin = position.latitude
            if (position.latitude > latMax) latMax = position.latitude
        }
        val bitsRequiredForLatDelta = BitBuilder.getBitsRequired(latMax - latMin)
        val bitsRequiredForLongDelta = BitBuilder.getBitsRequired(longMax - longMin)

        var absTimeMin: Long = Long.MAX_VALUE
        var absTimeMax: Long = 0
        for (absTime in addedAbsoluteTime) {
            if (absTime.compressedTime < absTimeMin) absTimeMin = absTime.compressedTime
            if (absTime.compressedTime > absTimeMax) absTimeMax = absTime.compressedTime
        }
        val bitsRequiredForAbsTimeDelta = BitBuilder.getBitsRequired(absTimeMax - absTimeMin)

        val costOfOffsetEncodingTime = calculateCostOffsetTimeEncoding(addedAbsoluteTime, absTimeMin, bitsRequiredForAbsTimeDelta)
        val costOfPropertyEncodingTime = calculateCostPropertyTimeEncoding(addedAbsoluteTime)

        val costOfOffsetEncodingPositions = calculateCostOffsetPositionsEncoding(addedPositions, bitsRequiredForLatDelta, latMin, bitsRequiredForLongDelta, longMin)
        val costOfPropertyEncodingPositions = calculateCostPropertyPositionsEncoding(addedPositions)

        val timeIsEnabled = costOfOffsetEncodingTime < costOfPropertyEncodingTime
        val positionIsEnabled = costOfOffsetEncodingPositions < costOfPropertyEncodingPositions
        //TODO evaluate if its better to not do offset and set offset for type to 0
        //TODO would it be better to do var int length for all offset and delta positions
        return OffsetValues(absTimeMin, bitsRequiredForAbsTimeDelta, timeIsEnabled, latMin, bitsRequiredForLatDelta, longMin, bitsRequiredForLongDelta, positionIsEnabled)
    }

    override fun addLatitude(precisionInMeters: Double, latitude: Double) {
        setLastLatitude(latitude, precisionInMeters)
    }

    override fun addLongitude(precisionInMeters: Double, longitude: Double) {
        if (lastLatitude == null) throw IllegalStateException("Latitude has not been read before Longitude.")
        val longitudeSteps = LossyPositionCompression.compressLongitude(lastLatitude!!, longitude, precisionInMeters)
        addedPositions.add(AddedPosition(lastLatitudeSteps!!, longitudeSteps, precisionInMeters))
        lastLatitude = null
        lastLatitudeSteps = null
    }

    private fun setLastLatitude(latitude: Double, precisionInMeters: Double) {
        if (lastLatitude != null || lastLatitudeSteps != null) throw IllegalStateException("Latitude was already assigned.")
        lastLatitudeSteps = LossyPositionCompression.compressLatitude(latitude, precisionInMeters)
        lastLatitude = LossyPositionCompression.decompressLatitude(lastLatitudeSteps!!, precisionInMeters)
    }

    override fun addAbsoluteTime(timeEpochMs: Long, timeResolutionMs: Int, timeOffsetEpochMs: Long, latestSupportedTime: Long) {
        val compressedTime = LossyTimeCompression.compressTime(timeEpochMs, timeResolutionMs, timeOffsetEpochMs).timeDeltaFragments
        addedAbsoluteTime.add(AddedTime(compressedTime = compressedTime, timeResolutionMs = timeResolutionMs, timeOffsetEpochMs = timeOffsetEpochMs, latestSupportedTime = latestSupportedTime))
    }

    private fun calculateCostPropertyPositionsEncoding(positions: List<AddedPosition>): Int {
        var sizeSum = 0
        positions.forEach { position ->
            sizeSum += BitBuilder.getBitsRequired(ceil(LossyPositionCompression.calculateMaxLatitudeSteps(position.precisionInMeters)).toInt())
            sizeSum += BitBuilder.getBitsRequired(ceil(LossyPositionCompression.calculateMaxLongitudeSteps(position.precisionInMeters)).toInt())
        }
        return sizeSum
    }

    private fun calculateCostOffsetPositionsEncoding(positions: List<AddedPosition>, bitsRequiredForLatDelta: Int, latMin: Int, bitsRequiredForLongDelta: Int, longMin: Int): Int {
        val offsetSize = OffsetValueEncoder.latLongOffsetEncoder.calculateSize(latMin) + OffsetValueEncoder.latLongOffsetEncoder.calculateSize(longMin) + (OffsetValueEncoder.positionDeltaLengthBits * 2) + 2
        val deltaSize = (bitsRequiredForLatDelta + bitsRequiredForLongDelta) * positions.size
        return offsetSize + deltaSize
    }

    private fun calculateCostPropertyTimeEncoding(timeValues: List<AddedTime>): Int {
        var sizeSum = 0
        timeValues.forEach { time ->
            sizeSum += BitBuilder.getBitsRequired(LossyTimeCompression.calculateMaxTimeFragments(timeOffsetEpochMs = time.timeOffsetEpochMs, timeResolutionMs = time.timeResolutionMs, latestSupportedPointInTimeEpochMs = time.latestSupportedTime))
        }
        return sizeSum
    }

    private fun calculateCostOffsetTimeEncoding(timeValues: List<AddedTime>, time: Long, bitsRequiredForDeltaTime: Int): Int {
        val offsetSize = OffsetValueEncoder.timeOffsetEncoder.calculateSize(time) + OffsetValueEncoder.absTimeDeltaLengthBits + 2
        val deltaSize = bitsRequiredForDeltaTime * timeValues.size
        return offsetSize + deltaSize + 2
    }
}
