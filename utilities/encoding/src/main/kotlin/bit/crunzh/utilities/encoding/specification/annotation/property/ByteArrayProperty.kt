package bit.crunzh.utilities.encoding.specification.annotation.property

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ByteArrayProperty(
        val seqNo: Int,
        val byteArrayLengthVarInt: IntArray = [4, 6, 10, 16]
)