package bit.crunzh.utilities.encoding.specification.annotation.collection

import java.time.LocalDateTime

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
/**
 * This annotations should only be used on [LocalDateTime] typed properties
 * If multiple absolute time is specified in any part of the root object, including lists etc. the time will be offset compressed.
 * The encoding will likely cause precision of the time property to loose precision, given the defined time resolution.
 */
annotation class TimeLocalDateCollectionValue(
        /**
         * The granularity of time represented by this property. The lower the resolution (the higher the number), the less bits it will take to encode.
         */
        val timeResolutionMs: Int = 1000,
        /**
         * Time offset, the earliest time possible to represent by this property. The closer the offset time is to the time to encode, the less bits it will take.
         */
        val minTime: String = "2020-01-01T00:00:00+00:00",
        /**
         * Latest supported point in time for this property.
         * The smaller interval between offset and latest point in time, the smaller it can be encoded.
         * Time offset specified as ISO_OFFSET_DATE_TIME
         */
        val maxTime: String = "2050-01-01T00:00:00+00:00"
)