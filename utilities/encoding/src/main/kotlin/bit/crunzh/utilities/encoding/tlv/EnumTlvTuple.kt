package bit.crunzh.utilities.encoding.tlv

class EnumTlvTuple<T : Enum<*>>(override val type: T, override val value: ByteArray) : TlvTuple<T> {
    override val length = value.size
}