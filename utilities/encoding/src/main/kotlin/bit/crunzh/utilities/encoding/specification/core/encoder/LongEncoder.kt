package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import java.math.BigInteger
import java.nio.ByteOrder
import kotlin.math.max

class LongEncoder(
        private val minValue: Long = Long.MIN_VALUE,
        private val maxValue: Long = Long.MAX_VALUE,
        private val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
) : ValueEncoder<Long> {
    private val signed: Boolean
    private val bitLength: Int
    private val offsetValue: Long

    init {
        if (maxValue < minValue) throw IllegalArgumentException("maxValue cannot be less than minValue")
        val diff: BigInteger = BigInteger.valueOf(maxValue).minus(BigInteger.valueOf(minValue))
        if (diff > BigInteger.valueOf(Long.MAX_VALUE)) {
            offsetValue = 0
            signed = minValue < 0
            bitLength = max(BitBuilder.getBitsRequired(minValue, signed), BitBuilder.getBitsRequired(maxValue, signed))
        } else {
            signed = false
            offsetValue = minValue
            bitLength = BitBuilder.getBitsRequired(diff)
        }
    }

    override fun encode(property: Long, encoderContext: EncoderContext): BitBuilder {
        val offSetValue = property - offsetValue
        return BitBuilder(offSetValue, bitLength, signed, byteOrder)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Long {
        return (bits.next(bitLength).toLong(signed = signed, byteOrder = byteOrder) + offsetValue)
    }

    override fun assertValid(property: Long) {
        if (property < minValue) throw IllegalArgumentException("Property value may not be less than '$minValue' but was '$property'.")
        if (property > maxValue) throw IllegalArgumentException("Property value may not be more than '$maxValue' but was '$property'")
    }
}