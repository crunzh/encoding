package bit.crunzh.utilities.encoding.byte

import bit.crunzh.utilities.encoding.bit.BitBuilder
import java.io.OutputStream

class BitBuilderByteRange(private val bitBuilder: BitBuilder, override val description: String?) : ByteBuilderRange {
    override val size: Int
        get() = bitBuilder.sizeBytes()

    override fun toByteArray(): ByteArray {
        return bitBuilder.toByteArray()
    }

    override fun writeToOutputStream(toWriteTo: OutputStream) {
        toWriteTo.write(bitBuilder.toByteArray())
    }

    override fun iterator(): ByteIterator {
        return ByteIterator(bitBuilder.toByteArray())
    }

    override fun toString(): String {
        return "BitBuilderByteRange(bitBuilder=$bitBuilder, description=$description, size=$size)"
    }
}