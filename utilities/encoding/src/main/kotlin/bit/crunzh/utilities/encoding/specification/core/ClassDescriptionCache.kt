package bit.crunzh.utilities.encoding.specification.core

import kotlin.reflect.KClass

class ClassDescriptionCache(rootClassDescriptions: List<ClassDescription<*>> = emptyList()) {
    private val typeDescriptionMap = HashMap<KClass<*>, ClassDescription<*>>()
    fun size(): Int {
        return typeDescriptionMap.size
    }

    init {
        rootClassDescriptions.forEach { descriptor ->
            addClassDescriptor(descriptor)
        }
    }

    fun addClassDescriptor(classDescription: ClassDescription<*>) {
        typeDescriptionMap[classDescription.type] = classDescription
    }

    fun <T : Any> getClassDescription(type: KClass<T>): ClassDescription<T> {
        val description = typeDescriptionMap[type] ?: throw UnknownTypeException("Unknown type '${type.simpleName}', it is not registered in encoder.")
        if (description.type != type) {
            throw IllegalArgumentException("ClassDescriptor '$description', is registered under '${type.simpleName}' but has type '${description.type}'.")
        }
        @Suppress("UNCHECKED_CAST")
        return description as ClassDescription<T>
    }

    fun getTypes(): List<KClass<*>> {
        return ArrayList(typeDescriptionMap.keys)
    }
}