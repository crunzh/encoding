package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import java.util.*

class UuidEncoder() : ValueEncoder<UUID> {

    override fun encode(property: UUID, encoderContext: EncoderContext): BitBuilder {
        val builder = BitBuilder()
        builder.append(property.leastSignificantBits, signed = true)
        builder.append(property.mostSignificantBits, signed = true)
        return builder
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): UUID {
        val leastSignificantBits = bits.next(Long.SIZE_BITS).toLong(signed = true)
        val mostSignificantBits = bits.next(Long.SIZE_BITS).toLong(signed = true)
        return UUID(mostSignificantBits, leastSignificantBits)
    }
}