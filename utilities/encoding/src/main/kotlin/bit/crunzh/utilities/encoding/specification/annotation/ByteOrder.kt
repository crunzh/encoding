package bit.crunzh.utilities.encoding.specification.annotation

enum class ByteOrder(val nioByteOrder: java.nio.ByteOrder) {
    BIG_ENDIAN(java.nio.ByteOrder.BIG_ENDIAN),
    LITTLE_ENDIAN(java.nio.ByteOrder.LITTLE_ENDIAN)
}