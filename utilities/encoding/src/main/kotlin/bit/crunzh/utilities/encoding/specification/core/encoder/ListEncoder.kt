package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.specification.core.UnknownTypeException
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueBuilder
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import bit.crunzh.utilities.encoding.varint.VarIntSpecification

class ListEncoder<ListElementType : Any>(
        private val elementEncoder: ValueEncoder<ListElementType>,
        listLengthVarIntSpecification: VarIntSpecification
) : ValueEncoder<List<ListElementType>> {
    private val listElementLengthEncoder = VarIntEncoder(listLengthVarIntSpecification)

    override fun collectOffsetValues(property: List<ListElementType>, offsetValueBuilder: OffsetValueBuilder) {
        for (element in property) {
            elementEncoder.collectOffsetValues(element, offsetValueBuilder)
        }
    }

    override fun encode(property: List<ListElementType>, encoderContext: EncoderContext): BitBuilder {
        val builder = listElementLengthEncoder.encode(property.size)
        for (element in property) {
            builder.append(elementEncoder.encode(element, encoderContext))
        }
        return builder
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): List<ListElementType> {
        val listSize = listElementLengthEncoder.decode(bits).toInt()
        val list = ArrayList<ListElementType>(listSize)
        for (i in 0 until listSize) {
            try {
                val element = elementEncoder.decode(bits, encoderContext)
                list.add(element)
            } catch (e: UnknownTypeException) {
                continue //An encoder may be expanded with new unknown types. Unknown key or value types will cause the entire key-value pair to be skipped.
            }
        }
        return list
    }

    override fun assertValid(property: List<ListElementType>) {
        for(element in property) {
            elementEncoder.assertValid(element)
        }
    }
}