package bit.crunzh.utilities.encoding.time

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.ZonedDateTime
import kotlin.math.ceil

class LossyTimeCompression {
    data class CompressedTime(val timeDeltaFragments: Long, val timeResolutionMs: Int, val timeOffsetEpochMs: Long)
    data class UncompressedTime(val timeEpochMs: Long, val timeResolutionMs: Int, val timeOffsetEpochMs: Long)
    companion object {
        fun compressTime(time: UncompressedTime): CompressedTime {
            val deltaTime = time.timeEpochMs - time.timeOffsetEpochMs
            return CompressedTime(timeDeltaFragments = deltaTime / time.timeResolutionMs, timeResolutionMs = time.timeResolutionMs, timeOffsetEpochMs = time.timeOffsetEpochMs)
        }

        fun compressTime(timeEpochMs: Long, timeResolutionMs: Int, timeOffsetEpochMs: Long): CompressedTime = compressTime(UncompressedTime(timeEpochMs, timeResolutionMs, timeOffsetEpochMs))

        fun decompressTime(time: CompressedTime): UncompressedTime {
            val deltaTime = time.timeDeltaFragments * time.timeResolutionMs
            return UncompressedTime(timeEpochMs = deltaTime + time.timeOffsetEpochMs, timeResolutionMs = time.timeResolutionMs, timeOffsetEpochMs = time.timeOffsetEpochMs)
        }

        fun decompressTime(timeDeltaFragments: Long, timeResolutionMs: Int, timeOffsetEpochMs: Long): UncompressedTime = decompressTime(CompressedTime(timeDeltaFragments, timeResolutionMs, timeOffsetEpochMs))

        fun removePrecision(time: UncompressedTime): UncompressedTime {
            return decompressTime(compressTime(time).timeDeltaFragments, time.timeResolutionMs, time.timeOffsetEpochMs)
        }

        fun removePrecision(timeEpochMs: Long, timeResolutionMs: Int, timeOffsetEpochMs: Long) = removePrecision(UncompressedTime(timeEpochMs, timeResolutionMs, timeOffsetEpochMs))

        fun calculateMaxTimeFragments(timeResolutionMs: Int, timeOffsetEpochMs: Long, latestSupportedPointInTimeEpochMs: Long): Int {
            if (latestSupportedPointInTimeEpochMs < timeOffsetEpochMs) throw IllegalArgumentException("latest point in time must be after offset")
            val interval = latestSupportedPointInTimeEpochMs - timeOffsetEpochMs
            return ceil(interval.toDouble() / timeResolutionMs).toInt()
        }
    }
}

fun Instant.removePrecision(timeResolutionMs: Int, timeOffsetEpochMs: Long): Instant {
    val epochMs = this.toEpochMilli()
    val result = LossyTimeCompression.removePrecision(epochMs, timeResolutionMs, timeOffsetEpochMs)
    return Instant.ofEpochMilli(result.timeEpochMs)
}

fun LocalDateTime.removePrecision(timeResolutionMs: Int, timeOffsetEpochMs: Long): Instant {
    return this.toInstant(ZoneOffset.UTC).removePrecision(timeResolutionMs, timeOffsetEpochMs)
}

fun ZonedDateTime.removePrecision(timeResolutionMs: Int, timeOffsetEpochMs: Long): Instant {
    return this.toInstant().removePrecision(timeResolutionMs, timeOffsetEpochMs)
}