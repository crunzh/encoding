package bit.crunzh.utilities.encoding.hash

import bit.crunzh.utilities.encoding.byte.ByteRange
import java.io.InputStream

interface HashAlgorithm {
    fun append(inputStream: InputStream): HashAlgorithm
    fun append(bytes: ByteIterator): HashAlgorithm
    fun append(bytes: ByteRange): HashAlgorithm
    fun append(bytes: ByteArray): HashAlgorithm
    fun getHash(): ByteArray
}
