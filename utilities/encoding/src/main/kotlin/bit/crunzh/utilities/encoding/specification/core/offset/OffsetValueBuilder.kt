package bit.crunzh.utilities.encoding.specification.core.offset

interface OffsetValueBuilder {
    fun addLatitude(precisionInMeters: Double, latitude: Double)
    fun addLongitude(precisionInMeters: Double, longitude: Double)
    fun addAbsoluteTime(timeEpochMs: Long, timeResolutionMs: Int, timeOffsetEpochMs: Long, latestSupportedTime: Long)
}