package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.varint.VarIntEncoder as VarIntEncoderFw
import bit.crunzh.utilities.encoding.varint.VarIntSpecification
import java.nio.ByteOrder

class VarIntEncoder (
        varIntSpecification: VarIntSpecification,
        byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
        ) : ValueEncoder<Int> {
    private val encoder = VarIntEncoderFw(varIntSpecification, byteOrder)

    override fun encode(property: Int, encoderContext: EncoderContext): BitBuilder {
        return encoder.encode(property)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Int {
        return encoder.decode(bits).toInt()
    }
}