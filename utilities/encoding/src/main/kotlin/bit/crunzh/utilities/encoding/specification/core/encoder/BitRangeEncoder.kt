package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.bit.BitRange
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import bit.crunzh.utilities.encoding.varint.VarIntSpecification

class BitRangeEncoder(lengthVarIntSpecification: VarIntSpecification = VarIntSpecification(4, 6, 10, 16)) : ValueEncoder<BitRange> {
    private val lengthEncoder = VarIntEncoder(lengthVarIntSpecification)

    override fun encode(property: BitRange, encoderContext: EncoderContext): BitBuilder {
        val builder = BitBuilder()
        builder.append(lengthEncoder.encode(property.size()))
        builder.append(property)
        return builder
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): BitRange {
        val length = lengthEncoder.decode(bits).toInt()
        return bits.next(length)
    }
}