package bit.crunzh.utilities.encoding.specification.reflection

import bit.crunzh.utilities.encoding.EnumCache
import bit.crunzh.utilities.encoding.specification.core.ClassDescription
import bit.crunzh.utilities.encoding.specification.core.PropertyDescription
import bit.crunzh.utilities.encoding.specification.core.PropertyValues
import bit.crunzh.utilities.encoding.specification.core.encoder.EnumEncoder
import kotlin.reflect.KClass

private const val ENUM_PROPERTY_NAME = "enumValue"

class ReflectionEnumClassDescription<T : Enum<*>>(override val type: KClass<T>, maxEnumOrdinal: Int, defaultOrdinal: Int = -1) : ClassDescription<T> {
    private val enumValuePropertyDescription: List<PropertyDescription<*, T>>

    init {
        val enumCache = bit.crunzh.utilities.encoding.EnumCache()
        val enumValues = bit.crunzh.utilities.encoding.EnumCache.getNoEnumValues(type)
        var maxEnumValues = maxEnumOrdinal
        if (maxEnumValues == -1) {
            maxEnumValues = enumValues
        }
        if (maxEnumValues < enumValues) throw IllegalStateException("Enum ${type.simpleName} has more values than the specified max '${maxEnumOrdinal}'")
        val encoder = EnumEncoder(
                maxEnumValues,
                defaultOrdinal,
                { ordinal, default -> enumCache.getEnumValueOrDefault2(type, ordinal, default) },
                { enum -> enum.ordinal }
        )
        enumValuePropertyDescription = listOf(
                PropertyDescription(
                        ENUM_PROPERTY_NAME,
                        1,
                        false,
                        type,
                        encoder
                ) { enum ->
                    enum
                }
        )
    }

    override val isExtendable: Boolean
        get() = false
    override val supportOffsetValues: Boolean
        get() = false
    override val propertyDescriptors: List<PropertyDescription<*, T>>
        get() = enumValuePropertyDescription

    override fun create(constParams: PropertyValues): T {
        return constParams.getValue(ENUM_PROPERTY_NAME)
    }
}
