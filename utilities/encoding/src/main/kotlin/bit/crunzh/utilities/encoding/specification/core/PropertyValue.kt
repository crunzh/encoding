package bit.crunzh.utilities.encoding.specification.core

data class PropertyValue<T : Any>(val seqNo: Int, val propertyName: String, val propertyValue: T?)
