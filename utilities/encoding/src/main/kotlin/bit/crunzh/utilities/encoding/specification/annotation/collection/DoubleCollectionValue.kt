package bit.crunzh.utilities.encoding.specification.annotation.collection

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class DoubleCollectionValue (
        val minValue: Double = 0.0,
        val maxValue: Double,
        val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
)