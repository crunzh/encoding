package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import java.nio.ByteOrder

class FloatEncoder(
        private val minValue: Float = Float.MIN_VALUE,
        private val maxValue: Float = Float.MAX_VALUE,
        private val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
) : ValueEncoder<Float> {

    override fun encode(property: Float, encoderContext: EncoderContext): BitBuilder {
        return BitBuilder(property, byteOrder)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Float {
        return bits.next(Float.SIZE_BITS).toFloat(byteOrder = byteOrder)
    }

    override fun assertValid(property: Float) {
        if (property < minValue) throw IllegalArgumentException("Property value may not be less than '$minValue' but was '$property'.")
        if (property > maxValue) throw IllegalArgumentException("Property value may not me more than '$maxValue' but was '$property'")
    }
}