package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueBuilder
import bit.crunzh.utilities.encoding.specification.core.*
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueEncoder

class ClassPropertyEncoder<ClassPropertyType : Any>(
        /**
         * TypeIndex cache, mapping type and index and describing room for future expansions.
         * Changing the index-type mapping or number of supported types (if it changes the number of bits used) will break backwards compatibility.
         */
        private val typeIndexCache: TypeIndexCache,
        /**
         * ClassDescriptionCache must match the types defined in [typeIndexCache]
         */
        private val classDescriptionCache: ClassDescriptionCache
) : ValueEncoder<ClassPropertyType> {

    override fun collectOffsetValues(property: ClassPropertyType, offsetValueBuilder: OffsetValueBuilder) {
        @Suppress("UNCHECKED_CAST")
        val classDescription: ClassDescription<ClassPropertyType> = classDescriptionCache.getClassDescription(property::class) as ClassDescription<ClassPropertyType>
        classDescription.propertyDescriptors.forEach { propertyDescriptor ->
            OffsetValueEncoder.collectPropertyOffsetValues(property, propertyDescriptor, offsetValueBuilder)
        }
    }

    override fun encode(property: ClassPropertyType, encoderContext: EncoderContext): BitBuilder {
        val builder = BitBuilder()
        builder.append(ClassEncoder.encodeMultiTypeHeader(property::class, typeIndexCache))
        @Suppress("UNCHECKED_CAST") val classDescriptor = classDescriptionCache.getClassDescription(property::class) as ClassDescription<ClassPropertyType>
        return builder.append(ClassEncoder.encode(property, classDescriptor, encoderContext))
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): ClassPropertyType {
        val typeClass = ClassEncoder.decodeMultiTypeHeader(bits, typeIndexCache)
        val classDescription = classDescriptionCache.getClassDescription(typeClass)
        @Suppress("UNCHECKED_CAST")
        return ClassEncoder.decode(bits, classDescription, encoderContext) as ClassPropertyType
    }
}