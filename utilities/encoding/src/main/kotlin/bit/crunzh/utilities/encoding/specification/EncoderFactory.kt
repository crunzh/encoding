package bit.crunzh.utilities.encoding.specification

import bit.crunzh.utilities.encoding.specification.core.ClassDescription
import bit.crunzh.utilities.encoding.specification.core.Encoder
import bit.crunzh.utilities.encoding.specification.core.EncoderImpl
import bit.crunzh.utilities.encoding.specification.reflection.ReflectionClassDescriptionFactory
import kotlin.reflect.KClass

object EncoderFactory {
    fun createEncoder(vararg types: KClass<*>, printEncoding: Boolean = false, numberOfTypesToSupport: Int = 16): Encoder {
        return EncoderImpl(numberOfTypesToSupport = numberOfTypesToSupport, classDescriptions = ReflectionClassDescriptionFactory.createClassDescriptions(listOf(*types)), print = printEncoding)
    }

    fun createEncoder(vararg descriptions: ClassDescription<*>, printEncoding: Boolean = false, numberOfTypesToSupport: Int = 16): Encoder {
        return EncoderImpl(numberOfTypesToSupport = numberOfTypesToSupport, classDescriptions = listOf(*descriptions), print = printEncoding)
    }
}