package bit.crunzh.utilities.encoding.byte

import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.bit.BitRange
import bit.crunzh.utilities.encoding.bit.ByteArrayBitIterator
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*

data class ByteRange(private val bytes: ByteArray, private val offset: Int = 0, override val size: Int = bytes.size - offset, override var description: String? = null) : ByteBuilderRange, Iterable<Byte> {
    var toByteArrayCache: ByteArray? = null

    init {
        if (size < 0 || size > bytes.size) {
            throw IllegalArgumentException("size must be larger than 0 but smaller than bytes.size. It was '$size' but should be [0-${bytes.size}]")
        }
        if (offset < 0 || offset + size > bytes.size) {
            throw IllegalArgumentException("offset must be 0 or larger, but smaller than bytes - size. It was '$offset' but should be [0-${bytes.size - size}[")
        }
    }

    override fun toByteArray(): ByteArray {
        if (offset == 0 && size == bytes.size) {
            return bytes
        }
        val tempValue = toByteArrayCache
        return if(tempValue == null) {
            val newValue = bytes.copyOfRange(offset, offset + size)
            toByteArrayCache = newValue
            newValue
        } else tempValue
    }

    fun subByteRange(start: Int = 0, length: Int = size - start): ByteRange {
        if (start < 0) throw IllegalArgumentException("Start must be larger than or equal to zero, but was $start")
        if (length < 1) throw IllegalArgumentException("Length must be larger than 0, but was $length")
        if (start == 0 && length == size) return this
        if (start + length > size) throw IllegalArgumentException("The sub ByteRange must be within the confined limits of this ByteRange. start: '$start', length: '$length', while this ByteRange has size: '$size'")
        return ByteRange(bytes, offset + start, length, description)
    }

    fun toByteBuffer(): ByteBuffer {
        return ByteBuffer.wrap(toByteArray())
    }

    fun toBitRange(): BitRange {
        return BitRange(bytes, offset * Byte.SIZE_BITS, size * Byte.SIZE_BITS)
    }

    fun toByte(): Byte {
        if (size < Byte.SIZE_BYTES) {
            throw IllegalArgumentException("size must be ${Byte.SIZE_BYTES}")
        }
        return bytes[offset]
    }

    fun toShort(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Short {
        if (size != Short.SIZE_BYTES) {
            throw IllegalArgumentException("size must be ${Short.SIZE_BYTES}")
        }
        val bb = ByteBuffer.wrap(bytes, offset, size).order(byteOrder)
        return bb.short
    }

    fun toInt(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Int {
        if (size != Int.SIZE_BYTES) {
            throw IllegalArgumentException("size must be ${Int.SIZE_BYTES}")
        }
        val bb = ByteBuffer.wrap(bytes, offset, size).order(byteOrder)
        return bb.int
    }

    fun toLong(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Long {
        if (size != Long.SIZE_BYTES) {
            throw IllegalArgumentException("size must be ${Long.SIZE_BYTES}")
        }

        val bb = ByteBuffer.wrap(bytes, offset, size).order(byteOrder)
        return bb.long
    }

    fun toFloat(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Float {
        if (size != Float.SIZE_BYTES) {
            throw IllegalArgumentException("size must be ${Float.SIZE_BYTES}")
        }
        val bb = ByteBuffer.wrap(bytes, offset, size).order(byteOrder)
        return bb.float
    }

    fun toDouble(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Double {
        if (size != Double.SIZE_BYTES) {
            throw IllegalArgumentException("size must be ${Double.SIZE_BYTES}")
        }
        val bb = ByteBuffer.wrap(bytes, offset, size).order(byteOrder)
        return bb.double
    }

    override fun iterator(): ByteIterator {
        return ByteIterator(bytes, offset, size)
    }

    fun bitIterator(): BitIterator {
        return ByteArrayBitIterator(bytes, offset * Byte.SIZE_BITS, size * Byte.SIZE_BITS)
    }

    override fun writeToOutputStream(toWriteTo: OutputStream) {
        toWriteTo.write(bytes, offset, size)
    }

    fun toInputStream(): InputStream {
        return ByteRangeInputStream(bytes, offset, size)
    }

    fun printBinaryString(maxLength: Int = 200): String {
        val bitSetOfRange = BitSet.valueOf(bytes)
        var binString = ""
        val bitOffset = offset * Byte.SIZE_BITS
        val bitSize = size * Byte.SIZE_BITS
        for (idx in 0 until bitSize) {
            binString += if (bitSetOfRange.get(idx + bitOffset)) {
                "1"
            } else {
                "0"
            }
            if (binString.length > maxLength) {
                break
            }
            if ((idx + 1) % Byte.SIZE_BITS == 0) {
                binString += " "
            }
        }
        return binString
    }

    fun printHexString(maxLength: Int = Int.MAX_VALUE): String {
        var hexString = ""
        val endIdx = offset + size
        for (byteIdx in offset until endIdx) {
            hexString += String.format("%02X", bytes[byteIdx])
            if (hexString.length > maxLength) {
                break
            }
            if (byteIdx < (endIdx - 1)) {
                hexString += "-"
            }
        }
        return hexString
    }

    fun subSet(subsetOffset: Int = 0, subsetSize: Int): ByteRange {
        if (subsetSize < 1 || subsetSize > size) {
            throw IllegalArgumentException("bitLength must be in the range [1-$size]")
        }
        if (subsetOffset > size || subsetOffset + subsetSize > size) {
            throw IllegalArgumentException("bitOffset must be in the range [0-$size[")
        }
        return ByteRange(bytes, offset + subsetOffset, subsetSize)
    }

    override fun toString(): String {
        return "$description, $size bytes, '${printHexString(200)}'"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ByteRange

        if (size != other.size) return false
        return toByteArray().contentEquals(other.toByteArray())
    }

    override fun hashCode(): Int {
        return toByteArray().contentHashCode()
    }

    fun copyInto(destination: ByteArray, destinationOffset: Int = 0) {
        if (destination.size < destinationOffset + size) throw IllegalArgumentException("It is not possible to copy this ByteRange of size '$size' into destination ByteArray size '${destination.size}' using destinationOffset '$destinationOffset'.")
        this.forEachIndexed { index, byte ->
            destination[destinationOffset + index] = byte
        }
    }

}