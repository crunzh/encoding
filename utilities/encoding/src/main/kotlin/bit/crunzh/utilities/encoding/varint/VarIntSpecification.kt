package bit.crunzh.utilities.encoding.varint

import bit.crunzh.utilities.encoding.bit.BitBuilder

class VarIntSpecification(vararg stepSizes: Int, val signed: Boolean = false) {
    val steps: List<SizeStep> = initializeSteps(stepSizes)
    val headerSize: Int = BitBuilder.getBitsRequired(stepSizes.size - 1)
    val minValue: Long = steps[steps.lastIndex].minStepValue
    val maxValue: Long = steps[steps.lastIndex].maxStepValue
    val minSize = headerSize + steps[0].bitSize
    val maxSize = headerSize + steps[steps.lastIndex].bitSize


    private fun initializeSteps(stepSizes: IntArray): List<SizeStep> {
        if (stepSizes.isEmpty()) {
            throw IllegalArgumentException("VarIntSpecification must have at least one stepSize.")
        }
        if (stepSizes.size !in intArrayOf(1, 2, 4, 8, 16, 32)) {
            throw IllegalArgumentException("VarIntSpecification must have 1, 2, 4, 8, 16 or 32 steps.")
        }
        val stepsList = ArrayList<SizeStep>()
        var prevValue = 0
        for (i in stepSizes.indices) {
            val stepSize = stepSizes[i]
            if (stepSize <= prevValue) {
                throw IllegalArgumentException("stepSizes must be increasing in size for each index.")
            }
            stepsList.add(SizeStep(i, stepSize, signed))
            prevValue = stepSize
        }
        return stepsList
    }

    fun getSizeStep(value: Number): SizeStep {
        val longValue = value.toLong()
        if (longValue < minValue || longValue > maxValue) {
            throw IllegalArgumentException("This specification only supports values in the range [$minValue-$maxValue], but was asked for a stepSize for '$value'.")
        }
        for (sizeStep in steps) {
            if (longValue >= sizeStep.minStepValue && longValue <= sizeStep.maxStepValue) {
                return sizeStep
            }
        }
        throw IllegalArgumentException("Unable to find an appropriate SizeStep.")
    }

    data class SizeStep(val stepIndex: Int, val bitSize: Int, private val signed: Boolean) {
        val maxStepValue = BitBuilder.getMaxValue(bitSize, signed)
        val minStepValue = BitBuilder.getMinValue(bitSize, signed)
    }
}
