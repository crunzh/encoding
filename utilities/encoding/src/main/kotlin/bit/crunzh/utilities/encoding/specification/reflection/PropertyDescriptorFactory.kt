package bit.crunzh.utilities.encoding.specification.reflection

import bit.crunzh.utilities.encoding.EnumCache
import bit.crunzh.utilities.encoding.bit.BitRange
import bit.crunzh.utilities.encoding.byte.ByteRange
import bit.crunzh.utilities.encoding.specification.annotation.position.PositionClass
import bit.crunzh.utilities.encoding.specification.annotation.property.*
import bit.crunzh.utilities.encoding.specification.core.*
import bit.crunzh.utilities.encoding.specification.core.encoder.*
import bit.crunzh.utilities.encoding.varint.VarIntSpecification
import java.nio.charset.Charset
import java.time.Duration
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.jvm.jvmErasure

object PropertyDescriptorFactory {
    fun <T : Any> createPropertyDescriptor(parentType: KClass<*>, property: KProperty1<T, *>, positionAnnotation: PositionClass? = null, classDescriptionCache: ClassDescriptionCache): PropertyDescription<*, T> {
        return when (val annotation = getPropertyAnnotation(property.annotations)) {
            is BooleanProperty -> createBooleanDescriptor(parentType, annotation, property)
            is ClassProperty -> createClassDescriptor(annotation, classDescriptionCache, property)
            is VarShortProperty -> createVarShortDescriptor(parentType, annotation, property)
            is VarIntProperty -> createVarIntDescriptor(parentType, annotation, property)
            is VarLongProperty -> createVarLongDescriptor(parentType, annotation, property)
            is ByteProperty -> createByteDescriptor(parentType, annotation, property)
            is ShortProperty -> createShortDescriptor(parentType, annotation, property)
            is IntProperty -> createIntDescriptor(parentType, annotation, property)
            is LongProperty -> createLongDescriptor(parentType, annotation, property)
            is FloatProperty -> createFloatDescriptor(parentType, annotation, property)
            is DoubleProperty -> createDoubleDescriptor(parentType, annotation, property)
            is ListProperty -> createListDescriptor(parentType, annotation, classDescriptionCache, property)
            is SetProperty -> createSetDescriptor(parentType, annotation, classDescriptionCache, property)
            is MapProperty -> createMapDescriptor(parentType, annotation, classDescriptionCache, property)
            is StringProperty -> createStringDescriptor(parentType, annotation, property)
            is EnumProperty -> createEnumDescriptor(parentType, annotation, property)
            is ByteStoreProperty -> createByteStoreDescriptor(parentType, annotation, property)
            is ByteStoreConstantSizeProperty -> createByteStoreConstantSizeDescriptor(parentType, annotation, property)
            is LatitudeProperty -> createLatitudeDescriptor(parentType, annotation, positionAnnotation, property)
            is LongitudeProperty -> createLongitudeDescriptor(parentType, annotation, positionAnnotation, property)
            is TimeProperty -> createTimeDescriptor(parentType, annotation, property)
            is UuidProperty -> createUuidDescriptor(parentType, annotation, property)
            is BitRangeProperty -> createBitRangeDescriptor(parentType, annotation, property)
            is DurationProperty -> createDurationDescriptor(parentType, annotation, property)
            else -> throw IllegalStateException("Property annotation '@${annotation::class.simpleName}' was not handled.")
        }
    }

    private fun <T : Any> createDurationDescriptor(parentType: KClass<*>, annotation: DurationProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, Duration::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Duration::class,
                DurationEncoder(annotation.timeResolutionMs, annotation.maxValue, annotation.canBeNegative)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createVarShortDescriptor(parentType: KClass<*>, annotation: VarShortProperty, property: KProperty1<T, *>): PropertyDescription<Short, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, Short::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Short::class,
                VarShortEncoder(VarIntSpecification(*annotation.varIntSpecification, signed = annotation.signed), annotation.byteOrder.nioByteOrder)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createVarIntDescriptor(parentType: KClass<*>, annotation: VarIntProperty, property: KProperty1<T, *>): PropertyDescription<Int, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, Int::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Int::class,
                VarIntEncoder(VarIntSpecification(*annotation.varIntSpecification, signed = annotation.signed), annotation.byteOrder.nioByteOrder)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createVarLongDescriptor(parentType: KClass<*>, annotation: VarLongProperty, property: KProperty1<T, *>): PropertyDescription<Long, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, Long::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Long::class,
                VarLongEncoder(VarIntSpecification(*annotation.varIntSpecification, signed = annotation.signed), annotation.byteOrder.nioByteOrder)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createByteDescriptor(parentType: KClass<*>, annotation: ByteProperty, property: KProperty1<T, *>): PropertyDescription<Byte, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, Byte::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Byte::class,
                ByteEncoder(annotation.minValue, annotation.maxValue)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createShortDescriptor(parentType: KClass<*>, annotation: ShortProperty, property: KProperty1<T, *>): PropertyDescription<Short, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, Short::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Short::class,
                ShortEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createIntDescriptor(parentType: KClass<*>, annotation: IntProperty, property: KProperty1<T, *>): PropertyDescription<Int, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, Int::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Int::class,
                IntEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createLongDescriptor(parentType: KClass<*>, annotation: LongProperty, property: KProperty1<T, *>): PropertyDescription<Long, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, Long::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Long::class,
                LongEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createFloatDescriptor(parentType: KClass<*>, annotation: FloatProperty, property: KProperty1<T, *>): PropertyDescription<Float, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, Float::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Float::class,
                FloatEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createDoubleDescriptor(parentType: KClass<*>, annotation: DoubleProperty, property: KProperty1<T, *>): PropertyDescription<Double, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, Double::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Double::class,
                DoubleEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createListDescriptor(parentType: KClass<*>, annotation: ListProperty, classDescriptionCache: ClassDescriptionCache, property: KProperty1<T, *>): PropertyDescription<out List<Any>, T> {
        val castProperty = SharedValidation.assertPropertyListType(parentType, property)
        val listValueEncoder = CollectionElementEncoderFactory.createCollectionEncoder(castProperty, classDescriptionCache)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                List::class,
                ListEncoder(listValueEncoder, VarIntSpecification(*annotation.listLengthVarInt))
        ) { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createSetDescriptor(parentType: KClass<*>, annotation: SetProperty, classDescriptionCache: ClassDescriptionCache, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertySetType(parentType, property)
        val setValueEncoder = CollectionElementEncoderFactory.createCollectionEncoder(castProperty, classDescriptionCache)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Set::class,
                SetEncoder(setValueEncoder, VarIntSpecification(*annotation.setSizeVarInt))
        ) { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createMapDescriptor(parentType: KClass<*>, annotation: MapProperty, classDescriptionCache: ClassDescriptionCache, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertyMapType(parentType, property)
        val keyEncoder = MapKeyEncoderFactory.createKeyEncoder(castProperty, classDescriptionCache)
        val valueEncoder = CollectionElementEncoderFactory.createMapValueEncoder(castProperty, classDescriptionCache)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Map::class,
                MapEncoder(keyEncoder, valueEncoder, VarIntSpecification(*annotation.mapLengthVarInt))
        ) { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createStringDescriptor(parentType: KClass<*>, annotation: StringProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, String::class, property)

        val regEx: Regex? = if (annotation.regEx.isEmpty()) {
            null
        } else {
            Regex(annotation.regEx)
        }

        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                String::class,
                StringEncoder(
                        Charset.forName(annotation.charset),
                        VarIntSpecification(*annotation.stringLengthVarInt),
                        listOf(*annotation.templateValues),
                        annotation.maxTemplateValues,
                        annotation.templateIgnoreCase,
                        regEx
                )
        ) { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createEnumDescriptor(parentType: KClass<*>, annotation: EnumProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertyTypeExtends(parentType, Enum::class, property)
        val enumCache = bit.crunzh.utilities.encoding.EnumCache()
        var maxEnumValues = annotation.maxEnumValues
        val enumType = property.returnType.jvmErasure
        val enumValues = bit.crunzh.utilities.encoding.EnumCache.getNoEnumValues(enumType)
        if (maxEnumValues == -1) {
            maxEnumValues = enumValues
        }
        if (maxEnumValues < enumValues) throw IllegalStateException("Enum ${enumType.simpleName} has more values than the specified max '${annotation.maxEnumValues}'")

        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                enumType as KClass<Any>,
                EnumEncoder(
                        maxEnumValues,
                        annotation.unknownValueOrdinal,
                        { ordinal, default -> enumCache.getEnumValueOrDefault(enumType, ordinal, default) },
                        { enum -> enum.ordinal }
                )
        ) { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createByteStoreDescriptor(parentType: KClass<*>, annotation: ByteStoreProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        return when (property.returnType.jvmErasure) {
            ByteArray::class -> createByteArrayDescriptor(parentType, annotation, property)
            ByteRange::class -> createByteRangeDescriptor(parentType, annotation, property)
            else -> throw IllegalArgumentException("Property '${parentType.simpleName}.${property.name}' type '${property.returnType.jvmErasure.simpleName}' must be 'ByteArray' or 'ByteRange'.")
        }
    }

    private fun <T : Any> createByteStoreConstantSizeDescriptor(parentType: KClass<*>, annotation: ByteStoreConstantSizeProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        return when (property.returnType.jvmErasure) {
            ByteArray::class -> createByteArrayConstantSizeDescriptor(parentType, annotation, property)
            ByteRange::class -> createByteRangeConstantSizeDescriptor(parentType, annotation, property)
            else -> throw IllegalArgumentException("Property type '${property.returnType.jvmErasure.simpleName}' must be 'ByteArray' or 'ByteRange'.")
        }
    }

    private fun <T : Any> createByteRangeDescriptor(parentType: KClass<*>, annotation: ByteStoreProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, ByteRange::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                ByteRange::class,
                ByteRangeEncoder(VarIntSpecification(*annotation.byteLengthLengthVarInt))
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createByteArrayDescriptor(parentType: KClass<*>, annotation: ByteStoreProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, ByteArray::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                ByteArray::class,
                ByteArrayEncoder(VarIntSpecification(*annotation.byteLengthLengthVarInt))
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createByteRangeConstantSizeDescriptor(parentType: KClass<*>, annotation: ByteStoreConstantSizeProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, ByteRange::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                ByteRange::class,
                ByteRangeConstantSizeEncoder(annotation.constantSize)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createByteArrayConstantSizeDescriptor(parentType: KClass<*>, annotation: ByteStoreConstantSizeProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, ByteArray::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                ByteArray::class,
                ByteArrayConstantSizeEncoder(annotation.constantSize)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createLatitudeDescriptor(parentType: KClass<*>, annotation: LatitudeProperty, positionAnnotation: PositionClass?, property: KProperty1<T, *>): PropertyDescription<*, T> {
        if (positionAnnotation == null) throw IllegalStateException("Cannot create LatitudeDescriptor if PositionClass anootation is null.")
        val castProperty = SharedValidation.assertPropertyType(parentType, Double::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Double::class,
                LatitudeEncoder(positionAnnotation.positionPrecisionInMeters)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createLongitudeDescriptor(parentType: KClass<*>, annotation: LongitudeProperty, positionAnnotation: PositionClass?, property: KProperty1<T, *>): PropertyDescription<*, T> {
        if (positionAnnotation == null) throw IllegalStateException("Cannot create LatitudeDescriptor if PositionClass anootation is null.")
        val castProperty = SharedValidation.assertPropertyType(parentType, Double::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Double::class,
                LongitudeEncoder(positionAnnotation.positionPrecisionInMeters)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createTimeDescriptor(parentType: KClass<*>, annotation: TimeProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        return when (property.returnType.jvmErasure) {
            Long::class -> createEpochMsDescriptor(parentType, annotation, property)
            Instant::class -> createInstantDescriptor(parentType, annotation, property)
            LocalDateTime::class -> createLocalDateTimeDescriptor(parentType, annotation, property)
            ZonedDateTime::class -> createZonedDateTimeDescriptor(parentType, annotation, property)
            else -> throw IllegalArgumentException("Cannot create property descriptor for property type ${property.returnType.jvmErasure.simpleName} and annotation TimeProperty.")
        }
    }

    private fun <T : Any> createZonedDateTimeDescriptor(parentType: KClass<*>, annotation: TimeProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, ZonedDateTime::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                ZonedDateTime::class,
                TimeZonedDateEncoder(annotation.timeResolutionMs, annotation.minTime, annotation.maxTime)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createLocalDateTimeDescriptor(parentType: KClass<*>, annotation: TimeProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, LocalDateTime::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                LocalDateTime::class,
                TimeLocalDateEncoder(annotation.timeResolutionMs, annotation.minTime, annotation.maxTime)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createInstantDescriptor(parentType: KClass<*>, annotation: TimeProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, Instant::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Instant::class,
                TimeInstantEncoder(annotation.timeResolutionMs, annotation.minTime, annotation.maxTime)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createEpochMsDescriptor(parentType: KClass<*>, annotation: TimeProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, Long::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                Long::class,
                TimeEpochMsEncoder(annotation.timeResolutionMs, annotation.minTime, annotation.maxTime)
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createUuidDescriptor(parentType: KClass<*>, annotation: UuidProperty, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, UUID::class, property)
        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                UUID::class,
                UuidEncoder()
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createBitRangeDescriptor(parentType: KClass<*>, annotation: BitRangeProperty, property: KProperty1<T, *>): PropertyDescription<BitRange, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, BitRange::class, property)
        return PropertyDescription(
                property.name,
                getSeqNo(annotation),
                property.returnType.isMarkedNullable,
                BitRange::class,
                BitRangeEncoder(VarIntSpecification(*annotation.bitRangeLengthVarInt))
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun <T : Any> createClassDescriptor(annotation: ClassProperty, classDescriptionCache: ClassDescriptionCache, property: KProperty1<T, *>): PropertyDescription<*, T> {
        val propertyType = property.returnType.jvmErasure
        SharedValidation.assertClassTypes(propertyType, annotation.subTypes)
        val types = ArrayList<KClass<*>>()
        if (!propertyType.isAbstract) {
            types.add(propertyType)
        }
        types.addAll(annotation.subTypes)

        val typeIndexCache = TypeIndexCache(types, annotation.numberOfTypesToSupport)

        return PropertyDescription(
                property.name,
                annotation.seqNo,
                property.returnType.isMarkedNullable,
                propertyType as KClass<Any>,
                ClassPropertyEncoder(typeIndexCache, classDescriptionCache)
        )
        { obj -> property.getter.call(obj) }
    }

    private fun <T : Any> createBooleanDescriptor(parentType: KClass<*>, annotation: BooleanProperty, property: KProperty1<T, *>): PropertyDescription<Boolean, T> {
        val castProperty = SharedValidation.assertPropertyType(parentType, Boolean::class, property)
        return PropertyDescription(
                property.name,
                getSeqNo(annotation),
                property.returnType.isMarkedNullable,
                Boolean::class,
                BooleanEncoder()
        )
        { obj -> castProperty.getter.call(obj) }
    }

    private fun getPropertyAnnotation(annotations: List<Annotation>): Annotation {
        var foundAnnotation: Annotation? = null
        for (annotation in annotations) {
            if (isPropertyAnnotation(annotation)) {
                if (foundAnnotation != null) throw IllegalArgumentException("Property has more than one property annotation. Found both '@${foundAnnotation::class.simpleName}' and '@${annotation::class.simpleName}'.")
                foundAnnotation = annotation
            }
        }
        if (foundAnnotation == null) {
            throw IllegalStateException("Property description not found for property.")
        }
        return foundAnnotation
    }
}