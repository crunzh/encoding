package bit.crunzh.utilities.encoding.tlv

import bit.crunzh.utilities.encoding.EnumCache
import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.bit.BitRange
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import kotlin.reflect.KClass


class EnumTlvEncoder<T : Enum<*>>(private val enumType: KClass<T>, private val lengthEncoder: VarIntEncoder = VarIntEncoder(8, 16, 24, 32), enumValuesToSupport: Int = bit.crunzh.utilities.encoding.EnumCache.getNoEnumValues(enumType)) : TlvEncoder<EnumTlvTuple<T>> {
    private val ordinalValueMap: Map<Int, T> = bit.crunzh.utilities.encoding.EnumCache.createTypedOrdinalMap(enumType)
    private val ordinalBitLength: Int

    init {
        if(enumValuesToSupport < ordinalValueMap.size) {
            throw IllegalArgumentException("enumValues")
        }
        ordinalBitLength = BitBuilder.getBitsRequired(enumValuesToSupport)
    }


    override fun encode(tuple: EnumTlvTuple<T>): BitRange {
        if(!enumType.isInstance(tuple.type)) {
            throw IllegalArgumentException("EnumTlvTuple type is type '${tuple.type::class.simpleName}', but encoder is initialized with type '${enumType.simpleName}'")
        }
        return BitBuilder()
                .append(tuple.type.ordinal, fitIntoBits = ordinalBitLength)
                .append(lengthEncoder.encode(tuple.length))
                .append(tuple.value)
                .toBitRange()
    }

    override fun decode(bitsToDecode: BitIterator): EnumTlvTuple<T> {
        if (!bitsToDecode.hasNext(ordinalBitLength)) {
            throw IllegalArgumentException("Insufficient bits to read type from TLV. Required '$ordinalBitLength', but only '${bitsToDecode.remaining()}' bits left.")
        }
        val typeOrdinal = bitsToDecode.next(ordinalBitLength).toInt()
        val type = ordinalValueMap[typeOrdinal]
                ?: throw IllegalArgumentException("Unknown enum '${enumType.qualifiedName}' value for ordinal '$typeOrdinal'")

        val bitLength = lengthEncoder.decode(bitsToDecode).toInt() * Byte.SIZE_BITS
        if (!bitsToDecode.hasNext(bitLength)) {
            throw IllegalArgumentException("Insufficient bits to read value from TLV. Required '$bitLength', but only '${bitsToDecode.remaining()}' bits left.")
        }
        val value = bitsToDecode.next(bitLength).toByteArray()
        return EnumTlvTuple(type = type, value = value)
    }

    override fun calculateSize(tuple: EnumTlvTuple<T>): Int {
        return ordinalBitLength + lengthEncoder.calculateSize(tuple.length) + (tuple.value.size * Byte.SIZE_BITS)
    }
}
