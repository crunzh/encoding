package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import bit.crunzh.utilities.encoding.varint.VarIntSpecification
import java.nio.ByteOrder

class VarShortEncoder(
        varIntSpecification: VarIntSpecification,
        byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
) : ValueEncoder<Short> {
    private val encoder = VarIntEncoder(varIntSpecification, byteOrder)

    override fun encode(property: Short, encoderContext: EncoderContext): BitBuilder {
        return encoder.encode(property)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Short {
        return encoder.decode(bits).toShort()
    }
}