package bit.crunzh.utilities.encoding.specification.annotation.collection

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class VarShortCollectionValue (
        val seqNo: Int,
        val signed: Boolean = false,
        val varIntSpecification: IntArray = [8, 15],
        val byteOrder: ByteOrder = ByteOrder.LITTLE_ENDIAN
)