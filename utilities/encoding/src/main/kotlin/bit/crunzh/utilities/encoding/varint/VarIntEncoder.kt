package bit.crunzh.utilities.encoding.varint

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.bit.BitRange
import java.nio.ByteOrder

class VarIntEncoder(val varIntBitSpecification: VarIntSpecification, private val byteOrder: ByteOrder = ByteOrder.LITTLE_ENDIAN) {
    constructor(vararg stepSizes: Int, signed: Boolean = false, byteOrder: ByteOrder = ByteOrder.LITTLE_ENDIAN) : this(varIntBitSpecification = VarIntSpecification(stepSizes = stepSizes, signed = signed), byteOrder = byteOrder)

    fun encode(valueToEncode: Number, description: String = "VarInt"): BitBuilder {
        val step = varIntBitSpecification.getSizeStep(valueToEncode)
        val builder = BitBuilder(toAppend = step.stepIndex, fitIntoBits = varIntBitSpecification.headerSize, description = "$description - VarInt - StepSize")
        builder.append(valueToEncode.toLong(), step.bitSize, varIntBitSpecification.signed, byteOrder, description = "$description - VarInt - Value")
        return builder
    }

    fun decode(bitsToDecode: BitIterator): Number {
        if (!bitsToDecode.hasNext(varIntBitSpecification.minSize)) {
            throw IllegalArgumentException("BitIterator has insufficient bits remaining to read variable length integer. Expect [${varIntBitSpecification.minSize}-${varIntBitSpecification.maxSize}] bits, but only '${bitsToDecode.remaining()}' bits remaining.")
        }
        val stepIndex = bitsToDecode.next(varIntBitSpecification.headerSize).toInt()
        val payloadBits = varIntBitSpecification.steps[stepIndex].bitSize
        if (!bitsToDecode.hasNext(payloadBits)) {
            throw IllegalArgumentException("BitIterator has insufficient bits remaining to read variable length integer payload. Expected '$payloadBits' bits, but only '${bitsToDecode.remaining()}' bits remaining.")
        }
        return bitsToDecode.next(payloadBits).toLong(signed = varIntBitSpecification.signed, byteOrder = byteOrder)
    }

    fun calculateSize(valueToCalculate: Number): Int {
        val step = varIntBitSpecification.getSizeStep(valueToCalculate)
        return varIntBitSpecification.headerSize + step.bitSize
    }
}