package bit.crunzh.utilities.encoding.specification.annotation.collection

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ByteArrayCollectionValue(
        val byteArrayLengthVarInt: IntArray = [4, 6, 10, 16]
)