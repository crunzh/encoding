package bit.crunzh.utilities.encoding.bit

import bit.crunzh.utilities.encoding.byte.ByteIterator

class ByteArrayBitIterator(private val bytes: ByteArray, private val bitOffset: Int = 0, private val bitLength: Int = (bytes.size * Byte.SIZE_BITS) - bitOffset) : BitIterator {
    private var pointer: Int

    internal constructor(byteIterator: ByteIterator) : this(byteIterator.bytes, byteIterator.pointer * Byte.SIZE_BITS, byteIterator.remaining() * Byte.SIZE_BITS)

    init {
        if (bytes.isEmpty()) {
            throw IllegalArgumentException("bytes must contain at least one byte.")
        }
        val bytesSizeBits = (bytes.size * Byte.SIZE_BITS)
        val maxOffset = bytesSizeBits - bitLength
        if (bitOffset < 0 || bitOffset > maxOffset) {
            throw IllegalArgumentException("bitOffset must be 0 or larger, but smaller than bitLength. It was '$bitOffset' but should be [0-$maxOffset]")
        }
        val maxLength = bytesSizeBits - bitOffset
        if (bitLength < 0 || bitLength > maxLength) {
            throw IllegalArgumentException("bitLength must be larger than 0 but smaller than bytes.size * Byte.BIT_SIZE. It was '$bitLength' but should be [0-$maxLength]")
        }
        pointer = bitOffset
    }

    override fun hasNext(): Boolean {
        return hasNext(1)
    }

    override fun hasNext(nextBits: Int): Boolean {
        return nextBits <= remaining()
    }

    override fun next(): Boolean {
        if (!hasNext()) {
            throw IllegalArgumentException("1 bit requested, but only ${remaining()} bits remaining.")
        }
        val rangeStart = pointer
        pointer++
        return BitUtil.getBit(bytes, rangeStart, 0)
        //return BitRange(bytes, rangeStart, 1).toBoolean()
    }

    override fun next(bitsToRead: Int): BitRange {
        if (!hasNext(bitsToRead)) {
            throw IllegalArgumentException("$bitsToRead bits requested, but only ${remaining()} bits remaining.")
        }
        val rangeStart = pointer
        pointer += bitsToRead
        return BitRange(bytes, rangeStart, bitsToRead)
    }

    override fun size(): Int {
        return bitLength
    }

    override fun byteSize(): Int {
        var byteSize = bitLength / Byte.SIZE_BITS
        if (bitLength % Byte.SIZE_BITS != 0) {
            byteSize++
        }
        return byteSize
    }

    override fun bitsRead(): Int {
        return pointer - bitOffset
    }

    override fun remaining(): Int {
        return bitLength - bitsRead()
    }

    override fun reset() {
        pointer = bitOffset
    }

    /**
     * Create a new iterator where start index is this iterators current index.
     */
    override fun iterator(): BitIterator {
        return ByteArrayBitIterator(bytes, pointer, remaining())
    }

    override fun toString(): String {
        return "Size: '${size()}', Bits read: '${bitsRead()}', Remaining: '${remaining()}'"
    }
}
