package bit.crunzh.utilities.encoding.specification.annotation.property

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class LongProperty(
        val seqNo: Int,
        val minValue: Long = 0,
        val maxValue: Long = 9_223_372_036_854_775_807L,
        val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
)