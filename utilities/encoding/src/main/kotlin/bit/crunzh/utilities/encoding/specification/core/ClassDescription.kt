package bit.crunzh.utilities.encoding.specification.core

import kotlin.reflect.KClass

interface ClassDescription<T : Any> {
    val type: KClass<T>
    val isExtendable: Boolean
    val supportOffsetValues: Boolean

    /**
     * The [PropertyDescription] for all properties of this class ordered by [PropertyDescription.seqNo]
     */
    val propertyDescriptors: List<PropertyDescription<*, T>>
    fun create(constParams: PropertyValues): T
}