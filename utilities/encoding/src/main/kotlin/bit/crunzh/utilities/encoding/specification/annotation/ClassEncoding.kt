package bit.crunzh.utilities.encoding.specification.annotation

@Target(AnnotationTarget.CLASS)
@MustBeDocumented
annotation class ClassEncoding(
        /**
         * Should encoder ignore properties not annotated or throw an exception if it finds properties not annotated?
         */
        val enforceAnnotatedProperties: Boolean = true,
        /**
         * Should this class allow expanding with new properties?
         * Default true, which will add a small overhead writing payload length. If false, it is not possible to ever add new properties to this class, without breaking backwards compatibility.
         */
        val isExtendable: Boolean = true,
        /**
         * Should this class support encoding offset values. Offset adds a small overhead of 1 bit even if no fields or values for offset compression is present.
         * It is not possible to change this value while maintaining backwards compatibility to previous version where it was disabled.
         */
        val supportOffsetEncoding: Boolean = true
)