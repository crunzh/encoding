package bit.crunzh.utilities.encoding.specification.core.offset

import bit.crunzh.utilities.encoding.position.LossyPositionCompression
import bit.crunzh.utilities.encoding.time.LossyTimeCompression

class OffsetValues(val absTimeOffset: Long, val bitsRequiredForTimeDelta: Int, val useTimeOffsetEncoding: Boolean, val latitudeOffset: Int, val bitsRequiredForLatitudeDelta: Int, val longitudeOffset: Int, val bitsRequiredForLongitudeDelta: Int, val usePositionOffsetEncoding: Boolean) {
    val useOffset = usePositionOffsetEncoding || useTimeOffsetEncoding

    fun getTimeEpochMs(deltaTimeFragment: Long, timeResolutionMs: Int, timeOffsetEpochMs: Long): Long {
        return LossyTimeCompression.decompressTime(absTimeOffset + deltaTimeFragment, timeResolutionMs, timeOffsetEpochMs).timeEpochMs
    }

    fun getOffsetDeltaTime(property: Long, timeResolutionMs: Int, minTimeMsEpoch: Long): Long {
        val compressedTime = LossyTimeCompression.compressTime(property, timeResolutionMs, minTimeMsEpoch)
        return compressedTime.timeDeltaFragments - absTimeOffset
    }

    var lastLatitude: Double? = null

    fun getDeltaLatitude(latitude: Double, precisionInMeters: Double): Int {
        return getCompressedLatitude(latitude, precisionInMeters) - latitudeOffset
    }

    private fun getCompressedLatitude(latitude: Double, precisionInMeters: Double): Int {
        if (lastLatitude != null) throw IllegalStateException("Latitude was already assigned.")
        val latitudeSteps = LossyPositionCompression.compressLatitude(latitude, precisionInMeters)
        lastLatitude = LossyPositionCompression.decompressLatitude(latitudeSteps, precisionInMeters)
        return latitudeSteps
    }

    fun getLatitude(deltaLatitude: Int, precisionInMeters: Double): Double {
        if (lastLatitude != null) throw IllegalStateException("Latitude was already assigned.")
        lastLatitude = LossyPositionCompression.decompressLatitude(deltaLatitude + latitudeOffset, precisionInMeters)
        return lastLatitude!!
    }

    fun getDeltaLongitude(longitude: Double, precisionInMeters: Double): Int {
        return getCompressLongitude(longitude, precisionInMeters) - longitudeOffset
    }

    private fun getCompressLongitude(longitude: Double, precisionInMeters: Double): Int {
        if (lastLatitude == null) throw IllegalStateException("Latitude has not been read before Longitude.")
        val longitudeSteps = LossyPositionCompression.compressLongitude(lastLatitude!!, longitude, precisionInMeters)
        lastLatitude = null
        return longitudeSteps
    }

    fun getLongitude(deltaLongitude: Int, precisionInMeters: Double): Double {
        if (lastLatitude == null) throw IllegalStateException("Latitude has not been read before Longitude.")
        val longitude = LossyPositionCompression.decompressLongitude(lastLatitude!!, deltaLongitude + longitudeOffset, precisionInMeters)
        lastLatitude = null
        return longitude
    }

    companion object {
        val empty = OffsetValues(0, 63, false, 0, 31, 0, 31, false)
    }
}
