package bit.crunzh.utilities.encoding.bit

import bit.crunzh.utilities.encoding.EnumCache
import bit.crunzh.utilities.encoding.byte.ByteRange
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import java.math.BigInteger
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.charset.Charset
import java.util.*
import kotlin.reflect.KClass

class BitBuilder() {
    private val enumCache = bit.crunzh.utilities.encoding.EnumCache()
    private val appendedBitRanges: LinkedList<BitRange> = LinkedList()
    private var sumSizeAppendedBitRanges: Int = 0

    constructor(toAppend: BitRange) : this() {
        append(toAppend)
    }

    constructor(toAppend: Byte, fitIntoBits: Int = Byte.SIZE_BITS, signed: Boolean = false, description: String = "Byte") : this() {
        append(toAppend, fitIntoBits, signed, description)
    }

    constructor(toAppend: Short, fitIntoBits: Int = Short.SIZE_BITS, signed: Boolean = false, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN, description: String = "Short") : this() {
        append(toAppend, fitIntoBits, signed, byteOrder, description)
    }

    constructor(toAppend: Int, fitIntoBits: Int = Int.SIZE_BITS, signed: Boolean = false, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN, description: String = "Int") : this() {
        append(toAppend, fitIntoBits, signed, byteOrder, description)
    }

    constructor(toAppend: Long, fitIntoBits: Int = Long.SIZE_BITS, signed: Boolean = false, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN, description: String = "Long") : this() {
        append(toAppend, fitIntoBits, signed, byteOrder, description)
    }

    constructor(toAppend: Float, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN, description: String = "Float") : this() {
        append(toAppend, byteOrder, description)
    }

    constructor(toAppend: Double, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN, description: String = "Double") : this() {
        append(toAppend, byteOrder, description)
    }

    constructor(toAppend: String, charsetToUseForEncode: Charset = Charsets.UTF_8, varIntEncoder: VarIntEncoder = defaultStringLengthEncoder, description: String = "String") : this() {
        append(toAppend, charsetToUseForEncode, varIntEncoder, description)
    }

    constructor(toAppend: Boolean, description: String = "Boolean") : this() {
        append(toAppend, description)
    }

    constructor(toAppend: ByteArray, description: String = "ByteArray") : this() {
        append(toAppend, description)
    }

    constructor(toAppend: ByteBuffer, description: String = "ByteBuffer") : this() {
        append(toAppend, description)
    }


    fun append(toAppend: BitRange): BitBuilder {
        //All below append functions must hit this append
        appendedBitRanges.addLast(toAppend)
        sumSizeAppendedBitRanges += toAppend.size()
        return this
    }

    private fun prepend(toPrepend: BitRange): BitBuilder {
        appendedBitRanges.addFirst(toPrepend)
        sumSizeAppendedBitRanges += toPrepend.size()
        return this
    }

    fun append(toAppend: BitBuilder): BitBuilder {
        toAppend.getBitRanges().forEach { append(it) }
        return this
    }

    fun prepend(toPrepend: BitBuilder): BitBuilder {
        toPrepend.getBitRanges().reversed().forEach { prepend(it) }
        return this
    }

    fun append(toAppend: Byte, fitIntoBits: Int = Byte.SIZE_BITS, signed: Boolean = false, description: String = "Byte"): BitBuilder {
        assertValueFitsIntoBits(toAppend, fitIntoBits, signed)
        var byteArr = byteArrayOf(toAppend)
        byteArr = moveSignBitAndCompress(signed, byteArr, fitIntoBits, ByteOrder.BIG_ENDIAN)
        append(BitRange(byteArr, bitLength = fitIntoBits, description = description))
        return this
    }

    fun prepend(toAppend: Byte, fitIntoBits: Int = Byte.SIZE_BITS, signed: Boolean = false, description: String = "Byte"): BitBuilder {
        assertValueFitsIntoBits(toAppend, fitIntoBits, signed)
        var byteArr = byteArrayOf(toAppend)
        byteArr = moveSignBitAndCompress(signed, byteArr, fitIntoBits, ByteOrder.BIG_ENDIAN)
        prepend(BitRange(byteArr, bitLength = fitIntoBits, description = description))
        return this
    }

    fun append(toAppend: Short, fitIntoBits: Int = Short.SIZE_BITS, signed: Boolean = false, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN, description: String = "Short"): BitBuilder {
        assertValueFitsIntoBits(toAppend, fitIntoBits, signed)
        var byteArr = ByteBuffer.allocate(Short.SIZE_BYTES).order(byteOrder).putShort(toAppend).array()
        byteArr = moveSignBitAndCompress(signed, byteArr, fitIntoBits, byteOrder)
        append(BitRange(byteArr, bitLength = fitIntoBits, description = description))
        return this
    }

    fun append(toAppend: Int, fitIntoBits: Int = Int.SIZE_BITS, signed: Boolean = false, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN, description: String = "Int"): BitBuilder {
        assertValueFitsIntoBits(toAppend, fitIntoBits, signed)
        var byteArr: ByteArray = ByteBuffer.allocate(Int.SIZE_BYTES).order(byteOrder).putInt(toAppend).array()
        byteArr = moveSignBitAndCompress(signed, byteArr, fitIntoBits, byteOrder)
        append(BitRange(byteArr, bitLength = fitIntoBits, description = description))
        return this
    }

    fun append(toAppend: Long, fitIntoBits: Int = Long.SIZE_BITS, signed: Boolean = false, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN, description: String = "Long"): BitBuilder {
        assertValueFitsIntoBits(toAppend, fitIntoBits, signed)
        var byteArr = ByteBuffer.allocate(Long.SIZE_BYTES).order(byteOrder).putLong(toAppend).array()
        byteArr = moveSignBitAndCompress(signed, byteArr, fitIntoBits, byteOrder)
        append(BitRange(byteArr, bitLength = fitIntoBits, description = description))
        return this
    }

    fun append(toAppend: Float, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN, description: String = "Float"): BitBuilder {
        val byteArr: ByteArray = ByteBuffer.allocate(floatSizeBytes).order(byteOrder).putFloat(toAppend).array()
        append(BitRange(byteArr, bitLength = floatSizeBits, description = description))
        return this
    }

    fun append(toAppend: Double, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN, description: String = "Double"): BitBuilder {
        val byteArr: ByteArray = ByteBuffer.allocate(doubleSizeBytes).order(byteOrder).putDouble(toAppend).array()
        append(BitRange(byteArr, bitLength = doubleSizeBits, description = description))
        return this
    }

    fun append(toAppend: String, charsetToUseForEncode: Charset = Charsets.UTF_8, stringLengthEncoder: VarIntEncoder = defaultStringLengthEncoder, description: String = "String"): BitBuilder {
        val strBytes = toAppend.toByteArray(charsetToUseForEncode)
        val strLength = stringLengthEncoder.encode(strBytes.size, "$description - string length")
        append(strLength)
        append(strBytes, "$description - string payload")
        return this
    }

    fun append(toAppend: Boolean, description: String = "Boolean"): BitBuilder {
        var byteValue = 0x00.toByte()
        if (toAppend) {
            byteValue = 0x01.toByte()
        }
        append(BitRange(byteArrayOf(byteValue), 0, 1, description))
        return this
    }

    fun <T : Enum<*>> append(enumValue: T, enumClass: KClass<T>, bitsToRepresentEnumOrdinal: Int = getBitsRequired(enumCache.getNoEnumValues(enumClass)), description: String = "Enum ${enumClass.simpleName}"): BitBuilder {
        append(enumValue.ordinal, bitsToRepresentEnumOrdinal, description = description)
        return this
    }

    fun append(toAppend: ByteArray, description: String = "ByteArray"): BitBuilder {
        if (!toAppend.isEmpty()) {
            append(BitRange(toAppend, description = description))
        }
        return this
    }

    fun append(toAppend: ByteBuffer, description: String = "ByteBuffer"): BitBuilder {
        val byteArray = ByteArray(toAppend.remaining())
        toAppend.get(byteArray)
        append(byteArray, description)
        return this
    }

    fun toByteArray(): ByteArray {
        val mutableRange = MutableBitRange(sizeBytes() * Byte.SIZE_BITS)
        var appendedBits = 0
        appendedBitRanges.forEach { bitRange ->
            mutableRange.set(appendedBits, bitRange)
            appendedBits += bitRange.size()
        }
        return mutableRange.bytes
    }

    fun toByteBuffer(): ByteBuffer {
        return ByteBuffer.wrap(toByteArray())
    }

    fun toBitRange(description: String? = null): BitRange {
        return BitRange(toByteArray(), bitLength = sumSizeAppendedBitRanges, description = description)
    }

    fun toByteRange(description: String? = null): ByteRange {
        return ByteRange(toByteArray(), description = description)
    }

    fun iterator(): BitIterator {
        return toBitRange().iterator()
    }

    fun getBitRanges(): List<BitRange> {
        return appendedBitRanges
    }

    fun size(): Int {
        return sumSizeAppendedBitRanges
    }

    fun sizeBytes(): Int {
        var bytes = size() / Byte.SIZE_BITS
        if (size() % Byte.SIZE_BITS != 0) {
            bytes++
        }
        return bytes
    }

    override fun toString(): String {
        var str = "Size: '$sumSizeAppendedBitRanges' bits, '${appendedBitRanges.size}' BitRanges"
        if(sumSizeAppendedBitRanges < 200) {
            appendedBitRanges.forEach { str += ", {${it}}" }
        }
        return str
    }

    private fun assertValueFitsIntoBits(toAppend: Number, fitIntoBits: Int, signed: Boolean) {
        if (!fitsInBits(toAppend, fitIntoBits, signed)) {
            throw IllegalArgumentException("The given value '$toAppend' does not fit into '$fitIntoBits' bits, signed: '$signed'")
        }
    }

    companion object Factory {
        private val two = BigInteger.valueOf(2)
        val defaultStringLengthEncoder = VarIntEncoder(4, 6, 12, 32)
        const val floatSizeBytes = 4
        const val floatSizeBits = Byte.SIZE_BITS * floatSizeBytes
        const val doubleSizeBytes = 8
        const val doubleSizeBits = Byte.SIZE_BITS * doubleSizeBytes

        fun calculateSignBitIdx(byteOrder: ByteOrder, bitLength: Int): Int {
            if (byteOrder == ByteOrder.BIG_ENDIAN) {
                return (bitLength - 1) % Byte.SIZE_BITS
            } else {
                return bitLength - 1
            }
        }

        fun fitsInBits(value: Number, numberOfBits: Int, signed: Boolean = false): Boolean {
            val minValue = getMinValue(numberOfBits, signed)
            val maxValue = getMaxValue(numberOfBits, signed)
            val longValue = value.toLong()
            if (longValue < minValue || longValue > maxValue) {
                return false
            }
            return true
        }

        /**
         * Ensures that the ByteArray returned matches the amount of bytes specified.
         * If number of bits is specified, it is added to the number of bytes. In case the number of bits doesn't add up to a whole number of bytes, the number of bytes is rounded up.
         */
        fun fitBytes(bytesToFit: ByteArray, noBytes: Int = 0, noBits: Int = 0, preBytePadding: Boolean = false): ByteArray {
            var totalNoBytes = noBytes + noBits / Byte.SIZE_BITS
            if (noBits % Byte.SIZE_BITS != 0) {
                totalNoBytes++
            }
            if (bytesToFit.size < totalNoBytes) {
                val fittedBytes = ByteArray(totalNoBytes)
                var destinationOffset = 0
                if (preBytePadding) {
                    destinationOffset = totalNoBytes - bytesToFit.size
                }
                return bytesToFit.copyInto(fittedBytes, destinationOffset = destinationOffset)
            } else if (bytesToFit.size > totalNoBytes) {
                return bytesToFit.copyOfRange(0, totalNoBytes)
            } else {
                return bytesToFit
            }
        }

        fun getMinValue(numberOfBits: Int, signed: Boolean = false): Long {
            if (numberOfBits < 0) {
                throw IllegalArgumentException("numberOfBits must be positive")
            }
            if (numberOfBits > Long.SIZE_BITS) {
                throw IllegalArgumentException("numberOfBits must be less than or equal to Long.SIZE_BITS")
            }

            if (!signed) {
                return 0
            }

            return two.pow(numberOfBits - 1).negate().toLong()
        }

        fun getMaxValue(numberOfBits: Int, signed: Boolean = false): Long {
            if (numberOfBits < 0) {
                throw IllegalArgumentException("numberOfBits must be positive")
            }
            if (numberOfBits > Long.SIZE_BITS) {
                throw IllegalArgumentException("numberOfBits must be less than or equal to Long.SIZE_BITS")
            }
            var valueBits = numberOfBits
            if (signed) {
                valueBits -= 1
            }

            val maxValue = two.pow(valueBits).minus(BigInteger.valueOf(1))
            if (maxValue > BigInteger.valueOf(Long.MAX_VALUE)) {
                return Long.MAX_VALUE
            }
            return maxValue.toLong()
        }

        fun getBitsRequired(valueToDetermineNumberOfBitsFor: Number, signed: Boolean = false): Int {
            val value = BigInteger.valueOf(valueToDetermineNumberOfBitsFor.toLong())
            val isPositive = value >= BigInteger.ZERO
            val absoluteValue = value.abs()

            for (power in (1..64)) {
                var maxValue = two.pow(power)
                if (isPositive) {
                    maxValue--
                }
                if (maxValue >= absoluteValue) {
                    return if (signed) {
                        power + 1
                    } else {
                        power
                    }
                }
            }
            return -1
        }

        fun ceilingBitSize(valueToCeil: Number, signed: Boolean = false): Number {
            val bitsRequired = getBitsRequired(valueToCeil, signed)
            return getMaxValue(bitsRequired, signed)
        }

        fun getNoBytesForBits(bits: Int): Int {
            var noBytes = bits / Byte.SIZE_BITS
            if (bits % Byte.SIZE_BITS != 0) {
                noBytes++
            }
            return noBytes
        }

        internal fun moveSignBitAndCompress(signed: Boolean, srcByteArr: ByteArray, fitIntoBits: Int, byteOrderToEncode: ByteOrder): ByteArray {
            val expandedBits = srcByteArr.size * Byte.SIZE_BITS
            if (expandedBits == fitIntoBits) {
                return srcByteArr
            }
            val payloadBits = if (signed) {
                fitIntoBits - 1
            } else {
                fitIntoBits
            }


            val compactByteArray = ByteArray(getNoBytesForBits(fitIntoBits))
            //TODO if we have a sign bit, we should not move it as payload!

            if (byteOrderToEncode == ByteOrder.BIG_ENDIAN) {
                //When big endian, the least significant bytes is written last for instance an 18 bit integer would look like this when bits and bytes are written left to right.
                //[0000 000(0), X(X)00 0000, XXXX XXXX, XXXX XXXX] - the sign bit in parentheses
                //Hence we must copy the last full bytes, and the remainder from the beginning of the byte before the last full bytes.
                val dstFullBytes = fitIntoBits / Byte.SIZE_BITS
                val dstFullBytesBits = dstFullBytes * Byte.SIZE_BITS
                val fullBytesSrcOffsetBytes = srcByteArr.size - dstFullBytes
                val srcRemainderByteOffset = if (fullBytesSrcOffsetBytes == 0) {
                    0
                } else {
                    fullBytesSrcOffsetBytes - 1
                }
                val remainderBits = fitIntoBits % Byte.SIZE_BITS
                val remainderBitsWithoutSign = if (signed) {
                    remainderBits - 1
                } else {
                    remainderBits
                }
                if (dstFullBytes > 0) {
                    BitUtil.setBitRange(srcByteArr, fullBytesSrcOffsetBytes * Byte.SIZE_BITS, dstFullBytesBits, compactByteArray, remainderBits)
                }
                BitUtil.setBitRange(srcByteArr, srcRemainderByteOffset * Byte.SIZE_BITS, remainderBitsWithoutSign, compactByteArray, 0)
            } else {
                BitUtil.setBitRange(srcByteArr, 0, payloadBits, compactByteArray, 0)
            }

            if (signed) {
                val signBitIdxExpanded = calculateSignBitIdx(byteOrderToEncode, expandedBits)
                val signBitIdxCompressed = calculateSignBitIdx(byteOrderToEncode, fitIntoBits)
                if (BitUtil.getBit(srcByteArr, signBitIdxExpanded, 0)) {
                    BitUtil.setBit(compactByteArray, signBitIdxCompressed, 0, true)
                }
            }
            return compactByteArray
        }
    }
}