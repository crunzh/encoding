package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.specification.core.UnknownTypeException
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueBuilder
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import bit.crunzh.utilities.encoding.varint.VarIntSpecification

class SetEncoder<ElementType : Any>(
        private val elementEncoder: ValueEncoder<ElementType>,
        listLengthVarIntSpecification: VarIntSpecification
) : ValueEncoder<Set<ElementType>> {
    private val setSizeEncoder = VarIntEncoder(listLengthVarIntSpecification)

    override fun collectOffsetValues(property: Set<ElementType>, offsetValueBuilder: OffsetValueBuilder) {
        for (element in property) {
            elementEncoder.collectOffsetValues(element, offsetValueBuilder)
        }
    }

    override fun encode(property: Set<ElementType>, encoderContext: EncoderContext): BitBuilder {
        val builder = setSizeEncoder.encode(property.size)
        for (element in property) {
            builder.append(elementEncoder.encode(element, encoderContext))
        }
        return builder
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Set<ElementType> {
        val listSize = setSizeEncoder.decode(bits).toInt()
        val set = HashSet<ElementType>(listSize)
        for (i in 0 until listSize) {
            try {
                val element = elementEncoder.decode(bits, encoderContext)
                set.add(element)
            } catch (e: UnknownTypeException) {
                continue //An encoder may be expanded with new unknown types. Unknown key or value types will cause the entire key-value pair to be skipped.
            }
        }
        return set
    }

    override fun assertValid(property: Set<ElementType>) {
        for(element in property) {
            elementEncoder.assertValid(element)
        }
    }
}