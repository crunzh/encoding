package bit.crunzh.utilities.encoding.specification.annotation.property

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class FloatProperty(
        val seqNo: Int,
        val minValue: Float = 0.0f,
        val maxValue: Float,
        val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
)