package bit.crunzh.utilities.encoding.specification.annotation.collection

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class StringCollectionValue (
        val stringLengthVarInt: IntArray = [4, 6, 10, 16],
        /**
         * if regEx is the empty string, no regular expression will be instantiated to evaluate any value string.
         */
        val regEx: String = "",
        val charset: String = "UTF-8",
        /**
         * Template String values, which will allow a very effective compression if thing value happens to match.
         * The order of the template values is significant, as only their array index is transferred.
         * If reading unknown template values. The string "Unknown" will be returned.
         */
        val templateValues: Array<String> = [],
        /**
         * Ignore character casing when comparing whether value is a template value.
         * This implies that casing is lost in compression.
         */
        val templateIgnoreCase: Boolean = false,
        /**
         * Zero means using the size of template values.
         * Specifying a larger number, means preparing for additional future values.
         */
        val maxTemplateValues: Int = 0
)