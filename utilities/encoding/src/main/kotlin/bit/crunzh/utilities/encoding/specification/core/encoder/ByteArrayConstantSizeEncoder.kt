package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import bit.crunzh.utilities.encoding.varint.VarIntSpecification

class ByteArrayConstantSizeEncoder(private val constantSize: Int) : ValueEncoder<ByteArray> {

    override fun encode(property: ByteArray, encoderContext: EncoderContext): BitBuilder {
        return BitBuilder(property)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): ByteArray {
        val length = constantSize * Byte.SIZE_BITS
        return bits.next(length).toByteArray()
    }

    override fun assertValid(property: ByteArray) {
        if (property.size != constantSize) throw IllegalArgumentException("ByteArray property is configured of a static size '$constantSize' but was '${property.size}'")
    }
}