package bit.crunzh.utilities.encoding.specification.annotation.property

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZonedDateTime

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
/**
 * This annotations should only be used on [Long], [Instant], [LocalDateTime] or [ZonedDateTime] properties.
 * The long property should represent
 * If multiple absolute time is specified in any part of the root object, including lists etc. the time will be offset compressed.
 * The encoding will likely cause precision of the time property to loose precision, given the defined time resolution.
 */
annotation class TimeProperty(
        val seqNo: Int,
        /**
         * The granularity of time represented by this property. The lower the resolution (the higher the number), the less bits it will take to encode.
         */
        val timeResolutionMs: Int = 1000,
        /**
         * Time offset, the earliest time possible to represent by this property. The closer the offset time is to the time to encode, the less bits it will take.
         * Time offset specified as ISO_OFFSET_DATE_TIME
         */
        val minTime: String = MIN_TIME_DEFAULT,
        /**
         * Latest supported point in time for this property.
         * The smaller interval between offset and latest point in time, the smaller it can be encoded.
         * Time offset specified as ISO_OFFSET_DATE_TIME
         */
        val maxTime: String = MAX_TIME_DEFAULT
) {
    companion object {
        const val MIN_TIME_DEFAULT = "2020-01-01T00:00:00+00:00"
        const val MAX_TIME_DEFAULT = "2050-01-01T00:00:00+00:00"
        val MIN_TIME_DEFAULT_EPOCH_MS = ZonedDateTime.parse(MIN_TIME_DEFAULT).toInstant().toEpochMilli()
        val MAX_TIME_DEFAULT_EPOCH_MS = ZonedDateTime.parse(MAX_TIME_DEFAULT).toInstant().toEpochMilli()
    }
}