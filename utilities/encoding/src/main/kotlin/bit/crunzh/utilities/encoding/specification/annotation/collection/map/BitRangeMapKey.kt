package bit.crunzh.utilities.encoding.specification.annotation.collection.map

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class BitRangeMapKey(
        val bitRangeLengthVarInt: IntArray = [4, 6, 10, 16]
)