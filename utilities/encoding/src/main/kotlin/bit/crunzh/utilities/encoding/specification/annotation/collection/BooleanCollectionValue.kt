package bit.crunzh.utilities.encoding.specification.annotation.collection

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class BooleanCollectionValue