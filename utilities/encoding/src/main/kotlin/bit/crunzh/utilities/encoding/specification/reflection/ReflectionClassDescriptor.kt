package bit.crunzh.utilities.encoding.specification.reflection

import bit.crunzh.utilities.encoding.specification.core.ClassDescription
import bit.crunzh.utilities.encoding.specification.core.PropertyDescription
import bit.crunzh.utilities.encoding.specification.core.PropertyValue
import bit.crunzh.utilities.encoding.specification.core.PropertyValues
import kotlin.reflect.KClass

class ReflectionClassDescriptor<T : Any>(override val type: KClass<T>, override val isExtendable: Boolean, override val supportOffsetValues: Boolean, override val propertyDescriptors: List<PropertyDescription<*, T>>, private val constructorFunction: (constParams: PropertyValues) -> T) : ClassDescription<T> {

    override fun create(constParams: PropertyValues): T {
        return constructorFunction(constParams)
    }
}
