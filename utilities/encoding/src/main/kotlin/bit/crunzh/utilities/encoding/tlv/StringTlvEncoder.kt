package bit.crunzh.utilities.encoding.tlv

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.bit.BitRange
import bit.crunzh.utilities.encoding.varint.VarIntEncoder

class StringTlvEncoder(private val stringLengthEncoder: VarIntEncoder = VarIntEncoder(4, 8), private val payloadLengthEncoder: VarIntEncoder = VarIntEncoder(8, 16, 24, 32)) : TlvEncoder<StringTlvTuple> {


    override fun encode(tuple: StringTlvTuple): BitRange {
        val bb = BitBuilder(tuple.type, varIntEncoder = stringLengthEncoder)
        bb.append(payloadLengthEncoder.encode(tuple.length))
        bb.append(tuple.value)
        return bb.toBitRange()
    }

    override fun decode(bitsToDecode: BitIterator): StringTlvTuple {
        val type = BitRange.readString(bitIterator = bitsToDecode, varIntEncoder = stringLengthEncoder)
        val length = payloadLengthEncoder.decode(bitsToDecode).toInt()
        val value = bitsToDecode.next(length * Byte.SIZE_BITS).toByteArray()
        return StringTlvTuple(type, value)
    }

    override fun calculateSize(tuple: StringTlvTuple): Int {
        return BitBuilder(tuple.type, varIntEncoder = stringLengthEncoder).size() + payloadLengthEncoder.calculateSize(tuple.length) + (tuple.length * Byte.SIZE_BITS)
    }
}