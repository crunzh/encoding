package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueBuilder
import java.time.ZonedDateTime

class TimeZonedDateEncoder(
        timeResolutionMs: Int = 1000,
        minTime: String = "2020-01-01T00:00:00+00:00",
        maxTime: String = "2050-01-01T00:00:00+00:00"
) : ValueEncoder<ZonedDateTime> {
    private val epochDescriptor = TimeEpochMsEncoder(timeResolutionMs, minTime, maxTime)

    override fun collectOffsetValues(property: ZonedDateTime, offsetValueBuilder: OffsetValueBuilder) {
        super.collectOffsetValues(property, offsetValueBuilder)
    }

    override fun encode(property: ZonedDateTime, encoderContext: EncoderContext): BitBuilder {
        val builder = epochDescriptor.encode(property.toInstant().toEpochMilli(), encoderContext)
        builder.append(property.zone.id)
        //TODO also encode time zone
        TODO("Not yet implemented")
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): ZonedDateTime {
        TODO("Not yet implemented")
    }

    override fun assertValid(property: ZonedDateTime) {
        epochDescriptor.assertValid(property.toInstant().toEpochMilli())
    }
}