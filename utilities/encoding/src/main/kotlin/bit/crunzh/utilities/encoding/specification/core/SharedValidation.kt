package bit.crunzh.utilities.encoding.specification.core

import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.full.isSuperclassOf
import kotlin.reflect.jvm.jvmErasure

object SharedValidation {
    fun <T : Any, V : Any> assertPropertyType(parentType: KClass<*>, expectedType: KClass<V>, actualProperty: KProperty1<T, *>): KProperty1<T, V> {
        val type = actualProperty.returnType.jvmErasure
        if (type != expectedType) throw IllegalArgumentException("Property '${parentType.simpleName}.${actualProperty.name}' is not type '${expectedType.simpleName}', but '${type.simpleName}'.")
        @Suppress("UNCHECKED_CAST")
        return actualProperty as KProperty1<T, V>
    }

    fun <T : Any, V : Any> assertPropertyTypeExtends(parentType: KClass<*>, topClass: KClass<out V>, actualProperty: KProperty1<T, *>): KProperty1<T, V> {
        val type = actualProperty.returnType.jvmErasure
        if (!topClass.isSuperclassOf(type)) throw IllegalArgumentException("Property '${parentType.simpleName}.${actualProperty.name}' type '${type.simpleName}' does not extend type '${topClass.simpleName}'.")
        @Suppress("UNCHECKED_CAST")
        return actualProperty as KProperty1<T, V>
    }

    fun <T : Any> assertPropertyListType(parentType: KClass<*>, actualProperty: KProperty1<T, *>): KProperty1<T, List<Any>?> {
        val actualType = actualProperty.returnType.jvmErasure
        if (!List::class.isSuperclassOf(actualType)) throw IllegalArgumentException("Property '${parentType.simpleName}.${actualProperty.name}' type '${actualType.simpleName}' does not extend type 'List'.")
        @Suppress("UNCHECKED_CAST")
        return actualProperty as KProperty1<T, List<Any>?>
    }

    fun <T : Any> assertPropertySetType(parentType: KClass<*>, actualProperty: KProperty1<T, *>): KProperty1<T, Set<Any>?> {
        val actualType = actualProperty.returnType.jvmErasure
        if (!Set::class.isSuperclassOf(actualType)) throw IllegalArgumentException("Property '${parentType.simpleName}.${actualProperty.name}' type '${actualType.simpleName}' does not extend type 'Set'.")
        @Suppress("UNCHECKED_CAST")
        return actualProperty as KProperty1<T, Set<Any>?>
    }

    fun <T : Any> assertPropertyMapType(parentType: KClass<*>, actualProperty: KProperty1<T, *>): KProperty1<T, Map<Any, Any>?> {
        val actualType = actualProperty.returnType.jvmErasure
        if (!Map::class.isSuperclassOf(actualType)) throw IllegalArgumentException("Property '${parentType.simpleName}.${actualProperty.name}' type '${actualType.simpleName}' does not extend type 'Map'.")
        @Suppress("UNCHECKED_CAST")
        return actualProperty as KProperty1<T, Map<Any, Any>?>
    }

    private fun printClassNames(types: Iterable<KClass<*>>): String {
        var printStr = ""
        types.forEach {
            printStr += "${it.simpleName}, "
        }
        return printStr
    }

    fun assertClassTypes(topType: KClass<*>, subTypes: Array<KClass<*>>) {
        if (topType.isAbstract && subTypes.isEmpty()) throw IllegalArgumentException("Top type '${topType.simpleName}' is abstract (incl. Interface), which requires concrete implementation classes in 'subTypes' to be specified.")
        subTypes.forEach { subType ->
            if (subType.isAbstract) throw IllegalArgumentException("Sub type '${subType.simpleName}' is abstract (incl. Interface), sub types must be concrete implementations extending the top type.")
        }
    }
}