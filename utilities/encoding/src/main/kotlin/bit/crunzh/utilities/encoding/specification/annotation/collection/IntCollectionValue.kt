package bit.crunzh.utilities.encoding.specification.annotation.collection

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class IntCollectionValue (
        val minValue: Int = 0,
        val maxValue: Int = 2147483647,
        val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
)