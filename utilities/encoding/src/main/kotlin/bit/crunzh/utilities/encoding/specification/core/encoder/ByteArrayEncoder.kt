package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import bit.crunzh.utilities.encoding.varint.VarIntSpecification

class ByteArrayEncoder(lengthVarIntSpecification: VarIntSpecification = VarIntSpecification(4, 6, 10, 16)) : ValueEncoder<ByteArray> {
    private val lengthEncoder = VarIntEncoder(lengthVarIntSpecification)

    override fun encode(property: ByteArray, encoderContext: EncoderContext): BitBuilder {
        return lengthEncoder.encode(property.size).append(property)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): ByteArray {
        val length = lengthEncoder.decode(bits).toInt() * Byte.SIZE_BITS
        return bits.next(length).toByteArray()
    }
}