package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.specification.core.UnknownTypeException
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueBuilder
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import bit.crunzh.utilities.encoding.varint.VarIntSpecification

class MapEncoder<KeyType : Any, ValueType : Any>(
        private val keyEncoder: ValueEncoder<KeyType>,
        private val valueEncoder: ValueEncoder<ValueType>,
        mapSizeVarIntSpecification: VarIntSpecification
) : ValueEncoder<Map<KeyType, ValueType>> {
    private val mapSizeEncoder = VarIntEncoder(mapSizeVarIntSpecification)

    override fun collectOffsetValues(property: Map<KeyType, ValueType>, offsetValueBuilder: OffsetValueBuilder) {
        for (element in property.entries) {
            keyEncoder.collectOffsetValues(element.key, offsetValueBuilder)
            valueEncoder.collectOffsetValues(element.value, offsetValueBuilder)
        }
    }

    override fun encode(property: Map<KeyType, ValueType>, encoderContext: EncoderContext): BitBuilder {
        val builder = mapSizeEncoder.encode(property.size)
        for (element in property.entries) {
            builder.append(keyEncoder.encode(element.key, encoderContext))
            builder.append(valueEncoder.encode(element.value, encoderContext))
        }
        return builder
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Map<KeyType, ValueType> {
        val map = HashMap<KeyType, ValueType>()
        val mapSize = mapSizeEncoder.decode(bits).toInt()
        for (idx in 0 until mapSize) {
            try {
                val key = keyEncoder.decode(bits, encoderContext)
                val value = valueEncoder.decode(bits, encoderContext)
                map[key] = value
            } catch (e: UnknownTypeException) {
                continue //An encoder may be expanded with new unknown types. Unknown key or value types will cause the entire key-value pair to be skipped.
            }
        }
        return map
    }

    override fun assertValid(property: Map<KeyType, ValueType>) {
        for(entry in property) {
            keyEncoder.assertValid(entry.key)
            valueEncoder.assertValid(entry.value)
        }
    }
}