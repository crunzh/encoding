package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueBuilder
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset

class TimeLocalDateEncoder(
        timeResolutionMs: Int = 1000,
        minTime: String = "2020-01-01T00:00:00+00:00",
        maxTime: String = "2050-01-01T00:00:00+00:00"
) : ValueEncoder<LocalDateTime> {
    private val epochDescriptor = TimeEpochMsEncoder(timeResolutionMs, minTime, maxTime)

    override fun collectOffsetValues(property: LocalDateTime, offsetValueBuilder: OffsetValueBuilder) {
        epochDescriptor.collectOffsetValues(property.toInstant(ZoneOffset.UTC).toEpochMilli(), offsetValueBuilder)
    }

    override fun encode(property: LocalDateTime, encoderContext: EncoderContext): BitBuilder {
        return epochDescriptor.encode(property.toInstant(ZoneOffset.UTC).toEpochMilli(), encoderContext)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): LocalDateTime {
        val epochMs = epochDescriptor.decode(bits, encoderContext)
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(epochMs), ZoneOffset.UTC)
    }

    override fun assertValid(property: LocalDateTime) {
        epochDescriptor.assertValid(property.toInstant(ZoneOffset.UTC).toEpochMilli())
    }
}