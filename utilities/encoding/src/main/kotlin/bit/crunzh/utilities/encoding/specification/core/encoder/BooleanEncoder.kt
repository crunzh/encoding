package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext

class BooleanEncoder : ValueEncoder<Boolean> {
    override fun encode(property: Boolean, encoderContext: EncoderContext): BitBuilder {
        return BitBuilder(property)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Boolean {
        return bits.next()
    }
}