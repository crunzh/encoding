package bit.crunzh.utilities.encoding.specification.reflection

import bit.crunzh.utilities.encoding.specification.annotation.ClassEncoding
import bit.crunzh.utilities.encoding.specification.annotation.EnumEncoding
import bit.crunzh.utilities.encoding.specification.annotation.ObjectEncoding
import bit.crunzh.utilities.encoding.specification.annotation.collection.ClassCollectionValue
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.ClassMapKey
import bit.crunzh.utilities.encoding.specification.annotation.position.PositionClass
import bit.crunzh.utilities.encoding.specification.annotation.property.ClassProperty
import bit.crunzh.utilities.encoding.specification.annotation.property.IgnoredProperty
import bit.crunzh.utilities.encoding.specification.core.*
import kotlin.reflect.*
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.full.isSuperclassOf
import kotlin.reflect.jvm.jvmErasure

object ReflectionClassDescriptionFactory {
    fun createClassDescriptions(types: Collection<KClass<*>>): List<ClassDescription<*>> {
        val topTypeClassDescriptors = ArrayList<ClassDescription<*>>()

        val subTypes = HashSet<KClass<*>>()
        for (type in types) {
            findClassSubTypes(type, subTypes)
            findCollectionSubTypes(type, subTypes)
            findMapKeySubTypes(type, subTypes)
        }

        val classDescriptionCache = ClassDescriptionCache()
        for (type in types) {
            val classDescription = createClassDescription(type, classDescriptionCache)
            classDescriptionCache.addClassDescriptor(classDescription)
            topTypeClassDescriptors.add(classDescription)
        }
        for (subType in subTypes) {
            val classDescription = createClassDescription(subType, classDescriptionCache)
            classDescriptionCache.addClassDescriptor(classDescription)
        }

        return topTypeClassDescriptors
    }

    private fun findClassSubTypes(type: KClass<*>, subTypes: MutableSet<KClass<*>>) {
        for (property in type.declaredMemberProperties) {
            if (!hasClassPropertyAnnotation(property)) {
                continue
            }
            val annotation: ClassProperty = property.findAnnotation() ?: throw IllegalStateException("ClassProperty Annotation not found.")
            val topType = property.returnType.jvmErasure
            val annotationSubTypes = annotation.subTypes
            addSubTypes(topType, annotationSubTypes, subTypes)
        }
    }

    private fun findCollectionSubTypes(type: KClass<*>, subTypes: MutableSet<KClass<*>>) {
        for (property in type.declaredMemberProperties) {
            val annotation: ClassCollectionValue = property.findAnnotation() ?: continue
            val annotationSubTypes = annotation.subTypes
            val topType = annotation.topType
            addSubTypes(topType, annotationSubTypes, subTypes)
        }
    }

    private fun findMapKeySubTypes(type: KClass<*>, subTypes: MutableSet<KClass<*>>) {
        for (property in type.declaredMemberProperties) {
            val annotation: ClassMapKey = property.findAnnotation() ?: continue
            val annotationSubTypes = annotation.subTypes
            val topType = annotation.topType
            addSubTypes(topType, annotationSubTypes, subTypes)
        }
    }

    private fun addSubTypes(topType: KClass<*>, annotationSubTypes: Array<KClass<*>>, subTypes: MutableSet<KClass<*>>) {
        val discoveredTypes = HashSet<KClass<*>>()
        if (topType.isAbstractOrInterface()) {
            if (annotationSubTypes.isEmpty()) throw IllegalArgumentException("Top type '${topType.simpleName}' is abstract and no sub types defined, making it impossible to encode.")
        } else {
            if (subTypes.add(topType)) {
                discoveredTypes.add(topType)
            }
        }
        for (subType in annotationSubTypes) {
            if (subTypes.add(subType)) {
                discoveredTypes.add(subType)
            }
        }
        for (discoveredType in discoveredTypes) {
            findClassSubTypes(discoveredType, subTypes)
            findCollectionSubTypes(discoveredType, subTypes)
            findMapKeySubTypes(discoveredType, subTypes)
        }
    }

    private fun <T : Any> createClassDescription(type: KClass<T>, classDescriptionCache: ClassDescriptionCache): ClassDescription<T> {
        val classAnnotation = assertValidType(type)
        return if (classAnnotation is ClassEncoding) {
            createClassDescriptor(classAnnotation, type, classDescriptionCache)
        } else if (classAnnotation is EnumEncoding) {
            createEnumClassDescription(type, classAnnotation)
        } else if (classAnnotation is ObjectEncoding) {
            createObjectClassDescription(type)
        } else throw IllegalStateException("type '${type.simpleName}' was marked as valid to create ClassDescription for but was not handled.")
    }

    private fun <T : Any> createClassDescriptor(classAnnotation: ClassEncoding, type: KClass<T>, classDescriptionCache: ClassDescriptionCache): ReflectionClassDescriptor<T> {
        if (type.isEnum()) throw IllegalArgumentException("Type '${type.simpleName}' is annotated with @ClassEncoding, but is an enum. Enums must use @EnumClassEncoding")
        if (type.isObject()) throw IllegalArgumentException("Type '${type.simpleName}' is annotated with @ClassEncoding, but is an object. 'object' type must use @ObjectClassEncoding")
        val isExtendable = classAnnotation.isExtendable
        val supportOffsetValues = classAnnotation.supportOffsetEncoding
        val propertyDescriptions = createPropertyDescriptions(type, classDescriptionCache, classAnnotation.enforceAnnotatedProperties)
        val constructorFunction = createConstructor(type, propertyDescriptions)
        return ReflectionClassDescriptor<T>(type, isExtendable, supportOffsetValues, propertyDescriptions, constructorFunction)
    }

    private fun <T : Any> createObjectClassDescription(type: KClass<T>): ClassDescription<T> {
        if (!type.isObject()) throw IllegalStateException("Type '${type.simpleName}' is annotated with @ObjectEncoding, is not an object")
        return ObjectClassDescription(type)
    }

    private fun <T : Any> createEnumClassDescription(type: KClass<T>, classEncoding: EnumEncoding): ClassDescription<T> {
        if (!type.isEnum()) throw IllegalArgumentException("Type '${type.simpleName}' annotated with @EnumEncoding, was not an enum.")
        val enumType = type as KClass<Enum<*>>
        return ReflectionEnumClassDescription(enumType, classEncoding.maxEnumValues, classEncoding.unknownValueOrdinal) as ClassDescription<T>
    }


    private fun <T : Any> assertValidType(type: KClass<T>): Annotation {
        assertClassType(type)
        type.annotations.forEach { annotation ->
            when (annotation) {
                is ClassEncoding -> return annotation
                is EnumEncoding -> return annotation
                is ObjectEncoding -> return annotation
            }
        }
        throw IllegalArgumentException("Type ${type.simpleName} is not annotated with @ClassEncoding, @EnumClassEncoding or @ObjectEncoding")
    }

    private fun <T : Any> createPropertyDescriptions(type: KClass<T>, classDescriptionCache: ClassDescriptionCache, enforceAnnotatedProperties: Boolean): List<PropertyDescription<*, T>> {
        val propertyDescriptions = ArrayList<PropertyDescription<*, T>>()
        val positionAnnotation = type.annotations.firstOrNull { annotation -> annotation is PositionClass } as PositionClass?

        for (property in type.declaredMemberProperties) {
            if (!hasPropertyAnnotation(property)) {
                if (enforceAnnotatedProperties && property.annotations.firstOrNull { annotation -> annotation is IgnoredProperty } == null) {
                    throw IllegalStateException("Class '${type.simpleName}' property '${property.name}' did not have an encoding annotation, which was required by class annotation.")
                } else {
                    continue
                }
            }
            if (positionAnnotation == null && property.annotations.any { annotation -> isLatLongProperty(annotation) }) throw IllegalArgumentException("Class '${type.simpleName}' property '${property.name}' was annotated as LatitudeProperty or LongitudeProperty, but class was not annotated as a PositionClass.")

            propertyDescriptions.add(createPropertyDescription(type, property, positionAnnotation, classDescriptionCache))
        }
        val sortedProperties = propertyDescriptions.sortedBy { desc -> desc.seqNo }
        sortedProperties.forEachIndexed { index, sortedProperty ->
            val expectedIndex = index + 1;
            if (expectedIndex != sortedProperty.seqNo) {
                throw IllegalArgumentException("Annotated properties of class '${type.simpleName}' must be unique sequence numbers and each increment by one, starting at 1. SeqNo should have been '$expectedIndex' but was '${sortedProperty.seqNo}' of ${sortedProperty.name}")
            }
            //TODO assert if position annotation, both latitude and longitude is set, and latitude with smaller sequence number than longitude
        }
        return sortedProperties
    }

    private fun <T : Any> createPropertyDescription(parentType: KClass<*>, property: KProperty1<T, *>, positionAnnotation: PositionClass?, classDescriptionCache: ClassDescriptionCache): PropertyDescription<*, T> {
        return PropertyDescriptorFactory.createPropertyDescriptor(parentType, property, positionAnnotation, classDescriptionCache)
    }

    private fun <T : Any> createConstructor(type: KClass<T>, propertyDescriptions: List<PropertyDescription<*, T>>): (PropertyValues) -> T {
        val propertyMap = HashMap<String, PropertyDescription<*, T>>()
        propertyDescriptions.forEach { property ->
            propertyMap[property.name] = property
        }

        for (constructor in type.constructors) {
            if (constructor.visibility != KVisibility.PUBLIC) {
                continue
            }
            var matchAllConstParams = true
            val parameters = HashMap<String, KParameter>()

            val unassignedParameters = HashSet<String>()
            propertyDescriptions.forEach { propertyDescription -> unassignedParameters.add(propertyDescription.name) }
            if (propertyDescriptions.isEmpty()) {
                if (isNoArgConstructor(constructor)) {
                    return { constructor.callBy(emptyMap()) }
                } else {
                    continue
                }
            }
            for (constParam in constructor.parameters) {
                val propertyMatch = propertyMap[constParam.name]
                if (propertyMatch == null && constParam.isOptional) {
                    continue //A not annotated property which has a default value is OK.
                }
                if (
                        constParam.name == null ||
                        propertyMatch == null ||
                        !constParam.type.jvmErasure.isSuperclassOf(propertyMatch.type) ||
                        (!constParam.type.isMarkedNullable && propertyMatch.isNullable) ||
                        (constParam.type.isMarkedNullable && !propertyMatch.isNullable)
                ) {
                    matchAllConstParams = false
                    break
                }
                parameters[constParam.name!!] = constParam
            }
            if (!matchAllConstParams) {
                break
            }
            val constParamName = ArrayList<String?>(constructor.parameters.size)
            for (constParam in constructor.parameters) {
                constParamName.add(constParam.index, constParam.name) //Constructor parameters comes in indexed order.
                unassignedParameters.remove(constParam.name)
            }

            val propertySetters = ArrayList<KMutableProperty<*>>()
            for (nonConstProperty in type.declaredMemberProperties) {
                if (!unassignedParameters.contains(nonConstProperty.name)) {
                    continue
                }
                if (nonConstProperty.annotations.firstOrNull { annotation -> annotation is IgnoredProperty } != null) {
                    continue
                }
                if (nonConstProperty !is KMutableProperty<*>) {
                    continue
                }
                if (nonConstProperty.visibility != KVisibility.PUBLIC) {
                    continue
                }
                propertySetters.add(nonConstProperty)
                unassignedParameters.remove(nonConstProperty.name)
            }
            if (unassignedParameters.isNotEmpty()) {
                continue //We haven't found a satisfying combination of constructor and modifiable properties
            }

            return { constParams: PropertyValues ->
                val constArgs = arrayOfNulls<Any?>(constParamName.size)
                constParamName.forEachIndexed { idx, name ->
                    val propValue = constParams.getNullableValue<Any>(name!!)
                    constArgs[idx] = propValue
                }
                val obj = constructor.call(*constArgs)
                for (propertySetter in propertySetters) {
                    propertySetter.setter.call(obj, constParams.getNullableValue(propertySetter.name))
                }
                obj
            }
        }
        throw IllegalStateException("No suitable constructor found for type $type.")
    }

    private fun <T : Any> isNoArgConstructor(constructor: KFunction<T>): Boolean {
        var emptyConstructorFound = true
        for (constParam in constructor.parameters) {
            if (!constParam.isOptional) {
                emptyConstructorFound = false
                break
            }
        }
        return emptyConstructorFound
    }

    private fun hasPropertyAnnotation(property: KProperty1<out Any, *>): Boolean {
        return property.annotations.any { annotation -> isPropertyAnnotation(annotation) }
    }

    private fun hasClassPropertyAnnotation(property: KProperty1<out Any, *>): Boolean {
        return property.annotations.any { annotation -> annotation is ClassProperty }
    }

    private fun assertClassType(type: KClass<*>) {
        if (type.isAbstractOrInterface() || type.isFun || type.isCompanion) throw IllegalArgumentException("Registered Class '${type.simpleName}' must be a data class, enum or regular class.")
    }

    private fun KClass<*>.isAbstractOrInterface(): Boolean {
        return isAbstract || java.isInterface
    }

    private fun KClass<*>.isEnum(): Boolean {
        return this.isSubclassOf(Enum::class)
    }

    private fun KClass<*>.isObject(): Boolean {
        return this.objectInstance != null
    }
}