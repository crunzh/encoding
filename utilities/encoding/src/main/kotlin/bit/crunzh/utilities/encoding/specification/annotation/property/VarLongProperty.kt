package bit.crunzh.utilities.encoding.specification.annotation.property

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class VarLongProperty (
        val seqNo: Int,
        val signed: Boolean = false,
        val varIntSpecification: IntArray = [8, 16, 24, 32, 40, 48, 56, 63],
        val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
)