package bit.crunzh.utilities.encoding.specification.annotation.property

/**
 * This annotation is only valid if exactly one other @LongitudeProperty annotation is on the same level of the same object and the parent class is annotated as @PositionClass.
 * The type this annotation supports is only Double.
 * The @LatitudeProperty must have a lower sequence number than the @LongitudeProperty
 * If multiple position (Lat, Long) is specified in any part of the root object, including lists etc. the position will be offset compressed.
 * The encoding will cause loss of precision as according to the defined precision in meters.
 *
 * Latitude is valid in the range [-90 to 90]
 */
@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class LatitudeProperty(
        val seqNo: Int
)