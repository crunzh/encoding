package bit.crunzh.utilities.encoding.specification.annotation.collection

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class FloatCollectionValue (
        val minValue: Float = 0.0f,
        val maxValue: Float,
        val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
)