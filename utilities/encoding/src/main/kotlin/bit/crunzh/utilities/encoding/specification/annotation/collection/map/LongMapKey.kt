package bit.crunzh.utilities.encoding.specification.annotation.collection.map

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class LongMapKey(
        val minValue: Long = 0,
        val maxValue: Long = 9223372036854775807L,
        val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
)