package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.byte.ByteRange
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import bit.crunzh.utilities.encoding.varint.VarIntSpecification

class ByteRangeConstantSizeEncoder(private val constantSize: Int) : ValueEncoder<ByteRange> {

    override fun encode(property: ByteRange, encoderContext: EncoderContext): BitBuilder {
        return BitBuilder(property.toBitRange())
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): ByteRange {
        val length = constantSize * Byte.SIZE_BITS
        return bits.next(length).toByteRange()
    }

    override fun assertValid(property: ByteRange) {
            if (property.size != constantSize) throw IllegalArgumentException("ByteRange property is configured of a static size '$constantSize' but was '${property.size}'")
        }
}