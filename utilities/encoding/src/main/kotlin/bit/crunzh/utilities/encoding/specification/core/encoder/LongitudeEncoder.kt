package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.position.LossyPositionCompression
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueBuilder
import kotlin.math.ceil

const val OFFSET_LONGITUDE_DESCRIPTION = "Offset Longitude"
const val LONGITUDE_DESCRIPTION = "Longitude"

class LongitudeEncoder (
        private val precisionInMeters: Double
) : ValueEncoder<Double> {
    private val maxBitSize = BitBuilder.getBitsRequired(ceil(LossyPositionCompression.calculateMaxLongitudeSteps(precisionInMeters)).toInt())

    override fun collectOffsetValues(property: Double, offsetValueBuilder: OffsetValueBuilder) {
        offsetValueBuilder.addLongitude(precisionInMeters, property)
    }

    override fun encode(property: Double, encoderContext: EncoderContext): BitBuilder {
        return if (encoderContext.offsetValues.usePositionOffsetEncoding) {
            val deltaLongitude = encoderContext.offsetValues.getDeltaLongitude(property, precisionInMeters)
            BitBuilder(deltaLongitude, encoderContext.offsetValues.bitsRequiredForLongitudeDelta, description = OFFSET_LONGITUDE_DESCRIPTION)
        } else {
            if (encoderContext.offsetValues.lastLatitude == null) throw IllegalStateException("Offset Latitude was not set.")
            val compressedLongitude = LossyPositionCompression.compressLongitude(encoderContext.offsetValues.lastLatitude!!, property, precisionInMeters)
            encoderContext.offsetValues.lastLatitude = null
            BitBuilder(compressedLongitude, fitIntoBits = maxBitSize, description = LONGITUDE_DESCRIPTION)
        }
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Double {
        return if (encoderContext.offsetValues.usePositionOffsetEncoding) {
            val deltaLongitude = bits.next(encoderContext.offsetValues.bitsRequiredForLongitudeDelta).toInt()
            val longitude = encoderContext.offsetValues.getLongitude(deltaLongitude, precisionInMeters)
            longitude
        } else {
            if (encoderContext.offsetValues.lastLatitude == null) throw IllegalStateException("Last Latitude was not set on decode.")
            val longitudeSteps = bits.next(maxBitSize).toInt()
            val longitude = LossyPositionCompression.decompressLongitude(encoderContext.offsetValues.lastLatitude!!, longitudeSteps, precisionInMeters)
            encoderContext.offsetValues.lastLatitude = null
            longitude
        }
    }

    override fun assertValid(property: Double) {
        if (property < -180) throw IllegalArgumentException("Property value may not be smaller than '-180.0' but was '$property'.")
        if (property > 180) throw IllegalArgumentException("Property value may not be larger than '180.0' but was '$property'")
    }
}