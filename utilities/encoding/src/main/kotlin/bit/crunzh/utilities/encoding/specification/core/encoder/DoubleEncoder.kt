package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import java.nio.ByteOrder

class DoubleEncoder(
        private val minValue: Double = Double.MIN_VALUE,
        private val maxValue: Double = Double.MAX_VALUE,
        private val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
) : ValueEncoder<Double> {

    override fun encode(property: Double, encoderContext: EncoderContext): BitBuilder {
        return BitBuilder(property, byteOrder)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Double {
        return bits.next(Double.SIZE_BITS).toDouble(byteOrder = byteOrder)
    }

    override fun assertValid(property: Double) {
        if (property < minValue) throw IllegalArgumentException("Property value may not be less than '$minValue' but was '$property'.")
        if (property > maxValue) throw IllegalArgumentException("Property value may not me more than '$maxValue' but was '$property'")
    }
}