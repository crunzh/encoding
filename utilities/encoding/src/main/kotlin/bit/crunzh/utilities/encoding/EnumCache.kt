package bit.crunzh.utilities.encoding

import kotlin.reflect.KClass

class EnumCache {
    val enumCache = HashMap<KClass<*>, Map<Int, Enum<*>>>()

    inline fun <reified T : Enum<*>> getEnumValue(enumClass: KClass<*>, ordinal: Int): T {
        var ordinalMap = enumCache[enumClass]
        if (ordinalMap == null) {
            ordinalMap = bit.crunzh.utilities.encoding.EnumCache.Companion.createOrdinalMap(enumClass)
            enumCache[enumClass] = ordinalMap
        }
        val enumValue = ordinalMap[ordinal]
        if (enumValue is T) {
            return enumValue
        }
        throw IllegalArgumentException("Wrong enum type saved.")
    }

    inline fun <reified T : Enum<*>> getEnumValueOrDefault(enumClass: KClass<*>, ordinal: Int, default: Int): T {
        var ordinalMap = enumCache[enumClass]
        if (ordinalMap == null) {
            ordinalMap = bit.crunzh.utilities.encoding.EnumCache.Companion.createOrdinalMap(enumClass)
            enumCache[enumClass] = ordinalMap
        }
        var enumValue = ordinalMap[ordinal]
        if (enumValue == null) {
            enumValue = ordinalMap[default]
        }
        if (enumValue is T) {
            return enumValue
        }
        throw IllegalArgumentException("Wrong enum type saved.")
    }

    fun <T : Enum<*>> getEnumValueOrDefault2(enumClass: KClass<T>, ordinal: Int, default: Int): T {
        var ordinalMap = enumCache[enumClass]
        if (ordinalMap == null) {
            ordinalMap = bit.crunzh.utilities.encoding.EnumCache.Companion.createOrdinalMap(enumClass)
            enumCache[enumClass] = ordinalMap
        }
        var enumValue = ordinalMap[ordinal]
        if (enumValue == null) {
            enumValue = ordinalMap[default]
        }
        if (enumValue != null && enumClass.isInstance(enumValue)) {
            @Suppress("UNCHECKED_CAST")
            return enumValue as T
        }
        throw IllegalArgumentException("Wrong enum type saved.")
    }

    fun getNoEnumValues(enumClass: KClass<*>): Int {
        var ordinalMap = enumCache[enumClass]
        if (ordinalMap == null) {
            ordinalMap = bit.crunzh.utilities.encoding.EnumCache.Companion.createOrdinalMap(enumClass)
            enumCache[enumClass] = ordinalMap
        }
        return ordinalMap.size
    }

    companion object {
        fun createOrdinalMap(clazz: KClass<*>): Map<Int, Enum<*>> {
            val enumClass = clazz as KClass<Enum<*>>
            val enumOrdinalTypeMap = HashMap<Int, Enum<*>>()
            for (enumInstance in enumClass.javaObjectType.enumConstants) {
                enumOrdinalTypeMap[enumInstance.ordinal] = enumInstance
            }
            return enumOrdinalTypeMap

        }

        fun getNoEnumValues(enumClass: KClass<*>): Int {
            return bit.crunzh.utilities.encoding.EnumCache.Companion.createOrdinalMap(enumClass).size
        }

        fun <T : Enum<*>> createTypedOrdinalMap(enumType: KClass<T>): Map<Int, T> {
            val enumOrdinalTypeMap = HashMap<Int, T>()
            for (enumInstance in enumType.javaObjectType.enumConstants) {
                enumOrdinalTypeMap[enumInstance.ordinal] = enumInstance
            }
            return enumOrdinalTypeMap
        }

    }
}