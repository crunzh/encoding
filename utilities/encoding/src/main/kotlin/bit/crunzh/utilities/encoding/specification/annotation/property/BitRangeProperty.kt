package bit.crunzh.utilities.encoding.specification.annotation.property

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class BitRangeProperty(
        val seqNo: Int,
        val bitRangeLengthVarInt: IntArray = [4, 6, 10, 16]
)