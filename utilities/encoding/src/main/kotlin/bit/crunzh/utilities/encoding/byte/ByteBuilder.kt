package bit.crunzh.utilities.encoding.byte

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitRange
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer
import java.util.*

class ByteBuilder(override val description: String? = null) : ByteBuilderRange {
    private val orderedByteRanges = LinkedList<ByteBuilderRange>()
    override var size = 0
        private set

    constructor() : this(null)

    constructor(byteArray: ByteArray, description: String? = "ByteArray") : this(description) {
        append(byteArray, description)
    }

    private fun append(range: ByteBuilderRange) {
        orderedByteRanges.addLast(range)
        size += range.size
    }

    fun append(byteBuilder: ByteBuilder) {
        byteBuilder.getByteRanges().forEach { byteBuilderRange -> append(byteBuilderRange) }
    }

    fun append(byteRange: ByteRange): ByteBuilder {
        append(byteRange as ByteBuilderRange)
        return this
    }

    fun append(byteArray: ByteArray, description: String? = "ByteArray"): ByteBuilder {
        append(ByteRange(byteArray, description = description))
        return this
    }

    /**
     * Appends a BitBuilder, keeping the below BitBuilder structure without converting it to a ByteArray, allowing to inspect the added byte and bit ranges.
     * When writing this ByteBuilder to any output (OutputStream, ByteArray or otherwise), the BitBuilder will be converted to a ByteArray, and thus byte aligned before the output of the next ByteRange.
     */
    fun append(bitBuilder: BitBuilder, description: String? = "BitBuilder"): ByteBuilder {
        append(BitBuilderByteRange(bitBuilder, description))
        return this
    }

    fun append(bitRange: BitRange): ByteBuilder {
        append(bitRange.toByteArray(), bitRange.description)
        return this
    }

    fun append(byte: Byte, description: String? = "Byte"): ByteBuilder {
        val ba = byteArrayOf(byte)
        append(ByteRange(ba, description = description))
        return this
    }

    fun append(short: Short, description: String? = "Short"): ByteBuilder {
        val ba = ByteArray(Short.SIZE_BYTES)
        val bb = ByteBuffer.wrap(ba)
        bb.putShort(short)
        append(ByteRange(ba, description = description))
        return this
    }

    fun append(integer: Int, description: String? = "Int"): ByteBuilder {
        val ba = ByteArray(Int.SIZE_BYTES)
        val bb = ByteBuffer.wrap(ba)
        bb.putInt(integer)
        append(ByteRange(ba, description = description))
        return this
    }

    fun append(long: Long, description: String? = "Long"): ByteBuilder {
        val ba = ByteArray(Long.SIZE_BYTES)
        val bb = ByteBuffer.wrap(ba)
        bb.putLong(long)
        append(ByteRange(ba, description = description))
        return this
    }

    fun append(float: Float, description: String? = "Float"): ByteBuilder {
        val ba = ByteArray(Float.SIZE_BYTES)
        val bb = ByteBuffer.wrap(ba)
        bb.putFloat(float)
        append(ByteRange(ba, description = description))
        return this
    }

    fun append(double: Double, description: String? = "Double"): ByteBuilder {
        val ba = ByteArray(Long.SIZE_BYTES)
        val bb = ByteBuffer.wrap(ba)
        bb.putDouble(double)
        append(ByteRange(ba, description = description))
        return this
    }

    fun prepend(byteBuilderRange: ByteBuilderRange): ByteBuilder {
        orderedByteRanges.addFirst(byteBuilderRange)
        size += byteBuilderRange.size
        return this
    }

    fun prepend(byteRange: ByteRange): ByteBuilder {
        prepend(byteRange as ByteBuilderRange)
        return this
    }

    fun prepend(byteBuilder: ByteBuilder): ByteBuilder {
        //When prepending, we must prepend in opposite order to maintain the correct order of the byte builder we are prepending as a unit.
        byteBuilder.getByteRanges().asReversed().forEach { byteBuilderRange -> prepend(byteBuilderRange) }
        return this
    }

    fun prepend(byteArray: ByteArray, description: String? = "ByteArray"): ByteBuilder {
        prepend(ByteRange(byteArray, description = description))
        return this
    }

    fun prepend(bitBuilder: BitBuilder, description: String? = "BitBuilder"): ByteBuilder {
        prepend(bitBuilder.toByteArray(), description)
        return this
    }

    fun prepend(bitRange: BitRange): ByteBuilder {
        prepend(bitRange.toByteArray(), bitRange.description)
        return this
    }

    fun prepend(byte: Byte, description: String? = "Byte"): ByteBuilder {
        val ba = byteArrayOf(byte)
        prepend(ByteRange(ba, description = description))
        return this
    }

    fun prepend(short: Short, description: String? = "Short"): ByteBuilder {
        val ba = ByteArray(Short.SIZE_BYTES)
        val bb = ByteBuffer.wrap(ba)
        bb.putShort(short)
        prepend(ByteRange(ba, description = description))
        return this
    }

    fun prepend(integer: Int, description: String? = "Int"): ByteBuilder {
        val ba = ByteArray(Int.SIZE_BYTES)
        val bb = ByteBuffer.wrap(ba)
        bb.putInt(integer)
        prepend(ByteRange(ba, description = description))
        return this
    }

    fun prepend(long: Long, description: String? = "Long"): ByteBuilder {
        val ba = ByteArray(Long.SIZE_BYTES)
        val bb = ByteBuffer.wrap(ba)
        bb.putLong(long)
        prepend(ByteRange(ba, description = description))
        return this
    }

    fun prepend(float: Float, description: String? = "Float"): ByteBuilder {
        val ba = ByteArray(Float.SIZE_BYTES)
        val bb = ByteBuffer.wrap(ba)
        bb.putFloat(float)
        prepend(ByteRange(ba, description = description))
        return this
    }

    fun prepend(double: Double, description: String? = "Double"): ByteBuilder {
        val ba = ByteArray(Long.SIZE_BYTES)
        val bb = ByteBuffer.wrap(ba)
        bb.putDouble(double)
        prepend(ByteRange(ba, description = description))
        return this
    }

    fun getByteRanges(): List<ByteBuilderRange> {
        return orderedByteRanges
    }

    override fun toByteArray(): ByteArray {
        val result = ByteArray(size)
        var pointer = 0
        orderedByteRanges.forEach { range ->
            val ba = range.toByteArray()
            ba.copyInto(result, pointer)
            pointer += range.size
        }
        return result
    }

    fun toByteRange(): ByteRange {
        return ByteRange(toByteArray())
    }

    fun toByteBuffer(): ByteBuffer {
        return ByteBuffer.wrap(toByteArray())
    }

    override fun iterator(): ByteIterator {
        return ByteIterator(toByteArray())
    }

    override fun writeToOutputStream(toWriteTo: OutputStream) {
        for (byteRange in orderedByteRanges) {
            byteRange.writeToOutputStream(toWriteTo)
        }
    }

    fun toInputStream(): InputStream {
        return ByteRangesInputStream(orderedByteRanges)
    }

    override fun toString(): String {
        var str = "Size: '$size' bytes, '${orderedByteRanges.size}' ByteRanges"
        if (size < 200) {
            orderedByteRanges.forEach { str += ", {${it}}" }
        }
        return str
    }

}