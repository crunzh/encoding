package bit.crunzh.utilities.encoding.hash

import bit.crunzh.utilities.encoding.byte.ByteRange
import java.io.InputStream
import java.math.BigInteger
import java.util.*

class Fnv1(private val fnvType: FnvType, init: BigInteger, private val prime: BigInteger, private val mod: BigInteger, private val outputSizeBytes: Int) : HashAlgorithm {

    private var hash: BigInteger = init
    override fun append(inputStream: InputStream): HashAlgorithm {
        val readBuffer = ByteArray(100)
        var readBytes = -1
        do {
            readBytes = inputStream.read(readBuffer)
            for (idx in 0 until readBytes) {
                append(readBuffer[idx])
            }
        } while (readBytes > -1)
        return this
    }

    override fun append(bytes: ByteIterator): HashAlgorithm {
        bytes.forEach { byte ->
            append(byte)
        }
        return this
    }

    override fun append(bytes: ByteRange): HashAlgorithm {
        bytes.forEach { byte ->
            append(byte)
        }
        return this
    }

    override fun append(bytes: ByteArray): HashAlgorithm {
        bytes.forEach { byte ->
            append(byte)
        }
        return this
    }

    private fun append(byte: Byte) {
        when(fnvType) {
            FnvType.Fnv1 -> appendFnv1(byte)
            FnvType.Fnv1a -> appendFnv1a(byte)
        }
    }

    private fun appendFnv1(byte: Byte) {
        hash = hash.multiply(prime).mod(mod)
        hash = hash.xor(BigInteger.valueOf((byte.toInt() and 0xff).toLong()))
    }

    private fun appendFnv1a(byte: Byte) {
        hash = hash.xor(BigInteger.valueOf((byte.toInt() and 0xff).toLong()))
        hash = hash.multiply(prime).mod(mod)
    }

    override fun getHash(): ByteArray {
        return getHashByteArrayFromBigInteger(hash, outputSizeBytes)
    }

    enum class FnvType {
        Fnv1,
        Fnv1a
    }

    companion object {
        fun getHashByteArrayFromBigInteger(hash: BigInteger, outputSizeBytes: Int): ByteArray {
            val byteArrayHash = hash.toByteArray()
            return when {
                byteArrayHash.size > outputSizeBytes -> {
                    val remainder = byteArrayHash.size % outputSizeBytes
                    Arrays.copyOfRange(byteArrayHash, remainder, byteArrayHash.size)
                }
                byteArrayHash.size < outputSizeBytes -> {
                    val fullArray = ByteArray(outputSizeBytes)
                    System.arraycopy(byteArrayHash, 0, fullArray, 0, byteArrayHash.size)
                    fullArray
                }
                else -> {
                    byteArrayHash
                }
            }
        }
    }
}