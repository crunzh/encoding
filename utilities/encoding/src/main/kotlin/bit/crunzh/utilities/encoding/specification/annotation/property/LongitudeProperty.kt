package bit.crunzh.utilities.encoding.specification.annotation.property

/**
 * This annotation is only valid if exactly one other @LatitudeProperty annotation is on the same level of the same object and the parent class is annotated as @PositionClass.
 * The @LatitudeProperty must have a lower sequence number than the @LongitudeProperty
 * The type this annotation supports is only Double.
 * The precision of the longitude is the one defined for the latitude
 * If multiple position (Lat, Long) is specified in any part of the root object, including lists etc. the position will be offset compressed.
 * The encoding will cause loss of precision as according to the defined precision in meters on the latitude annotation.
 *
 * Longitude is valid in the range [-180 to 180]
 */
@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class LongitudeProperty(
        val seqNo: Int
)