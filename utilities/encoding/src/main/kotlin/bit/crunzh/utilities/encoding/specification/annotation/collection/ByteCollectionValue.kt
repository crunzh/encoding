package bit.crunzh.utilities.encoding.specification.annotation.collection

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ByteCollectionValue(
        val minValue: Byte = 0,
        val maxValue: Byte = 127
)