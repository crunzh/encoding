package bit.crunzh.utilities.encoding.bit

/**
 * A BitRange which allows modifying the bits.
 */
class MutableBitRange(internal val bytes: ByteArray, internal val bitOffset: Int = 0, internal val bitLength: Int = (bytes.size * Byte.SIZE_BITS) - bitOffset, val description: String? = null) {
    /**
     * Create a MutableBitRange by cloning the [BitRange.bytes] backing [ByteArray] to avoid modifying the BitRange which should be treated as immutable.
     */
    constructor(bitRange: BitRange) : this(bitRange.bytes.clone(), bitRange.bitOffset, bitRange.bitLength)

    /**
     * Create a MutableBitRange with the specified bit size, backed by a new [ByteArray]
     */
    constructor(bitSize: Int) : this(ByteArray(BitBuilder.getNoBytesForBits(bitSize)), bitOffset = 0, bitLength = bitSize)

    /**
     * Get a specific bit from an index.
     */
    fun get(index: Int): Boolean {
        if (index >= bitLength) {
            throw IllegalArgumentException("The bit index to get '$index' is outside the range of this MutableBitRange with size '$bitLength'.")
        }
        return BitUtil.getBit(bytes, bitOffset, index)
    }

    fun set(index: Int, bitValue: Boolean) {
        if (index >= bitLength) {
            throw IllegalArgumentException("The bit index to set '$index' is outside the range of this MutableBitRange with size '$bitLength'.")
        }
        BitUtil.setBit(bytes, bitOffset, index, bitValue)
    }

    fun set(fromIndex: Int, toIndex: Int, bitValue: Boolean) {
        if (fromIndex > toIndex) {
            throw IllegalArgumentException("fromIndex '$fromIndex' must be smaller than toIndex '$toIndex'.")
        }
        if (toIndex >= bitLength) {
            throw IllegalArgumentException("The bit toIndex to set '$toIndex' is outside the range of this MutableBitRange with size '$bitLength'.")
        }
        for (idx in fromIndex..toIndex) {
            set(idx, bitValue)
        }
    }

    fun set(fromIndex: Int, bitRange: BitRange) {
        BitUtil.setBitRange(bitRange.bytes, bitRange.bitOffset, bitRange.bitLength, bytes, fromIndex)
    }

    fun expandHead(noOfBits: Int): MutableBitRange {
        val bytes = BitUtil.toByteArray(bytes, bitOffset, bitLength + noOfBits)

        TODO("bit shift bytes into the expandedBa making the old content aligned with the end bit of the end byte of the expandedBa.")
    }

    fun expandTail(noOfBits: Int): MutableBitRange {
        val expandedBa = BitUtil.toByteArray(bytes, bitOffset, bitLength + noOfBits)
        return MutableBitRange(expandedBa, 0, bitLength + noOfBits)
    }

    fun toBitRange(): BitRange {
        return BitRange(bytes, bitOffset, bitLength, description = description)
    }

    fun toByteArray(): ByteArray {
        if (bitOffset == 0 && bitLength % Byte.SIZE_BITS == 0 && bitLength / Byte.SIZE_BITS == bytes.size) {
            //The backing byte array represents exactly the required size.
            return bytes
        }
        return BitUtil.toByteArray(bytes, bitOffset, bitLength)
    }
}