package bit.crunzh.utilities.encoding.specification.core

class PropertyValues(values: Collection<PropertyValue<*>>) {
    private val nameValueMap = HashMap<String, PropertyValue<*>>()

    init {
        values.forEach { propValue ->
            nameValueMap[propValue.propertyName] = propValue
        }
    }
    fun <T : Any> getNullableValue(name: String): T? {
        val value = nameValueMap[name] ?: throw IllegalStateException("No value name '$name'.")
        if (value.propertyValue == null) return null
        return value.propertyValue as T
    }


    fun <T : Any> getValue(name: String): T {
        val value = nameValueMap[name] ?: throw IllegalStateException("No value name '$name'.")
        return value.propertyValue as T
    }
}