package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.specification.core.UnknownTypeException
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueBuilder

interface ValueEncoder<PropertyType : Any> {
    /**
     * If property is a complex type, which in may contain nested off set properties, re-delegate this call to the nested types.
     */
    fun collectOffsetValues(property: PropertyType, offsetValueBuilder: OffsetValueBuilder) {
        //Default NOP
    }

    /**
     * Encode this property to a [BitBuilder]
     */
    fun encode(property: PropertyType, encoderContext: EncoderContext): BitBuilder

    /**
     * Decode this property from [BitIterator]
     * @throws UnknownTypeException if a legal but unknown type is attempted read.
     */
    fun decode(bits: BitIterator, encoderContext: EncoderContext): PropertyType

    /**
     * Assert property is within the configured boundaries
     */
    fun assertValid(property: PropertyType) {
        //Default NOP
    }
}