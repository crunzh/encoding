package bit.crunzh.utilities.encoding.specification.annotation.collection

/**
 * Property must also additionally be annotated with one of the collection types to be valid.
 */
@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class SetCollectionValue(
        val setSizeVarInt: IntArray = [4, 6, 10, 16]
)