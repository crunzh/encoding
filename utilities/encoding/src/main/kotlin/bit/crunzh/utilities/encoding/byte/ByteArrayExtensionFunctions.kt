package bit.crunzh.utilities.encoding.byte

import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.bit.ByteArrayBitIterator

fun ByteArray.toByteRange(offset: Int = 0, size: Int = this.size - offset, description: String? = null): ByteRange {
    return ByteRange(this, offset, size, description)
}

fun ByteArray.toByteIterator(offset: Int = 0, size: Int = this.size - offset, description: String? = null): ByteIterator {
    return ByteIterator(this, offset, size, description)
}

fun ByteArray.toBitIterator(offset: Int = 0, size: Int = (this.size * Byte.SIZE_BITS) - offset): BitIterator {
    return ByteArrayBitIterator(this, offset, size)
}

fun ByteArray.wrapInByteBuilder(description: String? = null): ByteBuilder {
    return ByteBuilder(this, description)
}