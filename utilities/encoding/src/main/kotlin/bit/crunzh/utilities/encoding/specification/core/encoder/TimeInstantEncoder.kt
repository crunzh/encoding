package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueBuilder
import java.time.Instant

/**
 * Like the Epoch Time Descriptor, only working on Instant classes
 */
class TimeInstantEncoder(
        timeResolutionMs: Int = 1000,
        minTime: String = "2020-01-01T00:00:00+00:00",
        maxTime: String = "2050-01-01T00:00:00+00:00"
) : ValueEncoder<Instant> {

    private val epochEncoder = TimeEpochMsEncoder(timeResolutionMs, minTime, maxTime)

    override fun collectOffsetValues(property: Instant, offsetValueBuilder: OffsetValueBuilder) {
        epochEncoder.collectOffsetValues(property.toEpochMilli(), offsetValueBuilder)
    }

    override fun encode(property: Instant, encoderContext: EncoderContext): BitBuilder {
        return epochEncoder.encode(property.toEpochMilli(), encoderContext)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Instant {
        val epochMs = epochEncoder.decode(bits, encoderContext)
        return Instant.ofEpochMilli(epochMs)
    }

    override fun assertValid(property: Instant) {
        epochEncoder.assertValid(property.toEpochMilli())
    }
}