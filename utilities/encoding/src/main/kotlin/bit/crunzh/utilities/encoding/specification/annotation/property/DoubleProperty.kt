package bit.crunzh.utilities.encoding.specification.annotation.property

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class DoubleProperty(
        val seqNo: Int,
        val minValue: Double = 0.0,
        val maxValue: Double,
        val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
)