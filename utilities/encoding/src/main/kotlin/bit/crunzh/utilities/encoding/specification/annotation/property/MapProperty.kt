package bit.crunzh.utilities.encoding.specification.annotation.property

/**
 * Property must also additionally be annotated with two of the collection annotation types to be valid.
 * One must have CollectionElementType.KEY and the other CollectionElementType.VALUE.
 */
@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class MapProperty (
        val seqNo: Int,
        val mapLengthVarInt: IntArray = [4, 6, 10, 16]
)