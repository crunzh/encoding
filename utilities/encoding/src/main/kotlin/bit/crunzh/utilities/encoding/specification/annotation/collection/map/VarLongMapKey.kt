package bit.crunzh.utilities.encoding.specification.annotation.collection.map

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class VarLongMapKey (
        val signed: Boolean = false,
        val varIntSpecification: IntArray = [8, 16, 24, 32, 40, 48, 56, 63],
        val byteOrder: ByteOrder = ByteOrder.LITTLE_ENDIAN
)