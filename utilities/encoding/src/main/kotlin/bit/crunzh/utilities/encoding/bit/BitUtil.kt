package bit.crunzh.utilities.encoding.bit

import java.nio.ByteOrder
import kotlin.IllegalArgumentException
import kotlin.experimental.and
import kotlin.experimental.or
import kotlin.math.absoluteValue
import kotlin.math.min

object BitUtil {
    private const val EMPTY = 0
    private const val BIT_1 = 1
    private const val BIT_2 = 1 shl 1
    private const val BIT_3 = 1 shl 2
    private const val BIT_4 = 1 shl 3
    private const val BIT_5 = 1 shl 4
    private const val BIT_6 = 1 shl 5
    private const val BIT_7 = 1 shl 6
    private const val BIT_8 = 1 shl 7
    private const val BIT_1_INVERSE = BIT_1.inv()
    private const val BIT_2_INVERSE = BIT_2.inv()
    private const val BIT_3_INVERSE = BIT_3.inv()
    private const val BIT_4_INVERSE = BIT_4.inv()
    private const val BIT_5_INVERSE = BIT_5.inv()
    private const val BIT_6_INVERSE = BIT_6.inv()
    private const val BIT_7_INVERSE = BIT_7.inv()
    private const val BIT_8_INVERSE = BIT_8.inv()
    private const val MASK_INCLUDE_RIGHT_0 = 0
    private const val MASK_INCLUDE_RIGHT_1 = BIT_1
    private const val MASK_INCLUDE_RIGHT_2 = 0x03
    private const val MASK_INCLUDE_RIGHT_3 = 0x07
    private const val MASK_INCLUDE_RIGHT_4 = 0x0F
    private const val MASK_INCLUDE_RIGHT_5 = 0x1F
    private const val MASK_INCLUDE_RIGHT_6 = 0x3F
    private const val MASK_INCLUDE_RIGHT_7 = 0x7F
    private const val MASK_INCLUDE_RIGHT_8 = 0xFF
    private const val MASK_EXCLUDE_RIGHT_0 = MASK_INCLUDE_RIGHT_0.inv()
    private const val MASK_EXCLUDE_RIGHT_1 = MASK_INCLUDE_RIGHT_1.inv()
    private const val MASK_EXCLUDE_RIGHT_2 = MASK_INCLUDE_RIGHT_2.inv()
    private const val MASK_EXCLUDE_RIGHT_3 = MASK_INCLUDE_RIGHT_3.inv()
    private const val MASK_EXCLUDE_RIGHT_4 = MASK_INCLUDE_RIGHT_4.inv()
    private const val MASK_EXCLUDE_RIGHT_5 = MASK_INCLUDE_RIGHT_5.inv()
    private const val MASK_EXCLUDE_RIGHT_6 = MASK_INCLUDE_RIGHT_6.inv()
    private const val MASK_EXCLUDE_RIGHT_7 = MASK_INCLUDE_RIGHT_7.inv()
    private const val MASK_EXCLUDE_RIGHT_8 = MASK_INCLUDE_RIGHT_8.inv()
    private const val MASK_INCLUDE_LEFT_0 = 0
    private const val MASK_INCLUDE_LEFT_1 = MASK_EXCLUDE_RIGHT_7
    private const val MASK_INCLUDE_LEFT_2 = MASK_EXCLUDE_RIGHT_6
    private const val MASK_INCLUDE_LEFT_3 = MASK_EXCLUDE_RIGHT_5
    private const val MASK_INCLUDE_LEFT_4 = MASK_EXCLUDE_RIGHT_4
    private const val MASK_INCLUDE_LEFT_5 = MASK_EXCLUDE_RIGHT_3
    private const val MASK_INCLUDE_LEFT_6 = MASK_EXCLUDE_RIGHT_2
    private const val MASK_INCLUDE_LEFT_7 = MASK_EXCLUDE_RIGHT_1
    private const val MASK_INCLUDE_LEFT_8 = MASK_EXCLUDE_RIGHT_0
    private const val MASK_EXCLUDE_LEFT_0 = MASK_INCLUDE_LEFT_0.inv()
    private const val MASK_EXCLUDE_LEFT_1 = MASK_INCLUDE_LEFT_1.inv()
    private const val MASK_EXCLUDE_LEFT_2 = MASK_INCLUDE_LEFT_2.inv()
    private const val MASK_EXCLUDE_LEFT_3 = MASK_INCLUDE_LEFT_3.inv()
    private const val MASK_EXCLUDE_LEFT_4 = MASK_INCLUDE_LEFT_4.inv()
    private const val MASK_EXCLUDE_LEFT_5 = MASK_INCLUDE_LEFT_5.inv()
    private const val MASK_EXCLUDE_LEFT_6 = MASK_INCLUDE_LEFT_6.inv()
    private const val MASK_EXCLUDE_LEFT_7 = MASK_INCLUDE_LEFT_7.inv()
    private const val MASK_EXCLUDE_LEFT_8 = MASK_INCLUDE_LEFT_8.inv()

    fun toByteArray(array: ByteArray, bitOffset: Int, bitLength: Int, byteLength: Int = BitBuilder.getNoBytesForBits(bitLength)): ByteArray {
        if (BitBuilder.getNoBytesForBits(bitLength) > byteLength) {
            throw IllegalArgumentException("$bitLength bits cannot be squeezed into a $byteLength sized ByteArray.")
        }
        val dstBitRemainder = bitLength % Byte.SIZE_BITS

        val srcStartBitRemainder = bitOffset % Byte.SIZE_BITS
        val srcStartBitRemainderOtherHalf = Byte.SIZE_BITS - srcStartBitRemainder
        val srcStartByte = bitOffset / Byte.SIZE_BITS

        val srcByteEndRemainder = (bitOffset + bitLength) % Byte.SIZE_BITS
        val srcEndByte = if (srcByteEndRemainder > 0) {
            ((bitOffset + bitLength) / Byte.SIZE_BITS) + 1
        } else {
            (bitOffset + bitLength) / Byte.SIZE_BITS
        }


        val outputArray: ByteArray
        if (bitOffset == 0 && byteLength == array.size && dstBitRemainder == 0) {
            return array
        } else if (srcStartBitRemainder == 0) {
            outputArray = array.copyOfRange(srcStartByte, srcEndByte)
        } else {
            outputArray = ByteArray(byteLength)

            var outputArrayIdx = 0
            var srcIdx = srcStartByte
            while (outputArrayIdx < outputArray.size) {
                outputArray[outputArrayIdx] = getByteInternal(array, srcStartBitRemainder, srcStartBitRemainderOtherHalf, srcIdx)
                srcIdx++
                outputArrayIdx++
            }
        }
        if (dstBitRemainder > 0) {
            val bitsToClear = Byte.SIZE_BITS - dstBitRemainder
            val finalByte = outputArray[byteLength - 1]
            val finalByteMasked = clearRemainingBitsOfByte(finalByte.toInt() and 0xFF, bitsToClear)
            outputArray[byteLength - 1] = finalByteMasked.toByte()
        }
        return outputArray
    }

    fun getBit(array: ByteArray, bitOffset: Int = 0, bitIndex: Int): Boolean {
        val bitPosition = bitOffset + bitIndex
        val byteIndex = bitPosition / Byte.SIZE_BITS
        assertValidBitPointer(bitOffset, bitIndex, array, byteIndex)

        val andBitIndex = bitPosition % Byte.SIZE_BITS
        val andBit = getSingleBit(andBitIndex).toByte()
        return array[byteIndex] and andBit == andBit
    }

    fun getByte(bytes: ByteArray, bitOffset: Int): Byte {
        if (BitBuilder.getNoBytesForBits(bitOffset + Byte.SIZE_BITS) > bytes.size) {
            throw IllegalArgumentException("It is not possible to read a byte from ByteArray size '${bytes.size}' starting at bit offset '$bitOffset' (${bitOffset / Byte.SIZE_BITS} bytes, ${bitOffset % Byte.SIZE_BITS} bits).")
        }
        val startBitOffset = bitOffset % Byte.SIZE_BITS
        val excessBits = Byte.SIZE_BITS - startBitOffset
        val startByte = bitOffset / Byte.SIZE_BITS
        return getByteInternal(bytes, startBitOffset, excessBits, startByte)
    }

    private fun getByteInternal(bytes: ByteArray, startBitOffset: Int, excessBits: Int, startByteIdx: Int): Byte {
        if (startBitOffset == 0) {
            return bytes[startByteIdx]
        }
        val srcByte1 = bytes[startByteIdx].toInt() and 0xFF
        val srcByte1Br = BitBuilder().append(srcByte1, byteOrder = ByteOrder.LITTLE_ENDIAN).toBitRange()
        val outputByte1 = srcByte1 shr startBitOffset
        val outputByte1Br = BitBuilder().append(outputByte1, byteOrder = ByteOrder.LITTLE_ENDIAN).toBitRange()
        val srcByte2 = bytes[startByteIdx + 1].toInt() and 0xFF
        val srcByte2Br = BitBuilder().append(srcByte2, byteOrder = ByteOrder.LITTLE_ENDIAN).toBitRange()
        val outputByte2 = srcByte2 shl excessBits
        val outputByte2Br = BitBuilder().append(outputByte2, byteOrder = ByteOrder.LITTLE_ENDIAN).toBitRange()
        val outputByteFinal = (outputByte1 or outputByte2).toByte()
        return outputByteFinal
    }

    fun setBit(array: ByteArray, bitOffset: Int = 0, bitIndex: Int, bitValue: Boolean) {
        val bitPosition = bitOffset + bitIndex
        val byteIndex = bitPosition / Byte.SIZE_BITS
        assertValidBitPointer(bitOffset, bitIndex, array, byteIndex)

        val maskBitIndex = bitPosition % Byte.SIZE_BITS
        if (bitValue) {
            val bitMask = getSingleBit(maskBitIndex).toByte()
            array[byteIndex] = array[byteIndex] or bitMask
        } else {
            val bitMask = getSingleBitInverse(maskBitIndex).toByte()
            array[byteIndex] = array[byteIndex] and bitMask
        }
    }

    private fun assertValidBitPointer(bitOffset: Int, bitIndex: Int, array: ByteArray, byteIndex: Int) {
        if (bitOffset < 0) {
            throw IllegalArgumentException("bitOffset must not be less than 0, but was $bitOffset")
        }
        if (bitIndex < 0) {
            throw IllegalArgumentException("bitIndex must not be less than 0, but was $bitIndex")
        }
        if (array.size <= byteIndex) {
            throw IllegalArgumentException("Array must at least have a size of the bitOffset in bytes.")
        }
    }

    fun setBitRange(srcArray: ByteArray, srcBitOffset: Int, srcBitLength: Int, dstArray: ByteArray, dstBitOffset: Int, assumeDstClear: Boolean = true) {
        if (srcArray.size * Byte.SIZE_BITS < srcBitOffset + srcBitLength) {
            throw IllegalArgumentException("srcArray is smaller '${srcArray.size * Byte.SIZE_BITS} bits' than the bits requested read, srcBitOffset '$srcBitOffset' and srcBitLength '$srcBitLength'.")
        }
        if (BitBuilder.getNoBytesForBits(srcBitLength + dstBitOffset) > dstArray.size) {
            throw IllegalArgumentException("Not possible to set a bit range of size '$srcBitLength' into the dstArray of size '${dstArray.size * Byte.SIZE_BITS} bits' using offset '$dstBitOffset'.")
        }

        val srcByteFirstBits = srcBitOffset % Byte.SIZE_BITS
        val srcByteRemainingBits = Byte.SIZE_BITS - srcByteFirstBits
        val dstByteFirstBits = dstBitOffset % Byte.SIZE_BITS
        val dstByteRemainingBits = Byte.SIZE_BITS - dstByteFirstBits

        val srcByteOffset = srcBitOffset / Byte.SIZE_BITS
        val dstByteOffset = dstBitOffset / Byte.SIZE_BITS

        val wholeBytesToRead = srcBitLength / Byte.SIZE_BITS
        var byteReadOffset = 0
        while (byteReadOffset < wholeBytesToRead) {
            val srcBytePointer = srcByteOffset + byteReadOffset
            val dstBytePointer = dstByteOffset + byteReadOffset
            if (srcByteFirstBits == 0 && dstByteFirstBits == 0) {
                dstArray[dstBytePointer] = srcArray[srcBytePointer]
                byteReadOffset++
                continue
            }

            val positionToShift = (srcByteRemainingBits - dstByteRemainingBits).absoluteValue

            val srcByte1 = readSrcByte1(srcArray, srcBytePointer, srcByteFirstBits)
            val dstByte1 = readDstByte1(dstArray, dstBytePointer, dstByteRemainingBits, assumeDstClear)
            if (srcByteRemainingBits == dstByteRemainingBits) {
                //First byte
                dstArray[dstBytePointer] = (srcByte1 or dstByte1).toByte()

                //Second byte
                val srcByte2 = readSrcByte2(srcArray, srcBytePointer, srcByteRemainingBits)
                val dstByte2 = readDstByte2(dstArray, dstBytePointer, dstByteFirstBits, assumeDstClear)
                dstArray[dstBytePointer + 1] = (srcByte2 or dstByte2).toByte()
            } else if (srcByteRemainingBits > dstByteRemainingBits) {
                //First dst byte
                val srcByte1Part1Shifted = srcByte1 shl positionToShift
                dstArray[dstBytePointer] = (srcByte1Part1Shifted or dstByte1).toByte()

                //Second dst byte
                val srcByte1Part2Shifted = srcByte1 shr (Byte.SIZE_BITS - positionToShift)
                val srcByte2Shifted: Int = if (srcByteFirstBits == 0) {
                    0 //The src byte is confined in the first byte, don't read second, but provide a value that doesn't modify the result.
                } else {
                    readSrcByte2(srcArray, srcBytePointer, srcByteRemainingBits) shl positionToShift
                }
                val dstByte2 = readDstByte2(dstArray, dstBytePointer, dstByteFirstBits, assumeDstClear)
                dstArray[dstBytePointer + 1] = (srcByte1Part2Shifted or srcByte2Shifted or dstByte2).toByte()
            } else { //srcByteRemainingBits < dstByteRemainingBits
                //First dst byte
                val srcByte1Shifted = srcByte1 shr positionToShift
                val srcByte2 = readSrcByte2(srcArray, srcBytePointer, srcByteRemainingBits)
                val srcByte2Part1Shifted = srcByte2 shl (Byte.SIZE_BITS - positionToShift)
                dstArray[dstBytePointer] = (srcByte1Shifted or srcByte2Part1Shifted or dstByte1).toByte()

                //Second dst byte
                if (dstByteFirstBits > 0) {
                    val srcByte2Part2Shifted = srcByte2 shr positionToShift
                    val dstByte2 = readDstByte2(dstArray, dstBytePointer, dstByteFirstBits, assumeDstClear)
                    dstArray[dstBytePointer + 1] = (srcByte2Part2Shifted or dstByte2).toByte()
                }
            }
            byteReadOffset++
        }
        val remainingBits = srcBitLength % Byte.SIZE_BITS
        val bitOffset = wholeBytesToRead * Byte.SIZE_BITS
        if (remainingBits > 0) { //Set last less than a full byte worth of bits
            for (bitPointer in 0 until remainingBits) {
                setBit(dstArray, bitOffset + dstBitOffset, bitPointer, getBit(srcArray, bitOffset + srcBitOffset, bitPointer))
            }
        }
    }

    private fun readSrcByte1(srcArray: ByteArray, bytePointer: Int, srcByteFirstBits: Int): Int {
        val srcByte = srcArray[bytePointer].toInt() and 0xFF
        return clearFirstBitsOfByte(srcByte, srcByteFirstBits, srcByteFirstBits == 0)
    }

    private fun readSrcByte2(srcArray: ByteArray, bytePointer: Int, srcByteRemainingBits: Int, skipClear: Boolean = false): Int {
        val srcByte = srcArray[bytePointer + 1].toInt() and 0xFF
        return clearRemainingBitsOfByte(srcByte, srcByteRemainingBits, skipClear)
    }

    private fun readDstByte1(srcArray: ByteArray, bytePointer: Int, dstByteRemainingBits: Int, assumeClear: Boolean): Int {
        val dstByte = srcArray[bytePointer].toInt() and 0xFF
        return clearRemainingBitsOfByte(dstByte, dstByteRemainingBits, assumeClear)
    }

    private fun readDstByte2(srcArray: ByteArray, bytePointer: Int, dstByteFirstBits: Int, assumeClear: Boolean): Int {
        val dstByte = srcArray[bytePointer + 1].toInt() and 0xFF
        return clearFirstBitsOfByte(dstByte, dstByteFirstBits, assumeClear)
    }

    private fun clearRemainingBitsOfByte(byte: Int, amountOfBits: Int, skipClear: Boolean = false): Int {
        if (skipClear) {
            return byte
        }
        return byte and getLeftExcludeMask(amountOfBits)
    }

    private fun clearFirstBitsOfByte(byte: Int, amountOfBits: Int, skipClear: Boolean = false): Int {
        if (skipClear) {
            return byte
        }
        return byte and getRightExcludeMask(amountOfBits)
    }

    private fun getSingleBit(idx: Int): Int {
        return when (idx) {
            0 -> BIT_1
            1 -> BIT_2
            2 -> BIT_3
            3 -> BIT_4
            4 -> BIT_5
            5 -> BIT_6
            6 -> BIT_7
            7 -> BIT_8
            else -> throw IllegalArgumentException("Bit index must be [0-7]")
        }
    }

    private fun getSingleBitInverse(idx: Int): Int {
        return when (idx) {
            0 -> BIT_1_INVERSE
            1 -> BIT_2_INVERSE
            2 -> BIT_3_INVERSE
            3 -> BIT_4_INVERSE
            4 -> BIT_5_INVERSE
            5 -> BIT_6_INVERSE
            6 -> BIT_7_INVERSE
            7 -> BIT_8_INVERSE
            else -> throw IllegalArgumentException("Bit index must be [0-7]")
        }
    }

    /**
     * Right and left assumes reading bits in a byte with the least significant bit rightmost
     */
    private fun getRightExcludeMask(maskSize: Int): Int {
        return when (maskSize) {
            0 -> MASK_EXCLUDE_RIGHT_0
            1 -> MASK_EXCLUDE_RIGHT_1
            2 -> MASK_EXCLUDE_RIGHT_2
            3 -> MASK_EXCLUDE_RIGHT_3
            4 -> MASK_EXCLUDE_RIGHT_4
            5 -> MASK_EXCLUDE_RIGHT_5
            6 -> MASK_EXCLUDE_RIGHT_6
            7 -> MASK_EXCLUDE_RIGHT_7
            8 -> MASK_EXCLUDE_RIGHT_8
            else -> throw IllegalArgumentException("Bit index must be [0-7]")
        }
    }

    /**
     * Right and left assumes reading bits in a byte with the least significant bit rightmost
     */
    private fun getRightInclusiveMask(maskSize: Int): Int {
        return when (maskSize) {
            0 -> MASK_INCLUDE_RIGHT_0
            1 -> MASK_INCLUDE_RIGHT_1
            2 -> MASK_INCLUDE_RIGHT_2
            3 -> MASK_INCLUDE_RIGHT_3
            4 -> MASK_INCLUDE_RIGHT_4
            5 -> MASK_INCLUDE_RIGHT_5
            6 -> MASK_INCLUDE_RIGHT_6
            7 -> MASK_INCLUDE_RIGHT_7
            8 -> MASK_INCLUDE_RIGHT_8
            else -> throw IllegalArgumentException("Bit index must be [0-7]")
        }
    }

    /**
     * Right and left assumes reading bits in a byte with the least significant bit rightmost
     */
    private fun getLeftExcludeMask(maskSize: Int): Int {
        return when (maskSize) {
            0 -> MASK_EXCLUDE_LEFT_0
            1 -> MASK_EXCLUDE_LEFT_1
            2 -> MASK_EXCLUDE_LEFT_2
            3 -> MASK_EXCLUDE_LEFT_3
            4 -> MASK_EXCLUDE_LEFT_4
            5 -> MASK_EXCLUDE_LEFT_5
            6 -> MASK_EXCLUDE_LEFT_6
            7 -> MASK_EXCLUDE_LEFT_7
            8 -> MASK_EXCLUDE_LEFT_8
            else -> throw IllegalArgumentException("Bit index must be [0-7], but was $maskSize")
        }
    }

    /**
     * Right and left assumes reading bits in a byte with the least significant bit rightmost
     */
    private fun getLeftIncludeMask(maskSize: Int): Int {
        return when (maskSize) {
            0 -> MASK_INCLUDE_LEFT_0
            1 -> MASK_INCLUDE_LEFT_1
            2 -> MASK_INCLUDE_LEFT_2
            3 -> MASK_INCLUDE_LEFT_3
            4 -> MASK_INCLUDE_LEFT_4
            5 -> MASK_INCLUDE_LEFT_5
            6 -> MASK_INCLUDE_LEFT_6
            7 -> MASK_INCLUDE_LEFT_7
            8 -> MASK_INCLUDE_LEFT_8
            else -> throw IllegalArgumentException("Bit index must be [0-7]")
        }
    }
}