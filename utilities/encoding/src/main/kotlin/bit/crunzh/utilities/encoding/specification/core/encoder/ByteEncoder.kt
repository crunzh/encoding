package bit.crunzh.utilities.encoding.specification.core.encoder

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.EncoderContext
import kotlin.IllegalArgumentException
import kotlin.math.max

class ByteEncoder(private val minValue: Byte = Byte.MIN_VALUE, private val maxValue: Byte = Byte.MAX_VALUE) : ValueEncoder<Byte> {
    private val signed: Boolean
    private val bitLength: Int
    private val offsetValue: Byte

    init {
        if (maxValue < minValue) throw IllegalArgumentException("maxValue cannot be less than minValue")
        val diff = maxValue - minValue
        if (diff > Byte.MAX_VALUE) {
            offsetValue = 0
            signed = minValue < 0
            bitLength = max(BitBuilder.getBitsRequired(minValue, signed), BitBuilder.getBitsRequired(maxValue, signed))
        } else {
            signed = false
            offsetValue = minValue
            bitLength = BitBuilder.getBitsRequired(diff.toLong())
        }
    }

    override fun encode(property: Byte, encoderContext: EncoderContext): BitBuilder {
        val offSetValue = property - offsetValue
        return BitBuilder(offSetValue.toByte(), bitLength, signed)
    }

    override fun decode(bits: BitIterator, encoderContext: EncoderContext): Byte {
        return (bits.next(bitLength).toByte(signed = signed) + offsetValue).toByte()
    }

    override fun assertValid(property: Byte) {
        if (property < minValue) throw IllegalArgumentException("Property Value may not be less than '$minValue' but was '$property'.")
        if (property > maxValue) throw IllegalArgumentException("Property value may not me more than '$maxValue' but was '$property'")
    }
}