package bit.crunzh.utilities.encoding.specification.annotation.collection.map

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ShortMapKey(
        val minValue: Short = 0,
        val maxValue: Short = 32767,
        val byteOrder: ByteOrder = ByteOrder.LITTLE_ENDIAN
)