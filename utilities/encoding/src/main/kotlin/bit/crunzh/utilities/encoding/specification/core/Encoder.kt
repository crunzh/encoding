package bit.crunzh.utilities.encoding.specification.core

import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.bit.BitRange
import kotlin.reflect.KClass

interface Encoder {
    var print: Boolean
    fun <T : Any> encode(objectToEncode: T): BitRange
    fun <T : Any> decode(bitIterator: BitIterator, type: KClass<T>): T

    /**
     * @throws UnknownTypeException if one of the encoded types was unknown.
     */
    fun decode(bitIterator: BitIterator): Any
}