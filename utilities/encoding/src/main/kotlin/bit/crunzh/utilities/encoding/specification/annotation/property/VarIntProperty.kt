package bit.crunzh.utilities.encoding.specification.annotation.property

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class VarIntProperty (
        val seqNo: Int,
        val signed: Boolean = false,
        val varIntSpecification: IntArray = [8, 16, 24, 31],
        val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
)