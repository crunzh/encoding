package bit.crunzh.utilities.encoding.specification.annotation.collection

import kotlin.reflect.KClass

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class EnumCollectionValue (
        /**
         * If specified as -1, the enums current number of values is used.
         * If specified as > 0, the encoding will reserve room for additional values to be added in the future.
         * If specified as > 0, it must match a whole number of bits, ie. 2, 4, 8, 16, 32 etc.
         */
        val maxEnumValues: Int = -1,
        val enumType: KClass<*>,
        /**
         * If an unknown enum value is received, this known Enum ordinal value will be assigned.
         * If specified as -1, no unknown value is defined and decoding will throw an exception for unknown values.
         */
        val unknownValueOrdinal: Int = -1
)