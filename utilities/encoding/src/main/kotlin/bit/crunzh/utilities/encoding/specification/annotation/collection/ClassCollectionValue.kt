package bit.crunzh.utilities.encoding.specification.annotation.collection

import kotlin.reflect.KClass

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ClassCollectionValue (
        val topType: KClass<*>,
        /**
         * The order of the subtypes is significant, and re-ordering them will cause in-compatibility.
         */
        val subTypes: Array<KClass<*>> = [],
        /**
         * This value must be at least 1, and should be a value of 2^bits, ie. 1, 2, 4, 8, 16, 32 etc.
         * */
        val numberOfTypesToSupport: Int = 16
)

