package bit.crunzh.utilities.encoding.specification.annotation.property

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class BooleanProperty(
        val seqNo: Int
)