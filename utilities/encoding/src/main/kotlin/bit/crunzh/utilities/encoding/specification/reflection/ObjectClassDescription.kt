package bit.crunzh.utilities.encoding.specification.reflection

import bit.crunzh.utilities.encoding.specification.core.ClassDescription
import bit.crunzh.utilities.encoding.specification.core.PropertyDescription
import bit.crunzh.utilities.encoding.specification.core.PropertyValues
import kotlin.reflect.KClass

class ObjectClassDescription<T : Any>(override val type: KClass<T>) : ClassDescription<T> {
    override val isExtendable: Boolean
        get() = false
    override val supportOffsetValues: Boolean
        get() = false
    override val propertyDescriptors: List<PropertyDescription<*, T>>
        get() = emptyList()

    override fun create(constParams: PropertyValues): T {
        return type.objectInstance!!
    }
}