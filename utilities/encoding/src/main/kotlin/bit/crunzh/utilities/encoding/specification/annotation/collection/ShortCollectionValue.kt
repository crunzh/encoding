package bit.crunzh.utilities.encoding.specification.annotation.collection

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ShortCollectionValue(
        val minValue: Short = 0,
        val maxValue: Short = 32767,
        val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
)