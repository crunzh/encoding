package bit.crunzh.utilities.encoding.specification.core

class UnknownTypeException(message: String) : Exception(message) {
}