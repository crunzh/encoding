package bit.crunzh.utilities.encoding.specification.annotation.property

/**
 * This property contains a computed value, which is not possible to set using constructor or property setter.
 * Usually such a field will be assigned based on other properties or constructor input.
 */
@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ComputedValueProperty