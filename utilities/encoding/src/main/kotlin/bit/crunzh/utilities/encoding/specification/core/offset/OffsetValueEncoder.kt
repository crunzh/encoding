package bit.crunzh.utilities.encoding.specification.core.offset

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.ClassDescription
import bit.crunzh.utilities.encoding.specification.core.PropertyDescription
import bit.crunzh.utilities.encoding.varint.VarIntEncoder

object OffsetValueEncoder {
    val latLongOffsetEncoder = VarIntEncoder(8, 16, 24, 31)
    val timeOffsetEncoder = VarIntEncoder(8, 16, 24, 63)
    const val positionDeltaLengthBits = 5
    const val absTimeDeltaLengthBits = 6
    private const val positionOffsetEnabledDescription = "Position offset encoding enablement"
    private const val latitudeOffsetDescription = "Latitude offset value"
    private const val latitudeDeltaDescription = "Latitude delta length"
    private const val longitudeOffsetDescription = "Longitude offset value"
    private const val longitudeDeltaDescription = "Longitude delta length"
    private const val timeOffsetEncodingDescription = "Time offset encoding enablement"
    private const val timeOffsetDescription = "Time offset value"
    private const val timeDeltaDescription = "Time delta length"

    fun <T : Any> collect(objectToEncode: T, classDescription: ClassDescription<T>): OffsetValues {
        val offsetValueBuilder = OffsetValueBuilderImpl()
        classDescription.propertyDescriptors.forEach { propertyDescriptor ->
            collectPropertyOffsetValues(objectToEncode, propertyDescriptor, offsetValueBuilder)
        }
        return offsetValueBuilder.build()
    }

    fun <ClassType : Any, PropertyType : Any> collectPropertyOffsetValues(objectToEncode: ClassType, propertyDescriptor: PropertyDescription<PropertyType, ClassType>, offsetValueBuilder: OffsetValueBuilder) {
        val propertyValue = propertyDescriptor.getPropertyValue(objectToEncode)
        if (propertyValue != null) {
            propertyDescriptor.collectOffsetValues(propertyValue, offsetValueBuilder)
        }
    }

    fun encode(offsetValues: OffsetValues): BitBuilder {
        val builder = BitBuilder()
        builder.append(offsetValues.usePositionOffsetEncoding, positionOffsetEnabledDescription)
        if (offsetValues.usePositionOffsetEncoding) {
            builder.append(latLongOffsetEncoder.encode(offsetValues.latitudeOffset, latitudeOffsetDescription))
            builder.append(offsetValues.bitsRequiredForLatitudeDelta, positionDeltaLengthBits, false, description = latitudeDeltaDescription)
            builder.append(latLongOffsetEncoder.encode(offsetValues.longitudeOffset, longitudeOffsetDescription))
            builder.append(offsetValues.bitsRequiredForLongitudeDelta, positionDeltaLengthBits, false, description = longitudeDeltaDescription)
        }
        builder.append(offsetValues.useTimeOffsetEncoding, timeOffsetEncodingDescription)
        if (offsetValues.useTimeOffsetEncoding) {
            builder.append(timeOffsetEncoder.encode(offsetValues.absTimeOffset, timeOffsetDescription))
            builder.append(offsetValues.bitsRequiredForTimeDelta, absTimeDeltaLengthBits, false, description = timeDeltaDescription)
        }
        return builder
    }

    fun decode(bits: BitIterator): OffsetValues {
        var latitudeOffset = 0
        var bitsForLatitudeDelta = 31
        var longitudeOffset = 0
        var bitsForLongitudeDelta = 31

        val hasPositionOffset = bits.next()
        if (hasPositionOffset) {
            latitudeOffset = latLongOffsetEncoder.decode(bits).toInt()
            bitsForLatitudeDelta = bits.next(positionDeltaLengthBits).toInt()
            longitudeOffset = latLongOffsetEncoder.decode(bits).toInt()
            bitsForLongitudeDelta = bits.next(positionDeltaLengthBits).toInt()
        }

        var absTimeOffset: Long = 0
        var bitsForAbsTimeDelta = 63
        val hasTimeOffset = bits.next()
        if (hasTimeOffset) {
            absTimeOffset = timeOffsetEncoder.decode(bits).toLong()
            bitsForAbsTimeDelta = bits.next(absTimeDeltaLengthBits).toInt()
        }
        return OffsetValues(absTimeOffset, bitsForAbsTimeDelta, hasTimeOffset, latitudeOffset, bitsForLatitudeDelta, longitudeOffset, bitsForLongitudeDelta, hasPositionOffset)
    }
}