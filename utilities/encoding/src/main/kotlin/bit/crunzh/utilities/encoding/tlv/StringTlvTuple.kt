package bit.crunzh.utilities.encoding.tlv

class StringTlvTuple(override val type: String, override val value: ByteArray) : TlvTuple<String> {
    override val length = value.size
}