package bit.crunzh.utilities.encoding.specification.annotation.collection.map

import bit.crunzh.utilities.encoding.specification.annotation.ByteOrder

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class VarShortMapKey (
        val seqNo: Int,
        val signed: Boolean = false,
        val varIntSpecification: IntArray = [8, 15],
        val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
)