package bit.crunzh.utilities.encoding.specification.annotation.collection.map

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ByteMapKey(
        val minValue: Byte = 0,
        val maxValue: Byte = 127
)