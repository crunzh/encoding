package bit.crunzh.utilities.encoding.specification.annotation.collection

/**
 * Property must also additionally be annotated with two of the collection annotation types to be valid.
 * One must have CollectionElementType.KEY and the other CollectionElementType.VALUE.
 */
@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class MapCollectionValue (
        val mapLengthVarInt: IntArray = [4, 6, 10, 16]
)