package bit.crunzh.utilities.encoding.position

import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.round

const val earthEquatorialCircumferenceMeters: Long = 40075017
const val earthMeridionalCircumferenceMeters: Long = 40007860
const val totalLatitudeDegrees = 180.0
const val maxLatitudeDegrees = 90.0
const val minLatitudeDegrees = -90.0
const val totalLongitudeDegrees = 360.0
const val maxLongitudeDegrees = 180.0
const val minLongitudeDegrees = -180.0

const val degreesToRadians: Double = PI / 180

class LossyPositionCompression(private val precisionInMeters: Double = 1.0) {
    /**
     * The compressed position will describe the amount of steps (step size given by precision in meters), from the prime meridian (longitude) or equator (latitude).
     */
    data class CompressedPosition(val latitudeSteps: Int, val longitudeSteps: Int, val stepSizeInMeters: Double, val maxLatitudeSteps: Double = calculateMaxLatitudeSteps(stepSizeInMeters), val maxLongitudeSteps: Double = calculateMaxLongitudeSteps(stepSizeInMeters))
    data class UncompressedPosition(val latitude: Double, val longitude: Double, val precisionInMeters: Double) {
        init {
            if (latitude > maxLatitudeDegrees || latitude < minLatitudeDegrees) throw IllegalArgumentException("Latitude value '$latitude' must be in the range [-90 - 90]");
            if (longitude > maxLongitudeDegrees || longitude < minLongitudeDegrees) throw IllegalArgumentException("Longitude value '$longitude' must be in the range [-180 - 180]");
        }
    }

    fun compressPosition(latitude: Double, longitude: Double) = compressPosition(latitude, longitude, precisionInMeters)
    fun decompressPosition(latitudeSteps: Int, longitudeStep: Int) = decompressPosition(latitudeSteps, longitudeStep, precisionInMeters)

    companion object {
        fun compressPosition(position: UncompressedPosition): CompressedPosition {
            val compressedLatitude = compressLatitude(position.latitude, position.precisionInMeters)
            val latitudeWithoutPrecision = decompressLatitude(compressedLatitude, position.precisionInMeters)
            return CompressedPosition(compressLatitude(position.latitude, position.precisionInMeters), compressLongitude(latitudeWithoutPrecision, position.longitude, position.precisionInMeters), position.precisionInMeters)
        }

        fun compressPosition(latitude: Double, longitude: Double, precisionInMeters: Double) = compressPosition(UncompressedPosition(latitude, longitude, precisionInMeters))


        fun decompressPosition(position: CompressedPosition): UncompressedPosition {
            val latitude = decompressLatitude(position.latitudeSteps, position.stepSizeInMeters)
            val longitude = decompressLongitude(latitude, position.longitudeSteps, position.stepSizeInMeters)
            return UncompressedPosition(latitude, longitude, position.stepSizeInMeters)
        }

        fun decompressPosition(latitudeStep: Int, longitudeStep: Int, precisionInMeters: Double) = decompressPosition(CompressedPosition(latitudeStep, longitudeStep, precisionInMeters, calculateMaxLatitudeSteps(precisionInMeters), calculateMaxLongitudeSteps(precisionInMeters)))


        internal fun compressLatitude(latitude: Double, precisionInMeters: Double): Int {
            val positiveLatitude = latitude + maxLatitudeDegrees // to transform from [-90 - 90] to [0-180] degrees
            val latitudeFactor = positiveLatitude / totalLatitudeDegrees
            val maxLatitudeSteps = calculateMaxLatitudeSteps(precisionInMeters)
            return round(maxLatitudeSteps * latitudeFactor).toInt()
        }

        internal fun decompressLatitude(compressedLatitude: Int, precisionInMeters: Double): Double {
            val maxLatitudeSteps = calculateMaxLatitudeSteps(precisionInMeters)
            val latitudeFactor = compressedLatitude / maxLatitudeSteps
            val positiveLatitude = totalLatitudeDegrees * latitudeFactor
            val latitude = positiveLatitude - maxLatitudeDegrees //to transform from [0-180] to [-90 - 90] degrees
            return boxInsideValidRange(latitude, minLatitudeDegrees, maxLatitudeDegrees)
        }

        internal fun compressLongitude(latitudeWithoutPrecision: Double, longitude: Double, precisionInMeters: Double): Int {
            val longitudeSteps = calculateLongitudeStepsAtLatitude(latitudeWithoutPrecision, precisionInMeters)
            val positiveLongitude = longitude + maxLongitudeDegrees // to transform from [-180 - 180] to [0-360] degrees
            val longitudeFactor = positiveLongitude / totalLongitudeDegrees
            return round(longitudeSteps * longitudeFactor).toInt()
        }

        internal fun decompressLongitude(latitude: Double, compressedLongitude: Int, precisionInMeters: Double): Double {
            val maxLongitudeSteps = calculateLongitudeStepsAtLatitude(latitude, precisionInMeters)
            val longitudeFactor = compressedLongitude / maxLongitudeSteps
            val positiveLongitude = totalLongitudeDegrees * longitudeFactor
            val longitude = positiveLongitude - maxLongitudeDegrees //to transform from [0-360] to [-180 - 180] degrees
            return boxInsideValidRange(longitude, minLongitudeDegrees, maxLongitudeDegrees)
        }

        fun removePrecision(position: UncompressedPosition): UncompressedPosition {
            return decompressPosition(compressPosition(position))
        }

        fun removePrecision(latitude: Double, longitude: Double, precisionInMeters: Double): UncompressedPosition = removePrecision(UncompressedPosition(latitude, longitude, precisionInMeters))

        private fun calculateLongitudeStepsAtLatitude(latitude: Double, precisionInMeters: Double): Double {
            val circumferenceAtLatitude = calculateCircumferenceAtLatitude(latitude)
            return circumferenceAtLatitude / precisionInMeters
        }

        private fun calculateCircumferenceAtLatitude(latitude: Double): Double {
            val radians = convertDegreesToRadians(latitude)
            return cos(radians) * earthEquatorialCircumferenceMeters
        }

        private fun convertDegreesToRadians(degrees: Double): Double {
            return degrees * degreesToRadians
        }

        fun calculateMaxLatitudeSteps(precisionInMeters: Double): Double {
            return earthMeridionalCircumferenceMeters / precisionInMeters
        }

        fun calculateMaxLongitudeSteps(precisionInMeters: Double): Double {
            return earthEquatorialCircumferenceMeters / precisionInMeters
        }

        private fun boxInsideValidRange(degrees: Double, minDegrees: Double, maxDegrees: Double): Double {
            return if (degrees < minDegrees) {
                minDegrees
            } else if (degrees > maxDegrees) {
                maxDegrees
            } else {
                degrees
            }
        }
    }
}
