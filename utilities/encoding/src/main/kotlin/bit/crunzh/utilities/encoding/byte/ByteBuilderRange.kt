package bit.crunzh.utilities.encoding.byte

import java.io.OutputStream

interface ByteBuilderRange {
    val size: Int
    val description: String?
    fun toByteArray(): ByteArray
    fun writeToOutputStream(toWriteTo: OutputStream)
    fun iterator(): ByteIterator
}