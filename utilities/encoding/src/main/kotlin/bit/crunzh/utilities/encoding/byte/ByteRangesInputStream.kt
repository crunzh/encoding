package bit.crunzh.utilities.encoding.byte

import java.io.InputStream
import kotlin.math.min

class ByteRangesInputStream(byteRanges: List<ByteBuilderRange>) : InputStream() {
    private var bytesRead = 0
    val size = byteRanges.sumBy(ByteBuilderRange::size)
    private val byteRangeIterator = byteRanges.iterator()
    var internalIterator: ByteIterator = byteRangeIterator.next().iterator()

    private fun getIterator(): ByteIterator {
        if (!internalIterator.isEmpty()) {
            return internalIterator
        }
        if (!byteRangeIterator.hasNext()) {
            throw IllegalStateException("Cannot read outside the given list of ByteRanges.")
        }
        internalIterator = byteRangeIterator.next().iterator()
        return internalIterator
    }

    private fun remaining(): Int {
        return size - bytesRead
    }

    private fun hasNext(): Boolean {
        return remaining() > 0
    }

    override fun read(): Int {
        if (!hasNext()) {
            return -1
        }
        bytesRead++
        return getIterator().next().toInt()
    }

    override fun read(b: ByteArray): Int {
        if (!hasNext()) {
            return -1
        }
        val iterator = getIterator()
        val bytesReadFromIterator = iterator.next(b)
        bytesRead += bytesReadFromIterator
        return bytesReadFromIterator
    }

    override fun read(b: ByteArray, off: Int, len: Int): Int {
        if (!hasNext()) {
            return -1
        }
        val iterator = getIterator()
        val bytesReadFromIterator = iterator.next(b, off, len)
        bytesRead += bytesReadFromIterator
        return bytesReadFromIterator
    }

    fun readAllBytes(): ByteArray {
        if (!hasNext()) {
            return ByteArray(0)
        }
        val builder = ByteBuilder()
        while (hasNext()) {
            val iterator = getIterator()
            builder.append(iterator.next(iterator.remaining()))
        }
        bytesRead += builder.size
        return builder.toByteArray()
    }

    fun readNBytes(len: Int): ByteArray {
        if (!hasNext()) {
            return ByteArray(0)
        }
        val bytesToRead = min(len, remaining())
        val builder = ByteBuilder()
        var bytesReadFromIterator = 0
        while (bytesReadFromIterator < bytesToRead) {
            val iterator = getIterator()
            val bytesToReadFromIterator = min(bytesToRead - bytesReadFromIterator, iterator.remaining())
            val readRange = iterator.next(bytesToReadFromIterator)
            bytesReadFromIterator += readRange.size
            builder.append(readRange)
        }
        bytesRead += builder.size
        return builder.toByteArray()
    }

    override fun skip(n: Long): Long {
        if (n < 0) {
            return 0
        }
        val bytesToSkip = min(remaining().toLong(), n)
        var bytesSkipped = 0
        while(bytesSkipped < bytesToSkip) {
            val iterator = getIterator()
            bytesSkipped += iterator.skip(bytesToSkip.toInt())
        }
        bytesRead += bytesSkipped
        return bytesToSkip
    }

    override fun available(): Int {
        return remaining()
    }
}