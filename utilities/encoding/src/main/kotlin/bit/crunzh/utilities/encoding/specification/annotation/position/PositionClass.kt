package bit.crunzh.utilities.encoding.specification.annotation.position

/**
 * This annotation is only valid if the class contains exactly one Double type property with @LatitudeProperty annotation and exactly one Double type property with @LongitudeProperty annotation.
 * The @LatitudeProperty must have a lower sequence number than the @LongitudeProperty
 * The encoding will cause loss of precision as according to the defined precision in meters.
 */
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
annotation class PositionClass(
        val positionPrecisionInMeters: Double = 1.0
)