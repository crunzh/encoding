package bit.crunzh.utilities.encoding.specification.annotation.property

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ByteStoreConstantSizeProperty(
        val seqNo: Int,
        val constantSize: Int
)