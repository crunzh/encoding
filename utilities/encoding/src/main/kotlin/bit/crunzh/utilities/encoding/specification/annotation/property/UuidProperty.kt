package bit.crunzh.utilities.encoding.specification.annotation.property

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class UuidProperty(
        val seqNo: Int
)