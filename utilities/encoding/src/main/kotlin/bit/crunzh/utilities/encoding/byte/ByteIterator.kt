package bit.crunzh.utilities.encoding.byte

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.bit.BitRange
import bit.crunzh.utilities.encoding.bit.ByteArrayBitIterator
import java.nio.ByteOrder
import kotlin.math.min

class ByteIterator(internal val bytes: ByteArray, internal val offset: Int = 0, val size: Int = bytes.size - offset, var description: String? = null) : Iterator<Byte> {
    var pointer: Int

    init {
        if (bytes.isEmpty()) {
            throw IllegalArgumentException("bytes must contain at least one byte.")
        }
        val maxOffset = bytes.size - size
        if (offset < 0 || offset > maxOffset) {
            throw IllegalArgumentException("bitOffset must be 0 or larger, but smaller than bitLength. It was '$offset' but should be [0-$maxOffset]")
        }
        val maxSize = bytes.size - offset
        if (size < 0 || size > maxSize) {
            throw IllegalArgumentException("bitLength must be larger than 0 but smaller than bytes.size * Byte.BIT_SIZE. It was '$size' but should be [0-$maxSize]")
        }
        pointer = offset
    }


    override fun hasNext(): Boolean {
        return hasNext(1)
    }

    fun hasNext(nextBytes: Int = 1): Boolean {
        return nextBytes <= remaining()
    }

    override fun next(): Byte {
        if (!hasNext()) {
            throw IllegalArgumentException("1 byte requested, but no bytes remaining.")
        }
        val nextByte = bytes[pointer]
        pointer++
        return nextByte
    }

    fun next(bytesToRead: Int): ByteRange {
        if (!hasNext(bytesToRead)) {
            throw IllegalArgumentException("$bytesToRead bytes requested, but only ${remaining()} bytes remaining.")
        }
        val rangeStart = pointer
        pointer += bytesToRead
        return ByteRange(bytes, rangeStart, bytesToRead)
    }

    fun next(toCopyInto: ByteArray): Int {
        if (!hasNext()) {
            throw IllegalArgumentException("No bytes remaining.")
        }
        val bytesToRead = min(toCopyInto.size, remaining())
        val rangeStart = pointer
        pointer += bytesToRead
        bytes.copyInto(toCopyInto, 0, rangeStart, rangeStart + bytesToRead)
        return bytesToRead
    }

    fun next(toCopyInto: ByteArray, off: Int, len: Int): Int {
        if (toCopyInto.size < off + len) {
            throw IllegalArgumentException("ByteArray to read into is smaller '${toCopyInto.size}' than the range specified by offset '$off' and length '$len'.")
        }
        if (!hasNext()) {
            throw IllegalArgumentException("No bytes remaining.")
        }
        val bytesToRead = min(len, remaining())
        val rangeStart = pointer
        pointer += bytesToRead
        bytes.copyInto(toCopyInto, off, rangeStart, rangeStart + bytesToRead)
        return bytesToRead
    }

    fun bytesRead(): Int {
        return pointer - offset
    }

    fun remaining(): Int {
        return size - bytesRead()
    }

    fun skip(bytesToSkip: Int = 1): Int {
        val bytesToSkipMin = min(bytesToSkip, remaining())
        pointer += bytesToSkipMin
        return bytesToSkipMin
    }

    fun bitIterator(): BitIterator {
        return ByteBitIterator(this)
    }

    fun isEmpty(): Boolean {
        return remaining() <= 0
    }

    override fun toString(): String {
        return "Size: '$size', Bytes read: '${bytesRead()}', Remaining: '${remaining()}'"
    }

    fun nextByte(): Byte {
        return next(Byte.SIZE_BYTES).toByte()
    }

    fun nextShort(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Short {
        return next(Short.SIZE_BYTES).toShort(byteOrder)
    }

    fun nextInt(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Int {
        return next(Int.SIZE_BYTES).toInt(byteOrder)
    }

    fun nextLong(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Long {
        return next(Long.SIZE_BYTES).toLong(byteOrder)
    }

    fun nextFloat(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Float {
        return next(Float.SIZE_BYTES).toFloat(byteOrder)
    }

    fun nextDouble(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Double {
        return next(Double.SIZE_BYTES).toDouble(byteOrder)
    }

    private class ByteBitIterator(private val byteIterator: ByteIterator) : BitIterator {
        private val bitIterator = ByteArrayBitIterator(byteIterator)

        override fun hasNext(): Boolean {
            return bitIterator.hasNext()
        }

        override fun hasNext(nextBits: Int): Boolean {
            return bitIterator.hasNext(nextBits)
        }

        override fun next(): Boolean {
            if (bitIterator.bitsRead() % Byte.SIZE_BITS == 0) {
                byteIterator.nextByte()
            }
            return bitIterator.next()
        }

        override fun next(bitsToRead: Int): BitRange {
            val bitsReadOfFirstByte = bitIterator.bitsRead() % Byte.SIZE_BITS
            val freeBitsOfFirstByte = (Byte.SIZE_BITS - bitsReadOfFirstByte) % Byte.SIZE_BITS
            val bitsToAdvance = bitsToRead - freeBitsOfFirstByte
            if(bitsToAdvance > 0) {
                val bytesToAdvance = BitBuilder.getNoBytesForBits(bitsToAdvance)
                byteIterator.skip(bytesToAdvance)
            }
            return bitIterator.next(bitsToRead)
        }

        override fun size(): Int {
            return bitIterator.size()
        }

        override fun byteSize(): Int {
            return bitIterator.byteSize()
        }

        override fun bitsRead(): Int {
            return bitIterator.bitsRead()
        }

        override fun remaining(): Int {
            return bitIterator.remaining()
        }

        override fun reset() {
            return bitIterator.reset()
        }

        override fun iterator(): BitIterator {
            throw IllegalStateException("A sub iterator cannot be created from a ByteIterators BitIterator as they would compete in advancing the backing ByteIterator.")
        }

        override fun toString(): String {
            return bitIterator.toString()
        }

    }
}