package bit.crunzh.utilities.encoding.specification.reflection

import bit.crunzh.utilities.encoding.EnumCache
import bit.crunzh.utilities.encoding.specification.annotation.collection.map.*
import bit.crunzh.utilities.encoding.specification.core.ClassDescriptionCache
import bit.crunzh.utilities.encoding.specification.core.TypeIndexCache
import bit.crunzh.utilities.encoding.specification.core.encoder.*
import bit.crunzh.utilities.encoding.specification.core.isMapKeyAnnotation
import bit.crunzh.utilities.encoding.specification.core.SharedValidation
import bit.crunzh.utilities.encoding.varint.VarIntSpecification
import java.nio.charset.Charset
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

object MapKeyEncoderFactory {

    fun <T, K : Any, V : Any> createKeyEncoder(property: KProperty1<T, Map<K, V>?>, classDescriptionCache: ClassDescriptionCache): ValueEncoder<K> {
        return createEncoderInternal(property, classDescriptionCache) as ValueEncoder<K>
    }

    private fun <T> createEncoderInternal(property: KProperty1<T, Any?>, classDescriptionCache: ClassDescriptionCache): ValueEncoder<*> {
        return when (val annotation = getMapKeyAnnotation(property.annotations)) {
            is BooleanMapKey -> createBooleanValueEncoder()
            is ClassMapKey -> createClassValueEncoder(classDescriptionCache, annotation)
            is EnumMapKey -> createEnumValueEncoder(annotation)
            is FloatMapKey -> createFloatValueEncoder(annotation)
            is DoubleMapKey -> createDoubleValueEncoder(annotation)
            is ByteMapKey -> createByteValueEncoder(annotation)
            is ShortMapKey -> createShortValueEncoder(annotation)
            is IntMapKey -> createIntValueEncoder(annotation)
            is LongMapKey -> createLongValueEncoder(annotation)
            is StringMapKey -> createStringValueEncoder(annotation)
            is VarShortMapKey -> createVarShortValueEncoder(annotation)
            is VarIntMapKey -> createVarIntValueEncoder(annotation)
            is VarLongMapKey -> createVarLongValueEncoder(annotation)
            is TimeEpochMsMapKey -> createTimeEpochMsValueEncoder(annotation)
            is TimeInstantMapKey -> createTimeInstantValueEncoder(annotation)
            is TimeLocalDateMapKey -> createTimeLocalDateValueEncoder(annotation)
            is TimeZonedDateMapKey -> createTimeZonedDateValueEncoder(annotation)
            is UuidMapKey -> createUuidValueEncoder()
            is BitRangeMapKey -> createBitRangeValueEncoder(annotation)
            is DurationMapKey -> createDurationValueEncoder(annotation)
            else -> throw IllegalArgumentException("Unhandled collection value annotation '${annotation::class.simpleName}'")
        }
    }

    private fun createDurationValueEncoder(annotation: DurationMapKey): ValueEncoder<*> {
        return DurationEncoder(annotation.timeResolutionMs, annotation.maxValue, annotation.canBeNegative)
    }

    private fun createBitRangeValueEncoder(annotation: BitRangeMapKey): ValueEncoder<out Any> {
        return BitRangeEncoder(VarIntSpecification(*annotation.bitRangeLengthVarInt))
    }

    private fun createTimeEpochMsValueEncoder(annotation: TimeEpochMsMapKey): ValueEncoder<out Any> {
        return TimeEpochMsEncoder(annotation.timeResolutionMs, annotation.minTime, annotation.maxTime)
    }

    private fun createUuidValueEncoder(): ValueEncoder<out Any> {
        return UuidEncoder()
    }

    private fun createTimeInstantValueEncoder(annotation: TimeInstantMapKey): ValueEncoder<out Any> {
        return TimeInstantEncoder(annotation.timeResolutionMs, annotation.minTime, annotation.maxTime)
    }

    private fun createTimeLocalDateValueEncoder(annotation: TimeLocalDateMapKey): ValueEncoder<out Any> {
        return TimeLocalDateEncoder(annotation.timeResolutionMs, annotation.minTime, annotation.maxTime)
    }

    private fun createVarLongValueEncoder(annotation: VarLongMapKey): ValueEncoder<out Any> {
        return VarLongEncoder(VarIntSpecification(*annotation.varIntSpecification, signed = annotation.signed), annotation.byteOrder.nioByteOrder)
    }

    private fun createTimeZonedDateValueEncoder(annotation: TimeZonedDateMapKey): ValueEncoder<out Any> {
        return TimeZonedDateEncoder(annotation.timeResolutionMs, annotation.minTime, annotation.maxTime)
    }

    private fun createStringValueEncoder(annotation: StringMapKey): ValueEncoder<out Any> {
        val regEx: Regex? = if (annotation.regEx.isEmpty()) {
            null
        } else {
            Regex(annotation.regEx)
        }

        return StringEncoder(Charset.forName(annotation.charset), VarIntSpecification(*annotation.stringLengthVarInt), listOf(*annotation.templateValues), annotation.maxTemplateValues, annotation.templateIgnoreCase, regEx)
    }

    private fun createIntValueEncoder(annotation: IntMapKey): ValueEncoder<out Any> {
        return IntEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
    }

    private fun createVarIntValueEncoder(annotation: VarIntMapKey): ValueEncoder<out Any> {
        return VarIntEncoder(VarIntSpecification(*annotation.varIntSpecification, signed = annotation.signed), annotation.byteOrder.nioByteOrder)
    }

    private fun createShortValueEncoder(annotation: ShortMapKey): ValueEncoder<out Any> {
        return ShortEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
    }

    private fun createByteValueEncoder(annotation: ByteMapKey): ValueEncoder<out Any> {
        return ByteEncoder(annotation.minValue, annotation.maxValue)
    }

    private fun createDoubleValueEncoder(annotation: DoubleMapKey): ValueEncoder<out Any> {
        return DoubleEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
    }

    private fun createVarShortValueEncoder(annotation: VarShortMapKey): ValueEncoder<out Any> {
        return VarShortEncoder(VarIntSpecification(*annotation.varIntSpecification, signed = annotation.signed), annotation.byteOrder.nioByteOrder)
    }

    private fun createLongValueEncoder(annotation: LongMapKey): ValueEncoder<out Any> {
        return LongEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
    }

    private fun createFloatValueEncoder(annotation: FloatMapKey): ValueEncoder<out Any> {
        return FloatEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
    }

    private fun createEnumValueEncoder(annotation: EnumMapKey): ValueEncoder<out Any> {
        val enumCache = bit.crunzh.utilities.encoding.EnumCache()
        var maxEnumValues = annotation.maxEnumValues
        val enumValues = bit.crunzh.utilities.encoding.EnumCache.getNoEnumValues(annotation.enumType)
        if (maxEnumValues == -1) {
            maxEnumValues = enumValues
        }
        if (maxEnumValues < enumValues) throw IllegalStateException("Enum ${annotation.enumType.simpleName} has more values than the specified max '${annotation.maxEnumValues}'")
        return EnumEncoder(
                maxEnumValues,
                annotation.unknownValueOrdinal,
                { ordinal, default -> enumCache.getEnumValueOrDefault(annotation.enumType, ordinal, default) },
                { enum -> enum.ordinal }
        )
    }

    private fun createClassValueEncoder(classDescriptionCache: ClassDescriptionCache, annotation: ClassMapKey): ValueEncoder<out Any> {
        SharedValidation.assertClassTypes(annotation.topType, annotation.subTypes)
        val types = ArrayList<KClass<*>>()
        if (!annotation.topType.isAbstract) {
            types.add(annotation.topType)
        }
        types.addAll(annotation.subTypes)

        val typeIndexCache = TypeIndexCache(types, annotation.numberOfTypesToSupport)
        return ClassPropertyEncoder(typeIndexCache, classDescriptionCache)
    }

    private fun createBooleanValueEncoder(): ValueEncoder<out Any> {
        return BooleanEncoder()
    }

    private fun getMapKeyAnnotation(annotations: List<Annotation>): Annotation {
        var foundAnnotation: Annotation? = null
        for (annotation in annotations) {
            if (isMapKeyAnnotation(annotation)) {
                if (foundAnnotation != null) throw IllegalArgumentException("Property has more than one map key annotations. Found both '@${foundAnnotation::class.simpleName}' and '@${annotation::class.simpleName}'.")
                foundAnnotation = annotation
            }
        }
        if (foundAnnotation == null) {
            throw IllegalStateException("Map key description not found for map property.")
        }
        return foundAnnotation
    }

}
