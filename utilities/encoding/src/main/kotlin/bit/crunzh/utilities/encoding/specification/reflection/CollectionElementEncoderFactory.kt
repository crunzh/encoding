package bit.crunzh.utilities.encoding.specification.reflection

import com.squareup.kotlinpoet.asTypeName
import bit.crunzh.utilities.encoding.EnumCache
import bit.crunzh.utilities.encoding.specification.annotation.collection.*
import bit.crunzh.utilities.encoding.specification.core.ClassDescriptionCache
import bit.crunzh.utilities.encoding.specification.core.SharedValidation
import bit.crunzh.utilities.encoding.specification.core.TypeIndexCache
import bit.crunzh.utilities.encoding.specification.core.encoder.*
import bit.crunzh.utilities.encoding.specification.core.isCollectionValueAnnotation
import bit.crunzh.utilities.encoding.varint.VarIntSpecification
import java.nio.charset.Charset
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

object CollectionElementEncoderFactory {
    fun <T: Any, V : Any> createCollectionEncoder(property: KProperty1<T, Collection<V>?>, classDescriptionCache: ClassDescriptionCache): ValueEncoder<V> {
        return createEncoderInternal(property, classDescriptionCache) as ValueEncoder<V>
    }

    fun <T: Any, K : Any, V : Any> createMapValueEncoder(property: KProperty1<T, Map<K, V>?>, classDescriptionCache: ClassDescriptionCache): ValueEncoder<V> {
        return createEncoderInternal(property, classDescriptionCache) as ValueEncoder<V>
    }

    private fun <T: Any> createEncoderInternal(property: KProperty1<T, Any?>, classDescriptionCache: ClassDescriptionCache): ValueEncoder<*> {
        return when (val annotation = getCollectionValueAnnotation(property)) {
            is BooleanCollectionValue -> createBooleanValueEncoder()
            is ClassCollectionValue -> createClassValueEncoder(classDescriptionCache, annotation)
            is EnumCollectionValue -> createEnumValueEncoder(annotation)
            is FloatCollectionValue -> createFloatValueEncoder(annotation)
            is DoubleCollectionValue -> createDoubleValueEncoder(annotation)
            is ByteCollectionValue -> createByteValueEncoder(annotation)
            is ShortCollectionValue -> createShortValueEncoder(annotation)
            is IntCollectionValue -> createIntValueEncoder(annotation)
            is LongCollectionValue -> createLongValueEncoder(annotation)
            is StringCollectionValue -> createStringValueEncoder(annotation)
            is VarShortCollectionValue -> createVarShortValueEncoder(annotation)
            is VarIntCollectionValue -> createVarIntValueEncoder(annotation)
            is VarLongCollectionValue -> createVarLongValueEncoder(annotation)
            is ByteArrayCollectionValue -> createByteArrayValueEncoder(annotation)
            is TimeEpochMsCollectionValue -> createTimeEpochMsValueEncoder(annotation)
            is TimeInstantCollectionValue -> createTimeInstantValueEncoder(annotation)
            is TimeLocalDateCollectionValue -> createTimeLocalDateValueEncoder(annotation)
            is TimeZonedDateCollectionValue -> createTimeZonedDateValueEncoder(annotation)
            is UuidCollectionValue -> createUuidValueEncoder()
            is BitRangeCollectionValue -> createBitRangeValueEncoder(annotation)
            is DurationCollectionValue -> createDurationValueEncoder(annotation)
            else -> throw IllegalArgumentException("Unhandled collection value annotation '${annotation::class.simpleName}'")
        }
    }

    private fun createDurationValueEncoder(annotation: DurationCollectionValue): ValueEncoder<*> {
        return DurationEncoder(annotation.timeResolutionMs, annotation.maxValue, annotation.canBeNegative)
    }

    private fun createUuidValueEncoder(): ValueEncoder<UUID> {
        return UuidEncoder()
    }

    private fun createVarLongValueEncoder(annotation: VarLongCollectionValue): ValueEncoder<Long> {
        return VarLongEncoder(VarIntSpecification(*annotation.varIntSpecification), annotation.byteOrder.nioByteOrder)
    }

    private fun createBitRangeValueEncoder(annotation: BitRangeCollectionValue): ValueEncoder<out Any> {
        return BitRangeEncoder(VarIntSpecification(*annotation.bitRangeLengthVarInt))
    }

    private fun createVarIntValueEncoder(annotation: VarIntCollectionValue): ValueEncoder<Int> {
        return VarIntEncoder(VarIntSpecification(*annotation.varIntSpecification), annotation.byteOrder.nioByteOrder)
    }

    private fun createTimeEpochMsValueEncoder(annotation: TimeEpochMsCollectionValue): ValueEncoder<out Any> {
        return TimeEpochMsEncoder(annotation.timeResolutionMs, annotation.minTime, annotation.maxTime)
    }

    private fun createTimeZonedDateValueEncoder(annotation: TimeZonedDateCollectionValue): ValueEncoder<ZonedDateTime> {
        return TimeZonedDateEncoder(annotation.timeResolutionMs, annotation.minTime, annotation.maxTime)
    }

    private fun createTimeLocalDateValueEncoder(annotation: TimeLocalDateCollectionValue): ValueEncoder<LocalDateTime> {
        return TimeLocalDateEncoder(annotation.timeResolutionMs, annotation.minTime, annotation.maxTime)
    }

    private fun createTimeInstantValueEncoder(annotation: TimeInstantCollectionValue): ValueEncoder<Instant> {
        return TimeInstantEncoder(annotation.timeResolutionMs, annotation.minTime, annotation.maxTime)
    }

    private fun createVarShortValueEncoder(annotation: VarShortCollectionValue): ValueEncoder<Short> {
        return VarShortEncoder(VarIntSpecification(*annotation.varIntSpecification), annotation.byteOrder.nioByteOrder)
    }

    private fun createByteArrayValueEncoder(annotation: ByteArrayCollectionValue): ValueEncoder<ByteArray> {
        return ByteArrayEncoder(VarIntSpecification(*annotation.byteArrayLengthVarInt))
    }

    private fun createDoubleValueEncoder(annotation: DoubleCollectionValue): ValueEncoder<Double> {
        return DoubleEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
    }

    private fun createFloatValueEncoder(annotation: FloatCollectionValue): ValueEncoder<Float> {
        return FloatEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
    }

    private fun createIntValueEncoder(annotation: IntCollectionValue): ValueEncoder<Int> {
        return IntEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
    }

    private fun createEnumValueEncoder(annotation: EnumCollectionValue): ValueEncoder<out Any> {
        val enumCache = bit.crunzh.utilities.encoding.EnumCache()
        var maxEnumValues = annotation.maxEnumValues
        val enumValues = bit.crunzh.utilities.encoding.EnumCache.getNoEnumValues(annotation.enumType)
        if (maxEnumValues == -1) {
            maxEnumValues = enumValues
        }
        if (maxEnumValues < enumValues) throw IllegalStateException("Enum ${annotation.enumType.simpleName} has more values than the specified max '${annotation.maxEnumValues}'")
        return EnumEncoder(
                maxEnumValues,
                annotation.unknownValueOrdinal,
                { ordinal, default -> enumCache.getEnumValueOrDefault(annotation.enumType, ordinal, default) },
                { enum -> enum.ordinal }
        )
    }

    private fun createLongValueEncoder(annotation: LongCollectionValue): ValueEncoder<Long> {
        return LongEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
    }

    private fun createClassValueEncoder(classDescriptionCache: ClassDescriptionCache, annotation: ClassCollectionValue): ValueEncoder<out Any> {
        SharedValidation.assertClassTypes(annotation.topType, annotation.subTypes)
        val types = ArrayList<KClass<*>>()
        if (!annotation.topType.isAbstract) {
            types.add(annotation.topType)
        }
        types.addAll(annotation.subTypes)

        val typeIndexCache = TypeIndexCache(types, annotation.numberOfTypesToSupport)
        return ClassPropertyEncoder(typeIndexCache, classDescriptionCache)
    }

    private fun createShortValueEncoder(annotation: ShortCollectionValue): ValueEncoder<Short> {
        return ShortEncoder(annotation.minValue, annotation.maxValue, annotation.byteOrder.nioByteOrder)
    }

    private fun createStringValueEncoder(annotation: StringCollectionValue): ValueEncoder<String> {
        val regEx: Regex? = if (annotation.regEx.isEmpty()) {
            null
        } else {
            Regex(annotation.regEx)
        }

        return StringEncoder(
                Charset.forName(annotation.charset),
                VarIntSpecification(*annotation.stringLengthVarInt),
                listOf(*annotation.templateValues),
                annotation.maxTemplateValues,
                annotation.templateIgnoreCase,
                regEx
        )
    }

    private fun createByteValueEncoder(annotation: ByteCollectionValue): ValueEncoder<Byte> {
        return ByteEncoder(annotation.minValue, annotation.maxValue)
    }

    private fun createBooleanValueEncoder(): ValueEncoder<Boolean> {
        return BooleanEncoder()
    }

    private fun <T: Any> getCollectionValueAnnotation(property: KProperty1<T, Any?>): Annotation {
        var foundAnnotation: Annotation? = null
        for (annotation in property.annotations) {
            if (isCollectionValueAnnotation(annotation)) {
                if (foundAnnotation != null) throw IllegalArgumentException("Property has more than one collection value annotations. Found both '@${foundAnnotation::class.simpleName}' and '@${annotation::class.simpleName}'.")
                foundAnnotation = annotation
            }
        }
        if (foundAnnotation == null) {
            throw IllegalStateException("Collection value description not found for collection property '${property.name}::${property.returnType.asTypeName()}'.")
        }
        return foundAnnotation
    }
}
