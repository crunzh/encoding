package bit.crunzh.utilities.encoding.tlv

interface TlvTuple<T> {
    val type: T
    val length: Int
    val value: ByteArray
}