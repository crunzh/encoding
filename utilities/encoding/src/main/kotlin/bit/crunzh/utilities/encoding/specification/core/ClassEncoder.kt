package bit.crunzh.utilities.encoding.specification.core

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import kotlin.reflect.KClass

object ClassEncoder {
    private const val payloadLengthDescription = "Class property payload length"
    private const val classPropertyTypeDescription = "Class property type index"
    private val payloadLengthEncoder = VarIntEncoder(7, 11, 15, 32)

    fun <T : Any> encodeMultiTypeHeader(type: KClass<T>, typeIndexCache: TypeIndexCache): BitBuilder {
        val builder = BitBuilder()
        if (!typeIndexCache.isMultiType) {
            return builder
        }
        builder.append(typeIndexCache.getIdx(type), typeIndexCache.bitsForTypes, description = classPropertyTypeDescription)
        return builder
    }

    fun decodeMultiTypeHeader(bits: BitIterator, typeIndexCache: TypeIndexCache): KClass<*> {
        if (!typeIndexCache.isMultiType) {
            return typeIndexCache.getSingleton()
        }
        val typeIdx = bits.next(typeIndexCache.bitsForTypes).toInt()
        return typeIndexCache.getType(typeIdx)
    }

    fun <T : Any> encode(obj: T, classDescription: ClassDescription<T>, encoderContext: EncoderContext): BitBuilder {
        val propertyBuilder = BitBuilder()
        var prevDescriptor: PropertyDescription<*, *>? = null
        classDescription.propertyDescriptors.forEach { propertyDescriptor ->
            if ((prevDescriptor == null && propertyDescriptor.seqNo != 1) || (prevDescriptor != null && propertyDescriptor.seqNo != prevDescriptor!!.seqNo + 1)) throw IllegalArgumentException("Property sequence numbers must be sequential in order, starting from 1. Problem with ${obj::class.simpleName}.${propertyDescriptor.name}. Previous sequence number was ${prevDescriptor?.seqNo}, this sequence number was ${propertyDescriptor.seqNo}.")

            propertyBuilder.append(encodeProperty(propertyDescriptor, obj, encoderContext))
            prevDescriptor = propertyDescriptor
        }

        val classBuilder = BitBuilder()
        if (classDescription.isExtendable) {
            classBuilder.append(payloadLengthEncoder.encode(propertyBuilder.size(), payloadLengthDescription))
        }
        classBuilder.append(propertyBuilder)
        return classBuilder
    }

    private fun <ParentType : Any, PropertyType : Any> encodeProperty(propertyDescriptor: PropertyDescription<PropertyType, ParentType>, obj: ParentType, encoderContext: EncoderContext): BitBuilder {
        val builder = BitBuilder()
        val propertyValue: PropertyType? = propertyDescriptor.getPropertyValue(parentObj = obj)

        if (!propertyDescriptor.isNullable && propertyValue == null) throw IllegalArgumentException("Property ${propertyDescriptor.name} is null, but configured as not nullable.")
        if (propertyDescriptor.isNullable) {
            builder.append(propertyValue != null)
        }

        if (propertyValue == null) {
            return builder
        }
        propertyDescriptor.assertValid(propertyValue)
        builder.append(propertyDescriptor.encode(propertyValue, encoderContext))
        return builder
    }

    fun <T : Any> decode(bitIterator: BitIterator, classDescription: ClassDescription<T>, encoderContext: EncoderContext): T {
        val propertyBitIterator: BitIterator
        if (classDescription.isExtendable) {
            val propertiesSize = payloadLengthEncoder.decode(bitIterator)
            propertyBitIterator = bitIterator.next(propertiesSize.toInt()).iterator()
        } else {
            propertyBitIterator = bitIterator
        }
        val propertyValues = ArrayList<PropertyValue<*>>()
        classDescription.propertyDescriptors.forEach { propertyDescriptor ->
            val propertyValue = decodeProperty(propertyDescriptor, propertyBitIterator, encoderContext)
            propertyValues.add(propertyValue)
        }

        return classDescription.create(PropertyValues(propertyValues))
    }

    private fun <ParentType : Any, PropertyType : Any> decodeProperty(propertyDescriptor: PropertyDescription<PropertyType, ParentType>, propertyBitIterator: BitIterator, encoderContext: EncoderContext): PropertyValue<PropertyType> {
        if (propertyDescriptor.isNullable && !propertyBitIterator.next()) { //is null
            return PropertyValue(propertyDescriptor.seqNo, propertyDescriptor.name, null)
        }
        val propertyValue = propertyDescriptor.decode(propertyBitIterator, encoderContext)
        propertyDescriptor.assertValid(propertyValue)
        return PropertyValue(propertyDescriptor.seqNo, propertyDescriptor.name, propertyValue)
    }
}