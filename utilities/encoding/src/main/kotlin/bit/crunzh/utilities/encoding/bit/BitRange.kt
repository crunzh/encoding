package bit.crunzh.utilities.encoding.bit

import bit.crunzh.utilities.encoding.EnumCache
import bit.crunzh.utilities.encoding.bit.BitBuilder.Factory.calculateSignBitIdx
import bit.crunzh.utilities.encoding.bit.BitBuilder.Factory.doubleSizeBits
import bit.crunzh.utilities.encoding.bit.BitBuilder.Factory.doubleSizeBytes
import bit.crunzh.utilities.encoding.bit.BitBuilder.Factory.fitBytes
import bit.crunzh.utilities.encoding.bit.BitBuilder.Factory.floatSizeBits
import bit.crunzh.utilities.encoding.bit.BitBuilder.Factory.floatSizeBytes
import bit.crunzh.utilities.encoding.byte.ByteBuilder
import bit.crunzh.utilities.encoding.byte.ByteRange
import bit.crunzh.utilities.encoding.varint.VarIntEncoder
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.charset.Charset
import kotlin.reflect.KClass

data class BitRange(internal val bytes: ByteArray, internal val bitOffset: Int = 0, internal val bitLength: Int = (bytes.size * Byte.SIZE_BITS) - bitOffset, var description: String? = null) : Iterable<Boolean> {
    private var toByteArrayValueCache: ByteArray? = null

    init {
        val bytesSizeBits = (bytes.size * Byte.SIZE_BITS)
        if (bitLength < 0 || bitLength > (bytes.size * Byte.SIZE_BITS)) {
            throw IllegalArgumentException("bitLength must be larger than 0 but smaller than bytes.size * Byte.BIT_SIZE. It was '$bitLength' but should be [0-$bytesSizeBits]")
        }
        if (bitOffset < 0 || bitOffset + bitLength > (bytes.size * Byte.SIZE_BITS)) {
            throw IllegalArgumentException("bitOffset must be 0 or larger, but smaller than bytes - bitLength. It was '$bitOffset' but should be [0-${bytesSizeBits - bitLength}[")
        }
    }

    /**
     * Read BitRange as a byte
     * @throws IllegalStateException if not allowRemainder, and more than 8 bits in BitRange.
     */
    fun toByte(allowRemainder: Boolean = false, signed: Boolean = false): Byte {
        if (size() == Byte.SIZE_BITS) {
            if (!signed) {
                throw IllegalArgumentException("It is not possible to read 8 bits unsigned value to a Byte.")
            }
            return BitUtil.getByte(bytes, bitOffset)
        }
        val bitsAsBytes = expandToIntegerBytes(signed, allowRemainder, Byte.SIZE_BYTES, ByteOrder.BIG_ENDIAN)
        return bitsAsBytes[0]
    }

    fun toShort(allowRemainder: Boolean = false, signed: Boolean = false, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Short {
        val bitsAsBytes = expandToIntegerBytes(signed, allowRemainder, Short.SIZE_BYTES, byteOrder)
        val bb = ByteBuffer.allocate(Short.SIZE_BYTES).put(bitsAsBytes).order(byteOrder)
        bb.rewind()
        return bb.short
    }

    fun toInt(allowRemainder: Boolean = false, signed: Boolean = false, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Int {
        val bitsAsBytes = expandToIntegerBytes(signed, allowRemainder, Int.SIZE_BYTES, byteOrder)
        val bb = ByteBuffer.allocate(Int.SIZE_BYTES).put(bitsAsBytes).order(byteOrder)
        bb.rewind()
        return bb.int
    }

    fun toLong(allowRemainder: Boolean = false, signed: Boolean = false, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Long {
        val bitsAsBytes = expandToIntegerBytes(signed, allowRemainder, Long.SIZE_BYTES, byteOrder)
        val bb = ByteBuffer.allocate(Long.SIZE_BYTES).put(bitsAsBytes).order(byteOrder)
        bb.rewind()
        return bb.long
    }

    fun toFloat(allowRemainder: Boolean = false, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Float {
        if (size() < floatSizeBits) {
            throw IllegalStateException("toFloat requires a BitRange of at least '$floatSizeBits' bits, it was only ${size()}")
        }
        assertRemainder(allowRemainder, floatSizeBits)
        val floatBytes = fitBytes(toByteArray(), noBytes = floatSizeBytes)
        return ByteBuffer.wrap(floatBytes).order(byteOrder).float
    }

    fun toDouble(allowRemainder: Boolean = false, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Double {
        if (size() < doubleSizeBits) {
            throw IllegalStateException("toFloat requires a BitRange of at least '$floatSizeBits' bits, it was only ${size()}")
        }
        assertRemainder(allowRemainder, doubleSizeBits)
        val doubleBytes = fitBytes(toByteArray(), noBytes = doubleSizeBytes)
        return ByteBuffer.wrap(doubleBytes).order(byteOrder).double
    }

    fun toString(allowRemainder: Boolean = false, charsetToUseForDecode: Charset = Charsets.UTF_8): String {
        if (allowRemainder && size() % 8 != 0) {
            throw java.lang.IllegalStateException("BitRange bits is not a whole number of bytes.")
        }
        return String(toByteArray(), charsetToUseForDecode)
    }

    fun toByteArray(allowRemainder: Boolean = false): ByteArray {
        if (allowRemainder && size() % Byte.SIZE_BITS != 0) {
            throw IllegalStateException("Number of bits in range does not fit a whole number of bytes.")
        }
        val tempValue = toByteArrayValueCache
        return if (tempValue == null) {
            val newValue = BitUtil.toByteArray(bytes, bitOffset, bitLength)
            toByteArrayValueCache = newValue
            newValue
        } else tempValue
    }

    fun toByteRange(description: String? = null): ByteRange {
        val bytes = toByteArray()
        return ByteRange(bytes, description = description)
    }

    fun toByteBuilder(): ByteBuilder {
        return ByteBuilder().append(this)
    }

    fun toByteBuffer(allowRemainder: Boolean = false): ByteBuffer {
        return ByteBuffer.wrap(toByteArray(allowRemainder))
    }

    fun toBoolean(allowRemainder: Boolean = false): Boolean {
        assertRemainder(allowRemainder, 1)
        return BitUtil.getBit(array = bytes, bitOffset = bitOffset, bitIndex = 0)
    }

    fun toMutableBitRange(): MutableBitRange {
        val byteStartIdx = bitOffset / Byte.SIZE_BITS
        val bitEndIdx = bitOffset + bitLength
        var byteEndIdx = bitEndIdx / Byte.SIZE_BITS
        if (bitEndIdx % Byte.SIZE_BITS > 0) {
            byteEndIdx++
        }
        val arrayCopy = bytes.copyOfRange(byteStartIdx, byteEndIdx)
        return MutableBitRange(arrayCopy, bitOffset % Byte.SIZE_BITS, bitLength)
    }

    fun subSet(subsetBitOffset: Int = 0, subsetBitLength: Int): BitRange {
        if (subsetBitLength < 1 || subsetBitLength > size()) {
            throw IllegalArgumentException("bitLength must be in the range [1-${size()}]")
        }
        if (subsetBitOffset > size() || subsetBitOffset + subsetBitLength > size()) {
            throw IllegalArgumentException("bitOffset must be in the range [0-${size()}[")
        }
        return BitRange(bytes, this.bitOffset + subsetBitOffset, subsetBitLength)
    }

    fun get(index: Int = 0): Boolean {
        return BitUtil.getBit(bytes, bitOffset, index)
    }

    fun size(): Int {
        return bitLength
    }

    fun sizeBytes(): Int {
        var bytes = size() / Byte.SIZE_BITS
        if (size() % Byte.SIZE_BITS != 0) {
            bytes++
        }
        return bytes
    }

    override fun iterator(): BitIterator {
        return ByteArrayBitIterator(bytes, bitOffset, bitLength)
    }

    fun printBinaryString(maxSize: Int = Int.MAX_VALUE): String {
        var binString = ""
        val iterator = iterator()
        while (iterator.hasNext()) {
            val bit = iterator.next()
            binString += if (bit) {
                "1"
            } else {
                "0"
            }
            if (iterator.bitsRead() % Byte.SIZE_BITS == 0) {
                binString += " "
            }
            if (binString.length > maxSize) {
                break
            }
        }
        return binString.trimEnd()
    }

    fun printHexString(maxSize: Int = Int.MAX_VALUE): String {
        var hexString = ""
        for (byte in toByteArray()) {
            hexString += String.format("%02X", byte)
            hexString += "-"
            if (hexString.length > maxSize) {
                break
            }
        }
        return hexString
    }

    override fun toString(): String {
        return "$description, ${size()} bits, '${printBinaryString(200)}'"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BitRange

        val localByteArrayValue = toByteArray()
        val remoteByteArrayValue = other.toByteArray()
        if (!localByteArrayValue.contentEquals(remoteByteArrayValue)) return false
        return true
    }

    override fun hashCode(): Int {
        return toByteArray().contentHashCode()
    }

    internal fun expandToIntegerBytes(readAsSigned: Boolean, allowRemainder: Boolean, noOutputBytes: Int, byteOrderToRead: ByteOrder): ByteArray {
        val outputBits = noOutputBytes * Byte.SIZE_BITS
        assertRemainder(allowRemainder, outputBits)
        if (size() == outputBits) {
            return toByteArray()
        }

        val outputByteArray = ByteArray(noOutputBytes)
        if (readAsSigned) {
            val compactSignIndex = calculateSignBitIdx(byteOrderToRead, size())
            val signValue = get(compactSignIndex) //use BitUtil.getBit
            if (signValue) { //negative number
                setAllBits(outputByteArray)
                //Sign bit is implicitly set.
            }
        }

        if (byteOrderToRead == ByteOrder.BIG_ENDIAN) {
            //When big endian, the least significant bytes is written last for instance an 18 bit integer would look like this when bits and bytes are written left to right.
            //[0000 000(0), X(X)00 0000, XXXX XXXX, XXXX XXXX] - the sign bit in parentheses
            //Hence we must copy the last full bytes, and the remainder from the beginning of the byte before the last full bytes.
            val noSrcFullBytes = size() / Byte.SIZE_BITS
            val noSrcFullBytesInBits = noSrcFullBytes * Byte.SIZE_BITS
            val fullBytesSrcOffsetBits = size() - noSrcFullBytesInBits
            val dstFullBytesOffset = outputBits - noSrcFullBytesInBits
            val srcRemainderBits = size() % Byte.SIZE_BITS
            val dstRemainderOffsetBits = (noOutputBytes - (noSrcFullBytes + 1)) * Byte.SIZE_BITS

            val srcRemainderBitsWithoutSign = if (readAsSigned) {
                srcRemainderBits - 1
            } else {
                srcRemainderBits
            }
            if (noSrcFullBytes > 0) {
                BitUtil.setBitRange(bytes, bitOffset + fullBytesSrcOffsetBits, noSrcFullBytesInBits, outputByteArray, dstFullBytesOffset, !readAsSigned)
            }
            BitUtil.setBitRange(bytes, bitOffset, srcRemainderBitsWithoutSign, outputByteArray, dstRemainderOffsetBits, !readAsSigned)
        } else {
            val noSrcValueBits = if (readAsSigned) {
                size() - 1
            } else {
                size()
            }
            BitUtil.setBitRange(bytes, bitOffset, noSrcValueBits, outputByteArray, 0, !readAsSigned)
        }


        return outputByteArray
    }

    private fun setAllBits(outputByteArray: ByteArray) {
        val setByte = 0xff.toByte()
        for (byteIdx in outputByteArray.indices) {
            outputByteArray[byteIdx] = setByte
        }
    }

    private fun assertRemainder(allowRemainder: Boolean, outputBits: Int) {
        if (!allowRemainder && size() > outputBits) {
            throw IllegalStateException("BitRange was larger than output size '$outputBits', and the call did not allow remaining bits in the range.")
        }
    }

    companion object {
        val enumCache = bit.crunzh.utilities.encoding.EnumCache()

        fun readString(bitIterator: BitIterator, charsetToUseForDecode: Charset = Charsets.UTF_8, varIntEncoder: VarIntEncoder = BitBuilder.defaultStringLengthEncoder): String {
            val strLength = varIntEncoder.decode(bitIterator).toInt()
            if (strLength == 0) {
                return ""
            }
            val strBytes = bitIterator.next(strLength * Byte.SIZE_BITS).toByteArray()
            return String(strBytes, charsetToUseForDecode)
        }

        inline fun <reified T : Enum<*>> readEnum(bitIterator: BitIterator, enumClass: KClass<T>, bitsToRepresentEnumOrdinal: Int = BitBuilder.getBitsRequired(enumCache.getNoEnumValues(enumClass))): T {
            val ordinal = bitIterator.next(bitsToRepresentEnumOrdinal).toInt()
            return enumCache.getEnumValue<T>(enumClass, ordinal)
        }

    }
}
