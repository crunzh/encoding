package bit.crunzh.utilities.encoding.specification.core

import bit.crunzh.utilities.encoding.bit.BitBuilder
import bit.crunzh.utilities.encoding.bit.BitIterator
import bit.crunzh.utilities.encoding.specification.core.encoder.ValueEncoder
import bit.crunzh.utilities.encoding.specification.core.offset.OffsetValueBuilder
import kotlin.reflect.KClass

class PropertyDescription<PropertyType : Any, ParentType : Any>(
        val name: String,
        val seqNo: Int,
        val isNullable: Boolean,
        val type: KClass<in PropertyType>,
        private val propertyEncoder: ValueEncoder<PropertyType>,
        private val propertyGetFunction: (ParentType) -> PropertyType?
) {

    fun getPropertyValue(parentObj: ParentType): PropertyType? {
        return propertyGetFunction(parentObj)
    }

    fun collectOffsetValues(property: PropertyType, offsetValueBuilder: OffsetValueBuilder) {
        propertyEncoder.collectOffsetValues(property, offsetValueBuilder)
    }

    fun encode(property: PropertyType, encoderContext: EncoderContext): BitBuilder {
        return propertyEncoder.encode(property, encoderContext)
    }

    fun decode(bits: BitIterator, encoderContext: EncoderContext): PropertyType {
        return propertyEncoder.decode(bits, encoderContext)
    }

    fun assertValid(property: PropertyType) {
        propertyEncoder.assertValid(property)
    }
}