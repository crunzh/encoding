package bit.crunzh.utilities.encoding.specification.annotation.property

@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class ByteStoreProperty(
        val seqNo: Int,
        val byteLengthLengthVarInt: IntArray = [4, 6, 10, 16]
)