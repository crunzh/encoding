import org.gradle.jvm.tasks.Jar

plugins {
    id("org.jetbrains.dokka")
    //id("org.jetbrains.kotlin.android") version "1.3.72"
    // Apply the java-library plugin for API and implementation separation.
    `maven-publish`
    //id("org.jetbrains.kotlin.jvm")
    `java-library`
    kotlin("jvm")
    //id("bit.crunzh.class-desc-gen")
    id("jacoco")
}

tasks.dokka {
    outputFormat = "html"
    outputDirectory = "$buildDir/javadoc"
}

val dokkaJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Assembles Kotlin docs with Dokka"
    classifier = "javadoc"
    from(tasks.dokka)
}

publishing {
    publications {
        create<MavenPublication>("default") {
            from(components["java"])
            artifact(dokkaJar)
        }
    }
    repositories {
        maven {
            url = uri("$buildDir/repository")
        }
    }
}

dependencies {
    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation(kotlin("stdlib"))
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.6.0")
    implementation("com.squareup:kotlinpoet:1.10.2")

    // Test
    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("com.google.code.gson:gson:2.9.0")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}
